<?php
include_once('miAutoload.php');

$user_id = base64_decode($_REQUEST['id']);

$user = new user();
$user_data = $user->select($user_id);

?>
<!-- Inliner Build Version 4380b7741bb759d6cb997545f3add21ad48f010b -->
<html>
<head></head>
<body class="body_main"
      style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.42857143; color: #333;">
<div class="body_main_div" style="width: 600px; padding-bottom: 45px; margin: 20px auto auto;">
    <div class="body_main_header"
         style="padding-bottom: 20px; top: 0; border-radius: 0; right: 395px; left: 34px; z-index: 1030; position: relative; min-height: 50px; margin-bottom: 20px; display: block; background: #291429; border: 1px solid transparent;">
        <div class="header_div"
             style="padding-right: 15px; padding-left: 15px; margin-right: -15px; margin-left: -15px; float: left;">
            <a class="header_a"
               style="margin-top: -7px; float: left; height: 50px; font-size: 18px; line-height: 20px; color: #337ab7; text-decoration: none; outline: none !important; background: transparent; padding: 15px;">
                <img src="<?php echo $upload_image_url . 'logo.png' ?>" width="100" height="50" alt=""></a>
        </div>
    </div>
    <?php
    if (count($user_data) > 0) {

        ?>
        <div class="email_body_main_title"
             style="padding-bottom: 20px; right: 395px; left: 34px; z-index: 1030; position: relative; min-height: 0px; margin-bottom: 0px; display: block; border: 1px solid transparent;">
            Hello <?php echo $user_data[0]['name']; ?>,
        </div>
        <div class="email_body_main_content"
             style="padding-bottom: 20px; right: 395px; left: 34px; z-index: 1030; position: relative; min-height: 0px; margin-bottom: 0px; display: block; border: 1px solid transparent;">
            <?php
            if ($user_data[0]['status'] == '4') {
//                $user->setstatus('1');
                $user->update_status($user_id);
                echo 'Your Account Verified Successfully.';
            } else {
                echo 'Sorry, Link is Expired';
            } ?>
        </div>
        <?
    } else {
        ?>
        <div class="email_body_main_title"
             style="padding-bottom: 20px; right: 395px; left: 34px; z-index: 1030; position: relative; min-height: 0px; margin-bottom: 0px; display: block; border: 1px solid transparent;">
            Sorry, Link is incorrect;
        </div>
        <?php
    }
    ?>
    <div class="email_body_footer"
         style="padding-bottom: 20px; right: 395px; left: 34px; z-index: 1030; position: relative; min-height: 0px; margin-bottom: 20px; display: block; border: 1px solid transparent;">
    </div>
</div>
</body>
</html>

