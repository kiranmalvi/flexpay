<!-- Google Map API -->
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>

<!-- jQuery -->
<script type="text/javascript" src="<?php echo $admin_url ?>vendor/jquery/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="<?php echo $admin_url ?>vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<script src="assets/animation/animsition.min.js"></script>

<!-- Bootstrap -->
<script type="text/javascript" src="<?php echo $members_assets_url ?>js/bootstrap/bootstrap.min.js"></script>

<!-- Datatables -->
<script type="text/javascript"
        src="<?php echo $admin_url ?>vendor/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript"
        src="<?php echo $admin_url ?>vendor/plugins/datatables/media/js/dataTables.bootstrap.js"></script>
<script type="text/javascript"
        src="<?php echo $admin_url ?>vendor/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>

<!-- Page Plugins -->
<script type="text/javascript" src="<?php echo $admin_url ?>vendor/editors/xeditable/js/bootstrap-editable.js"></script>

<!-- Theme Javascript -->
<script type="text/javascript" src="<?php echo $members_assets_url ?>js/utility/utility.js"></script>
<script type="text/javascript" src="<?php echo $members_assets_url ?>js/main.js"></script>
<script type="text/javascript" src="<?php echo $members_assets_url ?>js/demo.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {

        "use strict";

        //test.init('in','data');
        // Init Theme Core
        Core.init();

        // Init Demo JS
        Demo.init();

        // Init Highlight.js Plugin
        $('pre code').each(function (i, block) {
            hljs.highlightBlock(block);
        });

        // Select all text in CSS Generate Modal
        $('#modal-close').click(function (e) {
            e.preventDefault();
            $('.datatables-demo-modal').modal('toggle');
        });

        $('.datatables-demo-code').on('click', function () {
            var modalContent = $(this).prev();
            var modalContainer = $('.datatables-demo-modal').find('.modal-body')

            // Empty Modal of Existing Content
            modalContainer.empty();

            // Clone Content and Place in Modal
            modalContent.clone(modalContent).appendTo(modalContainer);

            // Toggle Modal
            $('.datatables-demo-modal').modal({
                backdrop: 'static'
            })
        });


        $('#datatable3').dataTable({
            "aoColumnDefs": [{
                'bSortable': false,
                'aTargets': [0, -1, 1]
            }],
            "oLanguage": {
                "oPaginate": {
                    "sPrevious": "",
                    "sNext": ""
                }
            },
            "iDisplayLength": 10,
            "aLengthMenu": [
                [5, 10, 25, 50, -1],
                [5, 10, 25, 50, "All"]
            ],
            "sDom": '<"dt-panelmenu clearfix"lfr>t<"dt-panelfooter clearfix"ip>',
            "oTableTools": {
                "sSwfPath": "vendor/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
                "aButtons": [
                    {
                        "sExtends": "pdf",
                        "sButtonText": "<span style='color: dimgray;'>PDF</span>",
                        "mColumns": [2, 3, 4, 5, 6]
                    },
                ]
            }
        }).find('thead tr').find("th:first").removeClass('sorting_asc');


        // MISC DATATABLE HELPER FUNCTIONS

        // Add Placeholder text to datatables filter bar
        $('.dataTables_filter input').attr("placeholder", "Enter Filter Terms Here....");

        $('#datatable4').dataTable({
            "aoColumnDefs": [{
                'bSortable': false
            }],
            "oLanguage": {
                "oPaginate": {
                    "sPrevious": "",
                    "sNext": ""
                }
            },
            "iDisplayLength": 10,
            "aLengthMenu": [
                [5, 10, 25, 50, -1],
                [5, 10, 25, 50, "All"]
            ],
            "sDom": '<"dt-panelmenu clearfix"lfr>t<"dt-panelfooter clearfix"ip>',
            "oTableTools": {
                "sSwfPath": "vendor/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
                "aButtons": [
                    {
                        "sExtends": "pdf",
                        "sButtonText": "<span style='color: dimgray;'>PDF</span>",
                        "mColumns": [2, 3, 4, 5, 6]
                    },
                ]
            }
        }).find('thead tr').find("th:first").removeClass('sorting_asc');


        // MISC DATATABLE HELPER FUNCTIONS

        // Add Placeholder text to datatables filter bar
        $('.dataTables_filter input').attr("placeholder", "Enter Filter Terms Here....");
        // Manually Init Chosen on Datatables Filters
        // $("select[name='datatable2_length']").chosen();
        // $("select[name='datatable3_length']").chosen();
        // $("select[name='datatable4_length']").chosen();

        // Init Xeditable Plugin
        $.fn.editable.defaults.mode = 'popup';
        $('.xedit').editable();

    });
</script>