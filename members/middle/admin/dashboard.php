<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 5/10/15
 * Time: 3:16 PM
 */

$wallet = new wallet();
$UserId = $_SESSION[$Project_Name_Members]['MEMBERS']['id'];
$walletlist = $wallet->members_wallet($UserId);
?>

    <!-- Start: Topbar -->
    <header id="topbar" class="affix">
        <div class="topbar-left">
            <ol class="breadcrumb">
                <li class="crumb-link crumb-active ">
                    <a >My Balance</a>
                </li>
            </ol>
        </div>
    </header>
    <!-- End: Topbar -->

    <section id="content" class="pn">
        <!-- <div class="p40 bg-background bg-topbar bg-psuedo-tp"> -->
		<div id="msg" class="notification" data-note-stack="stack_bar_top"
			 data-note-style="<?php echo $_SESSION[$session_prefix . 'admin_var_msg_class']; ?>"></div>
        <div class="pv30 ph40 bg-light dark br-b br-grey posr">
            <div class="row mb10">
                <a href="javascript:void(0)" title="Total Amount">
                    <div  class="col-md-4 col-md-offset-4">
                        <div class="panel bg-info light of-h mb10">
                            <div class="pn pl20 p5">
                                <div class="icon-bg"> <i class="fa fa-dollar"></i> </div>
                                <h2 class="mt15 lh15"> <b><?php if (@$walletlist[0]['amount'] == '') {
                                            $amount = '0.0';
                                        } else {
                                            $amount = round($walletlist[0]['amount'], 2);
                                        }

                                        echo $amount; ?></b> </h2>
                                <h5 class="text-muted" style="color: #FFF;">Amount</h5>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </section>
<?php include_once($admin_path . 'js_table.php'); ?>
<?php include_once($admin_path . 'modalpopup.php'); ?>