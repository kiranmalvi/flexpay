<?php
/**
 * Created by PhpStorm.
 * User: mind
 * Date: 9/4/15
 * Time: 6:26 PM
 */

$mode = "changespassword";
$iId = $_SESSION[$Project_Name_Members]['MEMBERS']['id'];
$account_setting = new account_setting();
$db_res = $account_setting->select($iId);

$MODE_TYPE = ucfirst($mode);
$MODULE = 'Change Password';
$ADD_LINK = 'index.php?file=a-dashboard';
$ACT_LINK = 'index.php?file=a-admin_a';
$PRE_LINK = 'index.php?file=a-adminlist';

?>

<!-- Start: Topbar -->
<header id="topbar" class="affix">
    <div class="topbar-left">
        <ol class="breadcrumb">
            <li class="crumb-active">
                <a href="index.php">My Balance</a>
            </li>

            <li class="crumb-trail"><?php echo $MODULE; ?> Details</li>
        </ol>
    </div>
</header>
<!-- End: Topbar -->

<section id="content" class="table-layout">
	<div id="msg" class="notification" data-note-stack="stack_bar_top"
		 data-note-style="<?php echo $_SESSION[$session_prefix . 'admin_var_msg_class']; ?>"></div>
    <!-- begin: .tray-center -->
    <div class="tray tray-center pv40 ph30 va-t posr">
        <div class="center-block">

            <!-- begin: .admin-form -->
            <div class="admin-form">

                <div id="p1" class="panel heading-border panel-system">

                    <div class="panel-body bg-light">
                        <form method="post" id="admin_form" class="form-horizontal" role="form"
                              action="<?php echo $ACT_LINK; ?>" enctype="multipart/form-data">

                            <div class="section-divider mb40" id="spy1">
                                <span class="panel-primary"><?php echo $MODULE; ?> Details</span>
                            </div>

                            <input type="hidden" name="mode" value="<?php echo $mode; ?>">
                            <input type="hidden" name="admin_id" id="admin_id"
                                   value="<?php echo ($iId) ? $iId : '';?>">


                            <!-- .section-divider -->

                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> Old Password:</label>

                                <div class="col-md-8">
                                    <input type="password" name="old_password" id="old_password" class="form-control"
                                           value="" placeholder="Enter old password">
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> New Password:</label>

                                <div class="col-md-8">
                                    <input type="password" name="new_password" id="new_password" class="form-control"
                                           value="" placeholder="Enter new password">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> Confirm Password:</label>

                                <div class="col-md-8">
                                    <input type="password" name="cnf_password" id="cnf_password" class="form-control"
                                           value="" placeholder="Enter confirm password">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button id="addbutton" type="submit"
                                            class="btn  btn-primary animsition-link"><?php echo ($iId != '') ? 'Save' : 'Add' ?></button>
                                    <a href="<?php echo $ADD_LINK; ?>" class="btn btn-dark animsition-link">Back</a>
                                </div>
                            </div>
                            <!-- Basic Inputs -->


                        </form>
                    </div>
                </div>
            </div>
            <!-- end: .admin-form -->
        </div>
    </div>
</section>

<?php include_once(@$account_setting_path . 'js_form.php'); ?>
<?php include_once($admin_path . 'modalpopup.php'); ?>


<script src="<?php echo $members_assets_url ?>js/jquery.validate.min.js"></script>

<script>
    var FormAdminValidator = function () {
        // function to initiate Validation Sample 1
        var temp = 0;
        var runValidator1 = function () {
            var form1 = $('#admin_form');

            $('#admin_form').validate({

                errorElement: "span", // contain the error msg in a span tag
                errorClass: 'help-block',
                errorPlacement: function (error, element) { // render error placement for each input type
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
                },
                ignore: "",
                rules: {
                    old_password: {
                        required: true,
                        remote: {
                            url: "<?php echo $ACT_LINK . '&mode=checkPassword&admin_id='.$iId;?>",
                            type: "post",
                            data: {
                                password: function () {
                                    return $("#old_password").val();
                                }
                            }
                        }
                    },
                    new_password: {
                        required: true,
                        minlength :6
                    },
                    cnf_password: {
                        required: true,
                        equalTo:('#new_password')
                    }
                },
                messages: {
                    old_password: {
                        required: "Please enter old password",
                        remote: "Old password is not matched"
                    },
                    new_password: {
                        required: "Please enter new password",

                        minlength :"Please enter minimum length 6"
                    },
                    cnf_password: {
                        required: "Please enter confirm password ",

                        equalTo :"New password and confirm password does not match."
                    },
                    bank_name: "Please enter bank name",
                    bank_address: "Please enter bank address",
                    name_of_beneficiary: "Please enter name of beneficiary",
                    address_of_beneficiary: "Please enter address of beneficiary",

                    eStatus: "Please select status"
                },
                invalidHandler: function (event, validator) { //display error alert on form submit
                    //successHandler1.hide();
                    //errorHandler1.show();
                },
                highlight: function (element) {
                    $(element).closest('.help-block').removeClass('valid');
                    // display OK icon
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                    // add the Bootstrap error class to the control group
                },
                unhighlight: function (element) { // revert the change done by hightlight
                    $(element).closest('.form-group').removeClass('has-error');
                    // set error class to the control group
                },
                success: function (label, element) {
                    label.addClass('help-block valid');
                    // mark the current input as valid and display OK icon
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
                },
                submitHandler: function (frmadd) {
                    successHandler1.show();
                    errorHandler1.hide();
                }

            });

        };

        return {
            //main function to initiate template pages
            init: function () {
                runValidator1();
            }
        };

        //$('#frmadd').submit();
    }();

    //$('#frmadd').submit();
    $(document).ready(function () {
        FormAdminValidator.init();

        $('#addbutton').click(function () {
            $('#eStatus').prop("disabled", false);
            $('#admin_form').submit();

        });
    });

</script>