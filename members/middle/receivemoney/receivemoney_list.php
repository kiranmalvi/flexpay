<?php
$transaction = new transaction();
$MODULE = 'Receive Money ';
$UserId = $_SESSION[$Project_Name_Members]['MEMBERS']['id'];
$creditlist = $transaction->select_transaction_credit_admin($UserId, 'credit');
?>
<!-- Start: Topbar -->
<header id="topbar" class="affix">
    <div class="topbar-left">
        <ol class="breadcrumb">
            <li class="crumb-link crumb-active ">
                <a href="index.php">My Balance</a>
            </li>

            <li class="crumb-trail"><?php echo $MODULE; ?></li>
        </ol>
    </div>
</header>
<!-- End: Topbar -->

<section id="content" class="tray tray-center pv40 ph30 va-t posr">
    <div class="row">
        <div class="col-md-12">
            <div id="msg" class="notification" data-note-stack="stack_bar_top"
                 data-note-style="<?php echo $_SESSION[$session_prefix . 'admin_var_msg_class']; ?>"></div>
            <div class="panel panel-visible <?php echo $PANEL_HEADER; ?>">
                <div class="panel-body pn">
                    <form name="admin_list" id="admin_list" method="post" action="<?php echo $ACT_LINK; ?>">
                        <input type="hidden" name="mode" id="mode" value="">
                        <input type="hidden" name="delete_type" value="multi_delete">
                        <div  class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="datatable4"
                               cellspacing="0"
                               width="100%">
                            <thead>
                            <tr class="<?php echo $TBL_HEADER; ?>">
                                <th width="17%" class="text-left">Transaction Id</th>
                                <th width="17%" class="text-left">From User</th>
                                <th width="16%" class="text-right">Amount</th>
                                <th width="16%" class="text-right">Rate</th>
                                <th width="17%" class="text-center">Transaction Status</th>
                                <th width="17%" class="text-center">Transaction date</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($creditlist as $creditdata) {
                                ?>
                                <tr>
                                    <td class="text-left"><?php echo $creditdata['transaction_unique_id'] ?></td>
                                    <td>
                                        <?php
                                        if ($creditdata['to_user_id'] == "0") {
                                            echo $ADMIN_NAME;
                                        } else {
                                            echo $creditdata['name'];
                                        }
                                        ?>
                                    </td>
                                    <td class="text-right"><?php echo $creditdata['amount'] ?></td>
                                    <td class="text-right"><?php echo $creditdata['credit_rate'] ?></td>
                                    <?php
                                    if ($creditdata['transaction_status'] == 'pending') {
                                        $style = "text-warning";
                                    } elseif ($creditdata['transaction_status'] == 'confirm') {
                                        $style = "text-confirm";

                                    } elseif ($creditdata['transaction_status'] == 'cancel') {
                                        $style = "text-danger";
                                    }
                                    ?>
                                    <td class="<?php echo $style ?> text-center"><?php echo ucfirst($creditdata['transaction_status']); ?></td>
                                    <td class="text-center""><?php echo $generalfuncobj->full_date_formate($creditdata['added_at']); ?></td>
                                </tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Start: Content -->
<div id="modalpopup" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false"
     style="display: none;">
    <?php include_once($site_path . 'members/js_table.php'); ?>
    <?php include_once($site_path . 'members/modalpopup.php'); ?>
