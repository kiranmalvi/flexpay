<?php

require_once('include.php');

if (isset($_SESSION['OBA_LOGIN'])) {
    header('location:index.php?file=ad-adminlist');
} else {

    if (isset($_REQUEST['login'])) {
        $email = $_REQUEST['email'];
        $password = base64_encode($_REQUEST['password']);
        //print_r($_REQUEST); exit;
        $SQL = "SELECT * FROM admin WHERE vEmail = '{$email}' AND vPasswd = '{$password}' AND eStatus = '1'";
        $DATA = $obj->select($SQL);
        //print_R($DATA); exit;
        if (count($DATA) > 0) {
            $login = [
                'iAdminId' => $DATA[0]['iAdminId'],
                'vFirstName' => $DATA[0]['vFirstName'],
                'vLastName' => $DATA[0]['vLastName'],
                'vEmail' => $DATA[0]['vEmail'],
                'dtLastLogin' => $DATA[0]['dtLastLogin'],
            ];

            $_SESSION['OBA_LOGIN']['ADMIN'] = $login;

            $TIME = strtotime(gmdate('Y-m-d H:i:s'));
            #$UPDATE = "UPDATE admin SET dtLastLogin = '{$TIME}' , vLastLoginIP = '{$_SERVER['REMOTE_ADDR']}' WHERE iAdminId = '{$DATA[0]['iAdminId']}'";
            $obj->sql_query($UPDATE);

            $generalfuncobj->func_set_temp_sess_msg("Well Come" . " " . $DATA[0]['vFirstName'] . ' ' . $DATA[0]['vLastName'],
                "success");
            header('location:index.php?file=ad-adminlist');
        } else {
            $error = 'You are not Authorized User and try again.';
        }
    } else {
        $email = $_REQUEST['email'];
        $sql = "SELECT * FROM admin WHERE vEmail = '{$email}' AND eStatus = '1'";
        $RESET_DATA = $obj->select($sql);
        // print_r($RESET_DATA); exit;
    }

    ?>


    <!DOCTYPE html>
    <html>

    <head>
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <title><?php echo $ADMIN_PAGE_TITLE; ?></title>
        <meta name="keywords" content="HTML5 Bootstrap 3 Admin Template UI Theme"/>
        <meta name="description" content="AdminDesigns - A Responsive HTML5 Admin UI Framework">
        <meta name="author" content="AdminDesigns">
        <meta name="keywords" content=""/>
        <meta name="description" content="">
        <meta name="author" content="Mindinventory">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Font CSS (Via CDN) -->
        <link rel='stylesheet' type='text/css'
              href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800'>
        <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Roboto:400,500,700,300">

        <!-- Theme CSS -->
        <link rel="stylesheet" type="text/css" href="<?php echo $assets_url ?>skin/default_skin/css/theme.css">

        <!-- Admin Forms CSS -->
        <link rel="stylesheet" type="text/css"
              href="<?php echo $assets_url ?>admin-tools/admin-forms/css/admin-forms.css">

        <!-- Favicon -->
        <link rel="shortcut icon" href="../uploads/logo.png">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>

    <body class="external-page sb-l-c sb-r-c">

    <!-- Start: Main -->
    <div id="main" class="animated fadeIn">

        <!-- Start: Content -->
        <section id="content_wrapper">

            <!-- begin canvas animation bg -->
            <div id="canvas-wrapper">
                <canvas id="demo-canvas"></canvas>
            </div>

            <!-- Begin: Content -->
            <section id="content">

                <div class="admin-form theme-info" id="login1">

                    <div class="row mb15 table-layout">

                        <div class="col-xs-6 va-m pln">
                            <a href="javascript:" title="pigi7" style="text-decoration: none;">
                                <h1><i><span style="color: #ffffff;"><?php echo $ADMIN_PANEL_TITLE; ?></span></i></h1>
                            </a>
                        </div>

                        <div class="col-xs-6 text-right va-b pr5">
                            <div class="login-links">
                                <a href="javascript:" class="active" id="sign_in" title="Sign In"
                                   onclick="change_form('login');">Sign In</a>
                                <span class="text-white"> | </span>
                                <a href="javascript:" class="" id="forgot_password" title="Register"
                                   onclick="change_form('forgot_password');">Forgot Password</a>
                            </div>

                        </div>

                    </div>


                    <div class="panel panel-primary heading-border">
                        <div class="panel-heading">
                            <span class="panel-title"><i class="fa fa-sign-in"></i>Login</span>
                        </div>
                        <!-- end .form-header section -->

                        <form method="post" action="#" id="login_form">
                            <?php if (isset($error)) {
                                echo '<br>';
                                echo '<div class="alert alert-sm alert-border-left alert-danger light alert-dismissable">
                            <i class="fa fa-remove pr10"></i>
                            <strong>Sorry!</strong> ' . $error . '</div>';
                            }
                            ?>
                            <div class="panel-body p25 pt10">

                                <!-- .section-divider -->


                                <!--                                    <div class="section-divider mv40">-->
                                <!--                                        <span>Login</span>-->
                                <!--                                    </div>-->
                                <!-- .section-divider -->

                                <div class="section">
                                    <label for="username" class="field prepend-icon">

                                        <input type="text" name="email" id="email" class="gui-input"
                                               placeholder="Enter Email Address">
                                        <label for="username" class="field-icon"><i class="fa fa-user"></i>
                                        </label>
                                    </label>
                                </div>
                                <!-- end section -->

                                <div class="section">
                                    <label for="password" class="field prepend-icon">
                                        <input type="password" name="password" id="password" class="gui-input"
                                               placeholder="Enter password">
                                        <label for="password" class="field-icon"><i class="fa fa-lock"></i>
                                        </label>
                                    </label>
                                </div>
                                <!-- end section -->


                                <!-- end section -->

                            </div>
                            <!-- end .form-body section -->
                            <div class="panel-footer">
                                <button type="submit" class="button btn-primary">Sign in</button>
                            </div>
                            <!-- end .form-footer section -->
                        </form>
                        <!-- end .form-header section -->
                        <form method="post" action="" id="forgot_pass_form" style="display: none;">
                            <div class="panel-body bg-white p15 pt25">

                                <div class="alert alert-micro alert-border-left alert-info pastel alert-dismissable mn">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×
                                    </button>
                                    <i class="fa fa-info pr10"></i> Enter your <b>Email</b> and instructions will be
                                    sent to you!
                                </div>
                            </div>
                            <!-- end .form-body section -->
                            <div class="panel-footer p25 pv15">

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="section mn">
                                            <div class="smart-widget sm-right smr-80">
                                                <label for="email" class="field prepend-icon">
                                                    <input type="text" name="email" id="email" class="gui-input"
                                                           placeholder="Your Email Address">
                                                    <label for="email" class="field-icon"><i
                                                            class="fa fa-envelope-o"></i>
                                                    </label>
                                                </label>

                                                <input type="submit" name="reset" value="Reset"
                                                       class="button btn-primary mr10 pull-right">
                                            </div>
                                            <!-- end .smart-widget section -->
                                        </div>
                                        <!-- end section -->
                                    </div>

                                </div>

                            </div>
                            <!-- end .form-footer section -->
                        </form>
                    </div>
                    <!-- end .panel-->

                </div>

            </section>
            <!-- End: Content -->

        </section>
        <!-- End: Content-Wrapper -->

    </div>
    <!-- End: Main -->


    <!-- BEGIN: PAGE SCRIPTS -->
    <script src="<?php echo $assets_url ?>js/jquery.js"></script>
    <!-- Google Map API -->
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>

    <!-- jQuery -->
    <script type="text/javascript" src="<?php echo $admin_url ?>vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

    <script src="<?php echo $assets_url ?>js/jquery-1.8.3.min.js"></script>
    <script src="<?php echo $assets_url ?>js/jquery.validate.min.js"></script>
    <!-- Bootstrap -->
    <!--<script type="text/javascript" src="assets/js/bootstrap/bootstrap.min.js"></script>-->

    <!-- Page Plugins -->
    <script type="text/javascript" src="<?php echo $assets_url ?>js/pages/login/EasePack.min.js"></script>
    <script type="text/javascript" src="<?php echo $assets_url ?>js/pages/login/rAF.js"></script>
    <script type="text/javascript" src="<?php echo $assets_url ?>js/pages/login/TweenLite.min.js"></script>
    <script type="text/javascript" src="<?php echo $assets_url ?>js/pages/login/login.js"></script>

    <!-- Theme Javascript -->
    <script type="text/javascript" src="<?php echo $assets_url ?>js/utility/utility.js"></script>
    <script type="text/javascript" src="<?php echo $assets_url ?>js/main.js"></script>
    <script type="text/javascript" src="<?php echo $assets_url ?>js/demo.js"></script>

    <!-- Page Javascript -->
    <script type="text/javascript">
        jQuery(document).ready(function () {

            "use strict";

            // Init Theme Core
            Core.init();

            // Init Demo JS
            Demo.init();

            // Init CanvasBG and pass target starting location
            CanvasBG.init({
                Loc: {
                    x: window.innerWidth / 2,
                    y: window.innerHeight / 3.3
                }
            });
        });

        function change_form(form_type) {
            if (form_type == 'forgot_password') {
                jQuery('#forgot_pass_form').show();
                jQuery('#login_form').hide();
            }
            else if (form_type == 'login') {
                jQuery('#forgot_pass_form').hide();
                jQuery('#login_form').show();
            }
        }
    </script>


    <script>
        var FormAdminValidator = function () {
            // function to initiate Validation Sample 1
            var temp = 0;
            var runValidator1 = function () {
                var form1 = $('#login_form');

                $('#login_form').validate({

                    errorElement: "span", // contain the error msg in a span tag
                    errorClass: 'help-block',
                    errorPlacement: function (error, element) { // render error placement for each input type
                        error.insertAfter(element);
                        // for other inputs, just perform default behavior
                    },
                    ignore: "",
                    rules: {
                        email: {
                            email: true,
                            required: true
                        },

                        password: {
                            required: true
                        }

                    },
                    messages: {
                        email: "Please Enter Email Address",
                        password: "Plase Enter Password"
                    },
                    invalidHandler: function (event, validator) { //display error alert on form submit
                        //successHandler1.hide();
                        //errorHandler1.show();
                    },
                    highlight: function (element) {
                        $(element).closest('.help-block').removeClass('valid');
                        // display OK icon
                        $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                        // add the Bootstrap error class to the control group
                    },
                    unhighlight: function (element) { // revert the change done by hightlight
                        $(element).closest('.form-group').removeClass('has-error');
                        // set error class to the control group
                    },
                    success: function (label, element) {
                        label.addClass('help-block valid');
                        // mark the current input as valid and display OK icon
                        $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
                    },
                    submitHandler: function (frmadd) {
                        successHandler1.show();
                        errorHandler1.hide();
                    }

                });

            };

            return {
                //main function to initiate template pages
                init: function () {
                    runValidator1();
                }
            };

            //$('#frmadd').submit();
        }();

        //$('#frmadd').submit();
        $(document).ready(function () {
            FormAdminValidator.init();
            $('#login').click(function () {
                $('#login_form').submit();
            });
        });
    </script>

    <!-- END: PAGE SCRIPTS -->

    </body>

    </html>
<?php } ?>