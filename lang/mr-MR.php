<?php

/* TOP START */

define('DASHBOARD', 'डॅशबोर्ड');
define('HOME', 'मुख्यपृष्ठ');
define('COMING_SOON', 'लवकरच येत आहे');
define('MY_PROFILE', 'माझे प्रोफाइल');
define('CHANGE_PASSWORD', 'पासवर्ड बदला');
define('LOG_OUT', 'लॉग आउट');
define('ADMIN_LIST', 'प्रशासन यादी');
define('SETTING', 'सेटिंग्ज');

/* TOP END */

/* MENU START */

define('ADMIN', 'प्रशासन');
define('USER', 'सदस्य');
define('CATEGORY', 'श्रेणी');
define('BUILDING', 'इमारत');
define('UTILITY', 'उपयुक्तता');
define('CMS', 'सीएमएस');
define('GENERAL_SETTINGS', 'सामान्य सेटिंग्ज');
define('EMAIL_FORMAT', 'ईमेल स्वरूप');

/* MENU END */

/* User START */

define('USER_INFORMATION', 'सदस्य माहिती');
define('VIEW_USER_DETAILS', 'पहा वापरकर्ता तपशील');
define('USER_UPDATE', 'वापरकर्ता अद्यतनित करा माहिती');
define('EDIT_USER_DETAILS', 'वापरकर्ता प्रोफाइल संपादित करा');
define('PLAYER_IMAGE', 'प्रतिमा अपलोड करा');
define('PLAYER_SELECT_FILE', 'फाईल निवडा');
define('PLAYER_VIEW_IMAGE', 'पहा प्रतिमा');
define('PLAYER_REMOVE_IMAGE', 'प्रतिमा काढा');
define('ADDRESS', 'पत्ता');
define('FULL_NAME', 'पूर्ण नाव');
define('USER_LIST', 'सदस्यांची यादी');
define('USER_NAME', 'सदस्य नाव');
define('USER_DESC', 'सदस्य वर्णन');
define('USER_ADD_NEW', 'नवीन वापरकर्ता माहिती जोडा');
define('ADD_PLAYER_MSG', 'नवीन वापरकर्ता तपशील यशस्वीरित्या जोडले गेले आहे.');
define('UPDATE_PLAYER_MSG', 'सदस्य माहिती यशस्वीरित्या अद्यतनित केली.');
define('IMAGE', 'प्रतिमा');
define('USER_SOCIAL_LIST', 'सदस्य सामाजिक यादी');
define('TYPE', 'प्रकार');

/* User END */

/* Start Category */

define('CATEGORY_UPDATE', 'अद्यतन श्रेणी माहिती');
define('CATEGORY_ADD_NEW', 'नवीन जोडा श्रेणी माहिती');
define('EDIT_CATEGORY_DETAILS', 'संपादित करा श्रेणी प्रोफाइल');
define('CATEGORY_LIST', 'श्रेणी यादी');
define('CATEGORY_NAME', 'श्रेणी नाव');
define('VIEW_CATEGORY_DETAILS', 'पहा श्रेणी माहिती');
define('ADD_CATEGORY_MSG', 'नवीन श्रेणी तपशील यशस्वीपणे समाविष्ट केले आहे.');
define('UPDATE_CATEGORY_MSG', 'श्रेणी माहिती यशस्वीपणे अद्यतनित केले.');
define('CATEGORY_INFORMATION', 'श्रेणी माहिती');

/* End Category */

/* Start Building */

define('BUILDING_UPDATE', 'अद्यतन इमारत माहिती');
define('BUILDING_ADD_NEW', 'नवीन इमारत माहिती जोडा');
define('EDIT_BUILDING_DETAILS', 'संपादित करा इमारत प्रोफाइल');
define('BUILDING_LIST', 'इमारत यादी');
define('BUILDING_TYPE', 'इमारत प्रकार');
define('VIEW_BUILDING_DETAILS', 'पहा इमारत माहिती');
define('ADD_BUILDING_MSG', 'नवीन इमारत माहिती यशस्वीपणे समाविष्ट केले आहे.');
define('UPDATE_BUILDING_MSG', 'इमारत माहिती यशस्वीपणे अद्यतनित केले.');
define('BUILDING_INFORMATION', 'इमारत माहिती');

/* End Building */

/* Start Complaint */

define('COMPLAINT', 'तक्रार');
define('COMPLAINT_UPDATE', 'अद्यतनित करा तक्रार माहिती');
define('COMPLAINT_ADD_NEW', 'नवीन तक्रार तपशील जोडा');
define('EDIT_COMPLAINT_DETAILS', 'संपादित करा तक्रार तपशील');
define('LOG_COMPLAINT_DETAILS', 'तक्रार लॉग माहिती');
define('COMPLAINT_LIST', 'तक्रार यादी');
define('COMPLAINT_TYPE', 'तक्रार प्रकार');
define('VIEW_COMPLAINT_DETAILS', 'पहा तक्रार तपशील');
define('ADD_COMPLAINT_MSG', 'नवीन तक्रार तपशील यशस्वीरित्या समाविष्ट केले आहे.');
define('UPDATE_COMPLAINT_MSG', 'तक्रार तपशील यशस्वीरित्या अद्यतनित केले.');
define('COMPLAINT_INFORMATION', 'तक्रार माहिती');
define('COMPLAINTJOIN_INFORMATION', 'तक्रार लॉग माहिती');
define('BUILDING_NAME', 'इमारतीचे नाव');
define('LOCATION', 'स्थान');
define('DESCRIBEISSUE', 'समस्येचे वर्णन');
define('LATITUDE', 'अक्षांश');
define('LONGITUDE', 'रेखांश');

/* End Complaint */

/* ADMIN START */

define('NAME', 'नाव');
define('EMAIL', 'ईमेल');
define('ADDED_DATE', 'समाविष्ट तारीख');
define('LAST_LOGIN', 'अंतिम लॉग-इन');
define('FIRST_NAME', 'पहिले नाव');
define('LAST_NAME', 'अंतिम नाव');
define('EMAIL_ADDRESS', 'ईमेल पत्ता');
define('PASSWORD', 'संकेतशब्द');
define('CONFIRM_PASSWORD', 'संकेतशब्दाची पुष्टी');
define('ADD_NEW_ADMIN', 'नवीन प्रशासन वापरकर्ता जोडा');
define('UPDATE_ADMIN', 'अद्यतन प्रशासन');
define('UPDATE_ADMIN_MSG', 'प्रोफाइल यशस्वीपणे अद्यतनित');
define('ADD_ADMIN_MSG', 'नवीन प्रशासन तपशील यशस्वीरित्या जोडले गेले आहे');
define('EMAIL_MSG', 'ई-मेल आधीच अस्तित्वात');

/* ADMIN END */

/* GENERAL KEY WORD START */

define('SHOW', 'दर्शवा');
define('RECORDS', 'रेकॉर्ड');
define('SEARCH_BY', 'करून शोध');
define('STATUS', 'स्थिती');
define('W_Y_S_K', 'आपला शोध कीवर्ड लिहा');
define('GO', 'जा');
define('SHOWING', 'दर्शवित आहे');
define('TO', 'करण्यासाठी');
define('OF', 'च्या');
define('A_TO_Z', 'A B C D E F G H I J K L M N O P Q R S T U V W X Y Z सर्व');
define('ACTION', 'क्रिया');
define('N_R_A', 'रेकॉर्ड उपलब्ध नाही');
define('SHOW_ALL', 'सर्व दर्शवा');
define('ADD_NEW', 'नवीन जोडा');
define('SELECT_STATUS', 'सिलेक्ट करा स्थिती');
define('ACTIVE', 'सक्रीय');
define('INACTIVE', 'निष्क्रिय');
define('PENDING', 'प्रलंबित');
define('CLOSE', 'बंद करा');
define('DELETE', 'नष्ट करा');
define('ADD', 'जोडा');
define('CANCEL', 'रद्द करा');
define('SAVE', 'जतन करा');
define('EDIT', 'संपादित करा');
define('REMOVE', 'काढा');
define('ALL', 'सर्व');
define('PROFILE', 'प्रोफाइल');
define('CURPWD', 'वर्तमान संकेतशब्द');
define('NPWD', 'नवीन संकेतशब्द');
define('CONPWD', 'संकेतशब्दाची पुष्टी');
define('ENT_CURPWD', 'आपला संकेतशब्द प्रविष्ट करा');
define('ENT_NPWD', 'तुमचा नवीन पासवर्ड प्रविष्ट करा');
define('ENT_CONPWD', 'पुन्हा आपला संकेतशब्द प्रविष्ट');
define('YOUR', 'तुमची');
define('SEARCH', 'शोधा');
define('FOR', 'साठी');
define('HAS', 'आहे');
define('FOUND', 'आढळले');
define('SUCCESS', 'यशस्वीपूर्वक');
define('DELETE_MSG', 'रेकॉर्ड यशस्वीरित्या हटविले');

define('USER_SELECT', '- निवडा वापरकर्ता -');
define('BUILDING_SELECT', '- निवडा इमारत प्रकार -');
define('CATEGORY_SELECT', '- श्रेणी निवडा -');

/* GENERAL KEY WORD END */

/* Change Password Start */

define('CHNG_PWD', 'Password changed successfully.');
define('CHNG_PWD_ERROR', 'Your newpassword and confirmpassword is not matched.');
define('CUR_PWD_ERROR', 'Your current password is not matched');

/* Change Password  End */

/* Email format Start */

define('ADD_EMAIL_MSG', 'Add New Email Format');
define('UPDATE_EMAIL_MSG', 'Update Email Format');
define('EMAIL_NAME', 'Email Format Title');
define('EMAIL_SUB', 'Email Format Subject');
define('EMAIL_DESC', 'Email Format Description');
define('EMAIL_LIST', 'Email Format List');
define('NEW_EMAIL_MSG', 'New Email details has been added successfully.');
define('UPDATE_EMAIL', 'Email Format details updated successfully.');

/* Email format End */

/* All Error Message Start */

define('ADMIN_ERROR_FN', 'नाव निर्दिष्ट करा.');
define('ADMIN_ERROR_LN', 'आडनाव सांगा.');
define('ADMIN_EMAIL1', 'आपल्याशी संपर्क साधण्यासाठी आम्ही आपला ईमेल पत्ता आवश्यक आहे.');
define('ADMIN_EMAIL2', 'आपला ई-मेल पत्ता name@domain.com च्या स्वरूपात असणे आवश्यक आहे');
define('ADMIN_PWD', 'आपला संकेतशब्द प्रविष्ट करा');
define('ADMIN_PWD1', 'मि 6 अक्षर प्रविष्ट करा');
define('ADMIN_CPWD', 'संकेतशब्द कन्फर्म करा प्रविष्ट करा.');
define('ADMIN_CPWD1', 'पुन्हा समान मूल्य प्रविष्ट करा.');

define('USER_ERROR_FNAME', 'नाव निर्दिष्ट करा');
define('USER_ERROR_LNAME', 'आडनाव सांगा');
define('USER_ERROR_ADDRESS', 'पत्ता निर्देशीत करा');
define('USER_ERROR_EMAIL', 'ईमेल निर्दिष्ट करा');

define('CATEGORY_ERROR_NAME', 'वर्गवारी नाव निर्दिष्ट करा');

define('BUILDING_ERROR_NAME', 'इमारतीचे नाव निर्दिष्ट करा');

/* All Error Message End */

/* New Detail Start  */

define('CHAMPIONSHIP_TEAM_LIST', 'Championship Team List');
define('YEAR', 'Year');
define('ADD_NEW_CHAMP_WITH_TEAM', 'Add New Championship With Team');
define('UPDATE_CHAMP_TEAM_INFO', 'Update Championship Team Info');
define('ENTER_YEAR', 'Please Enter Year');

define('CHAMP_DETAIL_LIST', 'Championship Detail List');
define('TOTAL_MATCH', 'Total Match');
define('WON', 'Won');
define('DRAWN', 'Drawn');
define('LOST', 'Lost');
define('GOAL_SCORING', 'Goal Scoring');
define('GOAL_CONCEDING', 'Goal Conceding');

define('COACH_NATIONALITY', 'Coach Nationality');
define('UPLOAD_IMAGE', 'Upload Image');
define('IMAGE', 'Image');
define('SELECT_IMAGE', 'Select File');
define('CHANGE', 'Change');
define('VIEW_IMAGE', 'View Image');
define('REMOVE_IMAGE', 'Remove Image');
define('BIRTHDATE', 'BirthDate');
define('STARTDATE', 'Start Date');
define('ENDDATE', 'End Date');

define('COACH_DETAIL_LIST', 'Coach Detail List');
define('ADD_NEW_COACH_DETAIL', 'Add New Coach Detail');
define('UPDATE_COACH_DETAIL', 'Update Coach Detail');

define('PLAYER_NATIONALITY', 'Player Nationality');
define('PLAYER_BIRTHDATE', 'Player BirthDate');
define('PLAYER_TOTALMATCH', 'Player TotalMatch');
define('PLAYER_TOTALGOAL', 'Player TotalGoal');
define('PLAYER_IMAGE', 'Upload Image');
define('PLAYER_BDATE', 'Birth Date');
define('PLAYER_FDATE', 'Select Birth Date');
define('PLAYER_SELECT_FILE', 'Select file');
define('PLAYER_VIEW_IMAGE', 'View Image');
define('PLAYER_REMOVE_IMAGE', 'Remove Image');

define('PLAYER_DETAIL_LIST', 'Player Detail List');
define('ADD_NEW_PLAYER_DETAIL', 'Add New Player Detail');
define('UPDATE_PLAYER_DETAIL', 'Update Player Detail');
define('YELLOWCARD', 'Yellow Card');
define('REDCARD', 'Red Card');
define('PENALTY_GOAL', 'Penalty Goal');
define('MISSED_PENALTY_GOAL', 'Missed Penalty Goal');
define('FULL_MATCH_PLAYED', 'Full Match Played');
define('MATCH_AS_SUBSTITUTE', 'Played Match As Substitute');
define('MATCH_WAS_SUBSTITUTE', 'Played Match Was Substitute');

define('TEAM_DETAIL_LIST', 'Team Detail List');
define('ADD_NEW_TEAM_DETAIL', 'Add New Team Detail');
define('UPDATE_TEAM_DETAIL', 'Update Team Detail');
define('TOTAL_GOAL', 'Toatl Goal');

define('COACH_ERROR_COACH_NAME', 'Please Select Coach Name');
define('COACH_ERROR_CHAMP_NAME', 'Please Select Championship Name');
define('COACH_ERROR_TOTAL_MATCH', 'Enter Total Number of Match');
define('COACH_ERROR_WON', 'Please Enter Number of Won match');
define('COACH_ERROR_DRAWN', 'Please Enter Number of Drwan match');
define('COACH_ERROR_LOST', 'Please Enter Number of Lost match');
define('COACH_ERROR_GS', 'Please Enter Number of Goal Scoring ');
define('COACH_ERROR_GC', 'Please Enter Number of Goal Concending ');
define('COACH_ERROR_STATUS', 'Please Select Status');

define('PLAYER_ERROR_PLAYER_NAME', 'Please Select Player Name');
define('PLAYER_ERROR_CH_NAME', 'Please Select Championship Name');
define('PLAYER_ERROR_TOTALMATCH', 'Enter Total Number of Match');
define('PLAYER_ERROR_TOTALGOAL', 'Enter Total Number of Goal ');
define('PLAYER_ERROR_P', 'Please specify Penalty Goal');
define('PLAYER_ERROR_MP', 'Please specify Missed Penalty Goal');
define('PLAYER_ERROR_S1', 'Please specify Substitutions 1');
define('PLAYER_ERROR_R', 'Please specify Red Card');
define('PLAYER_ERROR_Y', 'Please specify Yellow cards');
define('PLAYER_FULL_MATCH_ERROR', 'Please Enter Full Match');
define('PLAYER_AS_SUB', 'Please Enter As Substitute Match');
define('PLAYER_WAS_SUB', 'Please Enter Was Substitute Match');

define('COACH_ERROR_VNATIONALITY', 'Please Specify Coach Nationality');

define('COACH_ERROR_VBIRTHDATE', 'Please Select Coach Birthdate');

define('PLAYER_ERROR_VNATIONALITY', 'Please Specify Player Nationality');

define('PLAYER_ERROR_VBIRTHDATE', 'Please Select Player Birthdate');
define('PLAYER_ERROR_VTOTALMATCH', 'Please Specify Number of match played');
define('PLAYER_ERROR_VTOTALGOAL', 'Please Specify Number of goal');

define('TEAM_ERROR_NAME', 'Please Select Team');
define('TEAM_ERROR_GOAL', 'Enter Total Number of Goal');
define('TEAM_ERROR_WON', 'Please Enter Won Match');
define('TEAM_ERROR_DRAWN', 'Please Enter Drawn Match');
define('TEAM_ERROR_LOST', 'Please Enter Lost Match');

define('CHAMPIONSHIP_CREATE_LIST', 'Championship Create List');
define('ADD_NEW_CREATE_CHAMP_WITH_SEASON', 'Create New Championship Season');
define('UPDATE_CREATE_CHAMP_SEASON_INFO', 'Update Create Championship Season Info');
define('SEASON_NAME', 'Season');
define('SEASON_ERROR_NAME', 'Please Add Season');
define('championship', 'championship');
define('Edit_champion', 'Edit Championship');
define('Match_list', 'Match list');

/* Player View start */

define('VIEW_PLAYER_DETAILS', 'View Player Details');
define('PLAYER_INFORMATION', 'Player Information');
define('FULL_NAME', 'Full Name');
define('TEAM_NAME', 'Team Name');
define('NATIONNALITY', 'Nationality');
define('GENERAL_INFORMATION', 'General Information');
define('TOTAL_MATCH', 'Total Match');
define('TOTAL_GOALS', 'Total Goals');
define('YELLOWCARD', 'Yellow card');
define('REDCARD', 'Red card');
define('PENALTYGOAL', 'Penalty Goal');
define('MISSED_PANALTY_GOALS', 'Missed penalty goal');
define('TOTALFULLMATCHPLAYED', 'Total full match played');
define('TOTALMATCHPLAYEDASSUBSTITUTE', 'Total match played as substitute');
define('TOTALMATCHPLAYEDWASUBSTITUTE', 'Total match played was substituted');

/* player view end */

/* start club member of club */

define('EDIT_CLUB_DETAILS', 'Edit Club Details');
define('COACH_DETAILS', 'Coach Details');
define('CHAMPIONSHIP_MATCH_LIST', 'Championship Match List');
define('COACH_LIST', 'Coach List');
define('MATCHID', 'MactchId');
define('TEAM1', 'Team 1');
define('TEAM2', 'Team 2');
define('PLACE', 'Place');
define('DATE', 'Date');
define('WINER', 'Winer');

/* End club member of club */

define('MATCH_CLICK_CHAMP', 'Click for Add New championship');
define('MATCH_CLICK_SEASON', 'Click for Add New Season');
define('MATCH_CLICK_VENUE', 'Click for Add venue');
define('MATCH_CLICK_MREFEREEE', 'Click for Add New Referee');
define('MATCH_CLICK_AREFEREEE', 'Click for Add New Assistant');
define('MATCH_CLICK_SREFEREEE', 'Click for Add New Substitute refree');
define('MATCH_CLICK_MNREFEREEE', 'Click for Add New Referee');
define('MATCH_CLICK_PRESIDENT', 'Click for Add New President');
define('MATCH_CLICK_COACH', 'Click for Add New coach');
define('MATCH_CLICK_PLAYER', 'Player List');
/* New Detail End  */

define('Championship_Details', 'Championship Details');
define('Referee_detail', 'Referee detail');
define('Club_1_Details', 'Club 1 Details');
define('Club_2_Details', 'Club 2 Details');
define('Match_Result', 'Match Result');
define('Team_1_score', 'Team 1 score');
define('Team_2_score', 'Team 2 score');

?>