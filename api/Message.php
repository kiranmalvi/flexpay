<?php

namespace MI\API;

class Message
{
    public static $invalidTag = "Invalid API tag";
    public static $invalidVersion = "Invalid Version";
    public static $invalidMethod = "Method not allowed";
    public static $invalideid = "Invalid id";
    public static $invalideuser_id = "Invalid user id";

    ## password changes
    public static $invalideoldpassword = "Invalid old password";
    public static $invalidenewpassword = "Invalid new password";
    ##pin changes
    public static $invalidenewpin = "Invalid new pin";
    public static $invalideoldpin = "Invalid old pin";
    public static $invalidmobileno = "Invalid mobileno";
    ##edit profile
    public static $invalidemail = "Invalid email";
    public static $invalidename = "Invalid name";

    public static $invalid_request_session_id = "Invalid SessionID";

    ## Sign up Message
    public static $invalid_sign_name = "Invalid Name";
    public static $invalid_sign_email = "Invalid email";
    public static $invalid_sign_mobile = "Invalid mobile";
    public static $invalid_sign_password = "Invalid password";
    public static $invalid_sign_security_pin = "Invalid security pin";
    public static $sign_email_already = "email already registered";
    public static $sign_success = "Congratulation registered successfully.";
    ##

    ## Request Money Message
    public static $invalid_request_money_type = "Invalid Type";
    public static $invalid_request_money_email = "Invalid Email";
    public static $invalid_request_money_amount = "Invalid Amount";
    public static $invalid_request_money_comment = "Invalid Comment";
    public static $invalid_request_money_receiver_id = "Invalid receiver_id";
    public static $invalid_request_money_sender_id = "Invalid sender_id";
    public static $invalid_request_money_wallet = "wallet not found.";
    public static $invalid_request_money_wallet_error = "insufficient wallet amount.";
    public static $invalid_request_transaction_money_error = "This transaction does not match with minimum amount criteria";
    public static $invalid_request_money_wallet_success = "Request send successfully.";
    public static $invalide_user = "User not register.";
    public static $invalid_send_money_wallet_success = "wallet amount send successfully .";
    ##
    ##check pin mesage
    public static $invalid_pin_to_use = "Please enter correct PIN.	";

    ## Action Money
    public static $invalid_transaction_id = "Invalid transaction_id";
    public static $invalid_action = "Invalid action";

    ## Security Parameter
    public static $invalid_session_id = "Invalid session_id";
    public static $invalid_info_type = "Invalid info_type";
    public static $invalid_device_id = "Invalid device_id";
    public static $invalid_device_model = "Invalid device_model";
    public static $invalid_system_name = "Invalid system_name";
    public static $invalid_system_version = "Invalid system_version";
    public static $invalid_app_version = "Invalid app_version";
    public static $invalid_country_code = "Invalid country_code";
    public static $invalid_latitude = "Invalid latitude";
    public static $invalid_longnitude = "Invalid longitude";

    ## notification message
    public static $invalidenotificationid = "Invalid notification id.";

    ## verifycode message
    public static $invalidcode = "Invalid verification Code.";

    ## Index
    public static $block_user_error = "Your account has been blocked contact admin for further information";
    public static $inactive_user_error = "This account has been inactivate";
    public static $delete_user_error = "This account has been deleted";
    public static $unverified_user_error = "This account has been unverified";
    public static $user_error = "Please contact admin to active your account";
    public static $block_receiver_user_error = "This account is temporarily out of service, please try again later";

	## Transfer limit exceeded
	public static $spend_limit_exceeded = "Your account has reached daily money spend limit. Try again later to spend money or contact your admin to increase your money spend limit.";
	public static $spend_limit_remain = "Your daily money spend limit will exceeded/reached by this transaction. Your today's remaining money spend limit is : ";

    public static $invalid_request_user_id = "Invalid request user_id";
    public static $invalid_request_user_biller_id = "Invalid request User biller Id";
    public static $invalid_request_biller_name = "Invalid request biller name";
    public static $invalid_request_biller_email = "Invalid request biller email";
    public static $invalid_request_account = "Invalid request account";
    public static $invalid_request_comment = "Invalid request comment";
    public static $success_add_biller = "Your biller added successfully.";
    public static $success_update_biller = "Your biller updated successfully.";
    public static $success_delete_biller = "Your biller deleted successfully.";

	## Sub Users API related
	public static $invalid_user_id = "User ID is required.";
	public static $invalid_player_id = "Player ID is required.";
	public static $invalid_subuser_id = "Sub User ID is required.";
	public static $invalid_employee_id = "Employee ID is required.";
	public static $subuser_sign_success = "Sub-User added successfully.";
	public static $sign_eid_already = "Employee ID already registered with us.";
	public static $invalid_sign_employee_id = "Employee ID is combination of a-z / A-Z / 0-9 / - (dash)/ _ underscore. No other characters are allowed.";
	public static $invalid_sub_user_id = "Invalid sub user";
	public static $success_delete_subUser = "Sub user deleted successfully.";
	public static $success_reset_subUser = "Sub user password reset successfully.";
	public static $Invalid_device_info = "Device Id is required.";
	public static $inactive_mainuser_error = "Your main user's account is inactivated. Please contact to your admin.";
	public static $delete_mainuser_error = "Your main user's account is deleted. Please contact to your admin.";
	public static $block_mainuser_error = "Your main user's account is blocked. Please contact to your admin.";
	public static $unverified_mainuser_error = "Your main user's account is unverified. Please contact to your admin.";
	public static $mainuser_error = "Please contact admin to activate your account.";
	public static $login_required = "Login is required.";
	public static $subUser_request_money_error = "You cannot request money to this User.";
	public static $subUser_refund_request_money = "Refund request initiated successfully.";

	public static $string_timezone_required = "Timezone in string is required";
	public static $time_timezone_required = "Timezone in time difference is required";
	public static $time_zone_update = "Timezone updated successfully";

	public static $paypal_payment_id_required = "PayPal payment id is missing.";
	public static $paypal_payment_amount_required = "PayPal payment amount is missing.";
	public static $paypal_payment_state_required = "PayPal payment state is missing.";
	public static $paypal_payment_success = "PayPal money added to your wallet successfully.";
	public static $paypal_payment_failure = "There is some problem to add money from PayPal. Please try again later.";
	public static $paypal_payment_already_added = "PayPal transaction money already added in your wallet.";
	public static $paypal_payment_access_token = "PayPal Access-Token is missing. Please try again later.";
	public static $paypal_payment_not_approved = "PayPal is not approved this transaction. Please try again later.";
}
