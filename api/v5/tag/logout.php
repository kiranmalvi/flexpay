<?php
/**
 * @api {get} /v3/?tag=logout Logout User
 * @apiName logout for main User
 * @apiVersion 1.0.0
 * @apiGroup user
 *
 * @apiParam {String} tag Tag for api.
 * @apiParam {integer} user_id    give main user's user id (required)
 * @apiParam {String} player_id    give player id (optional)
 * @apiParam {String} device_id    give device id (required)
 * @apiSuccess null data Response data.
 * @apiSuccess {String} message Message.
 * @apiSuccess {Number} status Status.
 *
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 200 OK
 * {
 *      data: null,
 * message: "You are logout successfully.",
 * status: 1
 * }
 *
 */

use MI\API\Response as response;
use MI\GEN as api;
use MI\API\Message as message;

$user_id = isset($_REQUEST['user_id']) && $_REQUEST['user_id'] != '' ? $_REQUEST['user_id'] : api::error(0, \MI\API\Message::$invalid_user_id);
$device_id = isset($_REQUEST['device_id']) && $_REQUEST['device_id'] != '' ? $_REQUEST['device_id'] : api::error(0, \MI\API\Message::$Invalid_device_info);
$player_id = isset($_REQUEST['player_id']) && $_REQUEST['player_id'] != '' ? $_REQUEST['player_id'] : null;

$user = new  user();
$uld = new user_login_devices();

$gmt = $generalfuncobj->gm_date();

$user_data = $user->select($user_id);
$status = $user_data[0]['status'];

if (count($user_data) > 0) {
	if ($status == 1) {
		$uld->resetUserDevice($user_id, $device_id, $player_id);
		$token_logObj->UpdateTokenLog($updateToken[0]['token_id']);
		api::success($data, 1, "You are logout successfully.");

	} else {

		## Mail
		if ($status == 0) {
			$error_msg = message::$inactive_mainuser_error;
		} else if ($status == 2) {
			$error_msg = message::$delete_mainuser_error;
		} else if ($status == 3) {
			$error_msg = message::$block_mainuser_error;
		} else if ($status == 4) {
			$error_msg = message::$unverified_mainuser_error;
		} else {
			$error_msg = message::$mainuser_error;
		}

		/*$email_template->sign_up($email, $name, $userid);*/
		api::error(9, $error_msg);
	}
} else {
	api::error(9, "Unauthorized access. Please contact your admin.");
}