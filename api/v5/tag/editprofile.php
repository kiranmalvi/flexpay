<?php
/**
 * @api {get} /v2/?tag=editprofile Request profile update
 * @apiName editprofile
 * @apiVersion 1.0.0
 * @apiGroup user
 *
 * @apiParam {String} tag Tag for api.
 * @apiParam {String} name   give user name
 * @apiParam {integer} user_id   give login user id
 * @apiParam {String} mobile   give user mobile
 * @apiParam {String} company <strong>(optional)</strong>  give user company name
 * @apiSuccess {Object} data Response data.
 * @apiSuccess {String} message Message.
 * @apiSuccess {Number} status Status.
 *
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 200 OK
 *{
 * data: {
 *          id: "1",
 *          name: "dhruvi",
 *          email: "dhruvika.mi@gmail.com",
 *          mobile: "9874561230",
 *          company: "mi"
 *          },
 * message: "profile update",
 * status: 1
 * }
 *
 */

use MI\API\Response as response;
use MI\GEN as api;

$_SERVER['REQUEST_METHOD'] == "POST" or api::error(0, \MI\API\Message::$invalidMethod);
//$email = isset($_REQUEST['email']) && $_REQUEST['email'] != '' ? $_REQUEST['email'] : api::error(0, \MI\API\Message::$invalidemail);
$name = isset($_REQUEST['name']) && $_REQUEST['name'] != '' ? mysqli_real_escape_string($obj->CONN, $_REQUEST['name']) : api::error(0, \MI\API\Message::$invalidename);
//$mobile = isset($_REQUEST['mobile']) && preg_match('/^[0-9]{10}$/', $_REQUEST['mobile']) && $_REQUEST['mobile'] != '' ? $_REQUEST['mobile'] : api::error(0, \MI\API\Message::$invalidmobileno);
$mobile = isset($_REQUEST['mobile']) && $_REQUEST['mobile'] != '' ? $_REQUEST['mobile'] : api::error(0, \MI\API\Message::$invalidmobileno);
$userid = isset($_REQUEST['user_id']) && $_REQUEST['user_id'] != '' ? $_REQUEST['user_id'] : api::error(0, \MI\API\Message::$invalideuser_id);
$company = (isset($_REQUEST['company'])) ? mysqli_real_escape_string($obj->CONN, $_REQUEST['company']) : null;
$user = new  user();
//$user_info = new user_info();
$userdata = $user->select($userid);
$gmt = $generalfuncobj->gm_date();
if (count($userdata) > 0) {
	if ($company == '') {
		$company = $userdata[0]['company_name'];
	}
	$user->profile_update($userid, $name, $mobile, $company, $gmt);
	$editdetails = $user->select($userid);
	$data = new MI\API\Response\V1\ProfileResponse();
	$data->id = $editdetails[0]['id'];
	$data->name = $editdetails[0]['name'];
	$data->mobile = $editdetails[0]['mobile'];
	$data->company = $editdetails[0]['company_name'];
	api::success($data, 1, "profile update");
} else {
	api::error(0, "user not register");
}

