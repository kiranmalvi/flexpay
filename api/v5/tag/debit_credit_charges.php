<?php
/**
 * @api {get} /v2/?tag=debit_credit_charges Request Credit / debit charges information
 * @apiName Get credit debit
 * @apiBase http://localhost/
 * @apiVersion 1.0.0
 * @apiGroup user
 *
 * @apiParam {String} tag Tag for api.
 * @apiParam {Number} id (Required) login user id
 *
 * @apiSuccess {array} data Response data.
 * @apiSuccess {String} message Message.
 * @apiSuccess {Number} status Status.
 *
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 200 OK
 *  {
 *      "data": [
 *          {
 *              user_transaction_rate_id: 3,
 *              user_id: 3,
 *              debit_rate: 3,
 *              credit_rate: 3
 *          }
 *      ],
 *      "status": 1,
 *      "message": "Transaction rates for user."
 *  }
 *
 */

use MI\API\Response as response;
use MI\GEN as api;

$user_id = isset($_REQUEST['id']) && $_REQUEST['id'] != '' ? $_REQUEST['id'] : api::error(0, \MI\API\Message::$invalideuser_id);

$user = new user();
$ut = new user_transaction_rate();

$user_data = $user->select($user_id);

if (count($user_data) > 0) {

	$ut_data = $ut->user_transaction_data($user_id);
	if (count($ut_data) > 0) {
		$data = $ut_data;
		api::success($data, 1, "Transaction rates for user.");
	} else {
		api::error(0, "No rate available for this user.");
	}

} else {
	api::error(0, \MI\API\Message::$invalide_user);
}