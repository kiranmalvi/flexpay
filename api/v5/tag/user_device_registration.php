<?php
/**
 * @api {get} /v3/?tag=user_device_registration Register User device and player id for notification.
 * @apiName user device registration
 * @apiVersion 1.0.0
 * @apiGroup user
 *
 * @apiParam {String} tag Tag for api.
 * @apiParam {String} user_id    give user id (required)
 * @apiParam {String} player_id    give player id (optional)
 * @apiParam {String} device_id    give device id (required)
 * @apiSuccess null data
 * @apiSuccess {String} message Message.
 * @apiSuccess {Number} status Status.
 *
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 200 OK
 * {
 *      data: null,
 *        message: "User device registered successfully.",
 *        status: 1
 * }
 *
 */

use MI\API\Response as response;
use MI\GEN as api;
use MI\API\Message as message;

//$_SERVER['REQUEST_METHOD'] == "POST" or api::error(0, \MI\API\Message::$invalidMethod);
$user_id = isset($_REQUEST['user_id']) && $_REQUEST['user_id'] != '' ? $_REQUEST['user_id'] : api::error(0, \MI\API\Message::$invalid_user_id);
$player_id = isset($_REQUEST['player_id']) && $_REQUEST['player_id'] != '' ? $_REQUEST['player_id'] : null;
$device_id = isset($_REQUEST['device_id']) ? $_REQUEST['device_id'] : api::error(0, \MI\API\Message::$Invalid_device_info);

$user = new  user();
$uld = new user_login_devices();

$gmt = $generalfuncobj->gm_date();

$user_data = $user->select($user_id);
$status = $user_data[0]['status'];
//pr(count($user_data));
//pr($status);exit;
if (count($user_data) > 0) {
	if ($status == 1) {
		$check = $uld->checkUserDevice($device_id);
		if ($check == 0) {
			$uld->setdevice_id($device_id);
			$uld->setuser_id($user_id);
			$uld->setplayer_id($player_id);
			$uld->setadded_at($gmt);
			$uld->insert();
		} else {
			$uld->registerUserDevice($check, $player_id, $user_id);
		}
		api::success($data, 1, "Your device registered successfully.");
	} else {

		## Mail
		if ($status == 0) {
			$error_msg = message::$inactive_mainuser_error;
		} else if ($status == 2) {
			$error_msg = message::$delete_mainuser_error;
		} else if ($status == 3) {
			$error_msg = message::$block_mainuser_error;
		} else if ($status == 4) {
			$error_msg = message::$unverified_mainuser_error;
		} else {
			$error_msg = message::$mainuser_error;
		}

		/*$email_template->sign_up($email, $name, $userid);*/
		api::error(0, $error_msg);
	}
} else {
	api::error(0, "Unauthorized access. Please contact your admin.");
}