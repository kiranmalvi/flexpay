<?php
/**
 * @api {get} /v2/?tag=list_biller Request for user biller's list
 * @apiName User Billers List
 * @apiVersion 1.0.0
 * @apiGroup Billers
 *
 * @apiParam {String} tag Tag for api.
 * @apiParam {integer} user_id give login user id
 * @apiSuccess {array} data Response data.
 * @apiSuccess {String} message Message.
 * @apiSuccess {Number} status Status.
 *
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 200 OK
 * {
 *      data:
 *      [
 *          {
 *              user_biller_id: 1,
 *              user_id: 9,
 *              biller_id: 1,
 *              biller_name: "John",
 *              biller_email: "john@xyz.com",
 *              account: "XYZ123456",
 *              comment: "XYZ is equal to zyx",
 *              status: 1,
 *              added_at: "2016-06-01 10:00:00",
 *              updated_at: "2016-06-01 10:00:00"
 *          },
 *            {....},
 *               .
 *               .
 *               .
 *            {....}
 *      ],
 *      message: "notification data list",
 *      status: 1
 * }
 *
 */


use MI\API\Response as response;
use MI\GEN as api;

//$_SERVER['REQUEST_METHOD'] == "POST" or api::error(0, \MI\API\Message::$invalidMethod);
$userid = isset($_REQUEST['user_id']) && $_REQUEST['user_id'] != '' ? $_REQUEST['user_id'] : api::error(0, \MI\API\Message::$invalideuser_id);
$timestamp = isset($_REQUEST['timestamp']) && $_REQUEST['timestamp'] != '' ? $_REQUEST['timestamp'] : 0;

$gmt = $generalfuncobj->gm_date();
$user = new  user();
$user_billers = new  user_billers();

$userdata = $user_billers->allActiveBillers($userid, $timestamp);

if (count($userdata) > 0) {
	$data = $userdata;

	$new_offset = min(($offset + $limit), ($offset + count($data)));
	if ($offset == $new_offset) {
		$new_offset = 0;
	}

	if (count($data) > 0) {
		api::success($data, 1, "All biller's list", $new_offset, $gmt);
	} else {
		api::success([], 1, "No biller's available", $new_offset, $gmt);
	}

} else {
	api::success($userdata, 1, "No biller's available");
}
