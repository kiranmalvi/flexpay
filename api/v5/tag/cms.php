<?php
/**
 * @api {get} /v2/?tag=cms Request CMS information
 * @apiName GetCMS
 * @apiBase http://http://localhost/
 * @apiVersion 1.0.0
 * @apiGroup CMS
 *
 * @apiParam {String} tag Tag for api.
 *
 * @apiSuccess {array} data Response data.
 * @apiSuccess {String} message Message.
 * @apiSuccess {Number} status Status.
 *
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 200 OK
 *  {
 *      "data": [
 *          {
 *              id: 3,
 *              type: "terms_condition",
 *              data: "Contrary to popular belief, Lorem Ipsum is not simply random text..."
 *          },
 *          {
 *              id: 2,
 *              type: "terms_condition",
 *              data: "Contrary to popular belief, Lorem Ipsum is not simply random text..."
 *          }
 *      ],
 *      "status": 1,
 *      "message": ""
 *  }
 *
 */

use MI\API\Response as response;
use MI\GEN as api;

$_SERVER['REQUEST_METHOD'] == "GET" or api::error(0, \MI\API\Message::$invalidMethod);
//$timestamp = isset($_GET['OTP']) && is_string($_GET['OTP']) && $_GET['OTP'] != '' ? $_GET['OTP'] : api::error(0, \MI\API\Message::$invalidotpcode);

//$options = ['timestamp' => $timestamp];

$cms = new cms();

$data = $cms->select();


api::success($data, 1, 'About us detail.');