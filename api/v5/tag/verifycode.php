<?php
/**
 * @api {get} /v2/?tag=verifycode Request Code Verification
 * @apiName Code Verification for both Forgot password and Forgot pin
 * @apiVersion 1.0.0
 * @apiGroup user
 *
 * @apiParam {String} tag Tag for api.
 * @apiParam {String} email   give email
 * @apiParam {String} verification_code   give verification code
 * @apiSuccess {array} data Response data.
 * @apiSuccess {String} message Message.
 * @apiSuccess {Number} status Status.
 *
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 200 OK
 * {
 *      data: [],
 *      message: "Verification completed successfully.",
 *       status: 1
 * }
 *
 */

use MI\API\Response as response;
use MI\GEN as api;

$email = isset($_REQUEST['email']) && $_REQUEST['email'] != '' ? $_REQUEST['email'] : api::error(0, \MI\API\Message::$invalidemail);
$verification_code = isset($_REQUEST['verification_code']) && $_REQUEST['verification_code'] != '' ? $_REQUEST['verification_code'] : api::error(0, \MI\API\Message::$invalidcode);

$user = new  user();
$userdata = $user->check_user($email);
$gmt = $generalfuncobj->gm_date();
//pr($userdata);exit;
if (count($userdata) > 0) {

	$user_status = $userdata[0]['status'];
	$user_id = $userdata[0]['id'];
	$user_name = $userdata[0]['name'];
	$user_email = $userdata[0]['email'];
	$user_code = $userdata[0]['verification_code'];

	if ($user_status == 1) {
		if (strcmp($user_code, $verification_code) == 0) {

			$update_user = $user->update_code('', $user_id, $gmt);

			api::success([], 1, "Verification completed successfully.");
		} else {
			api::error(0, "Invalid verification code.");
		}

	} else {
		api::error(0, "Invalid email");
	}

} else {
	api::error(0, "user not register");
}

