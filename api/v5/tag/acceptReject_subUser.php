<?php

/**
 * @api {get} /v3/?tag=acceptReject_subUser Accept / Reject subUser's request
 * @apiName acceptReject_subUser
 * @apiBase http://localhost/
 * @apiVersion 1.0.0
 * @apiGroup sub user transactions
 *
 * @apiParam {String} tag    Tag for api.
 * @apiParam {String} type    Type '1 => accept','0 => reject'
 * @apiParam {String} security_pin    Login User's Security Pin (Required when type=1).
 * @apiParam {integer} receiver_id    for Customer User id who will receive money.
 * @apiParam {integer} transaction_id    transaction id by which refund request initiated.
 * @apiParam {integer} sender_id    Login user's user id from which money is deducted.
 * @apiParam {integer} id    Sub user's user id.
 * @apiParam {integer} device_id    Sub user's device id.
 * @apiParam {integer} amount    Money amount to transfer.
 *
 * @apiSuccess {array} data    Response data.
 * @apiSuccess {String} message    Message.
 * @apiSuccess {Number} status    Status.
 *
 * @apiSuccessExample Success-Response for Accept:
 *  HTTP/1.1 200 OK
 * {
 * data: [ ],
 * message: "Request accepted successfully.",
 * status: 1
 * }
 * @apiSuccessExample Success-Response for Reject:
 *  HTTP/1.1 200 OK
 * {
 * data: [ ],
 * message: "Request rejected successfully.",
 * status: 1
 * }
 *
 */
use MI\API\Response as response;
use MI\GEN as api;

## Validation
$type = isset($_REQUEST['type']) && $_REQUEST['type'] != '' ? $_REQUEST['type'] : api::error(0, \MI\API\Message::$invalid_request_money_type);
$sender_id = isset($_REQUEST['sender_id']) && $_REQUEST['sender_id'] != '' ? $_REQUEST['sender_id'] : api::error(0, \MI\API\Message::$invalid_request_money_sender_id);
$receiver_id = isset($_REQUEST['receiver_id']) && $_REQUEST['receiver_id'] != '' ? $_REQUEST['receiver_id'] : api::error(0, \MI\API\Message::$invalid_request_money_receiver_id);
$transaction_id = isset($_REQUEST['transaction_id']) && $_REQUEST['transaction_id'] != '' ? $_REQUEST['transaction_id'] : api::error(0, \MI\API\Message::$invalid_transaction_id);
$id = isset($_REQUEST['id']) && $_REQUEST['id'] != '' ? $_REQUEST['id'] : api::error(0, \MI\API\Message::$invalid_sub_user_id);
$device_id = isset($_REQUEST['device_id']) && $_REQUEST['device_id'] != '' ? $_REQUEST['device_id'] : api::error(0, \MI\API\Message::$invalid_device_id);
$amount = isset($_REQUEST['amount']) && $_REQUEST['amount'] != '' ? $_REQUEST['amount'] : api::error(0, \MI\API\Message::$invalid_request_money_amount);

if ($type == '1') {
	$securitypin = isset($_REQUEST['security_pin']) && $_REQUEST['security_pin'] != '' ? $_REQUEST['security_pin'] : api::error(0, \MI\API\Message::$invalid_sign_security_pin);
} else if ($type != '0') {
	api::error(0, \MI\API\Message::$invalid_request_money_type);
}

$user = new user();
$uld = new user_login_devices();
$tran = new transaction();
$noti = new notification();
$api = new \API();

$gm_date_only = $generalfuncobj->gm_date_only();
$gmt = $generalfuncobj->gm_date();
$spendLimitCheck = $user->check_spending_limit($sender_id, $gmt_date, $amount);
$spend_limit = $spendLimitCheck->todays_spend;
$limit = $spendLimitCheck->transfer_limit;
$remain_limit = round($limit - $spend_limit, 2);

## Check Sender
if ($type == 1) {
	$securitydata = $user->get_pin($securitypin, $sender_id);
} elseif ($type == 0) {
	$securitydata = $user->select($sender_id);
} else {
	$securitydata = '';
}

$userdata = $user->select($sender_id);
$receiver = $user->select($receiver_id);
$subUserdata = $user->select($id);

if (count($securitydata) > 0) {
	if ($type == 1) {
		if ($remain_limit >= $amount) {    //check user daily spend limit
			if (count($userdata) > 0 && $userdata[0]['status'] == 1) {
				if (count($receiver) > 0 && $receiver[0]['status'] == 1) {

					## Check wallet Amount
					$wallet = new wallet();
					$user_wallet = $wallet->check_user_amount($sender_id);

					## Get Transaction Rates
					$trmobj = new transaction_rate_map();
					$trans_rate = $trmobj->transaction_rates($transaction_id);
					$debit_rate = ($amount * $trans_rate['debit_rate']) / 100;
					$credit_rate = ($amount * $trans_rate['credit_rate']) / 100;
					$debit_amount = $amount + $debit_rate;
					$credit_amount = $amount - $credit_rate;

					if (count($user_wallet) > 0) {
						$user_wallet_amount = $user_wallet[0]['amount'];
						if ($user_wallet_amount >= $debit_amount) {

							$tran_data = $tran->select($transaction_id);
							if (count($tran_data) > 0) {
								if ($tran_data[0]['type'] == 'request') {

									## Update Sender wallet
									$wallet->deduct_amount($sender_id, $debit_amount);

									## Update Receiver wallet
									$wallet->add_amount($receiver_id, $credit_amount);

									##Update Transaction status
									$tran->update_transaction_record($transaction_id, 'debit', 'confirm', $gmt);

									##Update refunded main transaction status
									$refunded_transaction_id = $tran_data[0]['refunded_transaction_id'];
									$tran->updateIsRefunded('2', $refunded_transaction_id, $gmt);

									$noti_ID = $noti->get_notificationid($transaction_id, $receiver_id);
									$noti_IDsubUser = $noti->get_notificationid($transaction_id, $sender_id);
//									$api->notify($receiver_id, "You have receive $$amount from '{$userdata[0]['name']}'.", '3', $amount, $noti_ID);
//									$api->notify($id, "Refund request for $$amount from '{$userdata[0]['name']}' is accepted.", '3', $amount, $noti_ID);
									$api->notifyOneSignal($receiver_id, "You have receive $$amount from '{$userdata[0]['name']}'.", '3', $amount, $noti_ID);
									$api->notifyOneSignal($id, "Refund request for $$amount from '{$userdata[0]['name']}' is accepted.", '3', $amount, $noti_IDsubUser);

									api::success([], 1, "Request accepted successfully.");
								} else {
									api::error(0, "This request is already accepted / rejected.");
								}
							} else {
								api::error(0, \MI\API\Message::$invalid_transaction_id);
							}
						} else {
							api::error(0, \MI\API\Message::$invalid_request_money_wallet_error);
						}
					} else {
						api::error(0, \MI\API\Message::$invalid_request_money_wallet);
					}
				} else {
					api::error(0, \MI\API\Message::$invalid_request_money_email);
				}
			} else {
				api::error(0, \MI\API\Message::$invalid_request_money_sender_id);
			}
		} else {
			if ($remain_limit < 0) {
				api::error(0, \MI\API\Message::$spend_limit_exceeded);
			} else {
				api::error(0, \MI\API\Message::$spend_limit_remain . $remain_limit);
			}
		}
	} else {
		if (count($userdata) > 0 && $userdata[0]['status'] == 1) {

			##Get transaction data.
			$tran_data = $tran->select($transaction_id);
			if (count($tran_data) > 0) {
				if ($tran_data[0]['type'] == 'request') {

					## Update Transaction
					$tran->update_transaction_record($transaction_id, 'reject', 'cancel', $gmt);

					##Update refunded main transaction status
					$refunded_transaction_id = $tran_data[0]['refunded_transaction_id'];
					$tran->updateIsRefunded('0', $refunded_transaction_id, $gmt);

					$noti_ID = $noti->get_notificationid($transaction_id, $receiver_id);
					$noti_IDsubUser = $noti->get_notificationid($transaction_id, $sender_id);
//					$api->notify($receiver_id, "Your refund request for $$amount have been rejected by '{$userdata[0]['name']}'.", '2', $amount, $noti_ID);
//					$api->notify($id, "Your refund request for $$amount have been rejected by '{$userdata[0]['name']}'.", '2', $amount, $noti_ID);
					$api->notifyOneSignal($receiver_id, "Your refund request for $$amount have been rejected by '{$userdata[0]['name']}'.", '2', $amount, $noti_ID);
					$api->notifyOneSignal($id, "Your refund request for $$amount have been rejected by '{$userdata[0]['name']}'.", '2', $amount, $noti_IDsubUser);

					api::success([], 1, "Request rejected successfully");
				} else {
					api::success([], 1, "This request is already accepted / rejected.");
				}
			} else {
				api::error(0, \MI\API\Message::$invalid_transaction_id);
			}
		} else {
			api::error(0, \MI\API\Message::$invalide_user);
		}
	}
} else {
	api::error(0, \MI\API\Message::$invalid_pin_to_use);
}