<?php
/**
 * @api {get} /v4/?tag=maintainPlayerDeviceID Delete player & device IDs
 * @apiName Clear Player/Device IDs
 * @apiVersion 1.0.0
 * @apiGroup user
 *
 * @apiParam {String} tag Tag for api.
 * @apiParam {String} player_id    give player id (required)
 * @apiParam {String} device_id    give device id (required)
 * @apiSuccess null data
 * @apiSuccess {String} message Message.
 * @apiSuccess {Number} status Status.
 *
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 200 OK
 * {
 *      data: null,
 *        message: "Device & Player ID are clear successfully.",
 *        status: 1
 * }
 *
 */

use MI\API\Response as response;
use MI\GEN as api;
use MI\API\Message as message;

$player_id = isset($_REQUEST['player_id']) ? $_REQUEST['player_id'] : api::error(0, \MI\API\Message::$invalid_player_id);
$device_id = isset($_REQUEST['device_id']) ? $_REQUEST['device_id'] : api::error(0, \MI\API\Message::$Invalid_device_info);

$uld = new user_login_devices();
$uld->clearPlayerDeviceID($device_id, $player_id);

$data = new stdClass();

api::success($data, 1, "Device & Player ID are clear successfully.");
