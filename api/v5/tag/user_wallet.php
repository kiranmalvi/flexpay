<?php
/**
 * @api {get} /v2/?tag=user_wallet Request give user wallet amount
 * @apiName user_wallet
 * @apiVersion 1.0.0
 * @apiGroup user
 *
 * @apiParam {String} tag Tag for api.
 * @apiParam {Number} user_id give login user id
 * @apiSuccess {Object} data Response data.
 * @apiSuccess {String} message Message.
 * @apiSuccess {Number} status Status.
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 200 OK
 * {
 *      data: {
 *              wallet_amount: "0"
 *           },
 * message: "user wallet amount",
 * status: 1
 * }
 *
 */

use MI\API\Response as response;
use MI\GEN as api;

//$_SERVER['REQUEST_METHOD'] == "POST" or api::error(0, \MI\API\Message::$invalidMethod);
$userid = isset($_REQUEST['user_id']) && $_REQUEST['user_id'] != '' ? $_REQUEST['user_id'] : api::error(0, \MI\API\Message::$invalideuser_id);
$user = new  user();
$utr = new  user_transaction_rate();
$user_info = new user_info();
$wallet = new wallet();

$gmt_date = $generalfuncobj->gm_date_only();


$userdata = $user->select($userid);
$gmt = $generalfuncobj->gm_date();
if (count($userdata) > 0) {
	//get wallet balance
	$wallet_amount = $wallet->userwallet($userid);
	$amount = $wallet_amount[0]['amount'];
	if ($amount == null) {
		$amount = '0';
	}

	//get remain spend limit
	$spendLimitCheck = $user->check_spending_limit($userid, $gmt_date, 0);
	$spend_limit = $spendLimitCheck->todays_spend;
	$limit = $spendLimitCheck->transfer_limit;
	$remain_limit = round($limit - $spend_limit, 2);

	//get debit & credit rate
	$rates = $utr->user_transaction_data($userid);
	$debitrate = $rates[0]['debit_rate'];
	$creditrate = $rates[0]['credit_rate'];

	//set response data
	$data->wallet_amount = $amount;
	$data->daily_limit = $limit;
	$data->remain_limit = ($remain_limit < 0) ? 0.00 : $remain_limit;
	$data->debit_rate = $debitrate;
	$data->credit_rate = $creditrate;

	api::success($data, 1, "user wallet amount");
} else {
	api::error(0, "user not register");
}

