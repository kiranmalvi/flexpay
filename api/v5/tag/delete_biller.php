<?php
/**
 * @api {get} /v2/?tag=delete_biller Request for delete biller
 * @apiName delete_biller
 * @apiBase http://localhost/
 * @apiVersion 1.0.0
 * @apiGroup Billers
 *
 * @apiParam {String} tag Tag for api.
 * @apiParam {integer} user_biller_id for user_biller_id
 * @apiParam {integer} user_id for user_id (login user id)
 *
 * @apiSuccessExample Success-Response for Accept:
 *  HTTP/1.1 200 OK
 * {
 * data: {},
 * message: "Biller deleted successfully.",
 * status: 1
 * }
 * @apiSuccessExample Success-Response for Reject:
 *  HTTP/1.1 200 OK
 * {
 * data: [ ],
 * message: "Biller not deleted. Please try again later.",
 * status: 1
 * }
 *
 */
use MI\API\Response as response;
use MI\GEN as api;
use MI\API\Message as msg;

## Validation
$user_biller_id = isset($_REQUEST['user_biller_id']) && $_REQUEST['user_biller_id'] != '' ? $_REQUEST['user_biller_id'] : api::error(0, msg::$invalid_request_user_biller_id);

$gmt_date = $generalfuncobj->gm_date();
$user_biller = new user_billers();
if (count($user_biller->select($user_biller_id)) > 0) {
	$user_biller->setstatus('2');
	$user_biller->setupdated_at($gmt_date);
	$user_biller->deleteBiller($user_biller_id);
	api::success(array(), 1, msg::$success_delete_biller);
} else {
	api::error(0, msg::$invalid_request_user_biller_id);
}
