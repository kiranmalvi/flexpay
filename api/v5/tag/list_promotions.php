<?php
/**
 * @api {get} /v4/?tag=list_promotions Request for all promotions
 * @apiName Promotions List
 * @apiVersion 1.0.0
 * @apiGroup Promotions
 *
 * @apiParam {String} tag Tag for api.
 * @apiParam {number} offset (optional) Offset for api.
 * @apiParam {number} limit (optional) limit for api.
 * @apiParam {string} timestamp (optional) timestamp for api.
 * @apiParam {string} search (optional) search promotions .
 * @apiSuccess {array} data Response data.
 * @apiSuccess {String} message Message.
 * @apiSuccess {Number} status Status.
 *
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 200 OK
 * {
 *      data:
 *      [
 *          {
 *           ...
 *          },
 *            {....},
 *               .
 *               .
 *               .
 *            {....}
 *      ],
 *      message: "Promotions list",
 *      status: 1
 * }
 *
 */


use MI\API\Response as response;
use MI\GEN as api;

//$_SERVER['REQUEST_METHOD'] == "POST" or api::error(0, \MI\API\Message::$invalidMethod);
$userid = isset($_REQUEST['user_id']) && $_REQUEST['user_id'] != '' ? $_REQUEST['user_id'] : api::error(0, \MI\API\Message::$invalideuser_id);
$timestamp = isset($_REQUEST['timestamp']) && $_REQUEST['timestamp'] != '' ? $_REQUEST['timestamp'] : 0;
$search = isset($_REQUEST['search']) && $_REQUEST['search'] != '' ? $_REQUEST['search'] : '';
$gmt = $generalfuncobj->gm_date();
$promotions = new promotions();


$promotionsData = $promotions->promotionList($timestamp, $search);
//pr($promotionsData);exit;
if (count($promotionsData) > 0) {
	for ($i = 0; $i < count($promotionsData); $i++) {
		$promotionsData[$i]['description'] = 'Minimum transaction of $' . $promotionsData[$i]['transaction_limit'] . ' is required to avail the cashback offer. Maximum cashback is $' . $promotionsData[$i]['cashback'] . ' per transaction. ' . $promotionsData[$i]['description'];
	}
	$data = $promotionsData;

	$new_offset = min(($offset + $limit), ($offset + count($data)));
	if ($offset == $new_offset) {
		$new_offset = 0;
	}
	if (count($data) > 0) {
		api::success($data, 1, "Promotions list", $new_offset, $gmt);
	} else {
		api::success([], 1, "No promotions available", $new_offset, $gmt);
	}
} else {
	api::success([], 1, "No promotions available");
}
