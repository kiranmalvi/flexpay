<?php
/**
 * @api {get} /v2/?tag=admin_details Request admin details
 * @apiName GetCMS
 * @apiBase http://http://localhost/
 * @apiVersion 1.0.0
 * @apiGroup CMS
 *
 * @apiParam {String} tag Tag for api.
 * @apiParam {Number} user_id login user id
 *
 * @apiSuccess {array} data Response data.
 * @apiSuccess {String} message Message.
 * @apiSuccess {Number} status Status.
 *
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 200 OK
 * {
 * data: [
 *          {
 *              id: "2",
 *              accountnumber: "1323f",
 *              bankname: "icici",
 *              isfc_code: "654",
 *              bankaddress: "102,smita tower",
 *              name_of_beneficiary: "35",
 *              address_of_beneficiary: "memnagr",
 *              mobile_number: "963264848"
 *          },
 *          {
 *               id: "1",
 *              accountnumber: "6565as",
 *              bankname: "hdfc",
 *              isfc_code: "654",
 *              bankaddress: "102,smita tower",
 *              name_of_beneficiary: "35",
 *              address_of_beneficiary: "memnagr",
 *              mobile_number: "963264848"
 *           }
 *      ],
 * message: "admin details",
 * status: 1
 * }
 *
 */

use MI\API\Response as response;
use MI\GEN as api;


$userid = isset($_REQUEST['user_id']) && $_REQUEST['user_id'] != '' ? $_REQUEST['user_id'] : api::error(0, \MI\API\Message::$invalideuser_id);

$user = new  user();
$accountdetails = new  account_setting();
//$user_info = new user_info();
$userdata = $user->select($userid);
$data = array();
if (count($userdata) > 0) {
	$accountlist = $accountdetails->select_data();
//    pr($accountlist);exit;

	foreach ($accountlist AS $account_details) {
		$admindetails = new MI\API\Response\V1\AccountReaponse();
		$admindetails->id = $account_details['account_setting_id'];
		$admindetails->accountnumber = $account_details['account_number'];
		$admindetails->bankname = $account_details['bank_name'];
		$admindetails->isfc_code = $account_details['isfc_code'];
		$admindetails->bankaddress = $account_details['bank_address'];
		$admindetails->name_of_beneficiary = $account_details['name_of_beneficiary'];
		$admindetails->address_of_beneficiary = $account_details['address_of_beneficiary'];
		$admindetails->mobile_number = $account_details['mobile_number'];
		$data[] = $admindetails;
	}

	api::success($data, 1, "admin details");


} else {
	api::error(0, "user not register");
}