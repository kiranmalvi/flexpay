<?php
/**
 * @api {get} /v5/?tag=add_money Request for add money
 * @apiName add_money
 * @apiBase http://localhost/
 * @apiVersion 1.0.0
 * @apiGroup transaction
 *
 * @apiParam {String} tag Tag for api.
 * @apiParam {String} paypal_id(required) for paypal payment id
 * @apiParam {String} amount(required) for amount to add
 * @apiParam {String} currency_code or payment amount currency
 * @apiParam {String} short_description for description during payment
 * @apiParam {String} processable for processable status
 * @apiParam {String} environment for type of payment Environment
 * @apiParam {String} paypal_sdk_version for Paypal SDK version in APP
 * @apiParam {String} platform for from which platform process is occurred
 * @apiParam {String} product_name for payment product name
 * @apiParam {String} create_time for paypal transaction time
 * @apiParam {String} intent for intent type
 * @apiParam {String} state(required) for paypal transaction status
 * @apiParam {String} response_type for paypal transaction response type
 *
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 200 OK
 * {
 * data: [ ],
 * message: "PayPal money added to your wallet successfully",
 * status: 1
 * }
 * @apiFailureExample Failure-Response:
 *  HTTP/1.1 200 OK
 * {
 * data: [ ],
 * message: "There is some problem to add money from PayPal. Please try later.",
 * status: 2
 * }
 *
 */
use MI\API\Response as response;
use MI\GEN as api;
use MI\API\Message as msg;

## Validation
$user_id = isset($_REQUEST['user_id']) && $_REQUEST['user_id'] != '' ? $_REQUEST['user_id'] : api::error(0, msg::$invalid_request_user_id);
$paypal_id = isset($_REQUEST['paypal_id']) && $_REQUEST['paypal_id'] != '' ? $_REQUEST['paypal_id'] : api::error(0, msg::$paypal_payment_id_required);
$amount = isset($_REQUEST['amount']) && $_REQUEST['amount'] != '' ? $_REQUEST['amount'] : api::error(0, msg::$paypal_payment_amount_required);
$state = isset($_REQUEST['state']) && $_REQUEST['state'] != '' ? $_REQUEST['state'] : api::error(0, msg::$paypal_payment_state_required);

$currency_code = isset($_REQUEST['currency_code']) && $_REQUEST['currency_code'] != '' ? $_REQUEST['currency_code'] : "";
$short_description = isset($_REQUEST['short_description']) && $_REQUEST['short_description'] != '' ? $_REQUEST['short_description'] : "";
$processable = isset($_REQUEST['processable']) && $_REQUEST['processable'] != '' ? $_REQUEST['processable'] : "";
$environment = isset($_REQUEST['environment']) && $_REQUEST['environment'] != '' ? $_REQUEST['environment'] : "";
$paypal_sdk_version = isset($_REQUEST['paypal_sdk_version']) && $_REQUEST['paypal_sdk_version'] != '' ? $_REQUEST['paypal_sdk_version'] : "";
$platform = isset($_REQUEST['platform']) && $_REQUEST['platform'] != '' ? $_REQUEST['platform'] : "";
$product_name = isset($_REQUEST['product_name']) && $_REQUEST['product_name'] != '' ? $_REQUEST['product_name'] : "";
$create_time = isset($_REQUEST['create_time']) && $_REQUEST['create_time'] != '' ? $_REQUEST['create_time'] : "";
$intent = isset($_REQUEST['intent']) && $_REQUEST['intent'] != '' ? $_REQUEST['intent'] : "";
$response_type = isset($_REQUEST['response_type']) && $_REQUEST['response_type'] != '' ? $_REQUEST['response_type'] : "";

$user = new user();
$api = new \API();

$userData = $user->select($user_id);
$gmt_date = $generalfuncobj->gm_date();

if (count($userData) > 0 && $userData[0]['status'] == '1') {
	$ptm = new paypal_transaction_map();

	if (!$ptm->check_paypal_payment_id($paypal_id)) {

		$live_state = array();
		$accessToken = "";
		$accessToken = $ptm->getPaypalToken();//get live access-token

		if (!empty($accessToken)) {

			$live_state = $ptm->getPaypalPaymentDetail($accessToken, $paypal_id);// get payment detail from live

			if ($live_state['state'] == 'approved') {

				$id = $live_state['id'];

				$final = $ptm->getPaypalTransactionDetail($accessToken, $id);

				$finalAmount = $final['finalAmount'];
				$amount = $final['amount'];
				$creditRate = $final['chargeRate'];

				## Update user wallet
				$wallet = new wallet();
				$wallet->add_amount($user_id, $finalAmount);

				## Transaction
				$transaction = new transaction();

				//check transaction_id is used
				$transaction_unique_id = $generalfuncobj->generateNumericUniqueToken(10);
				while ($transaction->checkUniqueCode($transaction_unique_id)) {
					$transaction_unique_id = $generalfuncobj->generateNumericUniqueToken(10);
				}

				$transaction->settransaction_unique_id($transaction_unique_id);
				$transaction->setrefunded_transaction_id(0);
				$transaction->setfrom_user_id($user_id);
				$transaction->setto_user_id(0);
				$transaction->setvia_user_id(0);
				$transaction->setamount($amount);
				$transaction->settype('debit');
				$transaction->setcomment($short_description);
				$transaction->settransaction_status('confirm');
				$transaction->setrefunded(0);
				$transaction->setadded_at($generalfuncobj->gm_date());
				$transaction->setupdated_at($generalfuncobj->gm_date());
				$transaction->setstatus('1');
				$transaction->settransaction_type('4');
				$transaction->setpromotion_id(0);

				$transaction_id = $transaction->insert();

				## Paypal payment details
				$ptm->settransaction_id($transaction_id);
				$ptm->setpaypal_id($paypal_id);
				$ptm->setcurrency_code($currency_code);
				$ptm->setprocessable($processable);
				$ptm->setenvironment($environment);
				$ptm->setpaypal_sdk_version($paypal_sdk_version);
				$ptm->setplatform($platform);
				$ptm->setproduct_name($product_name);
				$ptm->setcreate_time($create_time);
				$ptm->setintent($intent);
				$ptm->setstate($state);
				$ptm->setresponse_type($response_type);
				$ptm->setstatus('1');
				$ptm->setadded_at($generalfuncobj->gm_date());
				$ptm->setupdated_at($generalfuncobj->gm_date());

				$ptm_id = $ptm->insert();

				## Transaction rates
				$trmobj = new transaction_rate_map();
				$trmobj->settransaction_id($transaction_id);
				$trmobj->setdebit_rate(0.00);
				$trmobj->setcredit_rate($creditRate);

				$trmobj->insert();

				## Receiver Notification
				$notification = new notification();
				$notification->setuser_id($user_id);
				$notification->seteRead('0');
				$notification->settransaction_id($transaction_id);
				$notification->setadded_at($generalfuncobj->gm_date());
				$notification->setstatus('1');

				$noti_ID = $notification->insert();

				$message = "You have successfully added $" . $amount . " in your Flex pay wallet.";
//         $api->notify($user_id, $message, '3', $amount, $noti_ID);
				$api->notifyOneSignal($user_id, $message, '3', $amount, $noti_ID);

				api::success([], 1, $message);

			} else { //if transaction is not approved
				$amount = ($live_state['amount']) ? $live_state['amount'] : $amount;
				## Transaction
				$transaction = new transaction();

				//check transaction_id is used
				$transaction_unique_id = $generalfuncobj->generateNumericUniqueToken(10);
				while ($transaction->checkUniqueCode($transaction_unique_id)) {
					$transaction_unique_id = $generalfuncobj->generateNumericUniqueToken(10);
				}

				$transaction->settransaction_unique_id($transaction_unique_id);
				$transaction->setrefunded_transaction_id(0);
				$transaction->setfrom_user_id($user_id);
				$transaction->setto_user_id(0);
				$transaction->setvia_user_id(0);
				$transaction->setamount($amount);
				$transaction->settype('debit');
				$transaction->setcomment($short_description);
				$transaction->settransaction_status('pending');
				$transaction->setrefunded(0);
				$transaction->setadded_at($generalfuncobj->gm_date());
				$transaction->setupdated_at($generalfuncobj->gm_date());
				$transaction->setstatus('1');
				$transaction->settransaction_type('4');
				$transaction->setpromotion_id(0);

				$transaction_id = $transaction->insert();

				## Paypal payment details
				$ptm->settransaction_id($transaction_id);
				$ptm->setpaypal_id($paypal_id);
				$ptm->setcurrency_code($currency_code);
				$ptm->setprocessable($processable);
				$ptm->setenvironment($environment);
				$ptm->setpaypal_sdk_version($paypal_sdk_version);
				$ptm->setplatform($platform);
				$ptm->setproduct_name($product_name);
				$ptm->setcreate_time($create_time);
				$ptm->setintent($intent);
				$ptm->setstate($state);
				$ptm->setresponse_type($response_type);
				$ptm->setstatus('0');
				$ptm->setadded_at($generalfuncobj->gm_date());
				$ptm->setupdated_at($generalfuncobj->gm_date());

				$ptm_id = $ptm->insert();

				## Transaction rates
				$trmobj = new transaction_rate_map();
				$trmobj->settransaction_id($transaction_id);
				$trmobj->setdebit_rate(0.00);
				$trmobj->setcredit_rate(0.00);

				$trmobj->insert();

				api::success([], 2, msg::$paypal_payment_not_approved);
			}
		} else {
			api::success([], 2, msg::$paypal_payment_access_token);
		}
	} else {
		api::success([], 1, msg::$paypal_payment_already_added);
	}
} else {
	api::error(0, msg::$invalid_request_user_id);
}


