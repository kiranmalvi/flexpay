<?php
/**
 * @api {get} /v3/?tag=list_subUser Request for Main user's sub user list
 * @apiName Sub user's List
 * @apiVersion 1.0.0
 * @apiGroup sub user manage
 *
 * @apiParam {String} tag Tag for api.
 * @apiParam {integer} user_id give Main user user_id
 * @apiSuccess {array} data Response data.
 * @apiSuccess {String} message Message.
 * @apiSuccess {Number} status Status.
 *
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 200 OK
 * {
 *      data:
 *      [
 *          {
 *              "id": "194",
 *                "name": "Joseph123",
 *                "employee_id": "Flex_123",
 *                "description": "Test",
 *                "parent_user_id": "191",
 *                "unique_token": "0fByN6zS"
 *          },
 *            {....},
 *               .
 *               .
 *               .
 *            {....}
 *      ],
 *      message: "Sub users list",
 *      status: 1
 * }
 *
 */


use MI\API\Response as response;
use MI\GEN as api;

//$_SERVER['REQUEST_METHOD'] == "POST" or api::error(0, \MI\API\Message::$invalidMethod);
$userid = isset($_REQUEST['user_id']) && $_REQUEST['user_id'] != '' ? $_REQUEST['user_id'] : api::error(0, \MI\API\Message::$invalideuser_id);
$timestamp = isset($_REQUEST['timestamp']) && $_REQUEST['timestamp'] != '' ? $_REQUEST['timestamp'] : 0;
$gmt = $generalfuncobj->gm_date();
$user = new  user();


$userdata = $user->selectAllActiveSubusers($userid, $timestamp);

if (count($userdata) > 0) {
	$data = $userdata;

	$new_offset = min(($offset + $limit), ($offset + count($data)));
	if ($offset == $new_offset) {
		$new_offset = 0;
	}
	if (count($data) > 0) {
		api::success($data, 1, "Sub users list", $new_offset, $gmt);
	} else {
		api::success([], 1, "No Sub users available", $new_offset, $gmt);
	}
} else {
	api::success($userdata, 1, "No Sub users available");
}
