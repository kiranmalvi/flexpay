<?php
include_once("../../miAutoload.php");

use MI\API\Message as message;
use MI\GEN as api;
use Respect\Validation\Validator as v;

if (json_decode(file_get_contents('php://input'), true)) {
	$_REQUEST = json_decode(file_get_contents('php://input'), true);
}
//pr($_REQUEST);exit;
$validator = v::create();
$validator->key('tag', v::string()->noWhitespace()->length(1, 50)->notEmpty())->validate($_REQUEST) or api::error(0, message::$invalidTag);
//pr($_REQUEST);exit;
unset($validator);

$offset = isset($_REQUEST['offset']) ? $_REQUEST['offset'] != '' ? is_numeric($_REQUEST['offset']) ? $_REQUEST['offset'] : 0 : 0 : 0;
$limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] != '' ? is_numeric($_REQUEST['limit']) ? $_REQUEST['limit'] : 20 : 20 : 20;

$request_header = getallheaders();
//pr($request_header);exit;
$remoteAuthToken = "dee7e043a29f6442aa4da5490eaa28cb58f67259646dbd7a8a5b8e41d49b70aa3838573546a6df0f420ef4df425894e5";
$clientAuthToken = $request_header['Access-Token'];
$tag = $_REQUEST['tag'];
$file = "tag/" . $tag . '.php';
//pr($_SERVER);exit;

$userObj = new user();
$token_logObj = new token_log();
$tokenStatus = $generalfuncobj->validateJWTToken($clientAuthToken);

$userAgent = $_SERVER['HTTP_USER_AGENT'];

if (!is_file($file)) {
	api::error(0, message::$invalidTag);
} else {
	if ($_SERVER['SERVER_PROTOCOL'] == 'HTTP/1.1') {
//		if ($clientAuthToken === $remoteAuthToken) {
		/** @noinspection PhpIncludeInspection */
		if (!in_array($tag, $CHECK_SESSION_API)) {

			// user_id in request
			if ($tag == "changepin" || $tag == "changepassword" || $tag == "editprofile" || $tag == "notificationdetails" || $tag == "notificationlist" || $tag == "receiver_details" || $tag == "user_wallet" || $tag == "add_biller" || $tag == "edit_biller" || $tag == "list_biller" || $tag == "delete_biller" || $tag == "add_subUser" || $tag == "edit_subUser" || $tag == "list_subUser" || $tag == "reset_subUserPassword" || $tag == "delete_subUser" || $tag == "logout" || $tag == "admin_details" || $tag == "list_promotions" || $tag == "updateTimezone" || $tag == "add_money") {
				if ($tokenStatus['tokenstatus'] == 1) {
					if (isset($tokenStatus['user_id']) && $tokenStatus['user_id'] != '') {
						$_REQUEST['user_id'] = $tokenStatus['user_id'];
						$user_id = $_REQUEST['user_id'];
						$user_data = $userObj->select($user_id);
						$user_status = $user_data[0]['status'];

						# Token expired
						$updateToken = $token_logObj->getUpdateTokenLog($tokenStatus['user_id'], $clientAuthToken);
						$updateTokenLog = strtotime($updateToken[0]['updated_at']);
						$currentTime = strtotime(gmdate('YmdHis'));
						$updateTimestamp = $currentTime - $updateTokenLog;
						if ($updateTimestamp > 600) {
							$token_logObj->UpdateTokenLog($updateToken[0]['token_id']);
							\MI\GEN::error(9, "Your current session has been expired");
						}
						# pr($_REQUEST);exit;
						if ($user_status != 1) {
							if ($user_status == 0) {
								$error_msg = message::$inactive_user_error;
							} else if ($user_status == 2) {
								$error_msg = message::$delete_user_error;
							} else if ($user_status == 3) {
								$error_msg = message::$block_user_error;
							} else if ($user_status == 4) {
								$error_msg = message::$unverified_user_error;
							} else {
								$error_msg = message::$user_error;
							}
							#pr($user_data);exit;
							api::error(10, $error_msg);
						} else {
							$token_logObj->token_log_apicall($tokenStatus['user_id'], $clientAuthToken);
							include_once($file);
						}
					} else {
						include_once($file);
					}
				} else {
					\MI\GEN::error(9, $tokenStatus['error_msg']);
				}
			} // sender_id as user_id in request
			else if ($tag == "accept_reject" || $tag == "cancel_money_request" || $tag == "request_money" || $tag == "send_money" || $tag == "acceptReject_subUser") {
				if ($tokenStatus['tokenstatus'] == 1) {
					if (isset($tokenStatus['user_id']) && $tokenStatus['user_id'] != '') {
						$_REQUEST['sender_id'] = $tokenStatus['user_id'];
						$sender_id = $_REQUEST['sender_id'];
						$user_data = $userObj->select($sender_id);
						$user_status = $user_data[0]['status'];

						# Token expired
						$updateToken = $token_logObj->getUpdateTokenLog($tokenStatus['user_id'], $clientAuthToken);
						$updateTokenLog = strtotime($updateToken[0]['updated_at']);
						$currentTime = strtotime(gmdate('YmdHis'));
						$updateTimestamp = $currentTime - $updateTokenLog;
						if ($updateTimestamp > 600) {
							$token_logObj->UpdateTokenLog($updateToken[0]['token_id']);
							\MI\GEN::error(9, "Your current session has been expired");
						}
						# pr($_REQUEST);exit;
						if ($user_status != 1) {
							if ($user_status == 0) {
								$error_msg = message::$inactive_user_error;
							} else if ($user_status == 2) {
								$error_msg = message::$delete_user_error;
							} else if ($user_status == 3) {
								$error_msg = message::$block_user_error;
							} else if ($user_status == 4) {
								$error_msg = message::$unverified_user_error;
							} else {
								$error_msg = message::$user_error;
							}
							#pr($user_data);exit;
							api::error(10, $error_msg);
						} else {
							$token_logObj->token_log_apicall($tokenStatus['user_id'], $clientAuthToken);
							if (isset($_REQUEST['receiver_id']) && $_REQUEST['receiver_id'] != '') {
								$receiver_id = $_REQUEST['receiver_id'];
								$receiver_user_data = $userObj->select($receiver_id);
								$receiver_user_status = $receiver_user_data[0]['status'];

								if ($receiver_user_status != 1) {
									$error_msg = message::$block_receiver_user_error;
									api::error(10, $error_msg);
								} else {
									include_once($file);
								}
							} else {
								include_once($file);
							}
						}
					} else {
						include_once($file);
					}
				} else {
					\MI\GEN::error(9, $tokenStatus['error_msg']);
				}
			} // id as user_id in request
			else if ($tag == "debit_credit_charges" || $tag == "unread_count") {
				if ($tokenStatus['tokenstatus'] == 1) {
					if (isset($tokenStatus['user_id']) && $tokenStatus['user_id'] != '') {
						$_REQUEST['id'] = $tokenStatus['id'];
						$id = $_REQUEST['id'];
						$user_data = $userObj->select($id);
						$user_status = $user_data[0]['status'];

						# Token expired
						if ($tokenStatus['user_id'] == $tokenStatus['id']) {
							$updateToken = $token_logObj->getUpdateTokenLog($tokenStatus['user_id'], $clientAuthToken);
						} else {
							$updateToken = $token_logObj->getUpdateTokenLog($tokenStatus['id'], $clientAuthToken);
						}
						$updateTokenLog = strtotime($updateToken[0]['updated_at']);
						$currentTime = strtotime(gmdate('YmdHis'));
						$updateTimestamp = $currentTime - $updateTokenLog;
						if ($updateTimestamp > 600) {
							$token_logObj->UpdateTokenLog($updateToken[0]['token_id']);
							\MI\GEN::error(9, "Your current session has been expired");
						}
						# pr($_REQUEST);exit;
						if ($user_status != 1) {
							if ($user_status == 0) {
								$error_msg = message::$inactive_user_error;
							} else if ($user_status == 2) {
								$error_msg = message::$delete_user_error;
							} else if ($user_status == 3) {
								$error_msg = message::$block_user_error;
							} else if ($user_status == 4) {
								$error_msg = message::$unverified_user_error;
							} else {
								$error_msg = message::$user_error;
							}
							#pr($user_data);exit;
							api::error(10, $error_msg);
						} else {
							if ($tokenStatus['user_id'] == $tokenStatus['id']) {
								$token_logObj->token_log_apicall($tokenStatus['user_id'], $clientAuthToken);
							} else {
								$token_logObj->token_log_apicall($tokenStatus['id'], $clientAuthToken);
							}
							include_once($file);
						}
					} else {
						include_once($file);
					}
				} else {
					\MI\GEN::error(9, $tokenStatus['error_msg']);
				}
			} //for Sub User  id as sub_user_id in request and user_id as for Main user's id
			else if ($tag == "notificationlist_subUser" || $tag == "notificationdetails_subUser" || $tag == "receiverMoney_subUser" || $tag == "get_subUser" || $tag == "logout_subUser") {
				if ($tokenStatus['tokenstatus'] == 1) {
					if (isset($tokenStatus['user_id']) && $tokenStatus['user_id'] != '' && isset($tokenStatus['id']) && $tokenStatus['id'] != '') {
						$_REQUEST['user_id'] = $tokenStatus['user_id'];
						$_REQUEST['id'] = $tokenStatus['id'];
						$id = $_REQUEST['user_id'];
						$user_data = $userObj->select($id);
						$user_status = $user_data[0]['status'];

						# Token expired
						if ($tokenStatus['user_id'] == $tokenStatus['id']) {
							$updateToken = $token_logObj->getUpdateTokenLog($tokenStatus['user_id'], $clientAuthToken);
						} else {
							$updateToken = $token_logObj->getUpdateTokenLog($tokenStatus['id'], $clientAuthToken);
						}
						$updateTokenLog = strtotime($updateToken[0]['updated_at']);
						$currentTime = strtotime(gmdate('YmdHis'));
						$updateTimestamp = $currentTime - $updateTokenLog;
						if ($updateTimestamp > 600) {
							$token_logObj->UpdateTokenLog($updateToken[0]['token_id']);
							\MI\GEN::error(9, "Your current session has been expired");
						}
						# pr($_REQUEST);exit;
						if ($user_status != 1) {
							if ($user_status == 0) {
								$error_msg = message::$inactive_user_error;
							} else if ($user_status == 2) {
								$error_msg = message::$delete_user_error;
							} else if ($user_status == 3) {
								$error_msg = message::$block_user_error;
							} else if ($user_status == 4) {
								$error_msg = message::$unverified_user_error;
							} else {
								$error_msg = message::$user_error;
							}
							#pr($user_data);exit;
							api::error(10, $error_msg);
						} else {
							if ($tokenStatus['user_id'] == $tokenStatus['id']) {
								$token_logObj->token_log_apicall($tokenStatus['user_id'], $clientAuthToken);
							} else {
								$token_logObj->token_log_apicall($tokenStatus['id'], $clientAuthToken);
							}
							include_once($file);
						}
					} else {
						include_once($file);
					}
				} else {
					\MI\GEN::error(9, $tokenStatus['error_msg']);
				}
			} //for Sub User  id as sub_user_id in request and receiver_id as for Main user's id
			else if ($tag == "refundRequest_subUser") {
				if ($tokenStatus['tokenstatus'] == 1) {
					if (isset($tokenStatus['user_id']) && $tokenStatus['user_id'] != '' && isset($tokenStatus['id']) && $tokenStatus['id'] != '') {
						$_REQUEST['receiver_id'] = $tokenStatus['user_id'];
						$_REQUEST['id'] = $tokenStatus['id'];
						$id = $_REQUEST['receiver_id'];
						$user_data = $userObj->select($id);
						$user_status = $user_data[0]['status'];

						# Token expired
						if ($tokenStatus['user_id'] == $tokenStatus['id']) {
							$updateToken = $token_logObj->getUpdateTokenLog($tokenStatus['user_id'], $clientAuthToken);
						} else {
							$updateToken = $token_logObj->getUpdateTokenLog($tokenStatus['id'], $clientAuthToken);
						}
						$updateTokenLog = strtotime($updateToken[0]['updated_at']);
						$currentTime = strtotime(gmdate('YmdHis'));
						$updateTimestamp = $currentTime - $updateTokenLog;
						if ($updateTimestamp > 600) {
							$token_logObj->UpdateTokenLog($updateToken[0]['token_id']);
							\MI\GEN::error(9, "Your current session has been expired");
						}
						# pr($_REQUEST);exit;
						if ($user_status != 1) {
							if ($user_status == 0) {
								$error_msg = message::$inactive_user_error;
							} else if ($user_status == 2) {
								$error_msg = message::$delete_user_error;
							} else if ($user_status == 3) {
								$error_msg = message::$block_user_error;
							} else if ($user_status == 4) {
								$error_msg = message::$unverified_user_error;
							} else {
								$error_msg = message::$user_error;
							}
							#pr($user_data);exit;
							api::error(10, $error_msg);
						} else {
							if ($tokenStatus['user_id'] == $tokenStatus['id']) {
								$token_logObj->token_log_apicall($tokenStatus['user_id'], $clientAuthToken);
							} else {
								$token_logObj->token_log_apicall($tokenStatus['id'], $clientAuthToken);
							}
							include_once($file);
						}
					} else {
						include_once($file);
					}
				} else {
					\MI\GEN::error(9, $tokenStatus['error_msg']);
				}
			} //for Sub User id as user_id in request
			else if ($tag == "user_device_registration") {
				if ($tokenStatus['tokenstatus'] == 1) {
					if (isset($tokenStatus['user_id']) && $tokenStatus['user_id'] != '' && isset($tokenStatus['id']) && $tokenStatus['id'] != '') {
						$_REQUEST['user_id'] = $tokenStatus['id'];
						$id = $_REQUEST['user_id'];
						$user_data = $userObj->select($id);
						$user_status = $user_data[0]['status'];

						# Token expired
						if ($tokenStatus['user_id'] == $tokenStatus['id']) {
							$updateToken = $token_logObj->getUpdateTokenLog($tokenStatus['user_id'], $clientAuthToken);
						} else {
							$updateToken = $token_logObj->getUpdateTokenLog($tokenStatus['id'], $clientAuthToken);
						}
						$updateTokenLog = strtotime($updateToken[0]['updated_at']);
						$currentTime = strtotime(gmdate('YmdHis'));
						$updateTimestamp = $currentTime - $updateTokenLog;
						if ($updateTimestamp > 600) {
							$token_logObj->UpdateTokenLog($updateToken[0]['token_id']);
							\MI\GEN::error(9, "Your current session has been expired");
						}
						# pr($_REQUEST);exit;
						if ($user_status != 1) {
							if ($user_status == 0) {
								$error_msg = message::$inactive_user_error;
							} else if ($user_status == 2) {
								$error_msg = message::$delete_user_error;
							} else if ($user_status == 3) {
								$error_msg = message::$block_user_error;
							} else if ($user_status == 4) {
								$error_msg = message::$unverified_user_error;
							} else {
								$error_msg = message::$user_error;
							}
							#pr($user_data);exit;
							api::error(10, $error_msg);
						} else {
							if ($tokenStatus['user_id'] == $tokenStatus['id']) {
								$token_logObj->token_log_apicall($tokenStatus['user_id'], $clientAuthToken);
							} else {
								$token_logObj->token_log_apicall($tokenStatus['id'], $clientAuthToken);
							}
							include_once($file);
						}
					} else {
						include_once($file);
					}
				} else {
					\MI\GEN::error(9, $tokenStatus['error_msg']);
				}
			} else {
				include_once($file);
			}
		} else {
			if ($_COOKIE['PHPSESSID'] == $_REQUEST['session_id']) {
				include_once($file);
			} else {
				\MI\GEN::error(0, "Oops!! Session is Expired");
			}
		}
		/*}
		else {
			\MI\GEN::error(0, "Unauthorized Access");
		}*/
	} else {
		\MI\GEN::error(0, "Unauthorized Access");
	}
}
\MI\GEN::error(0, "Oops!! Response not provided");
