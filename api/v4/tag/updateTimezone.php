<?php
/**
 * @api {Post} /v4/?tag=updateTimezone Request for updateTimezone
 * @apiName updateTimezone
 * @apiBase http://localhost/
 * @apiVersion 1.0.0
 * @apiGroup user
 *
 * @apiParam {String} tag Tag for api.
 * @apiParam {String} timezone_str for Timezone in string (Asia/Kolkata)
 * @apiParam {String} timezone_time for Timezone in time (GMT+05:30)
 *
 * @apiSuccessExample Success-Response for Accept:
 *  HTTP/1.1 200 OK
 * {
 * data: {},
 * message: "User timezone updated successfully.",
 * status: 1
 * }
 * @apiSuccessExample Success-Response for Reject:
 *  HTTP/1.1 200 OK
 * {
 * data: [ ],
 * message: "Timezone not updated. Please try again later.",
 * status: 1
 * }
 *
 */
use MI\API\Response as response;
use MI\GEN as api;
use MI\API\Message as msg;

## Validation
$_SERVER['REQUEST_METHOD'] == "POST" or api::error(0, \MI\API\Message::$invalidMethod);
$user_id = isset($_REQUEST['user_id']) && $_REQUEST['user_id'] != '' ? $_REQUEST['user_id'] : api::error(0, msg::$invalid_request_user_id);
$timezone_str = isset($_REQUEST['timezone_str']) && $_REQUEST['timezone_str'] != '' ? $_REQUEST['timezone_str'] : api::error(0, msg::$string_timezone_required);
$timezone_time = isset($_REQUEST['timezone_time']) && $_REQUEST['timezone_time'] != '' ? $_REQUEST['timezone_time'] : api::error(0, msg::$time_timezone_required);

$user = new user();

$userData = $user->select($user_id);
$gmt_date = $generalfuncobj->gm_date();

if(count($userData) > 0 && $userData[0]['status'] == '1'){

	$ut = new user_timezone();
	$ut->updateUserTimezone($user_id,$timezone_str,$timezone_time,$gmt_date);
	$u = new stdClass();
    api::success( $u, 1, msg::$time_zone_update);
}
else{
    api::error(0, msg::$invalide_user);
}


