<?php
/**
 * @api {get} /v3/?tag=get_subUser Request get detail of sub user
 * @apiName get sub user
 * @apiVersion 1.0.0
 * @apiGroup sub user manage
 *
 * @apiParam {String} tag Tag for api.
 * @apiParam {integer} id	give sub user's user id (required)
 * @apiParam {integer} user_id	give main user's user id (required)
 * @apiParam {String} device_id	give user's device device id (required)
 * @apiSuccess {Object} data Response data.
 * @apiSuccess {String} message Message.
 * @apiSuccess {Number} status Status.
 *
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 200 OK
 * {
 *      data: {
 *              id: "1",
 *              name: "name",
 *              employee_id: "employee_id",
 *              unique_token: "u1ktmv4t",
 *              image: "http://localhost/flexpay/uploads/default.png",
 *              description : "description",
 *              parent_user_id : "parent_user_id",
 *              status : "1"
 *          },
 * message: "Details of sub user.",
 * status: 1
 * }
 *
 */

use MI\API\Response as response;
use MI\GEN as api;
use MI\API\Message as message;

$user_id	=	isset($_REQUEST['user_id']) && $_REQUEST['user_id'] != '' ? $_REQUEST['user_id'] : api::error(0, \MI\API\Message::$invalid_user_id);
$id	=	isset($_REQUEST['id']) && $_REQUEST['id'] != '' ? $_REQUEST['id'] : api::error(0, \MI\API\Message::$invalid_subuser_id);
$device_id	=	isset($_REQUEST['device_id']) && $_REQUEST['device_id'] != '' ? $_REQUEST['device_id'] : api::error(0, \MI\API\Message::$Invalid_device_info);

$user = new  user();
$uld = new user_login_devices();

$gmt = $generalfuncobj->gm_date();

$login_chk = $uld->checkSubUserDeviceLogin($id, $device_id);

$sub_user_data	=	$user->select($id);
$subuser_status	=	$sub_user_data[0]['status'];
$user_data	=	$user->select($user_id);
$status	=	$user_data[0]['status'];

if ($login_chk) {
	if(count($user_data) > 0) {
		if ($status	==	1) {
			if (count($sub_user_data) > 0 && $subuser_status == 1) {

				$data	=	new stdClass();
				$data->id	=	$sub_user_data[0]['id'];
				$data->name	=	$sub_user_data[0]['name'];
				$data->employee_id	=	$sub_user_data[0]['employee_id'];
				$data->image	=	(file_exists($Qrcode_image_path . $sub_user_data[0]['qr_code'])) ? $Qrcode_image_url . $sub_user_data[0]['qr_code'] : $default_image;
				$data->unique_token	=	$sub_user_data[0]['unique_token'];
				$data->description	=	$sub_user_data[0]['description'];
				$data->parent_user_id	=	$sub_user_data[0]['parent_user_id'];
				$data->status	=	$sub_user_data[0]['status'];

				api::success($data, 1, "Details of sub user.");

			} else {
				api::error(9, \MI\API\Message::$login_required);
			}
		} else {

			## Mail
			if($status == 0)
			{
				$error_msg = message::$inactive_mainuser_error;
			}
			else if($status == 2)
			{
				$error_msg = message::$delete_mainuser_error;
			}
			else if($status == 3)
			{
				$error_msg = message::$block_mainuser_error;
			}
			else if($status == 4)
			{
				$error_msg = message::$unverified_mainuser_error;
			}else{
				$error_msg = message::$mainuser_error;
			}

			/*$email_template->sign_up($email, $name, $userid);*/
			api::error(9, $error_msg);
		}
	}else{
		api::error(9, "Unauthorized access. Please contact your admin.");
	}
} else {
	api::error(9, \MI\API\Message::$login_required);
}