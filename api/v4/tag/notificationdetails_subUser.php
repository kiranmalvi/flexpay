<?php

/**
 * @api {get} /v3/?tag=notificationdetails_subUser Request notification details of sub user
 * @apiName Notification Detail of sub user
 * @apiVersion 1.0.0
 * @apiGroup sub user transactions
 *
 * @apiParam {String} tag Tag for api.
 * @apiParam {integer} user_id give main user id
 * @apiParam {integer} id give login user id
 * @apiParam {integer} device_id give device id
 * @apiParam {integer} notification_id give notification id
 * @apiSuccess {Object} data Response data.
 * @apiSuccess {String} message Message.
 * @apiSuccess {Number} status Status.
 *
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 200 OK
 * {
 *      data:
 *      [
 *          {
 *              notification_id: "5",
 *              transaction_id: "9",
 *              eRead: "1",
 *              to_user_id: "1",
 *              from_user_id: "2",
 *              type: "request",
 *              comment: "comment",
 *              date: "31 October 2015"
 *              name: "name"
 *          }
 *      ],
 *      message: "notification details",
 *      status: 1
 * }
 *
 */
use MI\API\Response as response;
use MI\GEN as api;

//$_SERVER['REQUEST_METHOD'] == "POST" or api::error(0, \MI\API\Message::$invalidMethod);

$user_id = isset($_REQUEST['user_id']) && $_REQUEST['user_id'] != '' ? $_REQUEST['user_id'] : api::error(0, \MI\API\Message::$invalideuser_id);
$id	=	isset($_REQUEST['id']) && $_REQUEST['id'] != '' ? $_REQUEST['id'] : api::error(0, \MI\API\Message::$invalid_subuser_id);
$device_id	=	isset($_REQUEST['device_id']) && $_REQUEST['device_id'] != '' ? $_REQUEST['device_id'] : api::error(0, \MI\API\Message::$Invalid_device_info);
$notification_id	=	isset($_REQUEST['notification_id']) && $_REQUEST['notification_id'] != '' ? $_REQUEST['notification_id'] : api::error(0, \MI\API\Message::$invalidenotificationid);

$notification = new notification();
$user = new user();
$uld = new user_login_devices();
$login_chk = $uld->checkSubUserDeviceLogin($id, $device_id);
$userdata = $user->select($user_id);
$subUser_data = $user->select($id);
//echo "dbhdbv";
$notificationdata = $notification->select($notification_id);

if (count($userdata) > 0) {
	if(count($subUser_data) > 0 && $subUser_data[0]['status'] == 1) {
		if($login_chk){
			if (count($notificationdata) > 0) {
				$read_status = $notification->read_notification($notification_id);
				if($read_status == true)
				{
					$notificatiodetails = $notification->get_notificationSubUser($notification_id, $user_id, $id);

					$data = new MI\API\Response\V1\NotificationResponse();
					$data->notification_id = $notificatiodetails[0]['notification_id'];
					$data->transaction_id = $notificatiodetails[0]['transaction_id'];
					$data->refunded_transaction_id = $notificatiodetails[0]['refunded_transaction_id'];
					$data->refunded = $notificatiodetails[0]['refunded'];
					$data->eRead = $notificatiodetails[0]['eRead'];
					$data->to_user_id = $notificatiodetails[0]['to_user_id'];
					$data->from_user_id = $notificatiodetails[0]['from_user_id'];
					$data->type = $notificatiodetails[0]['type'];
					$data->comment = $notificatiodetails[0]['comment'];
					$data->name = $notificatiodetails[0]['name'];
					$data->amount = $notificatiodetails[0]['amount'];
					$data->via_user_id = $notificatiodetails[0]['via_user_id'];
					$data->via_user_name = $notificatiodetails[0]['via_user_name'];
					$data->parent_user_name = $notificatiodetails[0]['parent_user_name'];
					$data->parent_user_email  = $notificatiodetails[0]['parent_user_email'];
					$data->email = $notificatiodetails[0]['email'];


					/*if($notificatiodetails['name'] == "")
					{
						$data->name = $ADMIN_NAME;
					}
					else{
						$data->name = $notificatiodetails['name'];
					}

					if($notificatiodetails['email'] == "")
					{
						$data->email =  $ADMIN_EMAIL;
					}
					else{
						$data->email = $notificatiodetails['email'];
					}*/

					$data->date = $generalfuncobj->full_date_formate($notificatiodetails[0]['added_at']);
					$data->date_timestamp = $generalfuncobj->date_timestamp($notificatiodetails[0]['added_at']);

					api::success($data, 1, 'notification details');
				}
				api::success($data, 1, "notification read");
			} else {
				api::error(0, "notification not available");
			}
		}else{
			api::error(9, \MI\API\Message::$login_required);
		}
	}else{
		api::error(9, \MI\API\Message::$login_required);
	}
} else {
    api::error(0, "user not register");
}