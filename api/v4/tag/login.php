<?php
/**
 * @api {get} /v2/?tag=login Request login
 * @apiName login
 * @apiVersion 1.0.0
 * @apiGroup user
 *
 * @apiParam {String} tag Tag for api.
 * @apiParam {String} password   give old password
 * @apiParam {String} email  give email
 * @apiParam {String} device_id  give device id
 * @apiParam {String} player_id	give player id (optional)
 * @apiParam {String} device_model  give device modal name
 * @apiParam {String} system_name  give system  name
 * @apiParam {String} system_version  give system  version
 * @apiParam {String} app_version  give app   version
 * @apiParam {String} country_code  give country code
 * @apiParam {String} latitude  give latitude
 * @apiParam {String} longnitude  give longnitude
 * @apiSuccess {Object} data Response data.
 * @apiSuccess {String} message Message.
 * @apiSuccess {Number} status Status.
 *
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 200 OK
 * {
 *      data: {
 *              id: "1",
 *              name: "Chintan",
 *              email: "chintan.mindinventory@gmail.com",
 *              mobile: "1234567890",
 *              company: "1_qr_code.png",
 *              pin: "123456",
 *              token: "u1ktmv4t",
 *              image: "http://localhost/flexpay/uploads/default.png",
 *              wallet_amount: "60900",
 *              notification: null,
 *              sessionid: "rjh60ljbs6uabe2vqgq8gh5rb6"
 * 				token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ"
 *          },
 * message: "login details",
 * status: 1
 * }
 *
 */

use MI\API\Response as response;
use MI\GEN as api;
use MI\API\Message as message;

//$_SERVER['REQUEST_METHOD'] == "POST" or api::error(0, \MI\API\Message::$invalidMethod);
$password = isset($_REQUEST['password']) && $_REQUEST['password'] != '' ? $_REQUEST['password'] : api::error(0, \MI\API\Message::$invalid_sign_password);
$email = isset($_REQUEST['email']) && $_REQUEST['email'] != '' ? $_REQUEST['email'] : api::error(0, \MI\API\Message::$invalidemail);

$info_type = isset($_REQUEST['info_type']) ? $_REQUEST['info_type'] : null;
$session_id = $_COOKIE['PHPSESSID'];
$device_id = isset($_REQUEST['device_id']) ? $_REQUEST['device_id'] : null;
$player_id = isset($_REQUEST['player_id']) ? $_REQUEST['player_id'] : null;
$device_model = isset($_REQUEST['device_model']) ? $_REQUEST['device_model'] : null;
$system_name = isset($_REQUEST['system_name']) ? $_REQUEST['system_name'] : null;
$system_version = isset($_REQUEST['system_version']) ? $_REQUEST['system_version'] : null;
$app_version = isset($_REQUEST['app_version']) ? $_REQUEST['app_version'] : null;
$country_code = isset($_REQUEST['country_code']) ? $_REQUEST['country_code'] : null;
$latitude = isset($_REQUEST['latitude']) ? $_REQUEST['latitude'] : null;
$longnitude = isset($_REQUEST['longnitude']) ? $_REQUEST['longnitude'] : null;
$user = new  user();
$uld = new  user_login_devices();
$user_info = new user_info();
$wallet = new wallet();
$email_template = new Email();
$gen_func = new generalfunc();

$gmt = $generalfuncobj->gm_date();

$login_chk = $user->get_login(md5($password), $email);
$userid = $login_chk[0]['id'];

$user_data = $user->select($userid);
$status = $user_data[0]['status'];
$name = $user_data[0]['name'];

if(count($user_data) > 0) {
    if ($status == 1) {
        if (count($login_chk) > 0) {

            $userid = $login_chk[0]['id'];

            $user_info->setlongnitude($longnitude);
            $user_info->setlatitude($latitude);
            $user_info->setsession_id($session_id);
            $user_info->setdevice_model($device_model);
            $user_info->setapp_version($app_version);
            $user_info->setcountry_code($country_code);
            $user_info->setdate_time($gmt);
            $user_info->setinfo_type('login');
            $user_info->setdevice_id($device_id);
            $user_info->setstatus('1');
            $user_info->setuser_id($userid);
            $tid = $user_info->insert();

            $wallet_amount = $wallet->userwallet($userid);

            $transactiondetails = $user_info->select($tid);

			$check	=	$uld->checkUserDevice($device_id);
			if($check == 0){
				$uld->setdevice_id($device_id);
				$uld->setuser_id($userid);
				$uld->setplayer_id($player_id);
				$uld->setadded_at($gmt);
				$uld->insert();
			} else {
				$uld->registerUserDevice($check, $player_id, $userid);
			}

			$token	=	$gen_func->createJWTTOken($userid,$userid);

            $data = new MI\API\Response\V1\LoginResponse();
            $data->id = $login_chk[0]['id'];
            $data->name = $login_chk[0]['name'];
            $data->email = $login_chk[0]['email'];
            $data->mobile = $login_chk[0]['mobile'];
            $data->company = $login_chk[0]['company_name'];
            $data->pin = $login_chk[0]['security_pin'];
            $data->image = (file_exists($Qrcode_image_path . $login_chk[0]['qr_code'])) ? $Qrcode_image_url . $login_chk[0]['qr_code'] : $default_image;
            $data->token = $login_chk[0]['unique_token'];
            $data->sessionid = $session_id;
            $data->wallet_amount = $wallet_amount[0]['amount'];
            $data->token = $token;

			$token_logObj = new token_log();
			$token_logObj->token_log_apicall($login_chk[0]['id'],$token);

            api::success($data, 1, "login details");

        } else {
            api::error(0, "Invalid login details.");
        }
    } else {
        ## Mail
        if($status == 0)
        {
            $error_msg = message::$inactive_user_error;
        }
        else if($status == 2)
        {
            $error_msg = message::$delete_user_error;
        }
        else if($status == 3)
        {
            $error_msg = message::$block_user_error;
        }
        else if($status == 4)
        {
            $error_msg = message::$unverified_user_error;
        }else{
            $error_msg = message::$user_error;
        }

        $email_template->sign_up($email, $name, $userid);
        api::error(0, $error_msg);
    }
}else{
    api::error(0, "Invalid user.");
}


