<?php
/**
 * @api {get} /v3/?tag=edit_subUser Request update subuser
 * @apiName edit_subUser
 * @apiVersion 1.0.0
 * @apiGroup sub user manage
 *
 * @apiParam {String} tag Tag for api.
 * @apiParam {String} name give name
 * @apiParam {integer} user_id  Main user's user_id
 * @apiParam {integer} id  sub user's user_id
 * @apiParam {String} employee_id User (Unique Employee Id)
 * @apiParam {String} description User description(Optional)
 *
 * @apiSuccess {Object} data Response data.
 * @apiSuccess {String} message Message.
 * @apiSuccess {Number} status Status.
 *
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 200 OK
 *{
 * data: {
 *          id": "194",
 *			"name": "Joseph123",
 *			"employee_id": "Flex_123",
 *			"description": "Test",
 *			"parent_user_id": "191",
 *			"unique_token": "0fByN6zS"
 *			"status": "status"
 *      },
 * message: "Sub user's profile updated successfully.",
 * status: 1
 * }
 *
 */

use MI\API\Response as response;
use MI\GEN as api;

$_SERVER['REQUEST_METHOD'] == "POST" or api::error(0, \MI\API\Message::$invalidMethod);
$employee_id = isset($_REQUEST['employee_id']) && $_REQUEST['employee_id'] != '' ? mysqli_real_escape_string($obj->CONN,$_REQUEST['employee_id']) : api::error(0, \MI\API\Message::$invalid_employee_id);
$name = isset($_REQUEST['name']) && $_REQUEST['name'] != '' ? mysqli_real_escape_string($obj->CONN,$_REQUEST['name']) : api::error(0, \MI\API\Message::$invalid_sign_name);
$userid = isset($_REQUEST['user_id']) && $_REQUEST['user_id'] != '' ? $_REQUEST['user_id'] : api::error(0, \MI\API\Message::$invalideuser_id);
$id = isset($_REQUEST['id']) && $_REQUEST['id'] != '' ? $_REQUEST['id'] : api::error(0, \MI\API\Message::$invalid_sub_user_id);
$description = (isset($_REQUEST['description'])) ? mysqli_real_escape_string($obj->CONN,$_REQUEST['description']) : '';
$user = new  user();
//$user_info = new user_info();
$userdata = $user->select($userid);
$subuserdata = $user->select($id);
$gmt = $generalfuncobj->gm_date();
//pr($_REQUEST);exit;
if(!preg_match('/[^a-zA-Z0-9\-_]/', $employee_id)){
	if (count($userdata) > 0) {
		if(count($subuserdata) > 0 && $subuserdata[0]['status'] == '1'){
			if($user->check_user_employeeid($employee_id, $id)){

				$description = ($description == '') ? $subuserdata[0]['description'] : $description;

				$user->profile_update_subUser($userid,$id,$name,$employee_id,$description,$gmt);
				$editdetails = $user->select($id);

				$data = new stdClass();
				$data->id = $editdetails[0]['id'];
				$data->name = $editdetails[0]['name'];
				$data->employee_id = $editdetails[0]['employee_id'];
				$data->description = $editdetails[0]['description'];
				$data->parent_user_id = $editdetails[0]['parent_user_id'];
				$data->unique_token = $editdetails[0]['unique_token'];
				$data->status = $editdetails[0]['status'];
				api::success($data, 1, "Sub user's profile updated successfully.");
			}else{
				api::error(0, \MI\API\Message::$sign_eid_already);
			}
		} else {
			api::error(0, \MI\API\Message::$invalid_sub_user_id);
		}
	} else {
		api::error(0, "user not register");
	}
}else{
	api::error(0, \MI\API\Message::$invalid_sign_employee_id);
}