<?php
/**
 * @api {get} /v2/?tag=add_biller Request for add biller
 * @apiName add_biller
 * @apiBase http://localhost/
 * @apiVersion 1.0.0
 * @apiGroup Billers
 *
 * @apiParam {String} tag Tag for api.
 * @apiParam {integer} user_id for login user_id
 * @apiParam {String} biller_name for biller name
 * @apiParam {String} biller_email for biller email
 * @apiParam {String} account for account
 * @apiParam {String} comment for comment
 *
 * @apiSuccessExample Success-Response for Accept:
 *  HTTP/1.1 200 OK
 * {
 * data: {
		user_biller_id: 1,
 *      user_id: 9,
 *      biller_id: 1,
 *      biller_name: "John",
 *      biller_email: "john@xyz.com",
 *      account: "XYZ123456",
 *      comment: "XYZ is equal to zyx",
 *      status: 1,
 *      added_at: "2016-06-01 10:00:00",
 *      updated_at: "2016-06-01 10:00:00"
 * 		},
 * message: "Your biller added successfully.",
 * status: 1
 * }
 * @apiSuccessExample Success-Response for Reject:
 *  HTTP/1.1 200 OK
 * {
 * data: [ ],
 * message: "Biller not added. Please try again later.",
 * status: 1
 * }
 *
 */
use MI\API\Response as response;
use MI\GEN as api;
use MI\API\Message as msg;

## Validation
$user_id = isset($_REQUEST['user_id']) && $_REQUEST['user_id'] != '' ? $_REQUEST['user_id'] : api::error(0, msg::$invalid_request_user_id);
$biller_name = isset($_REQUEST['biller_name']) && $_REQUEST['biller_name'] != '' ? $_REQUEST['biller_name'] : api::error(0, msg::$invalid_request_biller_name);
$biller_email = isset($_REQUEST['biller_email']) && $_REQUEST['biller_email'] != '' ? $_REQUEST['biller_email'] : api::error(0, msg::$invalid_request_biller_email);
$account = isset($_REQUEST['account']) && $_REQUEST['account'] != '' ? $_REQUEST['account'] : api::error(0, msg::$invalid_request_account);
//$comment = isset($_REQUEST['comment']) && $_REQUEST['comment'] != '' ? $_REQUEST['comment'] : api::error(0, msg::$invalid_request_comment);
$comment = isset($_REQUEST['comment']) && $_REQUEST['comment'] != '' ? $_REQUEST['comment'] : "";

$user = new user();

$userData = $user->check_user($biller_email);
$gmt_date = $generalfuncobj->gm_date();
if(count($userData) > 0 && $userData[0]['status'] == '1'){
    $user_biller = new user_billers();
    $user_biller->setuser_id($user_id);
    $user_biller->setbiller_id($userData[0]['id']);
    $user_biller->setbiller_name($biller_name);
    $user_biller->setbiller_email($biller_email);
    $user_biller->setaccount($account);
    $user_biller->setcomment($comment);
    $user_biller->setstatus('1');
    $user_biller->setadded_at($gmt_date);
    $user_biller->setupdated_at($gmt_date);
    $biller_id = $user_biller->insert();
    api::success($user_biller->select($biller_id)[0], 1, msg::$success_add_biller);
}
else{
    api::error(0, msg::$invalid_request_biller_email);
}


