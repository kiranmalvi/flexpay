<?php
/**
 * @api {get} /v2/?tag=notificationlist Request Notification List
 * @apiName Notification List
 * @apiVersion 1.0.0
 * @apiGroup Notification
 *
 * @apiParam {String} tag Tag for api.
 * @apiParam {Number} user_id give login user id
 * @apiSuccess {array} data Response data.
 * @apiSuccess {String} message Message.
 * @apiSuccess {Number} status Status.
 *
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 200 OK
 * {
 *      data:
 *      [
 *          {
 *              notification_id: "5",
 *              transaction_id: "9",
 *              eRead: "1",
 *              to_user_id: "1",
 *              from_user_id: "2",
 *              type: "request",
 *              comment: "comment",
 *              date: "31 October 2015"
 *              name: "name"
 *          }
 *      ],
 *      message: "notification data list",
 *      status: 1
 * }
 *
 */


use MI\API\Response as response;
use MI\GEN as api;

//$_SERVER['REQUEST_METHOD'] == "POST" or api::error(0, \MI\API\Message::$invalidMethod);
$userid = isset($_REQUEST['user_id']) && $_REQUEST['user_id'] != '' ? $_REQUEST['user_id'] : api::error(0, \MI\API\Message::$invalideuser_id);
$timestamp = isset($_REQUEST['timestamp']) && $_REQUEST['timestamp'] != '' ? $_REQUEST['timestamp'] : 0;
$gmt = $generalfuncobj->gm_date();
$user = new  user();
//$user_info = new user_info();
$userdata = $user->select($userid);
$notificationlist = new notification();

if (count($userdata) > 0) {
    $notificatiodetails = $notificationlist->notificationdetailsOffsetLimit($userid, $timestamp);
    #pr($notificatiodetails);exit;
    foreach ($notificatiodetails AS $notificatiodata) {
        $responsedata = new MI\API\Response\V1\NotificationResponse();
        $responsedata->notification_id = $notificatiodata['notification_id'];
        $responsedata->transaction_id = $notificatiodata['transaction_id'];
		$responsedata->refunded_transaction_id = $notificatiodetails[0]['refunded_transaction_id'];
		$responsedata->refunded = $notificatiodetails[0]['refunded'];
        $responsedata->eRead = $notificatiodata['eRead'];
        $responsedata->to_user_id = $notificatiodata['to_user_id'];
        $responsedata->from_user_id = $notificatiodata['from_user_id'];
        $responsedata->type = $notificatiodata['type'];
        $responsedata->comment = $notificatiodata['comment'];
        $responsedata->name = $notificatiodata['name'];
        $responsedata->email = $notificatiodata['email'];
        $responsedata->transaction_type = $notificatiodata['transaction_type'];
/*
        if($notificatiodata['name'] == "")
        {
            $responsedata->name = $ADMIN_NAME;
        }
        else{
            $responsedata->name = $notificatiodata['name'];
        }

        if($notificatiodata['email'] == "")
        {
            $responsedata->email =  $ADMIN_EMAIL;
        }
        else{
            $responsedata->email = $notificatiodata['email'];
        }*/

        $responsedata->amount	=	$notificatiodata['amount'];
        $responsedata->via_user_id	=	$notificatiodata['via_user_id'];
        $responsedata->via_user_name	=	$notificatiodata['via_user_name'];
        $responsedata->date	=	$generalfuncobj->full_date_formate($notificatiodata['added_at']);
        $responsedata->updated_at	=	$generalfuncobj->full_date_formate($notificatiodata['updated_at']);
		$responsedata->updated_timestamp	=	$generalfuncobj->date_timestamp($notificatiodata['updated_at']);
		$responsedata->date_timestamp	=	$generalfuncobj->date_timestamp($notificatiodata['added_at']);

        $data[] = $responsedata;
    }

	$new_offset = min(($offset + $limit), ($offset + count($data)));
	if ($offset == $new_offset) {
		$new_offset = 0;
	}

    if(count($data) > 0)
    {
        api::success($data, 1, 'notification data list', $new_offset, $gmt);
    }else{
        api::success([], 1, 'no more notification', $new_offset, $gmt);
    }

} else {
    api::error(0, "user not register");
}
