<?php
/**
 * @api {get} /v2/?tag=receiver_details Request receive and sending money transaction details
 * @apiName receiver_details
 * @apiBase http://http://localhost/
 * @apiVersion 1.0.0
 * @apiGroup transaction
 *
 * @apiParam {String} tag Tag for api.
 * @apiParam {String} type give type 'debit','credit'
 * @apiParam {integer} user_id  login user  id.
 * @apiSuccess {array} data Response data.
 * @apiSuccess {String} message Message.
 * @apiSuccess {Number} status Status.
 *
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 200 OK
 * 1)debit information
 * {
 * data: [
 *          {
 *               transaction_id: "1",
 *              transaction_unique_id: "2295181649",
 *               name: "dhruv123",
 *              amount: "10",
 *              comments: "",
 *              transaction_status: "confirm",
 *              date: "31 October 2015"
 *           },
 *           {
 *              transaction_id: "3",
 *              transaction_unique_id: "1715555938",
 *              name: "dhruv123",
 *              amount: "10",
 *              comments: "",
 *              transaction_status: "confirm",
 *              date: "31 October 2015"
 *          },
 *
 * ],
 * message: "sender transaction details",
 * status: 1
 * }
 * 2) credit information
 *{
        data: [
                    {
                        transaction_id: "2",
                        transaction_unique_id: "4440670964",
                        name: "dhruv123",
                        amount: "10",
                        comments: "wdsd",
                        transaction_status: "pending",
                        date: "31 October 2015"
                    },
                    {
                        transaction_id: "7",
                        transaction_unique_id: "1078404093",
                        name: "krishna23",
                        amount: "10",
                        comments: "adcd123",
                        transaction_status: "pending",
                        date: "31 October 2015"
}
],
message: "reciver transaction details",
status: 1
}
 */
use MI\API\Response as response;
use MI\GEN as api;

//$_SERVER['REQUEST_METHOD'] == "POST" or api::error(0, \MI\API\Message::$invalidMethod);
$type = isset($_REQUEST['type']) && $_REQUEST['type'] != '' ? $_REQUEST['type'] : api::error(0, \MI\API\Message::$invalid_request_money_type);
$userid = isset($_REQUEST['user_id']) && $_REQUEST['user_id'] != '' ? $_REQUEST['user_id'] : api::error(0, \MI\API\Message::$invalideuser_id);
$timestamp = isset($_REQUEST['timestamp']) && $_REQUEST['timestamp'] != '' ? $_REQUEST['timestamp'] : 0;
$gmt = $generalfuncobj->gm_date();

if ($type == 'credit') {
    $type = 'credit';
} elseif ($type == 'debit') {
    $type = 'debit';
} else {
    api::error(0, \MI\API\Message::$invalid_request_money_type);
}

$user = new user();
$transaction = new transaction();
$userdata = $user->select($userid);
if (count($userdata) > 0) {
    if ($type == 'credit') {
        $transactionlist = $transaction->transactionCreditOffsetLimit($userid, 'debit', $timestamp);
        $message= "receiver transaction details";


    } elseif ($type == 'debit') {
        $transactionlist = $transaction->transactionOffsetLimit($userid, $type, $timestamp);

        $message= "sender transaction details";

    }
	if(count($transactionlist) > 0) {
		foreach ($transactionlist AS $mytransaction) {

			$traresponse = new MI\API\Response\V1\ReciverResponse();
			$traresponse->transaction_id = $mytransaction['transaction_id'];
			$traresponse->transaction_unique_id = $mytransaction['transaction_unique_id'];
			$traresponse->name = $mytransaction['name'];
			$traresponse->email = $mytransaction['email'];


			/*if($mytransaction['name'] == "")
			{
				$traresponse->name = $ADMIN_NAME;
			}
			else{
				$traresponse->name = $mytransaction['name'];
			}

			if($mytransaction['email'] == "")
			{
				$traresponse->email =  $ADMIN_EMAIL;
			}
			else{
				$traresponse->email = $mytransaction['email'];
			}*/

			$traresponse->amount = $mytransaction['amount'];
			$traresponse->comments = $mytransaction['comment'];
			$traresponse->credit_rate = ($mytransaction['credit_rate']) ? $mytransaction['credit_rate'] : 0;
			$traresponse->debit_rate = ($mytransaction['debit_rate']) ? $mytransaction['debit_rate'] : 0;
			$traresponse->transaction_status = $mytransaction['transaction_status'];
			$traresponse->refunded_transaction_id = $mytransaction['refunded_transaction_id'];
			$traresponse->refunded = $mytransaction['refunded'];
			$traresponse->transaction_type = $mytransaction['transaction_type'];
			$traresponse->date = $generalfuncobj->full_date_formate($mytransaction['added_at']);
			$traresponse->date_timestamp = $generalfuncobj->date_timestamp($mytransaction['added_at']);
			$traresponse->updated_at = $generalfuncobj->full_date_formate($mytransaction['updated_at']);
			$traresponse->updated_timestamp = $generalfuncobj->date_timestamp($mytransaction['updated_at']);
			$data[] = $traresponse;

		}
	} else {
		$data = [];
	}


	$new_offset = min(($offset + $limit), ($offset + count($data)));
	if ($offset == $new_offset) {
		$new_offset = 0;
	}

	if(count($data) > 0){
		api::success($data, 1, $message, $new_offset, $gmt);
	}else{
		api::success([], 1, $message, $new_offset, $gmt);
	}


} else {
    api::error(0, "user not register");
}