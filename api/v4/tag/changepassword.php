<?php
/**
 * @api {get} /v2/?tag=changepassword Request password changes
 * @apiName changepassword
 * @apiVersion 1.0.0
 * @apiGroup user
 *
 * @apiParam {String} tag Tag for api.
 * @apiParam {String} oldpassowrd   give old password
 * @apiParam {integer} user_id   give login user id
 * @apiParam {String} newpassowrd   give new password
 *  @apiParam {String} device_id  give device id
 * @apiParam {String} device_model  give device modal name
 * @apiParam {String} system_name  give system  name
 * @apiParam {String} system_version  give system  version
 * @apiParam {String} app_version  give app   version
 * @apiParam {String} country_code  give country code
 * @apiParam {String} latitude  give latitude
 * @apiParam {String} longnitude  give longnitude
 * @apiSuccess {Object} data Response data.
 * @apiSuccess {String} message Message.
 * @apiSuccess {Number} status Status.
 *
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 200 OK
 * {
 *      data: true,
 *      message: "password changes",
 *       status: 1
 * }
 *
 */

use MI\API\Response as response;
use MI\GEN as api;

$_SERVER['REQUEST_METHOD'] == "POST" or api::error(0, \MI\API\Message::$invalidMethod);
$oldpassword = isset($_REQUEST['oldpassowrd']) && $_REQUEST['oldpassowrd'] != '' ? $_REQUEST['oldpassowrd'] : api::error(0, \MI\API\Message::$invalideoldpassword);
$newpassowrd = isset($_REQUEST['newpassowrd']) && $_REQUEST['newpassowrd'] != '' ? $_REQUEST['newpassowrd'] : api::error(0, \MI\API\Message::$invalidenewpassword);
$userid = isset($_REQUEST['user_id']) && $_REQUEST['user_id'] != '' ? $_REQUEST['user_id'] : api::error(0, \MI\API\Message::$invalideuser_id);
$info_type = isset($_REQUEST['info_type']) ? $_REQUEST['info_type'] : null;
$session_id = isset($_REQUEST['session_id']) ? $_REQUEST['session_id'] : null;
$device_id = isset($_REQUEST['device_id']) ? $_REQUEST['device_id'] : null;
$device_model = isset($_REQUEST['device_model']) ? $_REQUEST['device_model'] : null;
$system_name = isset($_REQUEST['system_name']) ? $_REQUEST['system_name'] : null;
$system_version = isset($_REQUEST['system_version']) ? $_REQUEST['system_version'] : null;
$app_version = isset($_REQUEST['app_version']) ? $_REQUEST['app_version'] : null;
$country_code = isset($_REQUEST['country_code']) ? $_REQUEST['country_code'] : null;
$latitude = isset($_REQUEST['latitude']) ? $_REQUEST['latitude'] : null;
$longnitude = isset($_REQUEST['longnitude']) ? $_REQUEST['longnitude'] : null;
$user = new  user();
$user_info = new user_info();
$userdata = $user->select($userid);
$gmt = $generalfuncobj->gm_date();
if (count($userdata) > 0) {
    $oldpassword_chk = $user->get_password(md5($oldpassword), $userid);
    if (count($oldpassword_chk) > 0) {
        $newpassword1 = md5($newpassowrd);

        $user_info->setlongnitude($longnitude);
        $user_info->setlatitude($latitude);
        $user_info->setsession_id($session_id);
        $user_info->setdevice_model($device_model);
        $user_info->setapp_version($app_version);
        $user_info->setcountry_code($country_code);
        $user_info->setdate_time($gmt);
        $user_info->setinfo_type('change_password');
        $user_info->setdevice_id($device_id);
        $user_info->setstatus('1');
        $user_info->setuser_id($userid);
        $user_info->insert();
        $data = $user->passwordupdate($userid, $newpassword1);
        api::success($data, 1, "Your password is changed successfully");

    } else {
        api::error(0, "old password not match");
    }

} else {
    api::error(0, "user not register");
}

