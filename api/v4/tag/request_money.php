<?php
/**
 * @api {get} /v2/?tag=request_money Request request money
 * @apiName request_money
 * @apiBase http://http://localhost/
 * @apiVersion 1.0.0
 * @apiGroup user
 *
 * @apiParam {String} tag Tag for api.
 * @apiParam {String} type give type 'qr_code','email'
 * type='qr_code'(use following parameter)
 * @apiParam {integer} receiver_id User id for transfer money
 *  type='email'(use following parameter)
 * @apiParam {String} email User Email
 * common parameter
 * @apiParam {String} comment User comment
 * @apiParam {integer} sender_id  login user  id.
 * @apiParam {integer} amount  give amount to transfer
 *
 * @apiSuccess {array} data Response data.
 * @apiSuccess {String} message Message.
 * @apiSuccess {Number} status Status.
 *
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 200 OK
 * {
 * data: [ ],
 * message: "Request send successfully.",
 * status: 1
 * }
 *
 */

use MI\API\Response as response;
use MI\GEN as api;

//$_SERVER['REQUEST_METHOD'] == "POST" or api::error(0, \MI\API\Message::$invalidMethod);

## Validation
$type = isset($_REQUEST['type']) && $_REQUEST['type'] != '' ? $_REQUEST['type'] : api::error(0, \MI\API\Message::$invalid_request_money_type);
$sender_id = isset($_REQUEST['sender_id']) && $_REQUEST['sender_id'] != '' ? $_REQUEST['sender_id'] : api::error(0, \MI\API\Message::$invalid_request_money_sender_id);

if ($type == 'email') {
    $email = isset($_REQUEST['email']) && $_REQUEST['email'] != '' ? $_REQUEST['email'] : api::error(0, \MI\API\Message::$invalid_request_money_email);
} elseif ($type == 'qr_code') {
    $receiver_id = isset($_REQUEST['receiver_id']) && $_REQUEST['receiver_id'] != '' ? $_REQUEST['receiver_id'] : api::error(0, \MI\API\Message::$invalid_request_money_receiver_id);
} else {
    api::error(0, \MI\API\Message::$invalid_request_money_type);
}

$amount = isset($_REQUEST['amount']) && $_REQUEST['amount'] != '' && $_REQUEST['amount'] > 0 ? $_REQUEST['amount'] : api::error(0, \MI\API\Message::$invalid_request_money_amount);
$comment = isset($_REQUEST['comment']) && $_REQUEST['comment'] != '' ? mysqli_real_escape_string($obj->CONN,$_REQUEST['comment']) : '';

$user = new user();
$api = new \API();
## Check Sender

$userdata = $user->select($sender_id);

if ($user->check_user_exsits($sender_id)) {
    if ($type == 'email') {
        ## Check Receiver
        $receiver = $user->check_email($email);

		//Check receiver is sub user ?
		if($receiver[0]['parent_user_id'] == 0) {
			if (count($receiver) > 0) {
				if($receiver[0]['status'] == 1) {

					$receiver_id = $receiver[0]['id'];

					##Transaction Rate
					$utrobj = new user_transaction_rate();
					$debit_user_tran_data = $utrobj->user_transaction_data($receiver_id);
					$debit_rate = $debit_user_tran_data[0]['debit_rate'];
					$debit_tran_rate    =   ($amount * $debit_rate) / 100;

//                $utrobj =   new user_transaction_rate();
					$credit_user_tran_data = $utrobj->user_transaction_data($sender_id);
					$credit_rate = $credit_user_tran_data[0]['credit_rate'];
					$credit_tran_rate    =   ($amount * $credit_rate) / 100;

					## Check wallet Amount
					$wallet = new wallet();
					$user_wallet = $wallet->check_user_amount($sender_id);
					if (count($user_wallet) > 0) {
						$user_wallet_amount = $user_wallet[0]['amount'];
						// if ($credit_tran_rate > 0.01 && $debit_tran_rate > 0.01) {

						## Update Sender wallet
						#$wallet->deduct_amount($sender_id,$amount);

						## Update Receiver wallet
						#$wallet->add_amount($receiver_id,$amount);

						## Transaction
						$transaction = new transaction();
						$transaction->settransaction_unique_id($generalfuncobj->generateNumericUniqueToken(10));
						$transaction->setfrom_user_id($sender_id);
						$transaction->setto_user_id($receiver_id);
						$transaction->setvia_user_id(0);
						$transaction->setamount($amount);
						$transaction->settype('request');
						$transaction->setcomment($comment);
						$transaction->settransaction_status('pending');
						$transaction->setrefunded(0);
						$transaction->setadded_at($generalfuncobj->gm_date());
						$transaction->setupdated_at($generalfuncobj->gm_date());
						$transaction->setstatus('1');
						$transaction->settransaction_type('1');

						$transaction_id = $transaction->insert();

						$trmobj = new transaction_rate_map();
						$trmobj->settransaction_id($transaction_id);
						$trmobj->setdebit_rate($debit_rate);
						$trmobj->setcredit_rate($credit_rate);
						$trmobj->insert();


						## Sender Notification
						$notification = new notification();
						$notification->setuser_id($sender_id);
						$notification->settransaction_id($transaction_id);
						$notification->seteRead('0');
						$notification->setadded_at($generalfuncobj->gm_date());
						$notification->setstatus('1');
						$notification->insert();

						## Receiver Notification
						$notification = new notification();
						$notification->setuser_id($receiver_id);
						$notification->settransaction_id($transaction_id);
						$notification->seteRead('0');
						$notification->setadded_at($generalfuncobj->gm_date());
						$notification->setstatus('1');
						$noti_ID = $notification->insert();

//						$api->notify($receiver_id, "'{$userdata[0]['name']}' have requested $$amount from you.", '1', $amount, $noti_ID);
						$api->notifyOneSignal($receiver_id, "'{$userdata[0]['name']}' have requested $$amount from you.", '1', $amount, $noti_ID);
						api::success([], 1, \MI\API\Message::$invalid_request_money_wallet_success);


						//} else {
						//    api::error(0, \MI\API\Message::$invalid_request_transaction_money_error);
						// }
					} else {
						api::error(0, \MI\API\Message::$invalid_request_money_wallet);
					}
				}
				else{
					api::error(0, \MI\API\Message::$block_receiver_user_error);
				}

			} else {
				api::error(0, \MI\API\Message::$invalid_request_money_email);
			}
		}else{
			api::error(0, \MI\API\Message::$subUser_request_money_error);
		}
    } else {

		## Check Receiver
		$receiver = $user->select($receiver_id);

		//Check receiver is sub user ?
		if($receiver[0]['parent_user_id'] == 0) {
			if (count($receiver) > 0) {
				if($receiver[0]['status'] == 1) {

					## Check wallet Amount
					$wallet = new wallet();
					$user_wallet = $wallet->check_user_amount($sender_id);

					##Transaction Rate
					$utrobj = new user_transaction_rate();
					$debit_user_tran_data = $utrobj->user_transaction_data($receiver_id);
					$debit_rate = $debit_user_tran_data[0]['debit_rate'];
					$debit_tran_rate    =   ($amount * $debit_rate) / 100;

//                $utrobj =   new user_transaction_rate();
					$credit_user_tran_data = $utrobj->user_transaction_data($sender_id);
					$credit_rate = $credit_user_tran_data[0]['credit_rate'];
					$credit_tran_rate    =   ($amount * $credit_rate) / 100;

					if (count($user_wallet) > 0) {
						$user_wallet_amount = $user_wallet[0]['amount'];
						//if ($credit_tran_rate > 0.01 && $debit_tran_rate > 0.01) {

						## Update Sender wallet
						#$wallet->deduct_amount($sender_id,$amount);

						## Update Receiver wallet
						#$wallet->add_amount($receiver_id,$amount);

						## Transaction
						$transaction = new transaction();
						$transaction->settransaction_unique_id($generalfuncobj->generateNumericUniqueToken(10));
						$transaction->setfrom_user_id($sender_id);
						$transaction->setto_user_id($receiver_id);
						$transaction->setvia_user_id(0);
						$transaction->setamount($amount);
						$transaction->settype('request');
						$transaction->setcomment($comment);
						$transaction->settransaction_status('pending');
						$transaction->setrefunded(0);
						$transaction->setadded_at($generalfuncobj->gm_date());
						$transaction->setupdated_at($generalfuncobj->gm_date());
						$transaction->setstatus('1');
						$transaction->settransaction_type('1');

						$transaction_id = $transaction->insert();

						$trmobj = new transaction_rate_map();
						$trmobj->settransaction_id($transaction_id);
						$trmobj->setdebit_rate($debit_rate);
						$trmobj->setcredit_rate($credit_rate);
						$trmobj->insert();

						## Sender Notification
						$notification = new notification();
						$notification->setuser_id($sender_id);
						$notification->settransaction_id($transaction_id);
						$notification->seteRead('0');
						$notification->setadded_at($generalfuncobj->gm_date());
						$notification->setstatus('1');
						$notification->insert();

						## Receiver Notification
						$notification = new notification();
						$notification->setuser_id($receiver_id);
						$notification->settransaction_id($transaction_id);
						$notification->seteRead('0');
						$notification->setadded_at($generalfuncobj->gm_date());
						$notification->setstatus('1');
						$noti_ID = $notification->insert();

						//$api->notify($receiver_id, "'{$userdata[0]['name']}' have requested $$amount from you.", '1', $amount, $noti_ID);
						$api->notifyOneSignal($receiver_id, "'{$userdata[0]['name']}' have requested $$amount from you.", '1', $amount, $noti_ID);

						api::success([], 1, \MI\API\Message::$invalid_request_money_wallet_success);


						// } else {
						//    api::error(0, \MI\API\Message::$invalid_request_transaction_money_error);
						// }
					} else {
						api::error(0, \MI\API\Message::$invalid_request_money_wallet);
					}
				}else{
					api::error(0, \MI\API\Message::$block_receiver_user_error);
				}
			}else{
				api::error(0, \MI\API\Message::$invalid_request_money_receiver_id);
			}
		}else{
			api::error(0, \MI\API\Message::$subUser_request_money_error);
		}
    }
} else {
    api::error(0, \MI\API\Message::$invalid_request_money_sender_id);
}
