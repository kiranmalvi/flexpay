<?php

/**
 * @api {get} /v2/?tag=accept_reject Request Accept / Reject
 * @apiName accept_reject
 * @apiBase http://localhost/
 * @apiVersion 1.0.0
 * @apiGroup user
 *
 * @apiParam {String} tag Tag for api.
 * @apiParam {String} type give type '1 => accept','0 => reject'
 * type='1'(use following parameter)
 * @apiParam {String} security_pin User Security Pin
 * common parameter
 * @apiParam {integer} receiver_id for transfer money
 * @apiParam {integer} transaction_id  transaction update.
 * @apiParam {integer} sender_id  login user  id.
 * @apiParam {integer} amount  give amount to transfer
 *
 * @apiSuccess {array} data Response data.
 * @apiSuccess {String} message Message.
 * @apiSuccess {Number} status Status.
 *
 * @apiSuccessExample Success-Response for Accept:
 *  HTTP/1.1 200 OK
 * {
 * data: [ ],
 * message: "Your request accepted successfully.",
 * status: 1
 * }
 * @apiSuccessExample Success-Response for Reject:
 *  HTTP/1.1 200 OK
 * {
 * data: [ ],
 * message: "Request rejected successfully.",
 * status: 1
 * }
 *
 */
use MI\API\Response as response;
use MI\GEN as api;

## Validation
$type = isset($_REQUEST['type']) && $_REQUEST['type'] != '' ? $_REQUEST['type'] : api::error(0, \MI\API\Message::$invalid_request_money_type);
$sender_id = isset($_REQUEST['sender_id']) && $_REQUEST['sender_id'] != '' ? $_REQUEST['sender_id'] : api::error(0, \MI\API\Message::$invalid_request_money_sender_id);

if ($type == '1') {
    $receiver_id = isset($_REQUEST['receiver_id']) && $_REQUEST['receiver_id'] != '' ? $_REQUEST['receiver_id'] : api::error(0, \MI\API\Message::$invalid_request_money_receiver_id);
    $securitypin = isset($_REQUEST['security_pin']) && $_REQUEST['security_pin'] != '' ? $_REQUEST['security_pin'] : api::error(0, \MI\API\Message::$invalid_sign_security_pin);
} elseif ($type == '0') {
    $receiver_id = isset($_REQUEST['receiver_id']) && $_REQUEST['receiver_id'] != '' ? $_REQUEST['receiver_id'] : api::error(0, \MI\API\Message::$invalid_request_money_receiver_id);
} else {
    api::error(0, \MI\API\Message::$invalid_request_money_type);
}

$transaction_id = isset($_REQUEST['transaction_id']) && $_REQUEST['transaction_id'] != '' ? $_REQUEST['transaction_id'] : api::error(0, \MI\API\Message::$invalid_transaction_id);
$amount = isset($_REQUEST['amount']) && $_REQUEST['amount'] != '' ? $_REQUEST['amount'] : api::error(0, \MI\API\Message::$invalid_request_money_amount);
//$securitypin = isset($_REQUEST['security_pin']) && $_REQUEST['security_pin'] != '' ? $_REQUEST['security_pin'] : api::error(0, \MI\API\Message::$invalid_sign_security_pin);


$user = new user();
$tran = new transaction();
$noti = new notification();
$api = new \API();

$gmt_date = $generalfuncobj->gm_date_only();
$spendLimitCheck = $user->check_spending_limit($sender_id, $gmt_date, $amount);
$spend_limit = $spendLimitCheck->todays_spend;
$limit = $spendLimitCheck->transfer_limit;
$remain_limit = round($limit - $spend_limit,2);

## Check Sender
if ($type == 1) {
    $securitydata = $user->get_pin($securitypin, $sender_id);
} elseif ($type == 0) {
    $securitydata = $user->select($sender_id);
} else {
    $securitydata = '';
}

$gmt = $generalfuncobj->gm_date();
if (count($securitydata) > 0) {

    if ($type == 1) {
		if ( $remain_limit >= $amount ) {	//check user daily spend limit
			if ($user->check_user_exsits($sender_id)) {
				##check security pin


				## Check Receiver
				$receiver = $user->select($receiver_id);
				$userdata = $user->select($sender_id);

				if (count($receiver) > 0) {
//                $receiver_id = $receiver[0]['id'];

					## Check wallet Amount
					$wallet = new wallet();
					$user_wallet = $wallet->check_user_amount($sender_id);

					## Get Transaction Rates
					$trmobj = new transaction_rate_map();
					$trans_rate = $trmobj->transaction_rates($transaction_id);
					$debit_rate = ($amount * $trans_rate['debit_rate']) / 100;
					$credit_rate = ($amount * $trans_rate['credit_rate']) / 100;
					$debit_amount = $amount + $debit_rate;
					$credit_amount = $amount - $credit_rate;

					if (count($user_wallet) > 0) {
						$user_wallet_amount = $user_wallet[0]['amount'];
						if ($user_wallet_amount >= $debit_amount) {

							$tran_data = $tran->select($transaction_id);
							if (count($tran_data) > 0) {
								if ($tran_data[0]['type'] == 'request') {
									## Update Sender wallet
									$wallet->deduct_amount($sender_id, $debit_amount);

									## Update Receiver wallet
									$wallet->add_amount($receiver_id, $credit_amount);

									## Transaction

									$tran->update_transaction_record($transaction_id, 'debit', 'confirm', $gmt);
									$noti_ID = $noti->get_notificationid($transaction_id, $receiver_id);
//									$api->notify($receiver_id, "You have receive $$amount from '{$userdata[0]['name']}'.", '3', $amount, $noti_ID);
									$api->notifyOneSignal($receiver_id, "You have receive $$amount from '{$userdata[0]['name']}'.", '3', $amount, $noti_ID);

									//code for apply promotions
									$promotions	=	new	promotions();//declare promotions object
									$offers	=	$promotions->runningPromotions($receiver_id, $amount);//get all valid running promotions of receiver as per spending limit

									if(count($offers) > 0) {//if promotions applied and transaction is not for refund process
										$promotionID	=	$offers[0]['promotion_id'];
										$discount_price = ($offers[0]['discount'] * $amount) / 100 ; //calculate discount price
										$max_cashback = $offers[0]['cashback'];//maximum cash back
										$final_cashback = ($discount_price > $max_cashback && $max_cashback !== 0) ? $max_cashback : $discount_price;//final cash back

										## Update Sender wallet
										$wallet->deduct_amount($receiver_id, $final_cashback);

										## Update Receiver wallet
										$wallet->add_amount($sender_id, $final_cashback);

										## Transaction
										$tran->settransaction_unique_id($generalfuncobj->generateNumericUniqueToken(10));
										$tran->setrefunded_transaction_id($transaction_id);
										$tran->setfrom_user_id($sender_id);
										$tran->setto_user_id($receiver_id);
										$tran->setamount($final_cashback);
										$tran->settype('debit');
										$tran->setcomment('');
										$tran->settransaction_status('confirm');
										$tran->setrefunded(0);
										$tran->setadded_at($generalfuncobj->gm_date());
										$tran->setupdated_at($generalfuncobj->gm_date());
										$tran->setstatus('1');
										$tran->settransaction_type('2');
										$tran->setpromotion_id($promotionID);

										$cashback_transaction_id = $tran->insert();

										## Notification
										## Sender Notification
										$noti->setuser_id($receiver_id);
										$noti->seteRead('0');
										$noti->settransaction_id($cashback_transaction_id);
										$noti->setadded_at($generalfuncobj->gm_date());
										$noti->setstatus('1');
										$noti->insert();

										## Receiver Notification
										$noti->setuser_id($sender_id);
										$noti->seteRead('0');
										$noti->settransaction_id($cashback_transaction_id);
										$noti->setadded_at($generalfuncobj->gm_date());
										$noti->setstatus('1');
										$cashback_noti_ID = $noti->insert();

										//$api->notify($receiver_id, "You have received $$amount from '{$userdata[0]['name']}'.", '3', $amount, $noti_ID);
										$api->notifyOneSignal($sender_id, "You have received cash back of $$final_cashback from '{$receiver[0]['name']}'.", '4', $final_cashback, $cashback_noti_ID);
									}

									//code to apply admin rewards
									$admin_rewards	=	$promotions->adminRewards($sender_id);
									if($admin_rewards['reward'] > 0 && $admin_rewards['current_spend'] > 0 && $admin_rewards['reward_limit'] > 0) {
										$currentSpend	=	$admin_rewards['current_spend'];
										$rewardLimit	=	$admin_rewards['reward_limit'];
										$rewards	=	$admin_rewards['reward'];

										$calculateReward	=	intval($currentSpend / $rewardLimit);
//										$finalReward	=	($calculateReward > 0) ? round($calculateReward * $rewards, 2) : 0;

										if($calculateReward > 0) {
											for($i = 0; $i < $calculateReward; $i++) {
												## Update Sender wallet
												//$wallet->deduct_amount($receiver_id, $final_cashback);

												## Update Receiver wallet
												$wallet->add_amount($sender_id, $rewards);

												## Transaction
												$tran->settransaction_unique_id($generalfuncobj->generateNumericUniqueToken(10));
												$tran->setrefunded_transaction_id($transaction_id);
												$tran->setfrom_user_id($sender_id);
												$tran->setto_user_id(0);
												$tran->setvia_user_id(0);
												$tran->setamount($rewards);
												$tran->settype('debit');
												$tran->setcomment('');
												$tran->settransaction_status('confirm');
												$tran->setrefunded(0);
												$tran->setadded_at($generalfuncobj->gm_date());
												$tran->setupdated_at($generalfuncobj->gm_date());
												$tran->setstatus('1');
												$tran->settransaction_type('3');
												$tran->setpromotion_id(0);

												$cashback_transaction_id = $tran->insert();

												## Notification
												## Receiver Notification
												$noti->setuser_id($sender_id);
												$noti->seteRead('0');
												$noti->settransaction_id($cashback_transaction_id);
												$noti->setadded_at($generalfuncobj->gm_date());
												$noti->setstatus('1');
												$adminCashback_noti_ID = $noti->insert();

												//$api->notify($receiver_id, "You have received $$amount from '{$userdata[0]['name']}'.", '3', $amount, $noti_ID);
												$api->notifyOneSignal($sender_id, "You have received reward of $$rewards from Flex pay.", '5', $rewards, $adminCashback_noti_ID);
											}
										}
									}

									api::success([], 1, "Your request accepted successfully.");
								} else {
									api::error(0, "This request is already accepted / rejected.");
								}
							} else {
								api::error(0, \MI\API\Message::$invalid_transaction_id);
							}
						} else {
							api::error(0, \MI\API\Message::$invalid_request_money_wallet_error);
						}
					} else {
						api::error(0, \MI\API\Message::$invalid_request_money_wallet);
					}
				} else {
					api::error(0, \MI\API\Message::$invalid_request_money_email);
				}

			} else {
				api::error(0, \MI\API\Message::$invalid_request_money_sender_id);
			}
		} else {
			if ( $remain_limit < 0 ) {
				api::error(0, \MI\API\Message::$spend_limit_exceeded);
			} else {
				api::error(0, \MI\API\Message::$spend_limit_remain.$remain_limit);
			}
		}

    } else {
        $userdata = $user->select($sender_id);
        if (count($userdata) > 0) {
            ## Update Transaction
            $tran_data = $tran->select($transaction_id);
//            pr($tran_data);exit;
            if (count($tran_data) > 0) {
                if ($tran_data[0]['type'] == 'request') {

                    $tran->update_transaction_record($transaction_id, 'reject', 'cancel', $gmt);
                    $noti_ID = $noti->get_notificationid($transaction_id, $receiver_id);
//                    $api->notify($receiver_id, "Your request for $$amount have been rejected by '{$userdata[0]['name']}'.", '2', $amount, $noti_ID);
                    $api->notifyOneSignal($receiver_id, "Your request for $$amount have been rejected by '{$userdata[0]['name']}'.", '2', $amount, $noti_ID);
                    api::success([], 1, "Request rejected successfully");
                } else {
                    api::success([], 1, "This request is already accepted / rejected.");
                }
            } else {
                api::error(0, \MI\API\Message::$invalid_transaction_id);
            }

        } else {
            api::error(0, \MI\API\Message::$invalide_user);
        }
    }
} else {
    api::error(0, \MI\API\Message::$invalid_pin_to_use);
}