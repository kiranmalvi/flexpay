<?php
/**
 * @api {get} /v3/?tag=refundRequest_subUser Refund request from sub User
 * @apiName refundRequest_subUser
 * @apiBase http://localhost/
 * @apiVersion 1.0.0
 * @apiGroup sub user transactions
 *
 * @apiParam {String} tag	Tag for api.
 * @apiParam {integer} receiver_id	Main user's User id who will refund the money.
 * @apiParam {integer} sender_id	Customer user id who will get refund money.
 * @apiParam {integer} id	Sub User's user id.
 * @apiParam {integer} transaction_id	Transaction id for which customer needs refund.
 * @apiParam {integer} amount	Money amount to transfer.
 * @apiParam {integer} device_id	Sub User's device id.
 * @apiParam {integer} comment	comment (optional).
 *
 * @apiSuccess {array} data Response data.
 * @apiSuccess {String} message Message.
 * @apiSuccess {Number} status Status.
 *
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 200 OK
 * {
 * data: [ ],
 * message: "Refund request initiated successfully.",
 * status: 1
 * }
 *
 */

use MI\API\Response as response;
use MI\GEN as api;

//$_SERVER['REQUEST_METHOD'] == "POST" or api::error(0, \MI\API\Message::$invalidMethod);

## Validation
$sender_id = isset($_REQUEST['sender_id']) && $_REQUEST['sender_id'] != '' ? $_REQUEST['sender_id'] : api::error(0, \MI\API\Message::$invalid_request_money_sender_id);
$receiver_id = isset($_REQUEST['receiver_id']) && $_REQUEST['receiver_id'] != '' ? $_REQUEST['receiver_id'] : api::error(0, \MI\API\Message::$invalid_request_money_receiver_id);
$id = isset($_REQUEST['id']) && $_REQUEST['id'] != '' ? $_REQUEST['id'] : api::error(0, \MI\API\Message::$invalid_sub_user_id);
$transactionID = isset($_REQUEST['transaction_id']) && $_REQUEST['transaction_id'] != '' ? $_REQUEST['transaction_id'] : api::error(0, \MI\API\Message::$invalid_transaction_id);
$device_id = isset($_REQUEST['device_id']) && $_REQUEST['device_id'] != '' ? $_REQUEST['device_id'] : api::error(0, \MI\API\Message::$invalid_device_id);
$amount = isset($_REQUEST['amount']) && $_REQUEST['amount'] != '' && $_REQUEST['amount'] > 0 ? $_REQUEST['amount'] : api::error(0, \MI\API\Message::$invalid_request_money_amount);
$comment = isset($_REQUEST['comment']) && $_REQUEST['comment'] != '' ? mysqli_real_escape_string($obj->CONN,$_REQUEST['comment']) : '';

$user = new user();
$uld = new user_login_devices();
$api = new \API();
$gmt_date = $generalfuncobj->gm_date();

## Check subUser login on device
$login_chk = $uld->checkSubUserDeviceLogin($id, $device_id);
## Check Sender

$userdata = $user->select($sender_id);

## Check Receiver
$mainuserdata = $user->select($receiver_id);

## Check subUser
$subuserdata = $user->select($id);

if($login_chk){
	if(count($userdata) > 0 && $userdata[0]['status'] == 1){
		if(count($subuserdata) > 0 && $subuserdata[0]['status'] == 1){
			if(count($mainuserdata) > 0 && $mainuserdata[0]['status'] == 1){
				if($mainuserdata[0]['parent_user_id'] == 0) {//Check receiver is sub user ?

					##Transaction Rate
					$utrobj = new user_transaction_rate();
					$debit_user_tran_data = $utrobj->user_transaction_data($receiver_id);
					$debit_rate = $debit_user_tran_data[0]['debit_rate'];
					$debit_tran_rate    =   ($amount * $debit_rate) / 100;

					$credit_user_tran_data = $utrobj->user_transaction_data($sender_id);
					$credit_rate = $credit_user_tran_data[0]['credit_rate'];
					$credit_tran_rate    =   ($amount * $credit_rate) / 100;

					## Check wallet Amount
					$wallet = new wallet();
					$user_wallet = $wallet->check_user_amount($sender_id);
					if (count($user_wallet) > 0) {
						$user_wallet_amount = $user_wallet[0]['amount'];
						// if ($credit_tran_rate > 0.01 && $debit_tran_rate > 0.01) {

						## Update Sender wallet
						#$wallet->deduct_amount($sender_id,$amount);

						## Update Receiver wallet
						#$wallet->add_amount($receiver_id,$amount);

						## Transaction
						$transaction = new transaction();
						$transaction->settransaction_unique_id($generalfuncobj->generateNumericUniqueToken(10));
						$transaction->setrefunded_transaction_id($transactionID);
						$transaction->setfrom_user_id($sender_id);
						$transaction->setto_user_id($receiver_id);
						$transaction->setvia_user_id($id);
						$transaction->setamount($amount);
						$transaction->settype('request');
						$transaction->setcomment($comment);
						$transaction->settransaction_status('pending');
						$transaction->setrefunded(1);
						$transaction->setadded_at($generalfuncobj->gm_date());
						$transaction->setupdated_at($generalfuncobj->gm_date());
						$transaction->setstatus('1');

						$transaction_id = $transaction->insert();

						#Update old transaction status as requested by transaction_id
						$transaction->updateIsRefunded('3',$transactionID,$gmt_date);

						$trmobj = new transaction_rate_map();
						$trmobj->settransaction_id($transaction_id);
						$trmobj->setdebit_rate($debit_rate);
						$trmobj->setcredit_rate($credit_rate);
						$trmobj->insert();


						## Sender Notification
						$notification = new notification();
						$notification->setuser_id($sender_id);
						$notification->settransaction_id($transaction_id);
						$notification->seteRead('0');
						$notification->setadded_at($generalfuncobj->gm_date());
						$notification->setstatus('1');
						$notification->insert();

						## Receiver Notification
						$notification = new notification();
						$notification->setuser_id($receiver_id);
						$notification->settransaction_id($transaction_id);
						$notification->seteRead('0');
						$notification->setadded_at($generalfuncobj->gm_date());
						$notification->setstatus('1');
						$noti_ID = $notification->insert();

						//$api->notify($receiver_id, "'{$subuserdata[0]['name']}' initiated refund request of $$amount for '{$userdata[0]['name']}', Do you Accept it?", '1', $amount, $noti_ID);
						$api->notifyOneSignal($receiver_id, "'{$subuserdata[0]['name']}' initiated refund request of $$amount for '{$userdata[0]['name']}', Do you Accept it?", '1', $amount, $noti_ID);
						api::success([], 1, \MI\API\Message::$subUser_refund_request_money);

						//} else {
						//    api::error(0, \MI\API\Message::$invalid_request_transaction_money_error);
						// }
					}else{
						api::error(0, \MI\API\Message::$invalid_request_money_wallet);
					}
				}else{
					api::error(0, \MI\API\Message::$subUser_request_money_error);
				}
			}else{
				api::error(0, \MI\API\Message::$invalid_request_money_receiver_id);
			}
		}else{
			api::error(0, \MI\API\Message::$invalid_sub_user_id);
		}
	}else{
		api::error(0, \MI\API\Message::$invalid_request_money_sender_id);
	}
}else{
	api::error(9, \MI\API\Message::$login_required);
}