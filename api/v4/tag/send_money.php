<?php
/**
 * @api {get} /v4/?tag=send_money Request Send Money
 * @apiName send_money
 * @apiBase http://http://localhost/
 * @apiVersion 1.0.0
 * @apiGroup user
 *
 * @apiParam {String} tag Tag for api.
 * @apiParam {String} type give type 'qr_code','email'
 * type='qr_code'(use following parameter)
 * @apiParam {integer} receiver_id User id for transfer money
 *  type='email'(use following parameter)
 * @apiParam {String} email User Email
 * common parameter
 * @apiParam {String} security_pin User Security Pin
 * @apiParam {integer} sender_id  login user  id.
 * @apiParam {integer} amount  give amount to transfer
 * @apiParam {integer} transaction_id  by default Optional/contain value when transaction is for refund
 *
 * @apiSuccess {array} data Response data.
 * @apiSuccess {String} message Message.
 * @apiSuccess {Number} status Status.
 *
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 200 OK
 * {
 * data: [ ],
 * message: "wallet amount send successfuly .",
 * status: 1
 * }
 *
 */

use MI\API\Response as response;
use MI\GEN as api;
/*echo "Hi";
$api = new \API();
$api->notifyOneSignal(65, "Test", '3', 0.1, 10);
exit;*/
## Validation
$type = isset($_REQUEST['type']) && $_REQUEST['type'] != '' ? $_REQUEST['type'] : api::error(0, \MI\API\Message::$invalid_request_money_type);
$sender_id = isset($_REQUEST['sender_id']) && $_REQUEST['sender_id'] != '' ? $_REQUEST['sender_id'] : api::error(0, \MI\API\Message::$invalid_request_money_sender_id);

if ($type == 'email') {
    $email = isset($_REQUEST['email']) && $_REQUEST['email'] != '' ? $_REQUEST['email'] : api::error(0, \MI\API\Message::$invalid_request_money_email);
} elseif ($type == 'qr_code') {
    $receiver_id = isset($_REQUEST['receiver_id']) && $_REQUEST['receiver_id'] != '' ? $_REQUEST['receiver_id'] : api::error(0, \MI\API\Message::$invalid_request_money_receiver_id);
} else {
    api::error(0, \MI\API\Message::$invalid_request_money_type);
}
$comment = isset($_REQUEST['comment']) && $_REQUEST['comment'] != '' ? mysqli_real_escape_string($obj->CONN,$_REQUEST['comment']) : "";

$amount = isset($_REQUEST['amount']) && $_REQUEST['amount'] != '' && $_REQUEST['amount'] > 0 ? $_REQUEST['amount'] : api::error(0, \MI\API\Message::$invalid_request_money_amount);
$securitypin = isset($_REQUEST['security_pin']) && $_REQUEST['security_pin'] != '' ? $_REQUEST['security_pin'] : api::error(0, \MI\API\Message::$invalid_sign_security_pin);
$session_id = isset($_REQUEST['session_id']) ? $_REQUEST['session_id'] : null;
$device_id = isset($_REQUEST['device_id']) ? $_REQUEST['device_id'] : null;
$device_model = isset($_REQUEST['device_model']) ? $_REQUEST['device_model'] : null;
$system_name = isset($_REQUEST['system_name']) ? $_REQUEST['system_name'] : null;
$system_version = isset($_REQUEST['system_version']) ? $_REQUEST['system_version'] : null;
$app_version = isset($_REQUEST['app_version']) ? $_REQUEST['app_version'] : null;
$country_code = isset($_REQUEST['country_code']) ? $_REQUEST['country_code'] : null;
$latitude = isset($_REQUEST['latitude']) ? $_REQUEST['latitude'] : null;
$longnitude = isset($_REQUEST['longnitude']) ? $_REQUEST['longnitude'] : null;
$trans_id = isset($_REQUEST['transaction_id']) ? $_REQUEST['transaction_id'] : null;
$refunded = ($trans_id) ? 1 : 0;

$user_info = new user_info();
$user = new user();
$api = new \API();
## Check Sender
$securitydata = $user->get_pin($securitypin, $sender_id);
$gmt = $generalfuncobj->gm_date();

$gmt_date = $generalfuncobj->gm_date_only();
$spendLimitCheck = $user->check_spending_limit($sender_id, $gmt_date, $amount);
$spend_limit = $spendLimitCheck->todays_spend;
$limit = $spendLimitCheck->transfer_limit;
$remain_limit = round($limit - $spend_limit,2);
//echo $remain_limit." >= ".$amount;exit;
if ( $remain_limit >= $amount ) {	//check user daily spend limit
	if (count($securitydata) > 0) {

		##Transaction Rate
		$utrobj = new user_transaction_rate();
		$debit_user_tran_data = $utrobj->user_transaction_data($sender_id);
		$debit_rate = $debit_user_tran_data[0]['debit_rate'];
		$debit_tran_rate    =   ($amount * $debit_rate) / 100;
		$debit_amount = $debit_tran_rate + $amount;

		$credit_user_tran_data = $utrobj->user_transaction_data($receiver_id);
		$credit_rate = $credit_user_tran_data[0]['credit_rate'];
		$credit_tran_rate    =   ($amount * $credit_rate) / 100;
		$credit_amount = $amount - $credit_tran_rate;

		if ($type == 'email') {
			if ($user->check_user_exsits($sender_id)) {
				##check security pin
				$userdata = $user->select($sender_id);


				## Check Receiver
				$receiver = $user->check_email($email);
//pr($receiver);exit;
				if (count($receiver) > 0) {

					if($receiver[0]['status'] == 1)
					{

						$receiver_id = $receiver[0]['id'];
						if($receiver_id != $sender_id) {
							$receiverdata	=	$user->select($receiver_id);

							$credit_user_tran_data = $utrobj->user_transaction_data($receiver_id);
							$credit_rate = $credit_user_tran_data[0]['credit_rate'];
							$credit_tran_rate    =   ($amount * $credit_rate) / 100;
							$credit_amount = $amount - $credit_tran_rate;

							## Check wallet Amount
							$wallet = new wallet();
							$user_wallet = $wallet->check_user_amount($sender_id);


							if (count($user_wallet) > 0) {
								$user_wallet_amount = $user_wallet[0]['amount'];
								if ($user_wallet_amount >= $debit_amount) {
									//if ($credit_tran_rate > 0.01 && $debit_tran_rate > 0.01) {

									## Update Sender wallet
									$wallet->deduct_amount($sender_id, $debit_amount);

									## Update Receiver wallet
									$wallet->add_amount($receiver_id, $credit_amount);

									## Transaction

									$transaction = new transaction();
									$transaction->settransaction_unique_id($generalfuncobj->generateNumericUniqueToken(10));
									$transaction->setrefunded_transaction_id(($trans_id) ? $trans_id : 0);
									$transaction->setfrom_user_id($receiver_id);
									$transaction->setto_user_id($sender_id);
									$transaction->setvia_user_id(0);
									$transaction->setamount($amount);
									$transaction->settype('debit');
									$transaction->setcomment($comment);
									$transaction->settransaction_status('confirm');
									$transaction->setrefunded($refunded);
									$transaction->setadded_at($generalfuncobj->gm_date());
									$transaction->setupdated_at($generalfuncobj->gm_date());
									$transaction->setstatus('1');
									$transaction->settransaction_type('1');
									$transaction->setpromotion_id(0);

									$transaction_id = $transaction->insert();
									if ($trans_id) {
										$upd_refund	=	$transaction->updateIsRefunded(2, $trans_id, $gmt);
									}

									## Trasaction rates
									$trmobj = new transaction_rate_map();
									$trmobj->settransaction_id($transaction_id);
									$trmobj->setdebit_rate($debit_rate);
									$trmobj->setcredit_rate($credit_rate);
									$trmobj->insert();

									## Notification
									## Sender Notification
									$notification = new notification();
									$notification->setuser_id($sender_id);
									$notification->seteRead('0');
									$notification->settransaction_id($transaction_id);
									$notification->setadded_at($generalfuncobj->gm_date());
									$notification->setstatus('1');
									$notification->insert();

									## Receiver Notification
									$notification->setuser_id($receiver_id);
									$notification->seteRead('0');
									$notification->settransaction_id($transaction_id);
									$notification->setadded_at($generalfuncobj->gm_date());
									$notification->setstatus('1');
									$noti_ID = $notification->insert();

									##user device info
									$user_info->setlongnitude($longnitude);
									$user_info->setlatitude($latitude);
									$user_info->setsession_id($session_id);
									$user_info->setdevice_model($device_model);
									$user_info->setapp_version($app_version);
									$user_info->setcountry_code($country_code);
									$user_info->setdate_time($gmt);
									$user_info->setinfo_type('transaction');
									$user_info->setdevice_id($device_id);
									$user_info->setstatus('1');
									$user_info->setuser_id($sender_id);
									$tid = $user_info->insert();

									//$api->notify($receiver_id, "You have received $$amount from '{$userdata[0]['name']}'.", '3', $amount, $noti_ID);
									$api->notifyOneSignal($receiver_id, "You have received $$amount from '{$userdata[0]['name']}'.", '3', $amount, $noti_ID);

									if($trans_id === null){
										//code to check cash back offers are applied or not on transaction
										$promotions	=	new	promotions();//declare promotions object
										$offers	=	$promotions->runningPromotions($receiver_id, $amount);//get all valid running promotions as per spending amount

										if(count($offers) > 0) {//if promotions applied and transaction is not for refund process
											$promotionID	=	$offers[0]['promotion_id'];
											$discount_price = ($offers[0]['discount'] * $amount) / 100 ; //calculate discount price
											$max_cashback = $offers[0]['cashback'];//maximum cash back
											$final_cashback = ($discount_price > $max_cashback && $max_cashback !== 0) ? $max_cashback : $discount_price;//final cash back
											## Update Sender wallet
											$wallet->deduct_amount($receiver_id, $final_cashback);

											## Update Receiver wallet
											$wallet->add_amount($sender_id, $final_cashback);

											## Transaction
											$transaction->settransaction_unique_id($generalfuncobj->generateNumericUniqueToken(10));
											$transaction->setrefunded_transaction_id($transaction_id);
											$transaction->setfrom_user_id($sender_id);
											$transaction->setto_user_id($receiver_id);
											$transaction->setamount($final_cashback);
											$transaction->settype('debit');
											$transaction->setcomment('');
											$transaction->settransaction_status('confirm');
											$transaction->setrefunded(0);
											$transaction->setadded_at($generalfuncobj->gm_date());
											$transaction->setupdated_at($generalfuncobj->gm_date());
											$transaction->setstatus('1');
											$transaction->settransaction_type('2');
											$transaction->setpromotion_id($promotionID);

											$cashback_transaction_id = $transaction->insert();

											## Notification
											## Sender Notification
											$notification->setuser_id($receiver_id);
											$notification->seteRead('0');
											$notification->settransaction_id($cashback_transaction_id);
											$notification->setadded_at($generalfuncobj->gm_date());
											$notification->setstatus('1');
											$notification->insert();

											## Receiver Notification
											$notification->setuser_id($sender_id);
											$notification->seteRead('0');
											$notification->settransaction_id($cashback_transaction_id);
											$notification->setadded_at($generalfuncobj->gm_date());
											$notification->setstatus('1');
											$cashback_noti_ID = $notification->insert();

											//$api->notify($receiver_id, "You have received $$amount from '{$userdata[0]['name']}'.", '3', $amount, $noti_ID);
											$api->notifyOneSignal($sender_id, "You have received cash back of $$final_cashback from '{$receiverdata[0]['name']}'.", '4', $final_cashback, $cashback_noti_ID);
										}

										//code for admin rewards
										$admin_rewards	=	$promotions->adminRewards($sender_id);
										if($admin_rewards['reward'] > 0 && $admin_rewards['current_spend'] > 0 && $admin_rewards['reward_limit'] > 0) {
											$currentSpend	=	$admin_rewards['current_spend'];
											$rewardLimit	=	$admin_rewards['reward_limit'];
											$rewards	=	$admin_rewards['reward'];

											$calculateReward	=	intval($currentSpend / $rewardLimit);
//										$finalReward	=	($calculateReward > 0) ? round($calculateReward * $rewards, 2) : 0;

											if($calculateReward > 0) {
												for($i=0; $i<$calculateReward; $i++) {
													## Update Sender wallet
													//$wallet->deduct_amount($receiver_id, $final_cashback);

													## Update Receiver wallet
													$wallet->add_amount($sender_id, $rewards);

													## Transaction
													$transaction->settransaction_unique_id($generalfuncobj->generateNumericUniqueToken(10));
													$transaction->setrefunded_transaction_id($transaction_id);
													$transaction->setfrom_user_id($sender_id);
													$transaction->setto_user_id(0);
													$transaction->setvia_user_id(0);
													$transaction->setamount($rewards);
													$transaction->settype('debit');
													$transaction->setcomment('');
													$transaction->settransaction_status('confirm');
													$transaction->setrefunded(0);
													$transaction->setadded_at($generalfuncobj->gm_date());
													$transaction->setupdated_at($generalfuncobj->gm_date());
													$transaction->setstatus('1');
													$transaction->settransaction_type('3');
													$transaction->setpromotion_id(0);

													$cashback_transaction_id = $transaction->insert();

													## Notification
													## Receiver Notification
													$notification->setuser_id($sender_id);
													$notification->seteRead('0');
													$notification->settransaction_id($cashback_transaction_id);
													$notification->setadded_at($generalfuncobj->gm_date());
													$notification->setstatus('1');
													$adminCashback_noti_ID = $notification->insert();

													//$api->notify($receiver_id, "You have received $$amount from '{$userdata[0]['name']}'.", '3', $amount, $noti_ID);
													$api->notifyOneSignal($sender_id, "You have received reward of $$rewards from Flex pay.", '5', $rewards, $adminCashback_noti_ID);
												}
											}
										}
									}

									api::success([], 1, \MI\API\Message::$invalid_send_money_wallet_success);
									// } else {
									//  api::error(0, \MI\API\Message::$invalid_request_transaction_money_error);
									// }
								} else {
									api::error(0, \MI\API\Message::$invalid_request_money_wallet_error);
								}
							} else {
								api::error(0, \MI\API\Message::$invalid_request_money_wallet);
							}
						} else {
							api::error(0, \MI\API\Message::$invalid_request_money_sender_id);
						}
					} else {
						api::error(0, \MI\API\Message::$block_receiver_user_error);
					}

				} else {
					api::error(0, \MI\API\Message::$invalid_request_money_email);
				}

			} else {
				api::error(0, \MI\API\Message::$invalid_request_money_sender_id);
			}

		} else {
			$userdata	=	$user->select($receiver_id);
			$senderdata	=	$user->select($sender_id);
			$via_user_id	=	$receiver_id;

			//for receiver user is valid
			if (count($userdata) > 0 && $userdata[0]['status'] == 1) {

				//to check receiver is main user or sub user if sub user then repeat some processes to initialize receiver again
				if($userdata[0]['parent_user_id'] > 0){

					//get main user
					$receiver_id	=	$userdata[0]['parent_user_id'];
					$userdata	=	$user->select($receiver_id);

					//check main user
					if (count($userdata) > 0 && $userdata[0]['status'] == 1) {
						$credit_user_tran_data = $utrobj->user_transaction_data($receiver_id);
						$credit_tran_rate    =   ($amount * $credit_rate) / 100;
						$credit_rate = $credit_user_tran_data[0]['credit_rate'];
						$credit_amount = $amount - $credit_tran_rate;
					}else{
						api::error(0, \MI\API\Message::$invalide_user);
						exit;
					}
				}

				if($receiver_id !== $sender_id){
					$wallet = new wallet();
					$user_wallet = $wallet->check_user_amount($sender_id);
					if (count($user_wallet) > 0) {
						$user_wallet_amount = $user_wallet[0]['amount'];
						if ($user_wallet_amount >= $debit_amount) {
							//if ($credit_tran_rate > 0.01 && $debit_tran_rate > 0.01) {

							## Update Sender wallet
							$wallet->deduct_amount($sender_id, $debit_amount);

							## Update Receiver wallet
							$wallet->add_amount($receiver_id, $credit_amount);

							## Transaction

							$transaction = new transaction();
							$transaction->settransaction_unique_id($generalfuncobj->generateNumericUniqueToken(10));
							$transaction->setrefunded_transaction_id(($trans_id) ? $trans_id : 0);
							$transaction->setfrom_user_id($receiver_id);
							$transaction->setto_user_id($sender_id);
							if($via_user_id != $receiver_id) {
								$transaction->setvia_user_id($via_user_id);
							} else {
								$transaction->setvia_user_id(0);
							}
							$transaction->setamount($amount);
							$transaction->settype('debit');
							$transaction->setcomment($comment);
							$transaction->settransaction_status('confirm');
							$transaction->setrefunded($refunded);
							$transaction->setadded_at($generalfuncobj->gm_date());
							$transaction->setupdated_at($generalfuncobj->gm_date());
							$transaction->setstatus('1');
							$transaction->settransaction_type('1');
							$transaction->setpromotion_id(0);

							$transaction_id = $transaction->insert();
							if ($trans_id) {
								$upd_refund	=	$transaction->updateIsRefunded(2, $trans_id, $gmt);
							}

							## Trasaction rates
							$trmobj = new transaction_rate_map();
							$trmobj->settransaction_id($transaction_id);
							$trmobj->setdebit_rate($debit_rate);
							$trmobj->setcredit_rate($credit_rate);
							$trmobj->insert();

							## Notification
							## Sender Notification
							$notification = new notification();
							$notification->setuser_id($sender_id);
							$notification->settransaction_id($transaction_id);
							$notification->seteRead('0');
							$notification->setadded_at($generalfuncobj->gm_date());
							$notification->setstatus('1');
							$notification->insert();

							## Receiver Notification
							$notification->setuser_id($receiver_id);
							$notification->seteRead('0');
							$notification->settransaction_id($transaction_id);
							$notification->setadded_at($generalfuncobj->gm_date());
							$notification->setstatus('1');
							$noti_ID = $notification->insert();

							##user device info
							$user_info->setlongnitude($longnitude);
							$user_info->setlatitude($latitude);
							$user_info->setsession_id($session_id);
							$user_info->setdevice_model($device_model);
							$user_info->setapp_version($app_version);
							$user_info->setcountry_code($country_code);
							$user_info->setdate_time($gmt);
							$user_info->setinfo_type('transaction');
							$user_info->setdevice_id($device_id);
							$user_info->setstatus('1');
							$user_info->setuser_id($sender_id);
							$tid = $user_info->insert();

							//$api->notify($receiver_id, "You have received $$amount from '{$senderdata[0]['name']}'.", '3', $amount, $noti_ID);
							$api->notifyOneSignal($via_user_id, "You have received $$amount from '{$senderdata[0]['name']}'.", '3', $amount, $noti_ID);

							if($trans_id === null){
								//code to check cash back offers are applied or not on transaction
								$promotions	=	new	promotions();//declare promotions object
								$offers	=	$promotions->runningPromotions($receiver_id, $amount);//get all valid running promotions as per spending amount

								if(count($offers) > 0) {//if promotions applied and transaction is not for refund process
									$promotionID	=	$offers[0]['promotion_id'];
									$discount_price = ($offers[0]['discount'] * $amount) / 100 ; //calculate discount price
									$max_cashback = $offers[0]['cashback'];//maximum cash back
									$final_cashback = ($discount_price > $max_cashback && $max_cashback !== 0) ? $max_cashback : $discount_price;//final cash back

									## Update Sender wallet
									$wallet->deduct_amount($receiver_id, $final_cashback);

									## Update Receiver wallet
									$wallet->add_amount($sender_id, $final_cashback);

									## Transaction
									$transaction->settransaction_unique_id($generalfuncobj->generateNumericUniqueToken(10));
									$transaction->setrefunded_transaction_id($transaction_id);
									$transaction->setfrom_user_id($sender_id);
									$transaction->setto_user_id($receiver_id);
									$transaction->setamount($final_cashback);
									$transaction->settype('debit');
									$transaction->setcomment('');
									$transaction->settransaction_status('confirm');
									$transaction->setrefunded(0);
									$transaction->setadded_at($generalfuncobj->gm_date());
									$transaction->setupdated_at($generalfuncobj->gm_date());
									$transaction->setstatus('1');
									$transaction->settransaction_type('2');
									$transaction->setpromotion_id($promotionID);

									$cashback_transaction_id = $transaction->insert();

									## Notification
									## Sender Notification
									$notification->setuser_id($receiver_id);
									$notification->seteRead('0');
									$notification->settransaction_id($cashback_transaction_id);
									$notification->setadded_at($generalfuncobj->gm_date());
									$notification->setstatus('1');
									$notification->insert();

									## Receiver Notification
									$notification->setuser_id($sender_id);
									$notification->seteRead('0');
									$notification->settransaction_id($cashback_transaction_id);
									$notification->setadded_at($generalfuncobj->gm_date());
									$notification->setstatus('1');
									$cashback_noti_ID = $notification->insert();

									//$api->notify($receiver_id, "You have received $$amount from '{$userdata[0]['name']}'.", '3', $amount, $noti_ID);
									$api->notifyOneSignal($sender_id, "You have received cash back of $$final_cashback from '{$userdata[0]['name']}'.", '4', $final_cashback, $cashback_noti_ID);
								}

								//code for admin rewards
								$admin_rewards	=	$promotions->adminRewards($sender_id);
								if($admin_rewards['reward'] > 0 && $admin_rewards['current_spend'] > 0 && $admin_rewards['reward_limit'] > 0) {
									$currentSpend	=	$admin_rewards['current_spend'];
									$rewardLimit	=	$admin_rewards['reward_limit'];
									$rewards	=	$admin_rewards['reward'];

									$calculateReward	=	intval($currentSpend / $rewardLimit);
//									$finalReward	=	($calculateReward > 0) ? round($calculateReward * $rewards, 2) : 0;

									if($calculateReward > 0) {
										for($i=0; $i<$calculateReward; $i++) {
											## Update Sender wallet
//										$wallet->deduct_amount($receiver_id, $final_cashback);

											## Update Receiver wallet
											$wallet->add_amount($sender_id, $rewards);

											## Transaction
											$transaction->settransaction_unique_id($generalfuncobj->generateNumericUniqueToken(10));
											$transaction->setrefunded_transaction_id($transaction_id);
											$transaction->setfrom_user_id($sender_id);
											$transaction->setto_user_id(0);
											$transaction->setvia_user_id(0);
											$transaction->setamount($rewards);
											$transaction->settype('debit');
											$transaction->setcomment('');
											$transaction->settransaction_status('confirm');
											$transaction->setrefunded(0);
											$transaction->setadded_at($generalfuncobj->gm_date());
											$transaction->setupdated_at($generalfuncobj->gm_date());
											$transaction->setstatus('1');
											$transaction->settransaction_type('3');
											$transaction->setpromotion_id(0);

											$cashback_transaction_id = $transaction->insert();

											## Notification
											## Receiver Notification
											$notification->setuser_id($sender_id);
											$notification->seteRead('0');
											$notification->settransaction_id($cashback_transaction_id);
											$notification->setadded_at($generalfuncobj->gm_date());
											$notification->setstatus('1');
											$adminCashback_noti_ID = $notification->insert();

											//$api->notify($receiver_id, "You have received $$amount from '{$userdata[0]['name']}'.", '3', $amount, $noti_ID);
											$api->notifyOneSignal($sender_id, "You have received reward of $$rewards from Flex pay.", '5', $rewards, $adminCashback_noti_ID);
										}
									}
								}
							}

							api::success([], 1, \MI\API\Message::$invalid_send_money_wallet_success);

							//} else {
							// api::error(0, \MI\API\Message::$invalid_request_transaction_money_error);
							//}
						} else {
							api::error(0, \MI\API\Message::$invalid_request_money_wallet_error);
						}
					} else {
						api::error(0, \MI\API\Message::$invalid_request_money_wallet);
					}
				} else {
					api::error(0, \MI\API\Message::$invalid_request_money_sender_id);
				}
			} else {
				api::error(0, \MI\API\Message::$invalide_user);
			}
		}
	} else {
		api::error(0, \MI\API\Message::$invalid_pin_to_use);
	}
} else {
	if ( $remain_limit < 0 ) {
		api::error(0, \MI\API\Message::$spend_limit_exceeded);
	} else {
		api::error(0, \MI\API\Message::$spend_limit_remain.$remain_limit);
	}
}