<?php
/**
 * @api {get} /v3/?tag=reset_subUserPassword Request for reset sub user password
 * @apiName reset_subUserPassword
 * @apiBase http://localhost/
 * @apiVersion 1.0.0
 * @apiGroup sub user manage
 *
 * @apiParam {String} tag Tag for api.
 * @apiParam {integer} user_id for main user's user_id
 * @apiParam {integer} id sub user_id
 * @apiParam {string} password new password
 *
 * @apiSuccessExample Success-Response for Accept:
 *  HTTP/1.1 200 OK
 * {
 * data: {},
 * message: "Sub user password reset successfully.",
 * status: 1
 * }
 *
 */
use MI\API\Response as response;
use MI\GEN as api;
use MI\API\Message as msg;

## Validation
$user_id	=	isset($_REQUEST['user_id']) && $_REQUEST['user_id'] != '' ? $_REQUEST['user_id'] : api::error(0, msg::$invalid_user_id);
$id	=	isset($_REQUEST['id']) && $_REQUEST['id'] != '' ? $_REQUEST['id'] : api::error(0, msg::$invalid_subuser_id);
$password	=	isset($_REQUEST['password']) && $_REQUEST['password'] != '' ? $_REQUEST['password'] : api::error(0, msg::$invalid_sign_password);

$gmt_date = $generalfuncobj->gm_date();

$user = new user();
$uld = new user_login_devices();

if(count($user->selectSubUser($id,$user_id)) > 0) {
    $user->resetSubUser($id, $user_id, md5($password), $gmt_date);
    $uld->removeSubUser($id);
    api::success(array(), 1, msg::$success_reset_subUser);
}
else{
    api::error(0, msg::$invalid_sub_user_id);
}
