<?php
/**
 * @api {get} /v3/?tag=login_subUser Request login of sub user
 * @apiName login sub user
 * @apiVersion 1.0.0
 * @apiGroup sub user manage
 *
 * @apiParam {String} tag Tag for api.
 * @apiParam {String} password	give old password (required)
 * @apiParam {String} employee_id	give employee_id (required)
 * @apiParam {String} device_id	give device id (required)
 * @apiParam {String} player_id	give player id (optional)
 * @apiParam {String} device_model	give device modal name (optional)
 * @apiParam {String} system_name	give system name (optional)
 * @apiParam {String} system_version	give system version (optional)
 * @apiParam {String} app_version	give app version (optional)
 * @apiParam {String} country_code	give country code (optional)
 * @apiParam {String} latitude	give latitude (optional)
 * @apiParam {String} longnitude	give longnitude (optional)
 * @apiSuccess {Object} data Response data.
 * @apiSuccess {String} message Message.
 * @apiSuccess {Number} status Status.
 *
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 200 OK
 * {
 *      data: {
 *              id: "1",
 *              name: "name",
 *              employee_id: "employee_id",
 *              unique_token: "u1ktmv4t",
 *              image: "http://localhost/flexpay/uploads/default.png",
 *              description : "description",
 *              parent_user_id : "parent_user_id",
 *              status : "1"
 * 				token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ"
 *          },
 * message: "Login successfully.",
 * status: 1
 * }
 *
 */

use MI\API\Response as response;
use MI\GEN as api;
use MI\API\Message as message;

//$_SERVER['REQUEST_METHOD'] == "POST" or api::error(0, \MI\API\Message::$invalidMethod);
$password = isset($_REQUEST['password']) && $_REQUEST['password'] != '' ? $_REQUEST['password'] : api::error(0, \MI\API\Message::$invalid_sign_password);
$employee_id = isset($_REQUEST['employee_id']) && $_REQUEST['employee_id'] != '' ? mysqli_real_escape_string($obj->CONN,$_REQUEST['employee_id']) : api::error(0, \MI\API\Message::$invalid_employee_id);

$session_id = $_COOKIE['PHPSESSID'];
$device_id = isset($_REQUEST['device_id']) ? $_REQUEST['device_id'] : api::error(0, \MI\API\Message::$Invalid_device_info);
$player_id = isset($_REQUEST['player_id']) ? $_REQUEST['player_id'] : null;
$device_model = isset($_REQUEST['device_model']) ? $_REQUEST['device_model'] : null;
$system_name = isset($_REQUEST['system_name']) ? $_REQUEST['system_name'] : null;
$system_version = isset($_REQUEST['system_version']) ? $_REQUEST['system_version'] : null;
$app_version = isset($_REQUEST['app_version']) ? $_REQUEST['app_version'] : null;
$country_code = isset($_REQUEST['country_code']) ? $_REQUEST['country_code'] : null;
$latitude = isset($_REQUEST['latitude']) ? $_REQUEST['latitude'] : null;
$longnitude = isset($_REQUEST['longnitude']) ? $_REQUEST['longnitude'] : null;
$user = new  user();
$user_info = new user_info();
$uld = new user_login_devices();

$gmt = $generalfuncobj->gm_date();

$login_chk = $user->get_subUserLogin(md5($password), $employee_id);

$id	=	$login_chk[0]['id'];
$user_id	=	$login_chk[0]['parent_user_id'];

$sub_user_data	=	$user->select($id);
$subuser_status	=	$sub_user_data[0]['status'];
$user_data	=	$user->select($user_id);
$status	=	$user_data[0]['status'];
//pr(count($user_data));
//pr($status);exit;
if(count($user_data) > 0) {
    if ($status	==	1) {
        if (count($login_chk) > 0) {

			//Insert user_info
            $id	=	$login_chk[0]['id'];
            $user_info->setlongnitude($longnitude);
            $user_info->setlatitude($latitude);
            $user_info->setsession_id($session_id);
            $user_info->setdevice_model($device_model);
            $user_info->setapp_version($app_version);
            $user_info->setcountry_code($country_code);
            $user_info->setdate_time($gmt);
            $user_info->setinfo_type('login');
            $user_info->setdevice_id($device_id);
            $user_info->setstatus('1');
            $user_info->setuser_id($id);
            $tid	=	$user_info->insert();

			//Set subUser login Log
			$check	=	$uld->checkUserDevice($device_id);
			if($check == 0){
				$uld->setdevice_id($device_id);
				$uld->setuser_id($id);
				$uld->setplayer_id($player_id);
				$uld->setadded_at($gmt);
				$uld->insert();
			} else {
				$uld->registerUserDevice($check, $player_id, $id);
			}

			$token	=	$generalfuncobj->createJWTTOken($user_id,$id);

			$data	=	new stdClass();
			$data->id	=	$login_chk[0]['id'];
			$data->name	=	$login_chk[0]['name'];
			$data->employee_id	=	$login_chk[0]['employee_id'];
			$data->image	=	(file_exists($Qrcode_image_path . $login_chk[0]['qr_code'])) ? $Qrcode_image_url . $login_chk[0]['qr_code'] : $default_image;
			$data->unique_token	=	$login_chk[0]['unique_token'];
			$data->sessionid	=	$session_id;
			$data->description	=	$login_chk[0]['description'];
			$data->parent_user_id	=	$login_chk[0]['parent_user_id'];
			$data->status	=	$login_chk[0]['status'];
			$data->token	=	$token;

			$token_logObj = new token_log();
			$token_logObj->token_log_apicall($login_chk[0]['id'],$token);

            api::success($data, 1, "You are login successfully.");

        } else {
            api::error(0, "Invalid login details.");
        }
    } else {

        ## Mail
        if($status == 0)
        {
            $error_msg = message::$inactive_mainuser_error;
        }
        else if($status == 2)
        {
            $error_msg = message::$delete_mainuser_error;
        }
        else if($status == 3)
        {
            $error_msg = message::$block_mainuser_error;
        }
        else if($status == 4)
        {
            $error_msg = message::$unverified_mainuser_error;
        }else{
            $error_msg = message::$mainuser_error;
        }

		/*$email_template->sign_up($email, $name, $userid);*/
        api::error(0, $error_msg);
    }
}else{
    api::error(0, "Unauthorized access. Please contact your admin.");
}