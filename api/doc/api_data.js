define({ "api": [
  {
    "type": "get",
    "url": "/v2/?tag=list_biller",
    "title": "Request for user biller's list",
    "name": "User_Billers_List",
    "version": "1.0.0",
    "group": "Billers",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tag",
            "description": "<p>Tag for api.</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "user_id",
            "description": "<p>give login user id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "data",
            "description": "<p>Response data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
	        "content": " HTTP/1.1 200 OK\n{\n     data:\n     [\n         {\n             user_biller_id: 1,\n             user_id: 9,\n             biller_id: 1,\n             biller_name: \"John\",\n             biller_email: \"john@xyz.com\",\n             account: \"XYZ123456\",\n             comment: \"XYZ is equal to zyx\",\n             status: 1,\n             added_at: \"2016-06-01 10:00:00\",\n             updated_at: \"2016-06-01 10:00:00\"\n         },\n           {....},\n              .\n              .\n              .\n           {....}\n     ],\n     message: \"notification data list\",\n     status: 1\n}",
          "type": "json"
        }
      ]
    },
	  "filename": "api/v5/tag/list_biller.php",
    "groupTitle": "Billers"
  },
  {
    "type": "get",
    "url": "/v2/?tag=add_biller",
    "title": "Request for add biller",
    "name": "add_biller",
    "version": "1.0.0",
    "group": "Billers",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tag",
            "description": "<p>Tag for api.</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "user_id",
            "description": "<p>for login user_id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "biller_name",
            "description": "<p>for biller name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "biller_email",
            "description": "<p>for biller email</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "account",
            "description": "<p>for account</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "comment",
            "description": "<p>for comment</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response for Accept:",
	        "content": " HTTP/1.1 200 OK\n{\ndata: {\nuser_biller_id: 1,\n     user_id: 9,\n     biller_id: 1,\n     biller_name: \"John\",\n     biller_email: \"john@xyz.com\",\n     account: \"XYZ123456\",\n     comment: \"XYZ is equal to zyx\",\n     status: 1,\n     added_at: \"2016-06-01 10:00:00\",\n     updated_at: \"2016-06-01 10:00:00\"\n       },\nmessage: \"Your biller added successfully.\",\nstatus: 1\n}",
          "type": "json"
        },
        {
          "title": "Success-Response for Reject:",
          "content": " HTTP/1.1 200 OK\n{\ndata: [ ],\nmessage: \"Biller not added. Please try again later.\",\nstatus: 1\n}",
          "type": "json"
        }
      ]
    },
	  "filename": "api/v5/tag/add_biller.php",
    "groupTitle": "Billers"
  },
  {
    "type": "get",
    "url": "/v2/?tag=delete_biller",
    "title": "Request for delete biller",
    "name": "delete_biller",
    "version": "1.0.0",
    "group": "Billers",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tag",
            "description": "<p>Tag for api.</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "user_biller_id",
            "description": "<p>for user_biller_id</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "user_id",
            "description": "<p>for user_id (login user id)</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response for Accept:",
          "content": " HTTP/1.1 200 OK\n{\ndata: {},\nmessage: \"Biller deleted successfully.\",\nstatus: 1\n}",
          "type": "json"
        },
        {
          "title": "Success-Response for Reject:",
          "content": " HTTP/1.1 200 OK\n{\ndata: [ ],\nmessage: \"Biller not deleted. Please try again later.\",\nstatus: 1\n}",
          "type": "json"
        }
      ]
    },
	  "filename": "api/v5/tag/delete_biller.php",
    "groupTitle": "Billers"
  },
  {
    "type": "get",
    "url": "/v2/?tag=edit_biller",
    "title": "Request for edit biller",
    "name": "edit_biller",
    "version": "1.0.0",
    "group": "Billers",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tag",
            "description": "<p>Tag for api.</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "user_biller_id",
            "description": "<p>for user_biller_id</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "user_id",
            "description": "<p>for login user_id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "biller_name",
            "description": "<p>for biller name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "account",
            "description": "<p>for account</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "comment",
            "description": "<p>for comment</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response for Accept:",
          "content": " HTTP/1.1 200 OK\n{\ndata: {},\nmessage: \"Biller details updated successfully.\",\nstatus: 1\n}",
          "type": "json"
        },
        {
          "title": "Success-Response for Reject:",
          "content": " HTTP/1.1 200 OK\n{\ndata: [ ],\nmessage: \"Biller not updated. Please try again later.\",\nstatus: 1\n}",
          "type": "json"
        }
      ]
    },
	  "filename": "api/v5/tag/edit_biller.php",
    "groupTitle": "Billers"
  },
  {
    "type": "get",
	  "url": "/v2/?tag=admin_details",
	  "title": "Request admin details",
    "name": "GetCMS",
    "version": "1.0.0",
    "group": "CMS",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tag",
            "description": "<p>Tag for api.</p>"
          },
	        {
		        "group": "Parameter",
		        "type": "Number",
		        "optional": false,
		        "field": "user_id",
		        "description": "<p>login user id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "data",
            "description": "<p>Response data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
	        "content": " HTTP/1.1 200 OK\n{\ndata: [\n         {\n             id: \"2\",\n             accountnumber: \"1323f\",\n             bankname: \"icici\",\n             isfc_code: \"654\",\n             bankaddress: \"102,smita tower\",\n             name_of_beneficiary: \"35\",\n             address_of_beneficiary: \"memnagr\",\n             mobile_number: \"963264848\"\n         },\n         {\n              id: \"1\",\n             accountnumber: \"6565as\",\n             bankname: \"hdfc\",\n             isfc_code: \"654\",\n             bankaddress: \"102,smita tower\",\n             name_of_beneficiary: \"35\",\n             address_of_beneficiary: \"memnagr\",\n             mobile_number: \"963264848\"\n          }\n     ],\nmessage: \"admin details\",\nstatus: 1\n}",
          "type": "json"
        }
      ]
    },
	  "filename": "api/v5/tag/admin_details.php",
    "groupTitle": "CMS"
  },
  {
    "type": "get",
	  "url": "/v2/?tag=cms",
	  "title": "Request CMS information",
    "name": "GetCMS",
    "version": "1.0.0",
    "group": "CMS",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tag",
            "description": "<p>Tag for api.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "data",
            "description": "<p>Response data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
	        "content": "HTTP/1.1 200 OK\n{\n    \"data\": [\n        {\n            id: 3,\n            type: \"terms_condition\",\n            data: \"Contrary to popular belief, Lorem Ipsum is not simply random text...\"\n        },\n        {\n            id: 2,\n            type: \"terms_condition\",\n            data: \"Contrary to popular belief, Lorem Ipsum is not simply random text...\"\n        }\n    ],\n    \"status\": 1,\n    \"message\": \"\"\n}",
          "type": "json"
        }
      ]
    },
	  "filename": "api/v5/tag/cms.php",
    "groupTitle": "CMS"
  },
  {
    "type": "get",
    "url": "/v2/?tag=unread_count",
    "title": "Unread Notifications",
    "name": "Get_Unread_Notifications",
    "version": "1.0.0",
    "group": "Notification",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tag",
            "description": "<p>Tag for api.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>(Required) login user id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "data",
            "description": "<p>Response data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"data\": [\n        {\n            unread_count:3\n        }\n    ],\n    \"status\": 1,\n    \"message\": \"Unread notifications.\"\n}",
          "type": "json"
        }
      ]
    },
	  "filename": "api/v5/tag/unread_count.php",
    "groupTitle": "Notification"
  },
  {
    "type": "get",
    "url": "/v2/?tag=notificationdetails",
    "title": "Request notification details",
    "name": "Notification_Details",
    "version": "1.0.0",
    "group": "Notification",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tag",
            "description": "<p>Tag for api.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>give login user id</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "notificationid",
            "description": "<p>give unread notification id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Response data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n{\n     data:\n     [\n         {\n             notification_id: \"5\",\n             transaction_id: \"9\",\n             eRead: \"1\",\n             to_user_id: \"1\",\n             from_user_id: \"2\",\n             type: \"request\",\n             comment: \"comment\",\n             date: \"31 October 2015\"\n             name: \"name\"\n         }\n     ],\n     message: \"notification details\",\n     status: 1\n}",
          "type": "json"
        }
      ]
    },
	  "filename": "api/v5/tag/notificationdetails.php",
    "groupTitle": "Notification"
  },
  {
    "type": "get",
    "url": "/v2/?tag=notificationlist",
    "title": "Request Notification List",
    "name": "Notification_List",
    "version": "1.0.0",
    "group": "Notification",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tag",
            "description": "<p>Tag for api.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>give login user id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "data",
            "description": "<p>Response data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n{\n     data:\n     [\n         {\n             notification_id: \"5\",\n             transaction_id: \"9\",\n             eRead: \"1\",\n             to_user_id: \"1\",\n             from_user_id: \"2\",\n             type: \"request\",\n             comment: \"comment\",\n             date: \"31 October 2015\"\n             name: \"name\"\n         }\n     ],\n     message: \"notification data list\",\n     status: 1\n}",
          "type": "json"
        }
      ]
    },
	  "filename": "api/v5/tag/notificationlist.php",
    "groupTitle": "Notification"
  },
  {
    "type": "get",
    "url": "/v4/?tag=list_promotions",
    "title": "Request for all promotions",
    "name": "Promotions_List",
    "version": "1.0.0",
    "group": "Promotions",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tag",
            "description": "<p>Tag for api.</p>"
          },
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "offset",
            "description": "<p>(optional) Offset for api.</p>"
          },
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "limit",
            "description": "<p>(optional) limit for api.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "timestamp",
            "description": "<p>(optional) timestamp for api.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "search",
            "description": "<p>(optional) search promotions .</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "data",
            "description": "<p>Response data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
	        "content": " HTTP/1.1 200 OK\n{\n     data:\n     [\n         {\n          ...\n         },\n           {....},\n              .\n              .\n              .\n           {....}\n     ],\n     message: \"Promotions list\",\n     status: 1\n}",
          "type": "json"
        }
      ]
    },
	  "filename": "api/v5/tag/list_promotions.php",
    "groupTitle": "Promotions"
  },
  {
    "type": "get",
    "url": "/v3/?tag=add_subUser",
    "title": "Request Sign Up for sub user",
    "name": "Add_Sub_User",
    "version": "1.0.0",
    "group": "sub_user_manage",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tag",
            "description": "<p>Tag for api.</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "user_id",
            "description": "<p>Main user Id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>User Name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "employee_id",
            "description": "<p>User (Unique Employee Id)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>User password</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>User description(Optional)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Response data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
	        "content": "HTTP/1.1 200 OK\n{\n    \"data\": {\n          \"id\" : 1,\n          \"name\" : \"name\",\n          \"employee_id\" : \"EmployeeID\",\n          \"unique_token\" : \"unique_token\",\n          \"description\" : \"description\",\n          \"parent_user_id\" : \"parent_user_id\",\n          \"status\" : \"status\",\n    },\n    \"status\": 1,\n    \"message\": \"Sub-User added successfully.\"\n}",
          "type": "json"
        }
      ]
    },
	  "filename": "api/v5/tag/add_subUser.php",
    "groupTitle": "sub_user_manage"
  },
  {
    "type": "get",
    "url": "/v3/?tag=list_subUser",
    "title": "Request for Main user's sub user list",
    "name": "Sub_user_s_List",
    "version": "1.0.0",
    "group": "sub_user_manage",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tag",
            "description": "<p>Tag for api.</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "user_id",
            "description": "<p>give Main user user_id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "data",
            "description": "<p>Response data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
	        "content": " HTTP/1.1 200 OK\n{\n     data:\n     [\n         {\n             \"id\": \"194\",\n               \"name\": \"Joseph123\",\n               \"employee_id\": \"Flex_123\",\n               \"description\": \"Test\",\n               \"parent_user_id\": \"191\",\n               \"unique_token\": \"0fByN6zS\"\n         },\n           {....},\n              .\n              .\n              .\n           {....}\n     ],\n     message: \"Sub users list\",\n     status: 1\n}",
          "type": "json"
        }
      ]
    },
	  "filename": "api/v5/tag/list_subUser.php",
    "groupTitle": "sub_user_manage"
  },
  {
    "type": "get",
    "url": "/v3/?tag=delete_subUser",
    "title": "Request for delete sub user",
    "name": "delete_subUser",
    "version": "1.0.0",
    "group": "sub_user_manage",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tag",
            "description": "<p>Tag for api.</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "user_id",
            "description": "<p>for main user's user_id</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "id",
            "description": "<p>sub user_id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response for Accept:",
          "content": " HTTP/1.1 200 OK\n{\ndata: {},\nmessage: \"Sub user deleted successfully.\",\nstatus: 1\n}",
          "type": "json"
        }
      ]
    },
	  "filename": "api/v5/tag/delete_subUser.php",
    "groupTitle": "sub_user_manage"
  },
  {
    "type": "get",
    "url": "/v3/?tag=edit_subUser",
    "title": "Request update subuser",
    "name": "edit_subUser",
    "version": "1.0.0",
    "group": "sub_user_manage",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tag",
            "description": "<p>Tag for api.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>give name</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "user_id",
            "description": "<p>Main user's user_id</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "id",
            "description": "<p>sub user's user_id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "employee_id",
            "description": "<p>User (Unique Employee Id)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>User description(Optional)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Response data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
	        "content": " HTTP/1.1 200 OK\n{\ndata: {\n         id\": \"194\",\n           \"name\": \"Joseph123\",\n           \"employee_id\": \"Flex_123\",\n           \"description\": \"Test\",\n           \"parent_user_id\": \"191\",\n           \"unique_token\": \"0fByN6zS\"\n           \"status\": \"status\"\n     },\nmessage: \"Sub user's profile updated successfully.\",\nstatus: 1\n}",
          "type": "json"
        }
      ]
    },
	  "filename": "api/v5/tag/edit_subUser.php",
    "groupTitle": "sub_user_manage"
  },
  {
    "type": "get",
    "url": "/v3/?tag=get_subUser",
    "title": "Request get detail of sub user",
    "name": "get_sub_user",
    "version": "1.0.0",
    "group": "sub_user_manage",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tag",
            "description": "<p>Tag for api.</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "id",
            "description": "<p>give sub user's user id (required)</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "user_id",
            "description": "<p>give main user's user id (required)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "device_id",
            "description": "<p>give user's device device id (required)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Response data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n{\n     data: {\n             id: \"1\",\n             name: \"name\",\n             employee_id: \"employee_id\",\n             unique_token: \"u1ktmv4t\",\n             image: \"http://localhost/flexpay/uploads/default.png\",\n             description : \"description\",\n             parent_user_id : \"parent_user_id\",\n             status : \"1\"\n         },\nmessage: \"Details of sub user.\",\nstatus: 1\n}",
          "type": "json"
        }
      ]
    },
	  "filename": "api/v5/tag/get_subUser.php",
    "groupTitle": "sub_user_manage"
  },
  {
    "type": "get",
    "url": "/v3/?tag=login_subUser",
    "title": "Request login of sub user",
    "name": "login_sub_user",
    "version": "1.0.0",
    "group": "sub_user_manage",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tag",
            "description": "<p>Tag for api.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>give old password (required)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "employee_id",
            "description": "<p>give employee_id (required)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "device_id",
            "description": "<p>give device id (required)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "player_id",
            "description": "<p>give player id (optional)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "device_model",
            "description": "<p>give device modal name (optional)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "system_name",
            "description": "<p>give system name (optional)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "system_version",
            "description": "<p>give system version (optional)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "app_version",
            "description": "<p>give app version (optional)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "country_code",
            "description": "<p>give country code (optional)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "latitude",
            "description": "<p>give latitude (optional)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "longnitude",
            "description": "<p>give longnitude (optional)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Response data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
	        "content": " HTTP/1.1 200 OK\n{\n     data: {\n             id: \"1\",\n             name: \"name\",\n             employee_id: \"employee_id\",\n             unique_token: \"u1ktmv4t\",\n             image: \"http://localhost/flexpay/uploads/default.png\",\n             description : \"description\",\n             parent_user_id : \"parent_user_id\",\n             status : \"1\"\n               token: \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ\"\n         },\nmessage: \"Login successfully.\",\nstatus: 1\n}",
          "type": "json"
        }
      ]
    },
	  "filename": "api/v5/tag/login_subUser.php",
    "groupTitle": "sub_user_manage"
  },
  {
    "type": "get",
    "url": "/v3/?tag=logout_subUser",
    "title": "Logout sub User",
    "name": "logout_subUser",
    "version": "1.0.0",
    "group": "sub_user_manage",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tag",
            "description": "<p>Tag for api.</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "id",
            "description": "<p>give sub user's user id (required)</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "user_id",
            "description": "<p>give main user's user id (required)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "device_id",
            "description": "<p>give user's device device id (required)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "player_id",
            "description": "<p>give player id (optional)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Response data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n{\n     data: {},\nmessage: \"You are logout successfully.\",\nstatus: 1\n}",
          "type": "json"
        }
      ]
    },
	  "filename": "api/v5/tag/logout_subUser.php",
    "groupTitle": "sub_user_manage"
  },
  {
    "type": "get",
    "url": "/v3/?tag=reset_subUserPassword",
    "title": "Request for reset sub user password",
    "name": "reset_subUserPassword",
    "version": "1.0.0",
    "group": "sub_user_manage",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tag",
            "description": "<p>Tag for api.</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "user_id",
            "description": "<p>for main user's user_id</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "id",
            "description": "<p>sub user_id</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "password",
            "description": "<p>new password</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response for Accept:",
          "content": " HTTP/1.1 200 OK\n{\ndata: {},\nmessage: \"Sub user password reset successfully.\",\nstatus: 1\n}",
          "type": "json"
        }
      ]
    },
	  "filename": "api/v5/tag/reset_subUserPassword.php",
    "groupTitle": "sub_user_manage"
  },
  {
    "type": "get",
    "url": "/v3/?tag=notificationdetails_subUser",
    "title": "Request notification details of sub user",
    "name": "Notification_Detail_of_sub_user",
    "version": "1.0.0",
    "group": "sub_user_transactions",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tag",
            "description": "<p>Tag for api.</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "user_id",
            "description": "<p>give main user id</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "id",
            "description": "<p>give login user id</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "device_id",
            "description": "<p>give device id</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "notification_id",
            "description": "<p>give notification id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Response data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n{\n     data:\n     [\n         {\n             notification_id: \"5\",\n             transaction_id: \"9\",\n             eRead: \"1\",\n             to_user_id: \"1\",\n             from_user_id: \"2\",\n             type: \"request\",\n             comment: \"comment\",\n             date: \"31 October 2015\"\n             name: \"name\"\n         }\n     ],\n     message: \"notification details\",\n     status: 1\n}",
          "type": "json"
        }
      ]
    },
	  "filename": "api/v5/tag/notificationdetails_subUser.php",
    "groupTitle": "sub_user_transactions"
  },
  {
    "type": "get",
    "url": "/v3/?tag=notificationlist_subUser",
    "title": "Request Notification List of subUser",
    "name": "Notification_List_for_subUser",
    "version": "1.0.0",
    "group": "sub_user_transactions",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tag",
            "description": "<p>Tag for api.</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "user_id",
            "description": "<p>give main user user id</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "id",
            "description": "<p>give login user id</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "device_id",
            "description": "<p>give login device_id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "data",
            "description": "<p>Response data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n{\n     data:\n     [\n         {\n             notification_id: \"5\",\n             transaction_id: \"9\",\n             eRead: \"1\",\n             to_user_id: \"1\",\n             from_user_id: \"2\",\n             type: \"request\",\n             comment: \"comment\",\n             date: \"31 October 2015\"\n             name: \"name\"\n         }\n     ],\n     message: \"Sub User notification list\",\n     status: 1\n}",
          "type": "json"
        }
      ]
    },
	  "filename": "api/v5/tag/notificationlist_subUser.php",
    "groupTitle": "sub_user_transactions"
  },
  {
    "type": "get",
    "url": "/v3/?tag=acceptReject_subUser",
    "title": "Accept / Reject subUser's request",
    "name": "acceptReject_subUser",
    "version": "1.0.0",
    "group": "sub_user_transactions",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tag",
            "description": "<p>Tag for api.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Type '1 =&gt; accept','0 =&gt; reject'</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "security_pin",
            "description": "<p>Login User's Security Pin (Required when type=1).</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "receiver_id",
            "description": "<p>for Customer User id who will receive money.</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "transaction_id",
            "description": "<p>transaction id by which refund request initiated.</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "sender_id",
            "description": "<p>Login user's user id from which money is deducted.</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "id",
            "description": "<p>Sub user's user id.</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "device_id",
            "description": "<p>Sub user's device id.</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "amount",
            "description": "<p>Money amount to transfer.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "data",
            "description": "<p>Response data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response for Accept:",
          "content": " HTTP/1.1 200 OK\n{\ndata: [ ],\nmessage: \"Request accepted successfully.\",\nstatus: 1\n}",
          "type": "json"
        },
        {
          "title": "Success-Response for Reject:",
          "content": " HTTP/1.1 200 OK\n{\ndata: [ ],\nmessage: \"Request rejected successfully.\",\nstatus: 1\n}",
          "type": "json"
        }
      ]
    },
	  "filename": "api/v5/tag/acceptReject_subUser.php",
    "groupTitle": "sub_user_transactions"
  },
  {
    "type": "get",
    "url": "/v3/?tag=receiverMoney_subUser",
    "title": "Request receive money transaction details",
    "name": "receiver_money",
    "version": "1.0.0",
    "group": "sub_user_transactions",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tag",
            "description": "<p>Tag for api.</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "user_id",
            "description": "<p>main user's user  id.</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "id",
            "description": "<p>login user  id.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "device_id",
            "description": "<p>login user device id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "data",
            "description": "<p>Response data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n1)debit information\n{\ndata: [\n         {\n              transaction_id: \"1\",\n             transaction_unique_id: \"2295181649\",\n              name: \"dhruv123\",\n             amount: \"10\",\n             comments: \"\",\n             transaction_status: \"confirm\",\n             date: \"31 October 2015\"\n          },\n          {\n             transaction_id: \"3\",\n             transaction_unique_id: \"1715555938\",\n             name: \"dhruv123\",\n             amount: \"10\",\n             comments: \"\",\n             transaction_status: \"confirm\",\n             date: \"31 October 2015\"\n         },\n\n],\nmessage: \"Received money details\",\nstatus: 1\n}",
          "type": "json"
        }
      ]
    },
	  "filename": "api/v5/tag/receiverMoney_subUser.php",
    "groupTitle": "sub_user_transactions"
  },
  {
    "type": "get",
    "url": "/v3/?tag=refundRequest_subUser",
    "title": "Refund request from sub User",
    "name": "refundRequest_subUser",
    "version": "1.0.0",
    "group": "sub_user_transactions",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tag",
            "description": "<p>Tag for api.</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "receiver_id",
            "description": "<p>Main user's User id who will refund the money.</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "sender_id",
            "description": "<p>Customer user id who will get refund money.</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "id",
            "description": "<p>Sub User's user id.</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "transaction_id",
            "description": "<p>Transaction id for which customer needs refund.</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "amount",
            "description": "<p>Money amount to transfer.</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "device_id",
            "description": "<p>Sub User's device id.</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "comment",
            "description": "<p>comment (optional).</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "data",
            "description": "<p>Response data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n{\ndata: [ ],\nmessage: \"Refund request initiated successfully.\",\nstatus: 1\n}",
          "type": "json"
        }
      ]
    },
	  "filename": "api/v5/tag/refundRequest_subUser.php",
    "groupTitle": "sub_user_transactions"
  },
  {
    "type": "get",
	  "url": "/v5/?tag=add_money",
	  "title": "Request for add money",
	  "name": "add_money",
	  "version": "1.0.0",
	  "group": "transaction",
	  "parameter": {
		  "fields": {
			  "Parameter": [
				  {
					  "group": "Parameter",
					  "type": "String",
					  "optional": false,
					  "field": "tag",
					  "description": "<p>Tag for api.</p>"
				  },
				  {
					  "group": "Parameter",
					  "type": "String",
					  "optional": false,
					  "field": "paypal_id",
					  "description": "<p>(required) for paypal payment id</p>"
				  },
				  {
					  "group": "Parameter",
					  "type": "String",
					  "optional": false,
					  "field": "amount",
					  "description": "<p>(required) for amount to add</p>"
				  },
				  {
					  "group": "Parameter",
					  "type": "String",
					  "optional": false,
					  "field": "currency_code",
					  "description": "<p>or payment amount currency</p>"
				  },
				  {
					  "group": "Parameter",
					  "type": "String",
					  "optional": false,
					  "field": "short_description",
					  "description": "<p>for description during payment</p>"
				  },
				  {
					  "group": "Parameter",
					  "type": "String",
					  "optional": false,
					  "field": "processable",
					  "description": "<p>for processable status</p>"
				  },
				  {
					  "group": "Parameter",
					  "type": "String",
					  "optional": false,
					  "field": "environment",
					  "description": "<p>for type of payment Environment</p>"
				  },
				  {
					  "group": "Parameter",
					  "type": "String",
					  "optional": false,
					  "field": "paypal_sdk_version",
					  "description": "<p>for Paypal SDK version in APP</p>"
				  },
				  {
					  "group": "Parameter",
					  "type": "String",
					  "optional": false,
					  "field": "platform",
					  "description": "<p>for from which platform process is occurred</p>"
				  },
				  {
					  "group": "Parameter",
					  "type": "String",
					  "optional": false,
					  "field": "product_name",
					  "description": "<p>for payment product name</p>"
				  },
				  {
					  "group": "Parameter",
					  "type": "String",
					  "optional": false,
					  "field": "create_time",
					  "description": "<p>for paypal transaction time</p>"
				  },
				  {
					  "group": "Parameter",
					  "type": "String",
					  "optional": false,
					  "field": "intent",
					  "description": "<p>for intent type</p>"
				  },
				  {
					  "group": "Parameter",
					  "type": "String",
					  "optional": false,
					  "field": "state",
					  "description": "<p>(required) for paypal transaction status</p>"
				  },
				  {
					  "group": "Parameter",
					  "type": "String",
					  "optional": false,
					  "field": "response_type",
					  "description": "<p>for paypal transaction response type</p>"
				  }
			  ]
		  }
	  },
	  "success": {
		  "examples": [
			  {
				  "title": "Success-Response:",
				  "content": " HTTP/1.1 200 OK\n{\ndata: [ ],\nmessage: \"PayPal money added to your wallet successfully\",\nstatus: 1\n}",
				  "type": "json"
			  }
		  ]
	  },
	  "filename": "api/v5/tag/add_money.php",
	  "groupTitle": "transaction"
  },
	{
		"type": "get",
    "url": "/v2/?tag=receiver_details",
    "title": "Request receive and sending money transaction details",
    "name": "receiver_details",
    "version": "1.0.0",
    "group": "transaction",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tag",
            "description": "<p>Tag for api.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>give type 'debit','credit'</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "user_id",
            "description": "<p>login user  id.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "data",
            "description": "<p>Response data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
	        "content": " HTTP/1.1 200 OK\n1)debit information\n{\ndata: [\n         {\n              transaction_id: \"1\",\n             transaction_unique_id: \"2295181649\",\n              name: \"dhruv123\",\n             amount: \"10\",\n             comments: \"\",\n             transaction_status: \"confirm\",\n             date: \"31 October 2015\"\n          },\n          {\n             transaction_id: \"3\",\n             transaction_unique_id: \"1715555938\",\n             name: \"dhruv123\",\n             amount: \"10\",\n             comments: \"\",\n             transaction_status: \"confirm\",\n             date: \"31 October 2015\"\n         },\n\n],\nmessage: \"sender transaction details\",\nstatus: 1\n}\n2) credit information\n{\ndata: [\n{\ntransaction_id: \"2\",\ntransaction_unique_id: \"4440670964\",\nname: \"dhruv123\",\namount: \"10\",\ncomments: \"wdsd\",\ntransaction_status: \"pending\",\ndate: \"31 October 2015\"\n},\n{\ntransaction_id: \"7\",\ntransaction_unique_id: \"1078404093\",\nname: \"krishna23\",\namount: \"10\",\ncomments: \"adcd123\",\ntransaction_status: \"pending\",\ndate: \"31 October 2015\"\n}\n],\nmessage: \"reciver transaction details\",\nstatus: 1\n}",
          "type": "json"
        }
      ]
    },
		"filename": "api/v5/tag/receiver_details.php",
    "groupTitle": "transaction"
  },
  {
    "type": "get",
    "url": "/v4/?tag=maintainPlayerDeviceID",
    "title": "Delete player & device IDs",
    "name": "Clear_Player_Device_IDs",
    "version": "1.0.0",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tag",
            "description": "<p>Tag for api.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "player_id",
            "description": "<p>give player id (required)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "device_id",
            "description": "<p>give device id (required)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "null",
            "description": "<p>data</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
	        "content": " HTTP/1.1 200 OK\n{\n     data: null,\n       message: \"Device & Player ID are clear successfully.\",\n       status: 1\n}",
          "type": "json"
        }
      ]
    },
	  "filename": "api/v5/tag/maintainPlayerDeviceID.php",
    "groupTitle": "user"
  },
  {
    "type": "get",
    "url": "/v2/?tag=verifycode",
    "title": "Request Code Verification",
    "name": "Code_Verification_for_both_Forgot_password_and_Forgot_pin",
    "version": "1.0.0",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tag",
            "description": "<p>Tag for api.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>give email</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "verification_code",
            "description": "<p>give verification code</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "data",
            "description": "<p>Response data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n{\n     data: [],\n     message: \"Verification completed successfully.\",\n      status: 1\n}",
          "type": "json"
        }
      ]
    },
	  "filename": "api/v5/tag/verifycode.php",
    "groupTitle": "user"
  },
  {
    "type": "get",
    "url": "/v2/?tag=forgotpassword",
    "title": "Request Forgot password",
    "name": "Forgot_password",
    "version": "1.0.0",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tag",
            "description": "<p>Tag for api.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>give email</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Response data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n{\n     data: [],\n     message: \"Verification code is sent on you mail.\",\n      status: 1\n}",
          "type": "json"
        }
      ]
    },
	  "filename": "api/v5/tag/forgotpassword.php",
    "groupTitle": "user"
  },
  {
    "type": "get",
    "url": "/v2/?tag=forgotpin",
    "title": "Request Forgot pin",
    "name": "Forgot_pin",
    "version": "1.0.0",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tag",
            "description": "<p>Tag for api.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>give email</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Response data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n{\n     data: [],\n     message: \"Verification code is sent on you mail.\",\n      status: 1\n}",
          "type": "json"
        }
      ]
    },
	  "filename": "api/v5/tag/forgotpin.php",
    "groupTitle": "user"
  },
  {
    "type": "get",
    "url": "/v2/?tag=debit_credit_charges",
    "title": "Request Credit / debit charges information",
    "name": "Get_credit_debit",
    "version": "1.0.0",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tag",
            "description": "<p>Tag for api.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>(Required) login user id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "data",
            "description": "<p>Response data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"data\": [\n        {\n            user_transaction_rate_id: 3,\n            user_id: 3,\n            debit_rate: 3,\n            credit_rate: 3\n        }\n    ],\n    \"status\": 1,\n    \"message\": \"Transaction rates for user.\"\n}",
          "type": "json"
        }
      ]
    },
	  "filename": "api/v5/tag/debit_credit_charges.php",
    "groupTitle": "user"
  },
  {
    "type": "get",
    "url": "/v2/?tag=resetpassword",
    "title": "Request Reset Password",
    "name": "Reset_Password",
    "version": "1.0.0",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tag",
            "description": "<p>Tag for api.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>give email</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>give password</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "data",
            "description": "<p>Response data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n{\n     data: [],\n     message: \"Password changed successfully.\",\n      status: 1\n}",
          "type": "json"
        }
      ]
    },
	  "filename": "api/v5/tag/resetpassword.php",
    "groupTitle": "user"
  },
  {
    "type": "get",
    "url": "/v2/?tag=resetpin",
    "title": "Request Reset Pin",
    "name": "Reset_Pin",
    "version": "1.0.0",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tag",
            "description": "<p>Tag for api.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>give email</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>give password</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "data",
            "description": "<p>Response data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n{\n     data: [],\n     message: \"Pin changed successfully.\",\n      status: 1\n}",
          "type": "json"
        }
      ]
    },
	  "filename": "api/v5/tag/resetpin.php",
    "groupTitle": "user"
  },
  {
    "type": "get",
    "url": "/v2/?tag=signup",
    "title": "Request Sign Up",
    "name": "Sign_Up",
    "version": "1.0.0",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tag",
            "description": "<p>Tag for api.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>User Name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>User Email</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mobile",
            "description": "<p>User Mobile Number</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>User password</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "security_pin",
            "description": "<p>User Security Pin</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "company_name",
            "description": "<p><strong>(Optional)</strong> User Company Name.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Response data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
	        "content": " HTTP/1.1 200 OK\n {\n     \"data\": {\n\"id\" : 1,\n\"name\" : \"name\",\n\"email\" : \"email@gmail.com\",\n\"unique_token\" : \"unique_token\",\n     },\n     \"status\": 1,\n     \"message\": \"Congratulation registered successfully.\"\n }",
          "type": "json"
        }
      ]
    },
	  "filename": "api/v5/tag/signup.php",
    "groupTitle": "user"
  },
  {
    "type": "get",
    "url": "/v2/?tag=accept_reject",
    "title": "Request Accept / Reject",
    "name": "accept_reject",
    "version": "1.0.0",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tag",
            "description": "<p>Tag for api.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>give type '1 =&gt; accept','0 =&gt; reject' type='1'(use following parameter)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "security_pin",
            "description": "<p>User Security Pin common parameter</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "receiver_id",
            "description": "<p>for transfer money</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "transaction_id",
            "description": "<p>transaction update.</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "sender_id",
            "description": "<p>login user  id.</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "amount",
            "description": "<p>give amount to transfer</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "data",
            "description": "<p>Response data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response for Accept:",
          "content": " HTTP/1.1 200 OK\n{\ndata: [ ],\nmessage: \"Your request accepted successfully.\",\nstatus: 1\n}",
          "type": "json"
        },
        {
          "title": "Success-Response for Reject:",
          "content": " HTTP/1.1 200 OK\n{\ndata: [ ],\nmessage: \"Request rejected successfully.\",\nstatus: 1\n}",
          "type": "json"
        }
      ]
    },
	  "filename": "api/v5/tag/accept_reject.php",
    "groupTitle": "user"
  },
  {
    "type": "get",
    "url": "/v2/?tag=cancel_money_request",
    "title": "Request Cancel money request",
    "name": "cancel_money_request",
    "version": "1.0.0",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tag",
            "description": "<p>Tag for api.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sender_id",
            "description": "<p>Login user id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "transaction_id",
            "description": "<p>Cancel transaction id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "data",
            "description": "<p>Response data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n{\ndata: [ ],\nmessage: \"Money request cancelled successfully.\",\nstatus: 1\n}",
          "type": "json"
        }
      ]
    },
	  "filename": "api/v5/tag/cancel_money_request.php",
    "groupTitle": "user"
  },
  {
    "type": "get",
    "url": "/v2/?tag=changepassword",
    "title": "Request password changes",
    "name": "changepassword",
    "version": "1.0.0",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tag",
            "description": "<p>Tag for api.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "oldpassowrd",
            "description": "<p>give old password</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "user_id",
            "description": "<p>give login user id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "newpassowrd",
            "description": "<p>give new password</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "device_id",
            "description": "<p>give device id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "device_model",
            "description": "<p>give device modal name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "system_name",
            "description": "<p>give system  name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "system_version",
            "description": "<p>give system  version</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "app_version",
            "description": "<p>give app   version</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "country_code",
            "description": "<p>give country code</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "latitude",
            "description": "<p>give latitude</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "longnitude",
            "description": "<p>give longnitude</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Response data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n{\n     data: true,\n     message: \"password changes\",\n      status: 1\n}",
          "type": "json"
        }
      ]
    },
	  "filename": "api/v5/tag/changepassword.php",
    "groupTitle": "user"
  },
  {
    "type": "get",
    "url": "/v2/?tag=changepin",
    "title": "Request pin changes",
    "name": "changepin",
    "version": "1.0.0",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tag",
            "description": "<p>Tag for api.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "oldpin",
            "description": "<p>give old password</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "user_id",
            "description": "<p>give login user id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "newpin",
            "description": "<p>give new password</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "device_id",
            "description": "<p>give device id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "device_model",
            "description": "<p>give device modal name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "system_name",
            "description": "<p>give system  name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "system_version",
            "description": "<p>give system  version</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "app_version",
            "description": "<p>give app   version</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "country_code",
            "description": "<p>give country code</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "latitude",
            "description": "<p>give latitude</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "longnitude",
            "description": "<p>give longnitude</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Response data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n{\n     data: true,\n     message: \"pin changes\",\n     status: 1\n}",
          "type": "json"
        }
      ]
    },
	  "filename": "api/v5/tag/changepin.php",
    "groupTitle": "user"
  },
  {
    "type": "get",
    "url": "/v2/?tag=editprofile",
    "title": "Request profile update",
    "name": "editprofile",
    "version": "1.0.0",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tag",
            "description": "<p>Tag for api.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>give user name</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "user_id",
            "description": "<p>give login user id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mobile",
            "description": "<p>give user mobile</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "company",
            "description": "<p><strong>(optional)</strong>  give user company name</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Response data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n{\ndata: {\n         id: \"1\",\n         name: \"dhruvi\",\n         email: \"dhruvika.mi@gmail.com\",\n         mobile: \"9874561230\",\n         company: \"mi\"\n         },\nmessage: \"profile update\",\nstatus: 1\n}",
          "type": "json"
        }
      ]
    },
	  "filename": "api/v5/tag/editprofile.php",
    "groupTitle": "user"
  },
  {
    "type": "get",
    "url": "/v2/?tag=login",
    "title": "Request login",
    "name": "login",
    "version": "1.0.0",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tag",
            "description": "<p>Tag for api.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>give old password</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>give email</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "device_id",
            "description": "<p>give device id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "player_id",
            "description": "<p>give player id (optional)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "device_model",
            "description": "<p>give device modal name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "system_name",
            "description": "<p>give system  name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "system_version",
            "description": "<p>give system  version</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "app_version",
            "description": "<p>give app   version</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "country_code",
            "description": "<p>give country code</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "latitude",
            "description": "<p>give latitude</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "longnitude",
            "description": "<p>give longnitude</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Response data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
	        "content": " HTTP/1.1 200 OK\n{\n     data: {\n             id: \"1\",\n             name: \"Chintan\",\n             email: \"chintan.mindinventory@gmail.com\",\n             mobile: \"1234567890\",\n             company: \"1_qr_code.png\",\n             pin: \"123456\",\n             token: \"u1ktmv4t\",\n             image: \"http://localhost/flexpay/uploads/default.png\",\n             wallet_amount: \"60900\",\n             notification: null,\n             sessionid: \"rjh60ljbs6uabe2vqgq8gh5rb6\"\n               token: \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ\"\n         },\nmessage: \"login details\",\nstatus: 1\n}",
          "type": "json"
        }
      ]
    },
	  "filename": "api/v5/tag/login.php",
    "groupTitle": "user"
  },
  {
    "type": "get",
    "url": "/v3/?tag=logout",
    "title": "Logout User",
    "name": "logout_for_main_User",
    "version": "1.0.0",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tag",
            "description": "<p>Tag for api.</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "user_id",
            "description": "<p>give main user's user id (required)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "player_id",
            "description": "<p>give player id (optional)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "device_id",
            "description": "<p>give device id (required)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "null",
            "description": "<p>data Response data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n{\n     data: null,\nmessage: \"You are logout successfully.\",\nstatus: 1\n}",
          "type": "json"
        }
      ]
    },
	  "filename": "api/v5/tag/logout.php",
    "groupTitle": "user"
  },
  {
    "type": "get",
    "url": "/v2/?tag=request_money",
    "title": "Request request money",
    "name": "request_money",
    "version": "1.0.0",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tag",
            "description": "<p>Tag for api.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>give type 'qr_code','email' type='qr_code'(use following parameter)</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "receiver_id",
            "description": "<p>User id for transfer money type='email'(use following parameter)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>User Email common parameter</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "comment",
            "description": "<p>User comment</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "sender_id",
            "description": "<p>login user  id.</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "amount",
            "description": "<p>give amount to transfer</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "data",
            "description": "<p>Response data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n{\ndata: [ ],\nmessage: \"Request send successfully.\",\nstatus: 1\n}",
          "type": "json"
        }
      ]
    },
	  "filename": "api/v5/tag/request_money.php",
    "groupTitle": "user"
  },
  {
    "type": "get",
    "url": "/v4/?tag=send_money",
    "title": "Request Send Money",
    "name": "send_money",
    "version": "1.0.0",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tag",
            "description": "<p>Tag for api.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>give type 'qr_code','email' type='qr_code'(use following parameter)</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "receiver_id",
            "description": "<p>User id for transfer money type='email'(use following parameter)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>User Email common parameter</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "security_pin",
            "description": "<p>User Security Pin</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "sender_id",
            "description": "<p>login user  id.</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "amount",
            "description": "<p>give amount to transfer</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "transaction_id",
            "description": "<p>by default Optional/contain value when transaction is for refund</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "data",
            "description": "<p>Response data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n{\ndata: [ ],\nmessage: \"wallet amount send successfuly .\",\nstatus: 1\n}",
          "type": "json"
        }
      ]
    },
	  "filename": "api/v5/tag/send_money.php",
    "groupTitle": "user"
  },
  {
    "type": "Post",
    "url": "/v4/?tag=updateTimezone",
    "title": "Request for updateTimezone",
    "name": "updateTimezone",
    "version": "1.0.0",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tag",
            "description": "<p>Tag for api.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "timezone_str",
            "description": "<p>for Timezone in string (Asia/Kolkata)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "timezone_time",
            "description": "<p>for Timezone in time (GMT+05:30)</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response for Accept:",
          "content": " HTTP/1.1 200 OK\n{\ndata: {},\nmessage: \"User timezone updated successfully.\",\nstatus: 1\n}",
          "type": "json"
        },
        {
          "title": "Success-Response for Reject:",
          "content": " HTTP/1.1 200 OK\n{\ndata: [ ],\nmessage: \"Timezone not updated. Please try again later.\",\nstatus: 1\n}",
          "type": "json"
        }
      ]
    },
	  "filename": "api/v5/tag/updateTimezone.php",
    "groupTitle": "user"
  },
  {
    "type": "get",
    "url": "/v3/?tag=user_device_registration",
    "title": "Register User device and player id for notification.",
    "name": "user_device_registration",
    "version": "1.0.0",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tag",
            "description": "<p>Tag for api.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "user_id",
            "description": "<p>give user id (required)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "player_id",
            "description": "<p>give player id (optional)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "device_id",
            "description": "<p>give device id (required)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "null",
            "description": "<p>data</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
	        "content": " HTTP/1.1 200 OK\n{\n     data: null,\n       message: \"User device registered successfully.\",\n       status: 1\n}",
          "type": "json"
        }
      ]
    },
	  "filename": "api/v5/tag/user_device_registration.php",
    "groupTitle": "user"
  },
  {
    "type": "get",
    "url": "/v2/?tag=user_wallet",
    "title": "Request give user wallet amount",
    "name": "user_wallet",
    "version": "1.0.0",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tag",
            "description": "<p>Tag for api.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>give login user id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Response data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n{\n     data: {\n             wallet_amount: \"0\"\n          },\nmessage: \"user wallet amount\",\nstatus: 1\n}",
          "type": "json"
        }
      ]
    },
	  "filename": "api/v5/tag/user_wallet.php",
    "groupTitle": "user"
  }
] });
