<?php
/**
 * @api {get} /v3/?tag=notificationlist_subUser Request Notification List of subUser
 * @apiName Notification List for subUser
 * @apiVersion 1.0.0
 * @apiGroup sub user transactions
 *
 * @apiParam {String} tag Tag for api.
 * @apiParam {integer} user_id give main user user id
 * @apiParam {integer} id give login user id
 * @apiParam {integer} device_id give login device_id
 * @apiSuccess {array} data Response data.
 * @apiSuccess {String} message Message.
 * @apiSuccess {Number} status Status.
 *
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 200 OK
 * {
 *      data:
 *      [
 *          {
 *              notification_id: "5",
 *              transaction_id: "9",
 *              eRead: "1",
 *              to_user_id: "1",
 *              from_user_id: "2",
 *              type: "request",
 *              comment: "comment",
 *              date: "31 October 2015"
 *              name: "name"
 *          }
 *      ],
 *      message: "Sub User notification list",
 *      status: 1
 * }
 *
 */


use MI\API\Response as response;
use MI\GEN as api;

//$_SERVER['REQUEST_METHOD'] == "POST" or api::error(0, \MI\API\Message::$invalidMethod);
$user_id = isset($_REQUEST['user_id']) && $_REQUEST['user_id'] != '' ? $_REQUEST['user_id'] : api::error(0, \MI\API\Message::$invalideuser_id);
$id	=	isset($_REQUEST['id']) && $_REQUEST['id'] != '' ? $_REQUEST['id'] : api::error(0, \MI\API\Message::$invalid_subuser_id);
$device_id	=	isset($_REQUEST['device_id']) && $_REQUEST['device_id'] != '' ? $_REQUEST['device_id'] : api::error(0, \MI\API\Message::$Invalid_device_info);


$user = new  user();
$uld = new user_login_devices();
$login_chk = $uld->checkSubUserDeviceLogin($id, $device_id);
$userdata = $user->select($user_id);
$subUser_data = $user->select($id);
$notificationlist = new notification();

if (count($userdata) > 0) {
	if(count($subUser_data) > 0 && $subUser_data[0]['status'] == 1){
		if($login_chk){
			$notificatiodetails = $notificationlist->notificationdetailsSubUser($user_id,$id);
			#pr($notificatiodetails);exit;
			foreach ($notificatiodetails AS $notificatiodata) {
				$responsedata = new MI\API\Response\V1\NotificationResponse();
				$responsedata->notification_id = $notificatiodata['notification_id'];
				$responsedata->transaction_id = $notificatiodata['transaction_id'];
				$responsedata->refunded_transaction_id = $notificatiodata[0]['refunded_transaction_id'];
				$responsedata->refunded = $notificatiodata[0]['refunded'];
				$responsedata->eRead = $notificatiodata['eRead'];
				$responsedata->to_user_id = $notificatiodata['to_user_id'];
				$responsedata->from_user_id = $notificatiodata['from_user_id'];
				$responsedata->type = $notificatiodata['type'];
				$responsedata->comment = $notificatiodata['comment'];
				$responsedata->name = $notificatiodata['name'];
				$responsedata->email = $notificatiodata['email'];
				/*
						if($notificatiodata['name'] == "")
						{
							$responsedata->name = $ADMIN_NAME;
						}
						else{
							$responsedata->name = $notificatiodata['name'];
						}

						if($notificatiodata['email'] == "")
						{
							$responsedata->email =  $ADMIN_EMAIL;
						}
						else{
							$responsedata->email = $notificatiodata['email'];
						}*/

				$responsedata->amount	=	$notificatiodata['amount'];
				$responsedata->via_user_id	=	$notificatiodata['via_user_id'];
				$responsedata->via_user_name	=	$notificatiodata['via_user_name'];
				$responsedata->parent_user_name	=	$notificatiodata['parent_user_name'];
				$responsedata->parent_user_email	=	$notificatiodata['parent_user_email'];
				$responsedata->date	=	$generalfuncobj->full_date_formate($notificatiodata['added_at']);
				$responsedata->date_timestamp	=	$generalfuncobj->date_timestamp($notificatiodata['added_at']);

				$data[] = $responsedata;
			}
			if(count($data) > 0)
			{
				api::success($data, 1, 'Sub User notifications list.');
			}else{
				api::success($data, 1, 'No more notification.');
			}

		}else{
			api::error(9, \MI\API\Message::$login_required);
		}
	}else{
		api::error(9, \MI\API\Message::$login_required);
	}
} else {
    api::error(0, "user not register");
}
