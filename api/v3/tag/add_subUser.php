<?php

/**
 * @api {get} /v3/?tag=add_subUser Request Sign Up for sub user
 * @apiRequestMethod POST
 * @apiName Add Sub User
 * @apiBase http://localhost/
 * @apiVersion 1.0.0
 * @apiGroup sub user manage
 *
 * @apiParam {String} tag Tag for api.
 * @apiParam {integer} user_id Main user Id
 * @apiParam {String} name User Name
 * @apiParam {String} employee_id User (Unique Employee Id)
 * @apiParam {String} password User password
 * @apiParam {String} description User description(Optional)
 *
 * @apiSuccess {Object} data Response data.
 * @apiSuccess {String} message Message.
 * @apiSuccess {Number} status Status.
 *
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 200 OK
 *  {
 *      "data": {
 *			"id" : 1,
 *			"name" : "name",
 *			"employee_id" : "EmployeeID",
 *			"unique_token" : "unique_token",
 *			"description" : "description",
 *			"parent_user_id" : "parent_user_id",
 *			"status" : "status",
 *      },
 *      "status": 1,
 *      "message": "Sub-User added successfully."
 *  }
 *
 */

use MI\API\Response as response;
use MI\GEN as api;
use Endroid\QrCode\QrCode;
$qrCode = new QrCode();
$sendGridEmail = new SendGridLocal();
//pr($_REQUEST);exit;
$_SERVER['REQUEST_METHOD'] == "POST" or api::error(0, \MI\API\Message::$invalidMethod);

## Validation
$id = isset($_REQUEST['user_id']) && $_REQUEST['user_id'] != '' ? $_REQUEST['user_id'] : api::error(0, \MI\API\Message::$invalid_user_id);
$name = isset($_REQUEST['name']) && $_REQUEST['name'] != '' ? mysqli_real_escape_string($obj->CONN,$_REQUEST['name']) : api::error(0, \MI\API\Message::$invalid_sign_name);
$employee_id= isset($_REQUEST['employee_id']) && $_REQUEST['employee_id'] != '' ? $_REQUEST['employee_id'] : api::error(0, \MI\API\Message::$invalid_employee_id);
$password = isset($_REQUEST['password']) && $_REQUEST['password'] != '' ? $_REQUEST['password'] : api::error(0, \MI\API\Message::$invalid_sign_password);
$description = (isset($_REQUEST['description'])) ? mysqli_real_escape_string($obj->CONN,$_REQUEST['description']) : '';

$user = new user();
$wallet =new wallet();
$user_transaction = new user_transaction_rate();
$USERDATA = $user->select($user_id);
$USER_RATE_DATA = $user_transaction->user_transaction_data($user_id);

if(!preg_match('/[^a-zA-Z0-9\-_]/', $employee_id)){
	if (count($USERDATA[0]) > 0) {
		if($user->check_user_employeeid($employee_id)) {
			$unique_token = $generalfuncobj->generateUniqueToken_with_special_character(8);

			$debit_rate		=	$USER_RATE_DATA[0]['debit_rate'];
			$credit_rate	=	$USER_RATE_DATA[0]['credit_rate'];
			$spend_limit	=	$USERDATA[0]['transfer_limit'];

			## Set Data
			$user->setUniqueToken($unique_token);
			$user->setemployee_id($employee_id);
			$user->setparent_user_id($id);
			$user->setdescription($description);
			$user->setname($name);
			$user->setemail('');
			$user->setmobile('');
			$user->setpassword(md5($password));
			$user->setsecurity_pin('');
			$user->setcompany_name('');
			$user->settransfer_limit($spend_limit);
			$user->setadded_at($generalfuncobj->gm_date());
			$user->setstatus('1');

			## insert
			$user_id = $user->insert();
			$user_transaction->setuser_id($user_id);
			$user_transaction->setcredit_rate($credit_rate);
			$user_transaction->setdebit_rate($debit_rate);
			$user_transaction->insert();
			$wallet->setuser_id($user_id);
			$wallet->setamount('0');
			$wallet->setupdated_at($generalfuncobj->gm_date());
			$wallet->setadded_at($generalfuncobj->gm_date());
			$wallet->setstatus('1');
			$wallet->insert();
			## Qr Code
			$USER = array(
				'id' => $user_id,
				'name' => $name,
				'employee_id' => $employee_id,
				'parent_user_id' => $id,
				'unique_token' => $unique_token,
			);

			$src = $qrCode
				->setText(base64_encode(json_encode($USER)))
				->setSize(300)
				->setPadding(10)
				->setErrorCorrection('high')
				->setForegroundColor(array('r' => 0, 'g' => 0, 'b' => 0, 'a' => 0))
				->setBackgroundColor(array('r' => 255, 'g' => 255, 'b' => 255, 'a' => 0))
				->setLabelFontSize(16)
				->get('png');

			## Upload QR code
			$QRCODE = $user_id.'_qr_code.png';
			$generalfuncobj->saveToFile($src,$Qrcode_image_path.$QRCODE);

			## Update Qr Code
			$user->update_qr_code($user_id,$QRCODE);

			## Mail
//		$LINK = $site_url.'verifyuser.php?id='.base64_encode($user_id);
//		$sendGridEmail->signup($email,$name,$LINK);

			$data = $USER;
			$data['description'] = $description;
			//$data['parent_user_id'] = $id;
			$data['status'] = '1';

			api::success($data,1,\MI\API\Message::$subuser_sign_success);

		}
		else{
			api::error(0, \MI\API\Message::$sign_eid_already);
		}
	}
	else {
		api::error(0, \MI\API\Message::$invalid_user_id);
	}
}else{
	api::error(0, \MI\API\Message::$invalid_sign_employee_id);
}
