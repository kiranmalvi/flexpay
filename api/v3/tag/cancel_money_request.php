<?php

/**
 * @api {get} /v2/?tag=cancel_money_request Request Cancel money request
 * @apiName cancel_money_request
 * @apiBase http://localhost/
 * @apiVersion 1.0.0
 * @apiGroup user
 *
 * @apiParam {String} tag Tag for api.
 * @apiParam {String} sender_id Login user id
 * @apiParam {String} transaction_id Cancel transaction id
 *
 * @apiSuccess {array} data Response data.
 * @apiSuccess {String} message Message.
 * @apiSuccess {Number} status Status.
 *
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 200 OK
 * {
 * data: [ ],
 * message: "Money request cancelled successfully.",
 * status: 1
 * }
 *
 */
use MI\API\Response as response;
use MI\GEN as api;

//$_SERVER['REQUEST_METHOD'] == "POST" or api::error(0, \MI\API\Message::$invalidMethod);

## Validation
$sender_id = isset($_REQUEST['sender_id']) && $_REQUEST['sender_id'] != '' ? $_REQUEST['sender_id'] : api::error(0, \MI\API\Message::$invalid_request_money_sender_id);
$transaction_id = isset($_REQUEST['transaction_id']) && $_REQUEST['transaction_id'] != '' ? $_REQUEST['transaction_id'] : api::error(0, \MI\API\Message::$invalid_transaction_id);

$user = new user();
$tran = new transaction();
$gmt = $generalfuncobj->gm_date();

## Check Sender
if ($user->check_user_exsits($sender_id)) {

    $tran_data = $tran->select($transaction_id);

    if (count($tran_data) > 0) {
        $requester_id = $tran_data[0]['from_user_id'];
        $curr_status = $tran_data[0]['type'];

        ## Check owner id

        if ($sender_id == $requester_id) {
            if ($curr_status == 'request') {
                $tran->update_transaction_record($transaction_id, 'cancel', 'cancel', $gmt);

                api::success([], 1, "Money request cancelled successfully.");
            } else {
                api::error(0, "This request is already accepted / rejected.");
            }

        } else {
            api::error(0, "Invalid sender id.");
        }
    } else {
        api::error(0, \MI\API\Message::$invalid_transaction_id);
    }

} else {
    api::error(0, \MI\API\Message::$invalid_request_money_sender_id);
}