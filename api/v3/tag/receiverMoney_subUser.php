<?php
/**
 * @api {get} /v3/?tag=receiverMoney_subUser Request receive money transaction details
 * @apiName receiver money
 * @apiBase http://localhost/
 * @apiVersion 1.0.0
 * @apiGroup sub user transactions
 *
 * @apiParam {String} tag Tag for api.
 * @apiParam {integer} user_id  main user's user  id.
 * @apiParam {integer} id  login user  id.
 * @apiParam {string} device_id  login user device id
 * @apiSuccess {array} data Response data.
 * @apiSuccess {String} message Message.
 * @apiSuccess {Number} status Status.
 *
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 200 OK
 * 1)debit information
 * {
 * data: [
 *          {
 *               transaction_id: "1",
 *              transaction_unique_id: "2295181649",
 *               name: "dhruv123",
 *              amount: "10",
 *              comments: "",
 *              transaction_status: "confirm",
 *              date: "31 October 2015"
 *           },
 *           {
 *              transaction_id: "3",
 *              transaction_unique_id: "1715555938",
 *              name: "dhruv123",
 *              amount: "10",
 *              comments: "",
 *              transaction_status: "confirm",
 *              date: "31 October 2015"
 *          },
 *
 * ],
 * message: "Received money details",
 * status: 1
 * }
 */
use MI\API\Response as response;
use MI\GEN as api;

//$_SERVER['REQUEST_METHOD'] == "POST" or api::error(0, \MI\API\Message::$invalidMethod);
$user_id = isset($_REQUEST['user_id']) && $_REQUEST['user_id'] != '' ? $_REQUEST['user_id'] : api::error(0, \MI\API\Message::$invalideuser_id);
$id	=	isset($_REQUEST['id']) && $_REQUEST['id'] != '' ? $_REQUEST['id'] : api::error(0, \MI\API\Message::$invalid_subuser_id);
$device_id	=	isset($_REQUEST['device_id']) && $_REQUEST['device_id'] != '' ? $_REQUEST['device_id'] : api::error(0, \MI\API\Message::$Invalid_device_info);

$user = new user();
$transaction = new transaction();
$uld = new user_login_devices();
$userdata = $user->select($user_id);
$subuserdata = $user->select($id);
$login_chk = $uld->checkSubUserDeviceLogin($id, $device_id);

if (count($userdata) > 0 && $userdata[0]['status'] == 1) {
	if (count($subuserdata) > 0 && $subuserdata[0]['status'] == 1) {
		if ($login_chk) {
			$transactionlist = $transaction->receivedMoneySubUser($user_id, $id);
			foreach ($transactionlist AS $mytransaction) {

				$traresponse = new MI\API\Response\V1\ReciverResponse();
				$traresponse->transaction_id = $mytransaction['transaction_id'];
				$traresponse->transaction_unique_id = $mytransaction['transaction_unique_id'];
				$traresponse->name = $mytransaction['name'];
				$traresponse->email = $mytransaction['email'];


				/*if($mytransaction['name'] == "")
				{
					$traresponse->name = $ADMIN_NAME;
				}
				else{
					$traresponse->name = $mytransaction['name'];
				}

				if($mytransaction['email'] == "")
				{
					$traresponse->email =  $ADMIN_EMAIL;
				}
				else{
					$traresponse->email = $mytransaction['email'];
				}*/

				$traresponse->amount = $mytransaction['amount'];
				$traresponse->comments = $mytransaction['comment'];
				$traresponse->credit_rate = $mytransaction['credit_rate'];
				$traresponse->debit_rate = $mytransaction['debit_rate'];
				$traresponse->transaction_status = $mytransaction['transaction_status'];
				$traresponse->refunded_transaction_id = $mytransaction['refunded_transaction_id'];
				$traresponse->refunded = $mytransaction['refunded'];
				$traresponse->via_user_id = $mytransaction['via_user_id'];
				$traresponse->sender_id = $mytransaction['to_user_id'];
				$traresponse->date = $generalfuncobj->full_date_formate($mytransaction['added_at']);
				$traresponse->date_timestamp = $generalfuncobj->date_timestamp($mytransaction['added_at']);
				$data[] = $traresponse;

			}
			api::success($data, 1, "Receiver money details.");
		}else{
			api::error(9, \MI\API\Message::$login_required);
		}
	}else{
		api::error(9, \MI\API\Message::$login_required);
	}
} else {
	api::error(9, \MI\API\Message::$login_required);
}