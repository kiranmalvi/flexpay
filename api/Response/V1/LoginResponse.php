<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 14/10/15
 * Time: 6:07 PM
 */

namespace MI\API\Response\V1;


class LoginResponse
{

    public $id;

    public $name;
    public $email;
    public $mobile;
    public $company;
    public $pin;
    public $token;
    public $image;
    public $wallet_amount;
    public $notification;
    public $sessionid;


}