<?php
/**
 * Created by PhpStorm.
 * User: mind
 * Date: 31/10/15
 * Time: 7:07 PM
 */

namespace MI\API\Response\V1;


class NotificationResponse
{

    public $notification_id;
    public $transaction_id;
    public $eRead;
    public $to_user_id;
    public $from_user_id;
    public $type;
    public $comment;
    public $date;
    public $name;


}