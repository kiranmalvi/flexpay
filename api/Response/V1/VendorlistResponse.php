<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 14/10/15
 * Time: 6:07 PM
 */

namespace MI\API\Response\V1;


class VendorlistResponse
{

    public $vendorid;

    public $name;
    public $storename;
    public $rating;
    public $authorised;
    public $sponsor;
    public $authorizedtext;
    public $image;
    public $deliverytime;
    public $deliverydistance;

}