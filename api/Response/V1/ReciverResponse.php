<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 14/10/15
 * Time: 6:07 PM
 */

namespace MI\API\Response\V1;


class ReciverResponse
{

    public $transaction_id;
    public $transaction_unique_id;
    public $name;
    public $amount;
    public $comments;
    public $transaction_status;
    public $date;


}