<?php
include_once("../../miAutoload.php");

use MI\API\Message as message;
use MI\GEN as api;
use Respect\Validation\Validator as v;

if (json_decode(file_get_contents('php://input'), true)) {
    $_REQUEST = json_decode(file_get_contents('php://input'), true);
}
//pr($_REQUEST);exit;
$validator = v::create();
$validator->key('tag', v::string()->noWhitespace()->length(1, 25)->notEmpty())->validate($_REQUEST) or api::error(0, message::$invalidTag);
//pr($_REQUEST);exit;
unset($validator);

$tag = $_REQUEST['tag'];
$file = "tag/" . $tag . '.php';
//pr($_REQUEST);exit;

$userObj = new user();


if (!is_file($file)) {
    api::error(0, message::$invalidTag);
}
else {
/** @noinspection PhpIncludeInspection */
if(!in_array($tag,$CHECK_SESSION_API)) {

        // user_id in request
        if($tag == "changepin" || $tag == "editprofile" || $tag == "notificationdetails" || $tag == "notificationlist" || $tag == "receiver_details" || $tag == "user_wallet")
        {
            if(isset($_REQUEST['user_id']) && $_REQUEST['user_id'] != '' )
            {

                $user_id = $_REQUEST['user_id'];
                $user_data = $userObj->select($user_id);
                $user_status = $user_data[0]['status'];
                # pr($_REQUEST);exit;
                if($user_status != 1)
                {
                    if($user_status == 0)
                    {
                        $error_msg = message::$inactive_user_error;
                    }
                    else if($user_status == 2)
                    {
                        $error_msg = message::$delete_user_error;
                    }
                    else if($user_status == 3)
                    {
                        $error_msg = message::$block_user_error;
                    }
                    else if($user_status == 4)
                    {
                        $error_msg = message::$unverified_user_error;
                    }
                    else{
                        $error_msg = message::$user_error;
                    }
                    #pr($user_data);exit;
                    api::error(0, $error_msg);
                }
                else{
                    include_once($file);
                }
            }
            else{
                include_once($file);
            }
        }
        // sender_id as user_id in request
        else if($tag == "accept_reject" || $tag == "cancel_money_request" || $tag == "request_money" || $tag == "send_money")
        {
            if(isset($_REQUEST['sender_id']) && $_REQUEST['sender_id'] != '' )
            {

                $sender_id = $_REQUEST['sender_id'];
                $user_data = $userObj->select($sender_id);
                $user_status = $user_data[0]['status'];
                # pr($_REQUEST);exit;
                if($user_status != 1)
                {
                    if($user_status == 0)
                    {
                        $error_msg = message::$inactive_user_error;
                    }
                    else if($user_status == 2)
                    {
                        $error_msg = message::$delete_user_error;
                    }
                    else if($user_status == 3)
                    {
                        $error_msg = message::$block_user_error;
                    }
                    else if($user_status == 4)
                    {
                        $error_msg = message::$unverified_user_error;
                    }
                    else{
                        $error_msg = message::$user_error;
                    }
                    #pr($user_data);exit;
                    api::error(0, $error_msg);
                }
                else{
                    if(isset($_REQUEST['receiver_id']) && $_REQUEST['receiver_id'] != '' )
                    {
                        $receiver_id = $_REQUEST['receiver_id'];
                        $receiver_user_data = $userObj->select($receiver_id);
                        $receiver_user_status = $receiver_user_data[0]['status'];

                        if($receiver_user_status != 1)
                        {
                            $error_msg = message::$block_receiver_user_error;
                            api::error(0, $error_msg);
                        }
                        else{
                            include_once($file);
                        }
                    }
                    else{
                        include_once($file);
                    }
                }
            }
            else{
                include_once($file);
            }
        }
        // id as user_id in request
        else if($tag == "debit_credit_charges" || $tag == "unread_count")
        {
            if(isset($_REQUEST['id']) && $_REQUEST['id'] != '' )
            {

                $id = $_REQUEST['id'];
                $user_data = $userObj->select($id);
                $user_status = $user_data[0]['status'];
                # pr($_REQUEST);exit;
                if($user_status != 1)
                {
                    if($user_status == 0)
                    {
                        $error_msg = message::$inactive_user_error;
                    }
                    else if($user_status == 2)
                    {
                        $error_msg = message::$delete_user_error;
                    }
                    else if($user_status == 3)
                    {
                        $error_msg = message::$block_user_error;
                    }
                    else if($user_status == 4)
                    {
                        $error_msg = message::$unverified_user_error;
                    }
                    else{
                        $error_msg = message::$user_error;
                    }
                    #pr($user_data);exit;
                    api::error(0, $error_msg);
                }
                else{
                    include_once($file);
                }
            }
            else{
                include_once($file);
            }
        }
        else{
            include_once($file);
        }


    } else {
        if($_COOKIE['PHPSESSID'] == $_REQUEST['session_id']) {
            include_once($file);
        } else {
            \MI\GEN::error(0, "Oops!! Session is Expired");
        }
    }
}
\MI\GEN::error(0, "Dear User, please update your app version to enjoy uninterrupted services.");
