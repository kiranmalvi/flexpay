<?php
/**
 * @api {get} /v1/?tag=notificationdetails Request notification details
 * @apiName Notification Details
 * @apiVersion 1.0.0
 * @apiGroup Notification
 *
 * @apiParam {String} tag Tag for api.
 * @apiParam {Number} user_id give login user id
 * @apiParam {Number} notificationid give unread notification id
 * @apiSuccess {Object} data Response data.
 * @apiSuccess {String} message Message.
 * @apiSuccess {Number} status Status.
 *
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 200 OK
 * {
 *      data:
 *      [
 *          {
 *              notification_id: "5",
 *              transaction_id: "9",
 *              eRead: "1",
 *              to_user_id: "1",
 *              from_user_id: "2",
 *              type: "request",
 *              comment: "comment",
 *              date: "31 October 2015"
 *              name: "name"
 *          }
 *      ],
 *      message: "notification details",
 *      status: 1
 * }
 *
 */

use MI\API\Response as response;
use MI\GEN as api;

//$_SERVER['REQUEST_METHOD'] == "POST" or api::error(0, \MI\API\Message::$invalidMethod);

$userid = isset($_REQUEST['user_id']) && $_REQUEST['user_id'] != '' ? $_REQUEST['user_id'] : api::error(0, \MI\API\Message::$invalideuser_id);
$notificationid = isset($_REQUEST['notificationid']) && $_REQUEST['notificationid'] != '' ? $_REQUEST['notificationid'] : api::error(0, \MI\API\Message::$invalidenotificationid);
$notification = new notification();
$user = new user();
$userdata = $user->select($userid);
//echo "dbhdbv";
$notificationdata = $notification->select($notificationid);
if (count($userdata) > 0) {
    if (count($notificationdata) > 0) {
        $read_status = $notification->read_notification($notificationid);
        if($read_status == true)
        {
            $notificatiodetails = $notification->get_notification($notificationid, $userid);

            $data = new MI\API\Response\V1\NotificationResponse();
            $data->notification_id = $notificatiodetails[0]['notification_id'];
            $data->transaction_id = $notificatiodetails[0]['transaction_id'];
            $data->eRead = $notificatiodetails[0]['eRead'];
            $data->to_user_id = $notificatiodetails[0]['to_user_id'];
            $data->from_user_id = $notificatiodetails[0]['from_user_id'];
            $data->type = $notificatiodetails[0]['type'];
            $data->comment = $notificatiodetails[0]['comment'];
            $data->name = $notificatiodetails[0]['name'];
            $data->amount = $notificatiodetails[0]['amount'];
            $data->email = $notificatiodetails[0]['email'];


            /*if($notificatiodetails['name'] == "")
            {
                $data->name = $ADMIN_NAME;
            }
            else{
                $data->name = $notificatiodetails['name'];
            }

            if($notificatiodetails['email'] == "")
            {
                $data->email =  $ADMIN_EMAIL;
            }
            else{
                $data->email = $notificatiodetails['email'];
            }*/

            $data->date = $generalfuncobj->full_date_formate($notificatiodetails[0]['added_at']);

            api::success($data, 1, 'notification details');
        }
        api::success($data, 1, "notification read");
    } else {
        api::error(0, "notification not available");
    }
} else {
    api::error(0, "user not register");
}