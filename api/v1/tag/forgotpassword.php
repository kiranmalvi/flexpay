<?php
/**
 * @api {get} /v1/?tag=forgotpassword Request Forgot password
 * @apiName Forgot password
 * @apiVersion 1.0.0
 * @apiGroup user
 *
 * @apiParam {String} tag Tag for api.
 * @apiParam {String} email   give email
 * @apiSuccess {Object} data Response data.
 * @apiSuccess {String} message Message.
 * @apiSuccess {Number} status Status.
 *
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 200 OK
 * {
 *      data: [],
 *      message: "Verification code is sent on you mail.",
 *       status: 1
 * }
 *
 */

use MI\API\Response as response;
use MI\GEN as api;

$email = isset($_REQUEST['email']) && $_REQUEST['email'] != '' ? $_REQUEST['email'] : api::error(0, \MI\API\Message::$invalidemail);

$user = new  user();
$userdata = $user->check_user($email);
$gmt = $generalfuncobj->gm_date();
//pr($userdata);exit;
if (count($userdata) > 0) {

    $user_status = $userdata[0]['status'];
    $user_id = $userdata[0]['id'];
    $user_name = $userdata[0]['name'];
    $user_email = $userdata[0]['email'];

    if ($user_status == 1) {
        $verification_code = $generalfuncobj->generateUniqueToken(4);
//echo $verification_code;exit;
        $update_code = $user->update_code($verification_code, $user_id,$gmt);

        ## Mail
		$sendGridEmail = new SendGridLocal();
		$sendGridEmail->forgotpassword($email, $user_name, $verification_code);

        api::success([], 1, "Verification code is sent on your mail.");

    } else {
        api::error(0, "Invalid email");
    }

} else {
    api::error(0, "user not register");
}

