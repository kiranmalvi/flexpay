<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 31/10/15
 * Time: 3:54 PM
 */

use MI\API\Response as response;
use MI\GEN as api;

//$_SERVER['REQUEST_METHOD'] == "POST" or api::error(0, \MI\API\Message::$invalidMethod);

$transaction_id = isset($_REQUEST['transaction_id']) && $_REQUEST['transaction_id'] != '' ? $_REQUEST['transaction_id'] : api::error(0, \MI\API\Message::$invalid_transaction_id);
$action = isset($_REQUEST['action']) && $_REQUEST['action'] != '' ? $_REQUEST['action'] : api::error(0, \MI\API\Message::$invalid_action);

## Security Parameter
$session_id = isset($_REQUEST['session_id']) && $_REQUEST['session_id'] != '' ? $_REQUEST['session_id'] : api::error(0, \MI\API\Message::$invalid_session_id);
$info_type = isset($_REQUEST['info_type']) && $_REQUEST['info_type'] != '' ? $_REQUEST['info_type'] : api::error(0, \MI\API\Message::$invalid_info_type);
$device_id = isset($_REQUEST['device_id']) && $_REQUEST['device_id'] != '' ? $_REQUEST['device_id'] : api::error(0, \MI\API\Message::$invalid_device_id);
$device_model = isset($_REQUEST['device_model']) && $_REQUEST['device_model'] != '' ? $_REQUEST['device_model'] : api::error(0, \MI\API\Message::$invalid_device_model);
$system_name = isset($_REQUEST['system_name']) && $_REQUEST['system_name'] != '' ? $_REQUEST['system_name'] : api::error(0, \MI\API\Message::$invalid_system_name);
$system_version = isset($_REQUEST['system_version']) && $_REQUEST['system_version'] != '' ? $_REQUEST['system_version'] : api::error(0, \MI\API\Message::$invalid_system_version);
$app_version = isset($_REQUEST['app_version']) && $_REQUEST['app_version'] != '' ? $_REQUEST['app_version'] : api::error(0, \MI\API\Message::$invalid_app_version);
$country_code = isset($_REQUEST['country_code']) && $_REQUEST['country_code'] != '' ? $_REQUEST['country_code'] : api::error(0, \MI\API\Message::$invalid_country_code);
$latitude = isset($_REQUEST['latitude']) && $_REQUEST['latitude'] != '' ? $_REQUEST['latitude'] : api::error(0, \MI\API\Message::$invalid_latitude);
$longnitude = isset($_REQUEST['longnitude']) && $_REQUEST['longnitude'] != '' ? $_REQUEST['longnitude'] : api::error(0, \MI\API\Message::$invalid_longnitude);

$transaction = new transaction();
## Check Transaction
$transaction_data = $transaction->select($transaction_id);

if (count($transaction_data) == 1) {
    ## Update Transaction Status
    $transaction->setupdated_at($generalfuncobj->gm_date());
    $transaction->update_transaction_status($transaction_id, $action);


    if ($action == 'confirm' && $transaction_data[0]['transaction_status'] == 'pending') {
        $sender_id = $transaction_data[0]['from_user_id'];
        $receiver_id = $transaction_data[0]['to_user_id'];
        $amount = $transaction_data[0]['amount'];

        $wallet = new wallet();

        ## Update Sender wallet
        $wallet->deduct_amount($sender_id, $amount);

        ## Update Receiver wallet
        $wallet->add_amount($receiver_id, $amount);
    }
    api::success([], 1, "Request for " . ucfirst($action) . " Successfully.");

} else {
    api::error(0, \MI\API\Message::$invalid_transaction_id);
}

