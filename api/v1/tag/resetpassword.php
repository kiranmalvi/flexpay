<?php
/**
 * @api {get} /v1/?tag=resetpassword Request Reset Password
 * @apiName Reset Password
 * @apiVersion 1.0.0
 * @apiGroup user
 *
 * @apiParam {String} tag Tag for api.
 * @apiParam {String} email   give email
 * @apiParam {String} password   give password
 * @apiSuccess {array} data Response data.
 * @apiSuccess {String} message Message.
 * @apiSuccess {Number} status Status.
 *
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 200 OK
 * {
 *      data: [],
 *      message: "Password changed successfully.",
 *       status: 1
 * }
 *
 */

use MI\API\Response as response;
use MI\GEN as api;

$email = isset($_REQUEST['email']) && $_REQUEST['email'] != '' ? $_REQUEST['email'] : api::error(0, \MI\API\Message::$invalidemail);
$password = isset($_REQUEST['password']) && $_REQUEST['password'] != '' ? $_REQUEST['password'] : api::error(0, \MI\API\Message::$invalid_sign_password);
$password   =   md5($password);
$user = new  user();
$userdata = $user->check_user($email);
$gmt = $generalfuncobj->gm_date();
//pr($userdata);exit;
if (count($userdata) > 0) {

    $user_status = $userdata[0]['status'];
    $user_id = $userdata[0]['id'];
    $user_name = $userdata[0]['name'];
    $user_email = $userdata[0]['email'];
    $user_code = $userdata[0]['verification_code'];

    if ($user_status == 1) {
        $update_user = $user->update_password($password, $user_id, $gmt);
        api::success([], 1, "Password changed successfully.");
    } else {
        api::error(0, "Invalid email");
    }

} else {
    api::error(0, "user not register");
}

