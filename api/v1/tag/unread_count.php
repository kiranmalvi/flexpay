<?php
/**
 * @api {get} /v1/?tag=unread_count Unread Notifications
 * @apiName Get Unread Notifications
 * @apiBase http://localhost/
 * @apiVersion 1.0.0
 * @apiGroup Notification
 *
 * @apiParam {String} tag Tag for api.
 * @apiParam {Number} id (Required) login user id
 *
 * @apiSuccess {array} data Response data.
 * @apiSuccess {String} message Message.
 * @apiSuccess {Number} status Status.
 *
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 200 OK
 *  {
 *      "data": [
 *          {
 *              unread_count:3
 *          }
 *      ],
 *      "status": 1,
 *      "message": "Unread notifications."
 *  }
 *
 */

use MI\API\Response as response;
use MI\GEN as api;

$user_id = isset($_REQUEST['id']) && $_REQUEST['id'] != '' ? $_REQUEST['id'] : api::error(0, \MI\API\Message::$invalideuser_id);

$user = new user();
$noti = new notification();

$user_data = $user->select($user_id);

if (count($user_data) > 0) {

    $data = $noti->unread_notifications($user_id);
    api::success($data, 1, "Users unread notifications.");


} else {
    api::error(0, \MI\API\Message::$invalide_user);
}