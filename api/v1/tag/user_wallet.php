<?php
/**
 * @api {get} /v1/?tag=user_wallet Request give user wallet amount
 * @apiName user_wallet
 * @apiVersion 1.0.0
 * @apiGroup user
 *
 * @apiParam {String} tag Tag for api.
 * @apiParam {Number} user_id give login user id
 * @apiSuccess {Object} data Response data.
 * @apiSuccess {String} message Message.
 * @apiSuccess {Number} status Status.
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 200 OK
 * {
 *      data: {
 *              wallet_amount: "0"
 *           },
 * message: "user wallet amount",
 * status: 1
 * }
 *
 */

use MI\API\Response as response;
use MI\GEN as api;

//$_SERVER['REQUEST_METHOD'] == "POST" or api::error(0, \MI\API\Message::$invalidMethod);
$userid = isset($_REQUEST['user_id']) && $_REQUEST['user_id'] != '' ? $_REQUEST['user_id'] : api::error(0, \MI\API\Message::$invalideuser_id);
$user = new  user();
$user_info = new user_info();
$wallet = new wallet();
$userdata = $user->select($userid);
$gmt = $generalfuncobj->gm_date();
if (count($userdata) > 0) {
    $wallet = new wallet();
    $wallet_amount = $wallet->userwallet($userid);
    $amount = $wallet_amount[0]['amount'];
    if ($amount == null) {
        $amount = '0';
    }
    $data->wallet_amount = $amount;

    api::success($data, 1, "user wallet amount");


} else {
    api::error(0, "user not register");
}

