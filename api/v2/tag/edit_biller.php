<?php
/**
 * @api {get} /v2/?tag=edit_biller Request for edit biller
 * @apiName edit_biller
 * @apiBase http://localhost/
 * @apiVersion 1.0.0
 * @apiGroup Billers
 *
 * @apiParam {String} tag Tag for api.
 * @apiParam {integer} user_biller_id for user_biller_id
 * @apiParam {integer} user_id for login user_id
 * @apiParam {String} biller_name for biller name
 * @apiParam {String} account for account
 * @apiParam {String} comment for comment
 *
 * @apiSuccessExample Success-Response for Accept:
 *  HTTP/1.1 200 OK
 * {
 * data: {},
 * message: "Biller details updated successfully.",
 * status: 1
 * }
 * @apiSuccessExample Success-Response for Reject:
 *  HTTP/1.1 200 OK
 * {
 * data: [ ],
 * message: "Biller not updated. Please try again later.",
 * status: 1
 * }
 *
 */
use MI\API\Response as response;
use MI\GEN as api;
use MI\API\Message as msg;

## Validation
$user_biller_id = isset($_REQUEST['user_biller_id']) && $_REQUEST['user_biller_id'] != '' ? $_REQUEST['user_biller_id'] : api::error(0, msg::$invalid_request_user_biller_id);
$user_id = isset($_REQUEST['user_id']) && $_REQUEST['user_id'] != '' ? $_REQUEST['user_id'] : api::error(0, msg::$invalid_request_user_id);
$biller_name = isset($_REQUEST['biller_name']) && $_REQUEST['biller_name'] != '' ? $_REQUEST['biller_name'] : api::error(0, msg::$invalid_request_biller_name);
$account = isset($_REQUEST['account']) && $_REQUEST['account'] != '' ? $_REQUEST['account'] : api::error(0, msg::$invalid_request_account);
//$comment = isset($_REQUEST['comment']) && $_REQUEST['comment'] != '' ? $_REQUEST['comment'] : api::error(0, msg::$invalid_request_comment);
$comment = $_REQUEST['comment'];

$user = new user();
$gmt_date = $generalfuncobj->gm_date();
$user_biller = new user_billers();
if(count($user_biller->select($user_biller_id)) > 0) {
    $user_biller->setbiller_name($biller_name);
    $user_biller->setaccount($account);
    $user_biller->setcomment($comment);
    $user_biller->setupdated_at($gmt_date);
    $user_biller->updateBiller($user_biller_id);
    api::success($user_biller->select($user_biller_id)[0], 1, msg::$success_update_biller);
}
else{
    api::error(0, msg::$invalid_request_user_biller_id);
}
