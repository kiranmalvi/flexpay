<?php

/**
 * @api {get} /v2/?tag=signup Request Sign Up
 * @apiName Sign Up
 * @apiBase http://http://localhost/
 * @apiVersion 1.0.0
 * @apiGroup user
 *
 * @apiParam {String} tag Tag for api.
 * @apiParam {String} name User Name
 * @apiParam {String} email User Email
 * @apiParam {String} mobile User Mobile Number
 * @apiParam {String} password User password
 * @apiParam {String} security_pin User Security Pin
 * @apiParam {String} company_name <strong>(Optional)</strong> User Company Name.
 *
 * @apiSuccess {Object} data Response data.
 * @apiSuccess {String} message Message.
 * @apiSuccess {Number} status Status.
 *
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 200 OK
 *  {
 *      "data": {
            "id" : 1,
            "name" : "name",
            "email" : "email@gmail.com",
            "unique_token" : "unique_token",
 *      },
 *      "status": 1,
 *      "message": "Congratulation registered successfully."
 *  }
 *
 */

use MI\API\Response as response;
use MI\GEN as api;
use Endroid\QrCode\QrCode;
$qrCode = new QrCode();
$sendGridEmail = new SendGridLocal();
//pr($_REQUEST);exit;
$_SERVER['REQUEST_METHOD'] == "POST" or api::error(0, \MI\API\Message::$invalidMethod);

## Validation
$name = isset($_REQUEST['name']) && $_REQUEST['name'] != '' ? $_REQUEST['name'] : api::error(0, \MI\API\Message::$invalid_sign_name);
$email = isset($_REQUEST['email']) && $_REQUEST['email'] != '' ? $_REQUEST['email'] : api::error(0, \MI\API\Message::$invalid_sign_email);
$mobile = isset($_REQUEST['mobile']) && $_REQUEST['mobile'] != '' ? $_REQUEST['mobile'] : api::error(0, \MI\API\Message::$invalid_sign_mobile);
$password = isset($_REQUEST['password']) && $_REQUEST['password'] != '' ? $_REQUEST['password'] : api::error(0, \MI\API\Message::$invalid_sign_password);
$security_pin = isset($_REQUEST['security_pin']) && $_REQUEST['security_pin'] != '' ? $_REQUEST['security_pin'] : api::error(0, \MI\API\Message::$invalid_sign_security_pin);
$company_name = (isset($_REQUEST['company_name'])) ? $_REQUEST['company_name'] : '';

$user = new user();
$wallet =new wallet();
$user_transaction = new user_transaction_rate();


if($user->check_user_email($email)) {
    $unique_token = $generalfuncobj->generateUniqueToken_with_special_character(8);

    $debit_rate	=	$user->get_default_debit();
    $creadit_rate	=	$user->get_default_credit();
    $spend_limit	=	$user->get_default_spend_limit();

    ## Set Data
    $user->setUniqueToken($unique_token);
    $user->setname($name);
    $user->setemail($email);
    $user->setmobile($mobile);
    $user->setpassword(md5($password));
    $user->setsecurity_pin($security_pin);
    $user->setcompany_name($company_name);
    $user->settransfer_limit($spend_limit);
    $user->setadded_at($generalfuncobj->gm_date());
    $user->setstatus('4');

    ## insert
    $user_id = $user->insert();
    $user_transaction->setuser_id($user_id);
    $user_transaction->setcredit_rate($creadit_rate);
    $user_transaction->setdebit_rate($debit_rate);
    $user_transaction->insert();
    $wallet->setuser_id($user_id);
    $wallet->setamount('0');
    $wallet->setupdated_at($generalfuncobj->gm_date());
    $wallet->setadded_at($generalfuncobj->gm_date());
    $wallet->setstatus('1');
    $wallet->insert();
    ## Qr Code
    $USER = array(
        'id' => $user_id,
        'name' => $name,
        'email' => $email,
        'unique_token' => $unique_token,
    );

    $src = $qrCode
        ->setText(base64_encode(json_encode($USER)))
        ->setSize(300)
        ->setPadding(10)
        ->setErrorCorrection('high')
        ->setForegroundColor(array('r' => 0, 'g' => 0, 'b' => 0, 'a' => 0))
        ->setBackgroundColor(array('r' => 255, 'g' => 255, 'b' => 255, 'a' => 0))
        ->setLabelFontSize(16)
        ->get('png');

    ## Upload QR code
    $QRCODE = $user_id.'_qr_code.png';
    $generalfuncobj->saveToFile($src,$Qrcode_image_path.$QRCODE);

    ## Update Qr Code
    $user->update_qr_code($user_id,$QRCODE);

    ## Mail
	$LINK = $site_url.'verifyuser.php?id='.base64_encode($user_id);
    $sendGridEmail->signup($email,$name,$LINK);

    $data = $USER;

    api::success($data,1,\MI\API\Message::$sign_success);

} else {
    $user_exist_with_delete_status = $user->check_user_email_with_status($email,"2");

    if($user_exist_with_delete_status != false) {

        $user_id = $user_exist_with_delete_status;

        ## Set Data
        $user->setUniqueToken($unique_token);
        $user->setname($name);
        $user->setemail($email);
        $user->setmobile($mobile);
        $user->setpassword(md5($password));
        $user->setsecurity_pin($security_pin);
        $user->setcompany_name($company_name);
        $user->setadded_at($generalfuncobj->gm_date());
        $user->setstatus('4');
        $user->update($user_id);

		$wallet->setuser_id($user_id);
        $wallet->setamount('0');
        $wallet->setupdated_at($generalfuncobj->gm_date());
        $wallet->setstatus('1');
        $wallet->update_wallet($user_id);

        ## Qr Code
        $USER = array(
            'id' => $user_id,
            'name' => $name,
            'email' => $email,
            'unique_token' => $unique_token,
        );

        $src = $qrCode
            ->setText(base64_encode(json_encode($USER)))
            ->setSize(300)
            ->setPadding(10)
            ->setErrorCorrection('high')
            ->setForegroundColor(array('r' => 0, 'g' => 0, 'b' => 0, 'a' => 0))
            ->setBackgroundColor(array('r' => 255, 'g' => 255, 'b' => 255, 'a' => 0))
            ->setLabelFontSize(16)
            ->get('png');

        ## Upload QR code
        $QRCODE = $user_id.'_qr_code.png';
        $generalfuncobj->saveToFile($src,$Qrcode_image_path.$QRCODE);

        ## Update Qr Code
        $user->update_qr_code($user_id,$QRCODE);

        ## Mail
		$LINK = $site_url.'verifyuser.php?id='.base64_encode($user_id);
		$sendGridEmail->signup($email,$name,$LINK);

        $data = $USER;

        api::success($data,1,\MI\API\Message::$sign_success);
    }
    else{
        api::error(0, \MI\API\Message::$sign_email_already);
    }
}