<?php
$mode = "dev";
ob_start();
session_start();
require_once('vendor/autoload.php');
require_once('library/settings.php');
require_once('library/dbconfig.php');
if ($mode == 'dev') {
    error_reporting(E_ALL);
} else {
    error_reporting(0);
}

$files = glob(__DIR__ . '/library/**/*.php');

if ($files === false) {
    throw new RuntimeException("Failed to glob for function files");
}
foreach ($files as $file) {
    /** @noinspection PhpIncludeInspection */
    require_once $file;
}
unset($file);
unset($files);


if (!isset($obj)) {
    $obj = new myclass($SERVER, $DBASE, $USERNAME, $PASSWORD);
}

$generalfuncobj = new generalfunc();

date_default_timezone_set('UTC');