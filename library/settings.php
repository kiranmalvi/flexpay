<?php
error_reporting(0);

$sitefolder = str_replace($_SERVER['DOCUMENT_ROOT'], "", (dirname($_SERVER["SCRIPT_FILENAME"]))) . "/";
$tmp = explode("/", $sitefolder);

if (count($tmp) > 2) {
    $sitefolder = "/" . $tmp[1] . "/" . $tmp[2] . "/";
}

if ($_SERVER['HTTP_HOST'] == "") {
    $sitefolder = "/";
    $adminfolder = "admin/";
    $membersfolder = "members/";
} else {
    $sitefolder = "/flexpay/";
    $adminfolder = "admin/";
    $membersfolder = "members/";
}

## Project Name
$Project_Name = "Flexpay";
$Project_Name_Members = "Flexpay Members";

## Title
$ADMIN_PANEL_TITLE = "Flexpay";
$MEMBER_PANEL_TITLE = "Flexpay Members";

## Page Title
$ADMIN_PAGE_TITLE = "Flexpay";
$MEMBER_PAGE_TITLE = "Flexpay Members";

### Site Path And URL
$site_path = $_SERVER["DOCUMENT_ROOT"] . $sitefolder;
$site_url = "http://" . $_SERVER["HTTP_HOST"] . $sitefolder;

### Admin Path And URL
$admin_path = $site_path . $adminfolder;
$admin_url = $site_url . $adminfolder;

### Members Path And URL
$members_path = $site_path . $membersfolder;
$members_url = $site_url . $membersfolder;

### Admin Side Assests Script Path And URL
$assets_path = $admin_path . "assets/";
$assets_url = $admin_url . "assets/";

### Members Side Assests Script Path And URL
$members_assets_path = $members_path . "assets/";
$members_assets_url = $members_url . "assets/";

## Uploads directory where we will upload all the images / videos
$upload_image_path = $site_path . "uploads/";
$cache_image_path = $site_path . "uploads/cache/";
$upload_image_url = $site_url . "uploads/";
$vendor_image_path = $site_path . "uploads/vendor/";
$vendor_image_url = $site_url . "uploads/vendor/";

$category_image_path = $site_path . "uploads/category/";
$category_image_url = $site_url . "uploads/category/";

$product_image_path = $site_path . "uploads/product/";
$product_image_url = $site_url . "uploads/product/";


$segment_image_path = $site_path . "uploads/segment/";
$segment_image_url = $site_url . "uploads/segment/";
$Qrcode_image_path =$site_path . "uploads/QR_code/";

$Qrcode_image_url = $site_url . "uploads/QR_code/";
$default_Qr ="QR-Code.jpg";
$default_image = $upload_image_url . "default.png";
$default_image_name = "default.png";

## Image height & width
$default_listing_size = "?h=80&w=80";
## admin password length
$creadit_rate = 3;
$debit_rate = 3;
$admin_password=6;
$allowedTags = [
    'general_data',
    'login',
];

$excludeTags = [];

// under construction; Used for API to allow discard specified parameters from response
$removeParams = [];

// under construction; Used concurrently with $responseTags
$basicResponseParams = [
    'response',
    'data',
    'status',
    'msg'
];

// under construction; Used for API to allow only specified parameters in response
$allowedResponseParams = [];

## Set redius for Event list
$redius = 10;

## Set radius for out_side current location
$radius_outside_location = 100;

###	Classes  Path
$inc_class_path = $site_path . "library/class/";

@$ENC_DEC_KEY = "3\$r$@";

$session_prefix = "sess_flexpay_";

$COMPANY_NAME = "Flexpay";

#### EMAIL ####
//$EMAIL_USER = 'php.mindinventory@gmail.com';
//$EMAIL_PASS = 'Mind@1234';
$EMAIL_USER = 'admin@jrful.com';
$EMAIL_PASS = 'PlsChgMe73';
$EMAIL_NAME = $Project_Name;
$EMAIL_REPLY_TO = 'noreply@gmail.com';
$EMAIL_BCC_ID = 'admin@jrful.com';
$EMAIL_REPLY_NAME = $Project_Name;
#### EMAIL ####

#### Email Subject ####
$SIGN_UP_SUBJECT = 'Welcome to '.ucfirst($Project_Name);
$FORGOT_PASSWORD_SUBJECT = ucfirst($Project_Name) . ' - forgot password';
$FORGOT_PIN_SUBJECT = ucfirst($Project_Name) . ' - forgot pin';
#### Email Subject ####

#### Check Session API List ####
$CHECK_SESSION_API = array(
);
#### Check Session API List ####

## SHOW ADMIN NAME IN TRANSACTION
$ADMIN_NAME = "Admin";
$ADMIN_EMAIL = "admin@gmail.com";
/*
$DEFAULT_CREDIT_RATE = 3;
$DEFAULT_DEBIT_RATE = 3;*/
