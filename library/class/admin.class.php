<?php
/**
*
* -------------------------------------------------------
* CLASSNAME:    admin
* DATE:         27.10.2015
* CLASS FILE:   /var/www/html/flexpay/library/class/class-making/generated_classes/admin.class.php
* TABLE:        admin
* DB:           flexpay
* -------------------------------------------------------
* AUTHOR:
* Mindinventory (MI)
* -------------------------------------------------------
*
*/

class admin
{


/**
*   @desc Variable Declaration with default value
*/

	protected $id;   // KEY ATTR. WITH AUTOINCREMENT

	protected $_id;  
	protected $_name;  
	protected $_email;  
	protected $_password;  
	protected $_added_at;  
	protected $_updated_at;  
	protected $_status;  



/**
*   @desc   CONSTRUCTOR METHOD
*/

	function __construct()
	{
		global $obj;
		$this->_obj = $obj;

		$this->_id = null; 
		$this->_name = null; 
		$this->_email = null; 
		$this->_password = null; 
		$this->_added_at = null; 
		$this->_updated_at = null; 
		$this->_status = null; 
	}

/**
*   @desc   DECONSTRUCTOR METHOD
*/

	function __destruct()
	{
		unset($this->_obj);
	}



/**
*   @desc   GETTER METHODS
*/


	public function getid()
	{
		return $this->_id;
	}

	/**
	 * @desc   SETTER METHODS
	 */


	public function setid($val)
	{
		$this->_id = $val;
	}

	public function getname()
	{
		return $this->_name;
	}

	public function setname($val)
	{
		$this->_name = $val;
	}

	public function getemail()
	{
		return $this->_email;
	}

	public function setemail($val)
	{
		$this->_email = $val;
	}

	public function getpassword()
	{
		return $this->_password;
	}

	public function setpassword($val)
	{
		$this->_password = $val;
	}

	public function getadded_at()
	{
		return $this->_added_at;
	}

	public function setadded_at($val)
	{
		$this->_added_at = $val;
	}

	public function getupdated_at()
	{
		return $this->_updated_at;
	}

	public function setupdated_at($val)
	{
		$this->_updated_at = $val;
	}

	public function getstatus()
	{
		return $this->_status;
	}

	public function setstatus($val)
	{
		 $this->_status =  $val;
	}


/**
*   @desc   SELECT METHOD / LOAD
*/

	function select($id=null)
	{
		 $WHERE = 'WHERE 1=1';
		 if(!empty($id)) {
			 $WHERE .= ' AND id = '.$id;
		 }
		 $sql =  "SELECT * FROM admin $WHERE  AND status!='2' ORDER by id DESC ";
		 $row =  $this->_obj->select($sql); 

		 $this->_id = $row[0]['id'];
		 $this->_name = $row[0]['name'];
		 $this->_email = $row[0]['email'];
		 $this->_password = $row[0]['password'];
		 $this->_added_at = $row[0]['added_at'];
		 $this->_updated_at = $row[0]['updated_at'];
		 $this->_status = $row[0]['status'];
		 return $row;
	}


/**
*   @desc   DELETE
*/

	function delete($id)
	{
		 $sql = "DELETE FROM admin WHERE id = $id";
		 $this->_obj->sql_query($sql);
	}


/**
*   @desc   INSERT
*/

	function insert()
	{
		 $this->id = ""; // clear key for autoincrement

		echo  $sql = "INSERT INTO admin ( name,email,password,added_at,updated_at,status ) VALUES ( '".$this->_name."','".$this->_email."','".$this->_password."','".$this->_added_at."','".$this->_updated_at."','".$this->_status."' )";
		 $result = $this->_obj->insert($sql);
		 return $result;
	}


/**
*   @desc   UPDATE
*/

	function update($id)
	{

		 $sql = " UPDATE admin SET  name = '".$this->_name."' , email = '".$this->_email."' , password = '".$this->_password."' , added_at = '".$this->_added_at."' , updated_at = '".$this->_updated_at."' , status = '".$this->_status."'  WHERE id = $id ";
		 $this->_obj->sql_query($sql);

	}
    function checkoldpassword($checkpassword,$id)
	{

		$sql = "SELECT * from admin where password='$checkpassword' AND id='$id'";
		$row= $this->_obj->select($sql);
        return $row;
	}
    function update_password($newpassword,$id)
    {

        $sql = " UPDATE admin SET  password = '$newpassword'   WHERE id = $id ";
        $this->_obj->sql_query($sql);

    }

	function check_email($email,$id=''){
		$wsql = '';
		if($id != "") {
			$wsql = " AND id != '{$id}'";
		}
		$sql = "SELECT count(*) as count FROM admin WHERE email = '{$email}'{$wsql}";
		//echo $sql;
		$row = $this->_obj->select($sql);
		return $row[0]['count'];
	}

	public function do_login($email, $password)
	{
		$SQL = "SELECT * FROM admin WHERE email = '{$email}' AND password = '{$password}'";
		$DATA = $this->_obj->select($SQL);

		if (count($DATA) == 1) {
			## Check Status
			if ($DATA[0]['status'] == '1') {
				## Set Session
				$this->set_session($DATA[0]);
				global $generalfuncobj;
				#$generalfuncobj->func_set_temp_sess_msg("Well Come" . " " . $DATA[0]['name'], "success");
				header('location:index.php?file=ad-dashboard');
			} else {
				return "Your account is not activated yet. please contact administrator";
			}
		} else {
			return "You are not authorized user and try again";
		}
	}

	private function set_session($data)
	{
		global $Project_Name;
		$session = array(
			'id' => $data['id'],
			'email' => $data['email'],
			'user_name' => $data['name'],
		);
		$_SESSION[$Project_Name]['ADMIN'] = $session;
	}

}
