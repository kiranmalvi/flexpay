<?php
use Imagine\Image\Point;

class generalfunc
{
	function get_lat_lng($address)
	{
		$URL = "https://maps.googleapis.com/maps/api/geocode/json?address=" . $address;
		$JSON = file_get_contents($URL);
		$DATA = json_decode($JSON, true);
		$location = $DATA['results'][0]['geometry']['location'];
		return $location;
	}

	function genfile_uploadFile($photopath, $vphoto, $vphoto_name, $prefix, $vaildExt = "png,jpg,jpeg,gif,bmp")
	{

		//echo $vphoto;exit;
		$msg = "";
		if (!empty($vphoto_name) and is_file($vphoto)) {
			// Remove Dots from File name
			$tmp = explode(".", $vphoto_name);
			for ($i = 0; $i < count($tmp) - 1; $i++)
				$tmp1[] = $tmp[$i];

			$file = implode("_", $tmp1);
			$ext = $tmp[count($tmp) - 1];

			$vaildExt_arr = explode(",", strtoupper($vaildExt));
			if (in_array(strtoupper($ext), $vaildExt_arr)) {
				$vphotofile = $prefix . $file . "." . $ext;
				$vphotofile = $this->replace_image_name_junk_char($vphotofile);
				$ftppath1 = $photopath . $vphotofile;;

				if (!copy($vphoto, $ftppath1)) {
					$vphotofile = '';
					$msg = "File Not Uploaded Successfully !!";
				} else
					$msg = "File Uploaded Successfully !!";
			} else {
				$vphotofile = '';
				$msg = "File Extension Is Not Valid, Vaild Ext are  $vaildExt !!!";
			}
		}
		$ret[0] = $vphotofile;
		$ret[1] = $msg;
		// pr($ret); exit;
		return $ret;
	}

	function replace_image_name_junk_char($str)
	{

		$rs_catname = trim($str);
		$rs_catname = str_replace("&", "", $rs_catname);
		$rs_catname = str_replace(" ", "", $rs_catname);
		$rs_catname = str_replace("---", "", $rs_catname);
		$rs_catname = str_replace("--", "", $rs_catname);
		$rs_catname = str_replace("-", "", $rs_catname);
		$rs_catname = str_replace("+", "", $rs_catname);
		$rs_catname = str_replace("%20", "", $rs_catname);
		$rs_catname = str_replace("`", "", $rs_catname);
		$rs_catname = str_replace("|", "", $rs_catname);
		$rs_catname = str_replace("^", "", $rs_catname);
		$rs_catname = str_replace("*", "", $rs_catname);
		$rs_catname = str_replace("#", "", $rs_catname);
		$rs_catname = str_replace("_", "", $rs_catname);
		$rs_catname = preg_replace('/\s+/', '', $rs_catname);
		$rs_catname = strtolower($rs_catname);

		return $rs_catname;
	}

	function resize_save($FOLDER, $IMAGE, $HEIGHT, $WIDTH)
	{
		ini_set('memory_limit', '-1');

		global $site_path;
		//        echo $site_path . 'vendor/autoload.php';exit;
		require_once $site_path . 'vendor/autoload.php';
		$imagine = new Imagine\Gd\Imagine();

		try {
			$im = $imagine->setMetadataReader(new Imagine\Image\Metadata\ExifMetadataReader())->open($FOLDER . $IMAGE);

			$size = new Imagine\Image\Box($WIDTH, $HEIGHT);
			$mode = Imagine\Image\ImageInterface::THUMBNAIL_OUTBOUND;

			if (strpos($IMAGE, ".jpg")) {
				$IMAGE = substr($IMAGE, 0, strpos($IMAGE, ".jpg")) . ".jpg";
			}
			if (strpos($IMAGE, ".jpeg")) {
				$IMAGE = substr($IMAGE, 0, strpos($IMAGE, ".jpeg")) . ".jpeg";
			}
			if (strpos($IMAGE, ".gif")) {
				$IMAGE = substr($IMAGE, 0, strpos($IMAGE, ".gif")) . ".gif";
			}
			if (strpos($IMAGE, ".bmp")) {
				$IMAGE = substr($IMAGE, 0, strpos($IMAGE, ".bmp")) . ".bmp";
			}
			$metadata = $im->metadata();
			$change = self::rotateImageWithMetadata($metadata['ifd0.Orientation']);
			$im = $im->rotate($change['angle']);
			if ($change['flip']) {
				$im = $im->flipVertically();
			}
			$im = $im->thumbnail($size, $mode);
			$im->save($FOLDER . "Thumb_" . $IMAGE);
			//            unset($im);
			//            unset($imagine);
			//            unset($metadata);
			//            unset($change);
		} catch (Exception $e) {
			echo "<pre/>";
			print_r($e);
			exit;
		}
	}

	function rotateImageWithMetadata($orientation)
	{
		// http://sylvana.net/jpegcrop/exif_orientation.html
		$change = [
			'angle' => 0,
			'flip' => false
		];
		switch ($orientation) {
			case 1:
				$change['angle'] = 0;
				break;
			case 2:
				$change['angle'] = 0;
				$change['flip'] = true;
				break;
			case 3:
				$change['angle'] = 180;
				break;
			case 4:
				$change['angle'] = 180;
				$change['flip'] = true;
				break;
			case 5:
				$change['angle'] = 90;
				$change['flip'] = true;
				break;
			case 6:
				$change['angle'] = 90;
				break;
			case 7:
				$change['angle'] = 270;
				$change['flip'] = true;
				break;
			case 8:
				$change['angle'] = 270;
				break;
		}

		return $change;
	}

	function no_record_found($colspan)
	{
		return '<tr><td colspan="' . $colspan . '" class="no-record-found" style="color:red">No Record found.</td></tr>';
	}

	function change_admin_date_format($date)
	{
		return date("d-m-Y", strtotime($date));
	}

	function gm_date()
	{
		return gmdate('Y-m-d H:i:s');
	}

	function gm_date_only()
	{
		return gmdate('Y-m-d');
	}

	function gm_date_string()
	{
		return strtotime(gmdate('Y-m-d H:i:s'));
	}

	function full_date_formate($date)
	{
		return date("d F Y", strtotime($date));
	}

	function date_timestamp($date)
	{
		return strtotime($date);
	}

	function redirect($filename)
	{
		if (!headers_sent()) {
			header('Location: ' . $filename);
			exit;
		} else {
			echo '<script type="text/javascript">';
			echo 'window.location.href="' . $filename . '";';
			echo '</script>';
			echo '<noscript>';
			echo '<meta http-equiv="refresh" content="0;url=' . $filename . '" />';
			echo '</noscript>';
		}
	}

	function func_set_temp_sess_msg($varmsg, $msgclass, $class = 'alert', $header = 'Success')    //alert-danger
	{
		global $session_prefix;
		$_SESSION[$session_prefix . 'admin_var_msg'] = $varmsg;
		$_SESSION[$session_prefix . 'admin_var_msg_class'] = $msgclass;
		$_SESSION[$session_prefix . 'admin_msg_class'] = $class;
		$_SESSION[$session_prefix . 'admin_var_msg_title'] = $header;
	}

	function generateUniqueToken($number)
	{
		$arr = array('a', 'b', 'c', 'd', 'e', 'f',
			'g', 'h', 'i', 'j', 'k', 'l',
			'm', 'n', 'o', 'p', 'r', 's',
			't', 'u', 'v', 'x', 'y', 'z',
			'A', 'B', 'C', 'D', 'E', 'F',
			'G', 'H', 'I', 'J', 'K', 'L',
			'M', 'N', 'O', 'P', 'R', 'S',
			'T', 'U', 'V', 'X', 'Y', 'Z',
			'1', '2', '3', '4', '5', '6',
			'7', '8', '9', '0');
		$token = "";
		for ($i = 0; $i < $number; $i++) {
			$index = rand(0, count($arr) - 1);
			$token .= $arr[$index];
		}
		return $token;
	}

	function generateUniqueToken_with_special_character($number)
	{
		$arr = array('a', 'b', 'c', 'd', 'e', 'f',
			'g', 'h', 'i', 'j', 'k', 'l',
			'm', 'n', 'o', 'p', 'r', 's',
			't', 'u', 'v', 'x', 'y', 'z',
			'A', 'B', 'C', 'D', 'E', 'F',
			'G', 'H', 'I', 'J', 'K', 'L',
			'M', 'N', 'O', 'P', 'R', 'S',
			'T', 'U', 'V', 'X', 'Y', 'Z',
			'1', '2', '3', '4', '5', '6',
			'7', '8', '9', '0');
		$token = "";
		for ($i = 0; $i < $number; $i++) {
			$index = rand(0, count($arr) - 1);
			$token .= $arr[$index];
		}
		return $token;
	}

	function base64_to_jpeg($base64_string, $output_file)
	{
		$ifp = fopen($output_file, "wb");
		$data = explode(',', $base64_string);
		fwrite($ifp, base64_decode($data[1]));
		fclose($ifp);
		return $output_file;
	}

	function saveToFile($fileData, $output_file)
	{
		$ifp = fopen($output_file, "wb");
		fwrite($ifp, $fileData);
		fclose($ifp);
	}

	function generateNumericUniqueToken($number)
	{
		$arr = [
			'1',
			'2',
			'3',
			'4',
			'5',
			'6',
			'7',
			'8',
			'9',
			'0'
		];
		$token = "";
		for ($i = 0; $i < $number; $i++) {
			$index = rand(0, count($arr) - 1);
			$token .= $arr[$index];
		}

		return $token;
	}

	##
	#
	#   IMAGE RESIZE CODE
	#
	##

	function cleanRequest(&$request)
	{
		global $obj;
		foreach ($request as $k => $v) {
			if (is_array($v)) {
				$request[$k] = $this->cleanRequest($v);
			} else {
				$request[$k] = mysqli_real_escape_string($obj->CONN, $v);
			}
		}

		return $request;
	}

	function resize($IMAGE, $HEIGHT, $WIDTH, $TYPE, $folder)
	{
		global $upload_image_path, $site_path;

		$Image = $IMAGE;
		$height = $HEIGHT;
		$width = $WIDTH;

		require_once $site_path . 'vendor/autoload.php';
		$imagine = new Imagine\Gd\Imagine();

		try {
			$upload_path = $upload_image_path . $TYPE . "/";
			$im = $imagine->setMetadataReader(new Imagine\Image\Metadata\ExifMetadataReader())->open($upload_path . $Image);
			$size = new Imagine\Image\Box($width, $height);
			$mode = Imagine\Image\ImageInterface::THUMBNAIL_INSET;

			$metadata = $im->metadata();
			$change = self::rotateImageWithMetadata($metadata['ifd0.Orientation']);
			$im = $im->rotate($change['angle']);
			if ($change['flip']) {
				$im = $im->flipVertically();
			}
			$im = $im->thumbnail($size, $mode);

			if (strpos($Image, ".jpg")) {
				$Image = substr($Image, 0, strpos($Image, ".jpg")) . ".png";
			}
			if (strpos($Image, ".jpeg")) {
				$Image = substr($Image, 0, strpos($Image, ".jpeg")) . ".png";
			}
			if (strpos($Image, ".gif")) {
				$Image = substr($Image, 0, strpos($Image, ".gif")) . ".png";
			}
			if (strpos($Image, ".bmp")) {
				$Image = substr($Image, 0, strpos($Image, ".bmp")) . ".png";
			}

			#$im->save($upload_image_path . $TYPE . "/" . $folder . "/" . $Image);
			$im->show('png');
			exit;
		} catch (Exception $e) {
			pr($e);
			exit;
		}
	}

	function createJWTTOken($user_id,$id="")
	{
		$key = '^BRAHMA&VISHNU&MAHESH^';
		$alg = 'HS256';
		$token = new \Gamegos\JWT\Token();
		$token->setClaim('user_id', $user_id);
		$token->setClaim('id', $id);
		$token->setClaim('exp', time() + 60000 * 5);
		$encoder = new \Gamegos\JWT\Encoder();
		$encoder->encode($token, $key, $alg);
		return $token->getJWT();
	}

	function validateJWTToken($token)
	{
		$jwtString = $token;
		$key = '^BRAHMA&VISHNU&MAHESH^';
		$value = [];
		try {
			$validator = new \Gamegos\JWT\Validator();
			$token = $validator->validate($jwtString, $key);
			$value['user_id'] = $token->getClaims()['user_id'];
			$value['id'] = $token->getClaims()['id'];
			$value['tokenstatus'] = 1;
		} catch (\Gamegos\JWT\Exception\JWTException $e) {
			$value['error_msg'] = $e->getMessage();
			$value['tokenstatus'] = 0;
		}
		return $value;
	}
}
///////////////////////////////////////////////////////////////////////

function pr($data)
{
    echo "<pre/>";
    print_r($data);
}

function pre($data)
{
    echo "<pre>";
    print_r($data);
    echo "</pre>";
    exit;
}

function cpre($data)
{
    ob_get_clean();
    ob_clean();
    ob_end_clean();
    echo "<pre>";
    print_r($data);
    echo "</pre>";
    exit;
}

function clean_sql($sql)
{
    $sql = str_replace(", ", " ", $sql);
    $sql = str_replace(" ,", " ", $sql);
    $sql = str_replace(",,", ",", $sql);

    return $sql;
}
