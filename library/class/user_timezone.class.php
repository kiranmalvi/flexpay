<?php
/**
*
* -------------------------------------------------------
* CLASSNAME:    user_timezone
* DATE:         20.08.2016
* CLASS FILE:   /var/www/html/flexpay/library/class/class-making/generated_classes/user_timezone.class.php
* TABLE:        user_timezone
* DB:           mind_flexpay
* -------------------------------------------------------
* AUTHOR:
* Mindinventory (MI)
* -------------------------------------------------------
*
*/

class user_timezone
{


/**
*   @desc Variable Declaration with default value
*/

	protected $timezone_id;   // KEY ATTR. WITH AUTOINCREMENT

	protected $_timezone_id;  
	protected $_user_id;  
	protected $_timezone_str;  
	protected $_timezone_time;  
	protected $_added_at;  
	protected $_Updated_at;  



/**
*   @desc   CONSTRUCTOR METHOD
*/

	function __construct()
	{
		global $obj;
		$this->_obj = $obj;

		$this->_timezone_id = null; 
		$this->_user_id = null; 
		$this->_timezone_str = null; 
		$this->_timezone_time = null; 
		$this->_added_at = null; 
		$this->_Updated_at = null; 
	}

/**
*   @desc   DECONSTRUCTOR METHOD
*/

	function __destruct()
	{
		unset($this->_obj);
	}



/**
*   @desc   GETTER METHODS
*/


	public function gettimezone_id()
	{
		return $this->_timezone_id;
	}

	public function getuser_id()
	{
		return $this->_user_id;
	}

	public function gettimezone_str()
	{
		return $this->_timezone_str;
	}

	public function gettimezone_time()
	{
		return $this->_timezone_time;
	}

	public function getadded_at()
	{
		return $this->_added_at;
	}

	public function getUpdated_at()
	{
		return $this->_Updated_at;
	}


/**
*   @desc   SETTER METHODS
*/


	public function settimezone_id($val)
	{
		 $this->_timezone_id =  $val;
	}

	public function setuser_id($val)
	{
		 $this->_user_id =  $val;
	}

	public function settimezone_str($val)
	{
		 $this->_timezone_str =  $val;
	}

	public function settimezone_time($val)
	{
		 $this->_timezone_time =  $val;
	}

	public function setadded_at($val)
	{
		 $this->_added_at =  $val;
	}

	public function setUpdated_at($val)
	{
		 $this->_Updated_at =  $val;
	}


/**
*   @desc   SELECT METHOD / LOAD
*/

	function select($id=null)
	{
		 $WHERE = 'WHERE 1=1';
		 if(!empty($id)) {
			 $WHERE .= ' AND timezone_id = '.$id;
		 }
		 $sql =  "SELECT * FROM user_timezone $WHERE";
		 $row =  $this->_obj->select($sql); 

		 $this->_timezone_id = $row[0]['timezone_id'];
		 $this->_user_id = $row[0]['user_id'];
		 $this->_timezone_str = $row[0]['timezone_str'];
		 $this->_timezone_time = $row[0]['timezone_time'];
		 $this->_added_at = $row[0]['added_at'];
		 $this->_Updated_at = $row[0]['Updated_at'];
		 return $row;
	}


/**
*   @desc   DELETE
*/

	function delete($id)
	{
		 $sql = "DELETE FROM user_timezone WHERE timezone_id = $id";
		 $this->_obj->sql_query($sql);
	}


/**
*   @desc   INSERT
*/

	function insert()
	{
		 $this->timezone_id = ""; // clear key for autoincrement

		 $sql = "INSERT INTO user_timezone ( user_id,timezone_str,timezone_time,added_at,Updated_at ) VALUES ( '".$this->_user_id."','".$this->_timezone_str."','".$this->_timezone_time."','".$this->_added_at."','".$this->_Updated_at."' )";
		 $result = $this->_obj->insert($sql);
		 return $result;
	}


/**
*   @desc   UPDATE
*/

	function update($id)
	{
		 $sql = " UPDATE user_timezone SET  user_id = '".$this->_user_id."' , timezone_str = '".$this->_timezone_str."' , timezone_time = '".$this->_timezone_time."' , added_at = '".$this->_added_at."' , Updated_at = '".$this->_Updated_at."'  WHERE timezone_id = $id ";
		 return	$this->_obj->sql_query($sql);
	}


/**
*   @desc   Get date as per user's timezone
*/

	function getDateByUserTimezone($userid)
	{
		$userTimezoneSql	=	"SELECT timezone_id, timezone_str, timezone_time FROM user_timezone WHERE user_id = '{$userid}' ";
		$timezoneData	=	$this->_obj->select($userTimezoneSql);

		$usersTimezone = $timezoneData[0]['timezone_str'];
		$datetime = new DateTime( 'now', new DateTimeZone($usersTimezone) );
		$date	=	$datetime->format('Y-m-d');

		return	$date;
	}


/**
*   @desc   Get date as per user's timezone
*/

	function updateUserTimezone($userid, $str, $time, $date)
	{
		$userTimezoneSql	=	"SELECT count(*) AS tot, timezone_id FROM user_timezone WHERE user_id = '{$userid}' ";
		$timezoneData	=	$this->_obj->select($userTimezoneSql);

		$usersTimezone = $timezoneData[0]['tot'];

		if($usersTimezone > 0) {
			$sql = "UPDATE user_timezone SET user_id = '{$userid}', timezone_str = '{$str}', timezone_time = '{$time}', Updated_at = '{$date}' WHERE timezone_id = '{$timezoneData[0]['timezone_id']}' ";
			$this->_obj->sql_query($sql);
		} else {
			$sql = "INSERT INTO user_timezone(user_id, timezone_str, timezone_time, added_at, Updated_at) VALUES ('{$userid}', '{$str}', '{$time}', '{$date}', '{$date}')";
			$this->_obj->insert($sql);
		}

		return;
	}


}
