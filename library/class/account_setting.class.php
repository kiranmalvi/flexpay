<?php
/**
*
* -------------------------------------------------------
* CLASSNAME:    account_setting
* DATE:         27.10.2015
* CLASS FILE:   /var/www/html/flexpay/library/class/class-making/generated_classes/account_setting.class.php
* TABLE:        account_setting
* DB:           flexpay
* -------------------------------------------------------
* AUTHOR:
* Mindinventory (MI)
* -------------------------------------------------------
*
*/

class account_setting
{


/**
*   @desc Variable Declaration with default value
*/

	protected $account_setting_id;   // KEY ATTR. WITH AUTOINCREMENT

	protected $_account_setting_id;  
	protected $_account_number;  
	protected $_bank_name;  
	protected $_isfc_code;  
	protected $_bank_address;  
	protected $_name_of_beneficiary;  
	protected $_address_of_beneficiary;  
	protected $_mobile_number;  
	protected $_added_at;  
	protected $_updated_at;  
	protected $_status;  



/**
*   @desc   CONSTRUCTOR METHOD
*/

	function __construct()
	{
		global $obj;
		$this->_obj = $obj;

		$this->_account_setting_id = null; 
		$this->_account_number = null; 
		$this->_bank_name = null; 
		$this->_isfc_code = null; 
		$this->_bank_address = null; 
		$this->_name_of_beneficiary = null; 
		$this->_address_of_beneficiary = null; 
		$this->_mobile_number = null; 
		$this->_added_at = null; 
		$this->_updated_at = null; 
		$this->_status = null; 
	}

/**
*   @desc   DECONSTRUCTOR METHOD
*/

	function __destruct()
	{
		unset($this->_obj);
	}



/**
*   @desc   GETTER METHODS
*/


	public function getaccount_setting_id()
	{
		return $this->_account_setting_id;
	}

	/**
	 * @desc   SETTER METHODS
	 */


	public function setaccount_setting_id($val)
	{
		$this->_account_setting_id = $val;
	}

	public function getaccount_number()
	{
		return $this->_account_number;
	}

	public function setaccount_number($val)
	{
		$this->_account_number = $val;
	}

	public function getbank_name()
	{
		return $this->_bank_name;
	}

	public function setbank_name($val)
	{
		$this->_bank_name = $val;
	}

	public function getisfc_code()
	{
		return $this->_isfc_code;
	}

	public function setisfc_code($val)
	{
		$this->_isfc_code = $val;
	}

	public function getbank_address()
	{
		return $this->_bank_address;
	}

	public function setbank_address($val)
	{
		$this->_bank_address = $val;
	}

	public function getname_of_beneficiary()
	{
		return $this->_name_of_beneficiary;
	}

	public function setname_of_beneficiary($val)
	{
		$this->_name_of_beneficiary = $val;
	}

	public function getaddress_of_beneficiary()
	{
		return $this->_address_of_beneficiary;
	}

	public function setaddress_of_beneficiary($val)
	{
		$this->_address_of_beneficiary = $val;
	}

	public function getmobile_number()
	{
		return $this->_mobile_number;
	}

	public function setmobile_number($val)
	{
		$this->_mobile_number = $val;
	}

	public function getadded_at()
	{
		return $this->_added_at;
	}

	public function setadded_at($val)
	{
		 $this->_added_at =  $val;
	}

	public function getupdated_at()
	{
		return $this->_updated_at;
	}

	public function setupdated_at($val)
	{
		 $this->_updated_at =  $val;
	}

	public function getstatus()
	{
		return $this->_status;
	}

	public function setstatus($val)
	{
		 $this->_status =  $val;
	}


/**
*   @desc   SELECT METHOD / LOAD
*/

	function select($id=null)
	{
		 $WHERE = 'WHERE 1=1';
		 if(!empty($id)) {
			 $WHERE .= ' AND account_setting_id = '.$id;
		 }
		@$sql =  "SELECT * FROM account_setting $WHERE AND  status!='2' order by account_setting_id DESC ";
		@$row =  $this->_obj->select($sql);

		@$this->_account_setting_id = $row[0]['account_setting_id'];
		@$this->_account_number = $row[0]['account_number'];
		@$this->_bank_name = $row[0]['bank_name'];
		@$this->_isfc_code = $row[0]['isfc_code'];
		@$this->_bank_address = $row[0]['bank_address'];
		@$this->_name_of_beneficiary = $row[0]['name_of_beneficiary'];
		@$this->_address_of_beneficiary = $row[0]['address_of_beneficiary'];
		@$this->_mobile_number = $row[0]['mobile_number'];
		@$this->_added_at = $row[0]['added_at'];
		@$this->_updated_at = $row[0]['updated_at'];
		@$this->_status = $row[0]['status'];
		 return $row;
	}

    function select_data()
    {


        $sql =  "SELECT * FROM account_setting WHERE  status ='1' order by account_setting_id DESC ";
        $row =  $this->_obj->select($sql);


        return $row;
    }

/**
*   @desc   DELETE
*/

	function delete($id)
	{
		 $sql = "DELETE FROM account_setting WHERE account_setting_id = $id";
		 $this->_obj->sql_query($sql);
	}


/**
*   @desc   INSERT
*/

	function insert()
	{
		 $this->account_setting_id = ""; // clear key for autoincrement

		 $sql = "INSERT INTO account_setting ( account_number,bank_name,isfc_code,bank_address,name_of_beneficiary,address_of_beneficiary,mobile_number,added_at,updated_at,status ) VALUES ( '".$this->_account_number."','".$this->_bank_name."','".$this->_isfc_code."','".$this->_bank_address."','".$this->_name_of_beneficiary."','".$this->_address_of_beneficiary."','".$this->_mobile_number."','".$this->_added_at."','".$this->_updated_at."','".$this->_status."' )";
		 $result = $this->_obj->insert($sql);
		 return $result;
	}


/**
*   @desc   UPDATE
*/

	function update($id)
	{

		 $sql = " UPDATE account_setting SET  account_number = '".$this->_account_number."' , bank_name = '".$this->_bank_name."' , isfc_code = '".$this->_isfc_code."' , bank_address = '".$this->_bank_address."' , name_of_beneficiary = '".$this->_name_of_beneficiary."' , address_of_beneficiary = '".$this->_address_of_beneficiary."' , mobile_number = '".$this->_mobile_number."' , added_at = '".$this->_added_at."' , updated_at = '".$this->_updated_at."' , status = '".$this->_status."'  WHERE account_setting_id = $id ";
		 $this->_obj->sql_query($sql);

	}


}
