<?php
/**
*
* -------------------------------------------------------
* CLASSNAME:    user_billers
* DATE:         29.06.2016
* CLASS FILE:   /var/www/html/flexpay/library/class/class-making/generated_classes/user_billers.class.php
* TABLE:        user_billers
* DB:           mind_flexpay
* -------------------------------------------------------
* AUTHOR:
* Mindinventory (MI)
* -------------------------------------------------------
*
*/

class user_billers
{


/**
*   @desc Variable Declaration with default value
*/

	protected $user_biller_id;   // KEY ATTR. WITH AUTOINCREMENT

	protected $_user_biller_id;  
	protected $_user_id;  
	protected $_biller_id;  
	protected $_biller_name;  
	protected $_biller_email;  
	protected $_account;  
	protected $_comment;  
	protected $_status;  
	protected $_added_at;  
	protected $_updated_at;  



/**
*   @desc   CONSTRUCTOR METHOD
*/

	function __construct()
	{
		global $obj;
		$this->_obj = $obj;

		$this->_user_biller_id = null; 
		$this->_user_id = null; 
		$this->_biller_id = null; 
		$this->_biller_name = null; 
		$this->_biller_email = null; 
		$this->_account = null; 
		$this->_comment = null; 
		$this->_status = null; 
		$this->_added_at = null; 
		$this->_updated_at = null; 
	}

/**
*   @desc   DECONSTRUCTOR METHOD
*/

	function __destruct()
	{
		unset($this->_obj);
	}



/**
*   @desc   GETTER METHODS
*/


	public function getuser_biller_id()
	{
		return $this->_user_biller_id;
	}

	public function getuser_id()
	{
		return $this->_user_id;
	}

	public function getbiller_id()
	{
		return $this->_biller_id;
	}

	public function getbiller_name()
	{
		return $this->_biller_name;
	}

	public function getbiller_email()
	{
		return $this->_biller_email;
	}

	public function getaccount()
	{
		return $this->_account;
	}

	public function getcomment()
	{
		return $this->_comment;
	}

	public function getstatus()
	{
		return $this->_status;
	}

	public function getadded_at()
	{
		return $this->_added_at;
	}

	public function getupdated_at()
	{
		return $this->_updated_at;
	}


/**
*   @desc   SETTER METHODS
*/


	public function setuser_biller_id($val)
	{
		 $this->_user_biller_id =  $val;
	}

	public function setuser_id($val)
	{
		 $this->_user_id =  $val;
	}

	public function setbiller_id($val)
	{
		 $this->_biller_id =  $val;
	}

	public function setbiller_name($val)
	{
		 $this->_biller_name =  mysqli_real_escape_string($this->_obj->CONN, $val);
	}

	public function setbiller_email($val)
	{
		 $this->_biller_email =  $val;
	}

	public function setaccount($val)
	{
		 $this->_account =  $val;
	}

	public function setcomment($val)
	{
		 $this->_comment =  mysqli_real_escape_string($this->_obj->CONN, $val);
	}

	public function setstatus($val)
	{
		 $this->_status =  $val;
	}

	public function setadded_at($val)
	{
		 $this->_added_at =  $val;
	}

	public function setupdated_at($val)
	{
		 $this->_updated_at =  $val;
	}


/**
*   @desc   SELECT METHOD / LOAD
*/

	function select($id=null)
	{
		 $WHERE = 'WHERE 1=1';
		 if(!empty($id)) {
			 $WHERE .= ' AND user_biller_id = '.$id;
		 }
		 $sql =  "SELECT * FROM user_billers $WHERE";
		 $row =  $this->_obj->select($sql); 

		 $this->_user_biller_id = $row[0]['user_biller_id'];
		 $this->_user_id = $row[0]['user_id'];
		 $this->_biller_id = $row[0]['biller_id'];
		 $this->_biller_name = $row[0]['biller_name'];
		 $this->_biller_email = $row[0]['biller_email'];
		 $this->_account = $row[0]['account'];
		 $this->_comment = $row[0]['comment'];
		 $this->_status = $row[0]['status'];
		 $this->_added_at = $row[0]['added_at'];
		 $this->_updated_at = $row[0]['updated_at'];
		 return $row;
	}


/**
*   @desc   DELETE
*/

	function delete($id)
	{
		 $sql = "DELETE FROM user_billers WHERE user_biller_id = $id";
		 $this->_obj->sql_query($sql);
	}


/**
*   @desc   INSERT
*/

	function insert()
	{
		 $this->user_biller_id = ""; // clear key for autoincrement

		 $sql = "INSERT INTO user_billers ( user_id,biller_id,biller_name,biller_email,account,comment,status,added_at,updated_at ) VALUES ( '".$this->_user_id."','".$this->_biller_id."','".$this->_biller_name."','".$this->_biller_email."','".$this->_account."','".$this->_comment."','".$this->_status."','".$this->_added_at."','".$this->_updated_at."' )";
		 $result = $this->_obj->insert($sql);
		 return $result;
	}


/**
*   @desc   UPDATE
*/

	function update($id)
	{

		 $sql = " UPDATE user_billers SET  user_id = '".$this->_user_id."' , biller_id = '".$this->_biller_id."' , biller_name = '".$this->_biller_name."' , biller_email = '".$this->_biller_email."' , account = '".$this->_account."' , comment = '".$this->_comment."' , status = '".$this->_status."' , added_at = '".$this->_added_at."' , updated_at = '".$this->_updated_at."'  WHERE user_biller_id = $id ";
		 $this->_obj->sql_query($sql);

	}


/**
*   @desc   Select All user billers
*/

	function selectAll($user_id)
	{

		$sql = "SELECT * FROM user_billers WHERE user_id = '{$user_id}' ORDER BY biller_name ASC";
		$row = $this->_obj->select($sql);
		return $row;
	}


	/**
	 *   @desc   updateBiller
	 */

	function updateBiller($id)
	{

		$sql = " UPDATE user_billers SET biller_name = '".$this->_biller_name."' , account = '".$this->_account."' , comment = '".$this->_comment."' , updated_at = '".$this->_updated_at."'  WHERE user_biller_id = $id ";
		$this->_obj->sql_query($sql);
		return;
	}


	/**
	 *   @desc   deleteBiller
	 */

	function deleteBiller($id)
	{

		$sql = " UPDATE user_billers SET status = '".$this->_status."' , updated_at = '".$this->_updated_at."'  WHERE user_biller_id = $id ";
		$this->_obj->sql_query($sql);
		return;
	}

	/**
	 *   @desc   Select All active user biller's
	 */

	function allActiveBillers($user_id, $timestamp = 0)
	{
		global $offset, $limit;

		$sql = "SELECT * FROM user_billers WHERE user_id = '{$user_id}' AND status = '1' AND updated_at > '{$timestamp}' ORDER BY updated_at DESC LIMIT {$offset},{$limit}";
		$row = $this->_obj->select($sql);
		return $row;
	}
}
