<?php

use League\Glide\ServerFactory;
use League\Route\Http\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Image
{

    public function QR_code(Request $req, Response $res, array $args)
    {
        self::display($req, $res, $args, 'QR_code');
        return $res;
    }

    public function display(Request $req, Response $res, array $args, $type)
    {
        global $cache_image_path, $upload_image_path, $site_url;
        if (!is_dir($cache_image_path . $type)) {
            mkdir($cache_image_path . $type);
        }

        $server = ServerFactory::create([
            'source' => $upload_image_path . $type,
            'cache' => $cache_image_path . $type
        ]);

        try {
            ob_get_clean();
            $server->outputImage($args['image'], $_GET);
        } catch (\Exception $e) {
            header('location: ' . $site_url . 'uploads/default.png');
        }
        return $res;
    }

    public function vendor(Request $req, Response $res, array $args)
    {
        self::display($req, $res, $args, 'vendor');
        return $res;
    }

    public function product(Request $req, Response $res, array $args)
    {
        self::display($req, $res, $args, 'product');
        return $res;
    }

    public function segment(Request $req, Response $res, array $args)
    {
        self::display($req, $res, $args, 'segment');
        return $res;
    }

    public function defaultImg(Request $req, Response $res, array $args)
    {
        self::display($req, $res, $args, '');
        return $res;
    }

}
