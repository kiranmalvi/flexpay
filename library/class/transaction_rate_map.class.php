<?php
/**
*
* -------------------------------------------------------
* CLASSNAME:    transaction_rate_map
* DATE:         11.01.2016
* CLASS FILE:   /var/www/html/flexpay/library/class/class-making/generated_classes/transaction_rate_map.class.php
* TABLE:        transaction_rate_map
* DB:           flexpay
* -------------------------------------------------------
* AUTHOR:
* Mindinventory (MI)
* -------------------------------------------------------
*
*/

class transaction_rate_map
{


/**
*   @desc Variable Declaration with default value
*/

	protected $trm_id;   // KEY ATTR. WITH AUTOINCREMENT

	protected $_trm_id;  
	protected $_transaction_id;  
	protected $_credit_rate;  
	protected $_debit_rate;  



/**
*   @desc   CONSTRUCTOR METHOD
*/

	function __construct()
	{
		global $obj;
		$this->_obj = $obj;

		$this->_trm_id = null; 
		$this->_transaction_id = null; 
		$this->_credit_rate = null; 
		$this->_debit_rate = null; 
	}

/**
*   @desc   DECONSTRUCTOR METHOD
*/

	function __destruct()
	{
		unset($this->_obj);
	}



/**
*   @desc   GETTER METHODS
*/


	public function gettrm_id()
	{
		return $this->_trm_id;
	}

	public function gettransaction_id()
	{
		return $this->_transaction_id;
	}

	public function getcredit_rate()
	{
		return $this->_credit_rate;
	}

	public function getdebit_rate()
	{
		return $this->_debit_rate;
	}


/**
*   @desc   SETTER METHODS
*/


	public function settrm_id($val)
	{
		 $this->_trm_id =  $val;
	}

	public function settransaction_id($val)
	{
		 $this->_transaction_id =  $val;
	}

	public function setcredit_rate($val)
	{
		 $this->_credit_rate =  $val;
	}

	public function setdebit_rate($val)
	{
		 $this->_debit_rate =  $val;
	}


/**
*   @desc   SELECT METHOD / LOAD
*/

	function select($id=null)
	{
		 $WHERE = 'WHERE 1=1';
		 if(!empty($id)) {
			 $WHERE .= ' AND trm_id = '.$id;
		 }
		 $sql =  "SELECT * FROM transaction_rate_map $WHERE";
		 $row =  $this->_obj->select($sql); 

		 $this->_trm_id = $row[0]['trm_id'];
		 $this->_transaction_id = $row[0]['transaction_id'];
		 $this->_credit_rate = $row[0]['credit_rate'];
		 $this->_debit_rate = $row[0]['debit_rate'];
		 return $row;
	}


/**
*   @desc   DELETE
*/

	function delete($id)
	{
		 $sql = "DELETE FROM transaction_rate_map WHERE trm_id = $id";
		 $this->_obj->sql_query($sql);
	}


/**
*   @desc   INSERT
*/

	function insert()
	{
		 $this->trm_id = ""; // clear key for autoincrement

		 $sql = "INSERT INTO transaction_rate_map ( transaction_id,credit_rate,debit_rate ) VALUES ( '".$this->_transaction_id."','".$this->_credit_rate."','".$this->_debit_rate."' )";
		 $result = $this->_obj->insert($sql);
		 return $result;
	}


/**
*   @desc   UPDATE
*/

	function update($id)
	{

		 $sql = " UPDATE transaction_rate_map SET  transaction_id = '".$this->_transaction_id."' , credit_rate = '".$this->_credit_rate."' , debit_rate = '".$this->_debit_rate."'  WHERE trm_id = $id ";
		 return	$this->_obj->sql_query($sql);

	}

	function transaction_rates($id){
		$sql	=	"SELECT debit_rate, credit_rate FROM transaction_rate_map WHERE transaction_id = '{$id}'";
		$result	=	$this->_obj->select($sql);
		return	$result[0];
	}


}
