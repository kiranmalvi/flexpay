<?php
/**
*
* -------------------------------------------------------
* CLASSNAME:    wallet
* DATE:         28.10.2015
* CLASS FILE:   /var/www/html/flexpay/library/class/class-making/generated_classes/wallet.class.php
* TABLE:        wallet
* DB:           flexpay
* -------------------------------------------------------
* AUTHOR:
* Mindinventory (MI)
* -------------------------------------------------------
*
*/

class wallet
{


/**
*   @desc Variable Declaration with default value
*/

	protected $wallet_id;   // KEY ATTR. WITH AUTOINCREMENT

	protected $_wallet_id;  
	protected $_user_id;  
	protected $_amount;  
	protected $_added_at;  
	protected $_updated_at;  
	protected $_status;  



/**
*   @desc   CONSTRUCTOR METHOD
*/

	function __construct()
	{
		global $obj;
		$this->_obj = $obj;

		$this->_wallet_id = null; 
		$this->_user_id = null; 
		$this->_amount = null; 
		$this->_added_at = null; 
		$this->_updated_at = null; 
		$this->_status = null; 
	}

/**
*   @desc   DECONSTRUCTOR METHOD
*/

	function __destruct()
	{
		unset($this->_obj);
	}



/**
*   @desc   GETTER METHODS
*/


	public function getwallet_id()
	{
		return $this->_wallet_id;
	}

/**
*   @desc   SETTER METHODS
*/


	public function setwallet_id($val)
	{
		 $this->_wallet_id =  $val;
	}

	public function getuser_id()
	{
		return $this->_user_id;
	}

	public function setuser_id($val)
	{
		 $this->_user_id =  $val;
	}

	public function getamount()
	{
		return $this->_amount;
	}

	public function setamount($val)
	{
		 $this->_amount =  $val;
	}

	public function getadded_at()
	{
		return $this->_added_at;
	}

	public function setadded_at($val)
	{
		 $this->_added_at =  $val;
	}

	public function getupdated_at()
	{
		return $this->_updated_at;
	}

	public function setupdated_at($val)
	{
		 $this->_updated_at =  $val;
	}

	public function getstatus()
	{
		return $this->_status;
	}

	public function setstatus($val)
	{
		 $this->_status =  $val;
	}


/**
*   @desc   SELECT METHOD / LOAD
*/

	function select($id=null)
	{
		 $WHERE = 'WHERE 1=1';
		 if(!empty($id)) {
			 $WHERE .= ' AND wallet_id = '.$id;
		 }
		 $sql =  "SELECT * FROM wallet $WHERE AND  status!='2' order by wallet_id";
		 $row =  $this->_obj->select($sql); 

		 $this->_wallet_id = $row[0]['wallet_id'];
		 $this->_user_id = $row[0]['user_id'];
		 $this->_amount = $row[0]['amount'];
		 $this->_added_at = $row[0]['added_at'];
		 $this->_updated_at = $row[0]['updated_at'];
		 $this->_status = $row[0]['status'];
		 return $row;
	}

	function check_user_amount($user_id) {
		$sql =  "SELECT amount FROM wallet WHERE user_id = $user_id";
		$row =  $this->_obj->select($sql);
		return $row;
	}

	function deduct_amount($user_id,$amount) {
//		$sql = " UPDATE wallet SET amount = amount - '{$amount}' WHERE wallet_id = $user_id";
		$sql = " UPDATE wallet SET amount = CASE WHEN amount - '{$amount}' < 0 THEN 0.00 ELSE amount - '{$amount}' END WHERE wallet_id = $user_id";
		$this->_obj->sql_query($sql);
	}

	function add_amount($user_id,$amount) {
		$sql = " UPDATE wallet SET amount = amount + '{$amount}' WHERE wallet_id = $user_id";
		$this->_obj->sql_query($sql);
	}


/**
*   @desc   DELETE
*/

	function delete($id)
	{
		 $sql = "DELETE FROM wallet WHERE wallet_id = $id";
		 $this->_obj->sql_query($sql);
	}


/**
*   @desc   INSERT
*/

	function insert()
	{
		 $this->wallet_id = ""; // clear key for autoincrement

		 $sql = "INSERT INTO wallet ( user_id,amount,added_at,updated_at,status ) VALUES ( '".$this->_user_id."','".$this->_amount."','".$this->_added_at."','".$this->_updated_at."','".$this->_status."' )";
		 $result = $this->_obj->insert($sql);
		 return $result;
	}


/**
*   @desc   UPDATE
*/

	function update($id)
	{

		$sql = " UPDATE wallet SET  user_id = '".$this->_user_id."' , amount =amount + '".$this->_amount."' , added_at = '".$this->_added_at."' , updated_at = '".$this->_updated_at."' , status = '".$this->_status."'  WHERE wallet_id = $id ";
		 $this->_obj->sql_query($sql);

	}
	function reduce_amount($id)
	{

		$sql = " UPDATE wallet SET  user_id = '" . $this->_user_id . "' , amount = CASE WHEN amount - '" . $this->_amount . "' < 0 THEN 0.00 ELSE amount - '" . $this->_amount . "' END , added_at = '" . $this->_added_at . "' , updated_at = '" . $this->_updated_at . "' , status = '" . $this->_status . "'  WHERE wallet_id = $id ";
		 $this->_obj->sql_query($sql);

	}
    function userwallet($id=null)
    {

        $sql =  "SELECT * FROM wallet where  user_id IN ($id) AND  status!='2' order by wallet_id";
        $row =  $this->_obj->select($sql);

        return $row;
    }

function update_wallet($user_id)
	{

		$sql = " UPDATE wallet SET   amount =amount + '".$this->_amount."', updated_at = '".$this->_updated_at."' , status = '".$this->_status."'  WHERE user_id = $user_id ";
		$this->_obj->sql_query($sql);

	}

	function members_wallet($id=null)
	{

		$sql =  "SELECT * FROM wallet where  user_id IN ($id) AND  status!='2' order by wallet_id";
		$row =  $this->_obj->select($sql);

		return $row;
	}

}
