<?php

class myclass
{

    public $DBASE = "";

    public $CONN = "";

    public $_COUNT = "0";

    // ////////////////////////////////////////////////////
    //	***************************************************
    //	PHP and MySQL Connection and Error Specific methods
    //	***************************************************

    function myclass($server = "", $dbase = "", $user = "", $pass = "")
    {
        $this->DBASE = $dbase;
        //$conn = mysqli_connect($server,$user,$pass);
        $conn = new mysqli($server, $user, $pass, $dbase);
        if (!$conn) {
            $this->error("Connection attempt failed");
        }

        if (!$conn->select_db($dbase)) {
            $this->error("Dbase Select failed");
        }

        $this->CONN = $conn;

        return true;
    }


    function error($text)
    {
        $conn = $this->CONN;
        $no = mysqli_errno($conn);
        $msg = mysqli_error($conn);
        exit;
    }


    function select_database($dbase)
    {
        $conn = $this->CONN;
        $this->DBASE = $dbase;
        if (!$this->CONN->select_db($dbase)) {
            $this->error("Dbase Select failed");
        }
    }


    function close()
    {
        $conn = $this->CONN;
        $close = mysqli_close($this->CONN);
        unset($this->CONN);
        if (!$close) {
            $this->error("Connection close failed");
        }

        return true;
    }


    function select_object($sql = "", $fetch = "mysqli_fetch_assoc", $flag = 'No')
    {

        $this->_COUNT++;
        if (empty($sql)) {
            return false;
        }
        if (empty($this->CONN)) {
            return false;
        }
        $conn = $this->CONN;
        $results = mysqli_query($conn, $sql);
        mysqli_next_result($conn);
        if ($flag == 'Yes') {
            print_r($results);
        }
        if ((!$results) or (empty($results))) {
            return false;
        }
        $count = 0;
        $data = mysqli_fetch_object($results);
        //		print_r($results);exit;
        //		while ( $row = $fetch($results))
        //		{
        //			$data[$count] = $row;
        //			print_r($row);			exit;
        //			$count++;
        //		}
        @mysqli_free_result($results);

        return $data;
    }


    function selectfetch($sql = "", $fetch = "mysqli_fetch_assoc", $assoc_fields = '')
    {
        $this->_COUNT++;
        if (empty($sql)) {
            return false;
        }
        if (empty($this->CONN)) {
            return false;
        }
        $conn = $this->CONN;
        $results = mysqli_query($conn, $sql);
        mysqli_next_result($conn);
        if ((!$results) or (empty($results))) {
            return false;
        }
        $count = 0;
        $data = [];
        if ($fetch == "mysqli_fetch_assoc") {
            if ($assoc_fields == '') {
                while ($row = $fetch($results)) {
                    $data[$count] = $row;
                    $count++;
                }
            } else {
                while ($row = $fetch($results)) {
                    $data[$row[$assoc_fields]][] = $row;
                }
            }
        } else {
            while ($row = $fetch($results)) {
                $data[$count] = $row;
                $count++;
            }
        }
        @mysqli_free_result($results);

        return $data;
    }


    function execute($sql = "", $fetch = "mysqli_fetch_assoc")
    {
        $this->_COUNT++;
        if (empty($this->CONN)) {
            return false;
        }
        $conn = $this->CONN;
        $results = @mysqli_query($conn, $sql);
        $res1 = mysqli_next_result($conn);
        mysqli_next_result($conn);
        if ((!$results) or (empty($results))) {
            return false;
        }
        $count = 0;
        $data = [];
        while ($row = $fetch($results)) {
            $data[$count] = $row;
            $count++;
        }
        mysqli_free_result($results);

        return $data;
    }


    function affected($sql = "")
    {
        $this->_COUNT++;
        if (empty($sql)) {
            return false;
        }
        if (!eregi("^select", $sql)) {
            echo "wrongquery<br>$sql<p>";
            echo "<H2>Wrong function silly!</H2>\n";

            return false;
        }
        if (empty($this->CONN)) {
            return false;
        }
        $conn = $this->CONN;
        $results = @mysqli_query($conn, $sql);
        if ((!$results) or (empty($results))) {
            return false;
        }
        $tot = 0;
        $tot = mysqli_affected_rows();

        return $tot;
    }


    function getfields($table)
    {
        $this->_COUNT++;
        $fields = mysqli_list_fields($this->DBASE, $table, $this->CONN);
        $columns = mysqli_num_fields($fields);

        for ($i = 0; $i < $columns; $i++) {
            $arr[] = mysqli_field_name($fields, $i);
        }

        return $arr;
    }


    function no_of_fields($query)
    {
        return mysql_num_fields($query);
    }


    function InsertUpdate($table, $data, $action = 'insert', $parameters = '')
    {
        $this->_COUNT++;
        $conn = $this->CONN;
        reset($data);
        if ($action == 'insert') {
            $query = 'INSERT INTO ' . $table . ' (';
            while (list($columns,) = each($data)) {
                $query .= $columns . ', ';
            }
            $query = substr($query, 0, -2) . ') values (';
            reset($data);
            while (list(, $value) = each($data)) {
                switch ((string)$value) {
                    case 'null':
                        $query .= 'null, ';
                        break;
                    default:
                        $query .= '\'' . $value . '\', ';
                        break;
                }
            }
            $query = substr($query, 0, -2) . ')'; //Insert Query ready
            //echo $query;exit;
            $results = mysqli_query($conn, $query) or die("Query failed: " . mysqli_error());
            $last_Id = mysqli_insert_id($conn);
            mysqli_next_result($conn);
            if ($last_Id > 0) {
                return $last_Id;
            } else {
                $this->error("Query went bad!");

                return false;
            }
        } elseif ($action == 'update') {
            $query = 'update ' . $table . ' set ';
            while (list($columns, $value) = each($data)) {
                switch ((string)$value) {
                    case 'null':
                        $query .= $columns .= ' = null, ';
                        break;
                    default:
                        $query .= $columns . ' = \'' . $value . '\', ';
                        break;
                }
            }
            $where = " where 1=1 ";
            $where .= $parameters;
            $query = substr($query, 0, -2) . $where; //Update Query ready
            $results = @mysqli_query($conn, $query) or die("Query failed: " . mysqli_error());
            mysqli_next_result($conn);
            if (!$results) {
                $this->error("Query went bad!");

                return false;
            }
        }
        $this->writeQueryInFile($action, $query);

        return $results;
    }


    function writeQueryInFile($query_type, $query)
    {
        global $site_path;
        $content = "<hr>";
        $content .= '<table cellpadding="2" width="100%" cellpadding="2" border="1">';
        $content .= '<tr><td width="13%">Query :</td><td>' . $query;
        $content .= "</td></tr>";
        $content .= "<tr><td>Date Time : </td><td>" . date('Y-m-d h:i:s');
        $content .= "</td></tr>";
        $content .= "<tr><td>HTTP Referrer : </td><td>" . @$_SERVER['HTTP_REFERER'];
        $content .= "</td></tr>";
        $content .= "<tr><td>From IP : </td><td>" . $_SERVER['REMOTE_ADDR'];
        $content .= "</td></tr>";
        $content .= "</table>";
        $content .= "<br>";
        $filename = $site_path . "lib/query/" . $query_type . "_" . date('Y_m_d') . ".html";
        /*
        $fp = fopen($filename, 'a+');
           if (!fwrite($fp, $content)) {
           print "Cannot write to file ($filename)";
           exit;
        }
           fclose($fp);
        */
    }


    function Optimize()
    {
        $this->_COUNT++;
        $conn = $this->CONN;
        $alltables = mysqli_query($conn, "SHOW TABLES") or die("Error: " . mysqli_error());

        while ($table = mysqli_fetch_assoc($alltables)) {
            foreach ($table as $db => $tablename) {
                $query = "OPTIMIZE TABLE " . $tablename . ";";
                mysqli_query($conn, $query) or die("Query failed: " . mysqli_error());
                echo "TABLE " . $tablename . " OPTIMIZEED SUCCESSFULLY......<BR>";
            }
        }
    }


    function ChangeStatus($table, $ids, $keyId, $field = 'eStatus', $updateValue = 'active')
    {

        $this->_COUNT++;
        $conn = $this->CONN;
        //if($updateValue == 'Delete')
        // $sql = "delete from ".$table." where ".$keyId." in (".$ids.")";
        //else
        $sql = "update " . $table . " set " . $field . " = '" . $updateValue . "' where " . $keyId . " in (" . $ids . ")";
        //$sql = "update ".$table." set ".$field." = '".$updateValue."' where ".$keyId." = ".$ids;

        $results = mysqli_query($conn, $sql) or die("query fail");
        mysqli_next_result($conn);
        if (!$results) {
            $this->error("Query went bad!");

            return false;
        }

        return true;
    }
    #-------------------------------------------------------------------------------------<br>

    #-------------------------------------------------------------------------------------
    # OPTIMIZE TABLE
    # Usage:

    # $obj->Optimize();
    #-------------------------------------------------------------------------------------

    function mysql_enum_values($table, $field)
    {
        $this->_COUNT++;
        $conn = $this->CONN;
        $sql = "SHOW COLUMNS FROM $table LIKE '$field'";
        $sql_res = mysqli_query($conn, $sql); //or die("Could not query:\n$sql");
        $row = mysqli_fetch_assoc($sql_res);
        mysqli_free_result($sql_res);

        return explode("','", preg_replace("/.*\('(.*)'\)/", "\\1", $row["Type"]));
    }


    function GetAllTheTables()
    {
        $this->_COUNT++;
        $conn = $this->CONN;
        $db_name = $this->DBASE;
        $sql = "SHOW TABLES FROM  " . $db_name;

        return $this->select($sql);
        /*$sql_res = mysqli_query($conn,$sql) or die("Could not query:\n$sql");
        return $sql_res;*/
    }


    function select($sql = "", $fetch = "mysqli_fetch_assoc", $flag = 'No')
    {
        $this->_COUNT++;
        if (empty($sql)) {
            return false;
        }
        if (empty($this->CONN)) {
            return false;
        }
        $conn = $this->CONN;
        $results = mysqli_query($conn, $sql);
        #mysqli_next_result($conn);
        #echo $sql;exit;
        mysqli_more_results($conn);
        if ($flag == 'Yes') //print_r($results);
        {
            if ((!$results) or (empty($results))) {
                return false;
            }
        }
        $count = 0;
        $data = [];
        #print_r($results);exit;
        while ($row = $fetch($results)) {
            $data[$count] = $row;
            //			print_r($row);			exit;
            $count++;
        }
        @mysqli_free_result($results);

        return $data;
    }


    //for Showing All the Tables

    function select_assoc_array($sql = "", $key = '', $fetch = MYSQL_ASSOC)
    {
        $this->_COUNT++;
        if (empty($sql)) {
            return false;
        }
        if (!@eregi("^select", $sql) && !eregi("^call", $sql)) {
            echo "wrongquery<br>$sql<p>";
            echo "<H2>Wrong function silly!</H2>\n";

            return false;
        }
        if (empty($this->CONN)) {
            return false;
        }
        $conn = $this->CONN;
        $results = @mysqli_query($conn, $sql);
        if ((!$results) or (empty($results))) {
            return false;
        }
        $count = 0;
        $data = [];
        while ($row = mysqli_fetch_array($results, $fetch)) {
            $data[$row[$key]][] = $row;
            $count++;
        }
        mysqli_free_result($results);

        return $data;
    }


    // Call Below function to get the Results for a Query. It will return Two Dimesional Array.

    function getEnumFields($table, $fieldname)
    {
        $this->_COUNT++;
        $db = [];
        $result = mysqli_query($this->CONN, "SHOW COLUMNS FROM $table LIKE '" . $fieldname . "'");
        //if ($result->mysqli_num_rows > 0) {
        if (mysqli_num_rows($result) > 0) {
            while ($row = mysqli_fetch_assoc($result)) {
                $db = $row[Type];
                //$db[$row[Field]] = $row;
            }
        }
        $db = str_replace('enum', 'Array', $db);
        eval("\$db = " . $db . ";");

        return $db;
    }


    function insert_multi($table = "", $insert_array = "")
    {
        global $obj;
        $field = "";
        $sql_table_columns = "show columns from $table";
        $db_table_columns = $obj->select($sql_table_columns);

        for ($i = 1; $i < count($db_table_columns); $i++) {
            $field .= $db_table_columns[$i]['Field'] . ",";
        }

        $field_str = substr($field, 0, -1);
        echo $sql = "INSERT INTO $table ($field_str) VALUES  $insert_array ";
        $result = $obj->insert($sql);
    }


    //ends the class over here

    function insert($sql = "")
    {
        $this->_COUNT++;
        if (empty($sql)) {
            return false;
        }
        if (!@eregi("^insert", $sql)) {
            return false;
        }
        if (empty($this->CONN)) {
            return false;
        }
        $conn = $this->CONN;
        $results = mysqli_query($conn, $sql);
        $this->writeQueryInFile('insert', $sql);
        //   mysqli_next_result($conn);
        mysqli_more_results($conn);

        if (!$results) {
            $this->error("<H2>No results!</H2>\n");

            return false;
        }
        $id = mysqli_insert_id($conn);

        return $id;
    }


    function insert_into_apicall($VAR_QUERY_STRING, $jsondata)
    {
        $gmt_timestamp = strtotime(date('Y-m-d H:i:s'));

        $insert_sql = "INSERT INTO apicall (tApiCall, tApiResponse, dtApiCall) VALUES ('" . addslashes($VAR_QUERY_STRING) . "', '" . addslashes($jsondata) . "', '" . $gmt_timestamp . "')";
        $res_sql = $this->insert($insert_sql);

        $delete_sql = "delete from apicall where dtApiCall < '$gmt_timestamp' - INTERVAL 7 DAY";
        $del_sql = $this->sql_query($delete_sql);

        //pr($data);exit;
    }


    function sql_query($sql = "", $fetch = "mysqli_fetch_assoc")
    {

        $this->_COUNT++;
        if (empty($sql)) {
            return false;
        }
        if (empty($this->CONN)) {
            return false;
        }
        $conn = $this->CONN;
        $results = mysqli_query($conn, $sql) or die("query fail");
        $this->writeQueryInFile('update', $sql);
        @mysqli_next_result($conn);
        if (!$results) {
            $message = "Query went bad!";
            $this->error($message);

            return false;
        }
        if (!@eregi("^select", $sql) && !@eregi("^call", $sql)) {
            return true;
        } else {
            $count = 0;
            $data = [];
            while ($row = @$fetch($results)) {
                $data[$count] = $row;
                $count++;
            }
            @mysqli_free_result($results);

            return $data;
        }
    }

}

?>