<?php
/**
*
* -------------------------------------------------------
* CLASSNAME:    cms
* DATE:         27.10.2015
* CLASS FILE:   /var/www/html/flexpay/library/class/class-making/generated_classes/cms.class.php
* TABLE:        cms
* DB:           flexpay
* -------------------------------------------------------
* AUTHOR:
* Mindinventory (MI)
* -------------------------------------------------------
*
*/

class cms
{


/**
*   @desc Variable Declaration with default value
*/

	protected $cms_id;   // KEY ATTR. WITH AUTOINCREMENT

	protected $_cms_id;  
	protected $_type;  
	protected $_text;  
	protected $_added_at;  
	protected $_update_at;  
	protected $_status;  



/**
*   @desc   CONSTRUCTOR METHOD
*/

	function __construct()
	{
		global $obj;
		$this->_obj = $obj;

		$this->_cms_id = null; 
		$this->_type = null; 
		$this->_text = null; 
		$this->_added_at = null; 
		$this->_update_at = null; 
		$this->_status = null; 
	}

/**
*   @desc   DECONSTRUCTOR METHOD
*/

	function __destruct()
	{
		unset($this->_obj);
	}



/**
*   @desc   GETTER METHODS
*/


	public function getcms_id()
	{
		return $this->_cms_id;
	}

	public function gettype()
	{
		return $this->_type;
	}

	public function gettext()
	{
		return $this->_text;
	}

	public function getadded_at()
	{
		return $this->_added_at;
	}

	public function getupdate_at()
	{
		return $this->_update_at;
	}

	public function getstatus()
	{
		return $this->_status;
	}


/**
*   @desc   SETTER METHODS
*/


	public function setcms_id($val)
	{
		 $this->_cms_id =  $val;
	}

	public function settype($val)
	{
		 $this->_type =  $val;
	}

	public function settext($val)
	{
		 $this->_text =  $val;
	}

	public function setadded_at($val)
	{
		 $this->_added_at =  $val;
	}

	public function setupdate_at($val)
	{
		 $this->_update_at =  $val;
	}

	public function setstatus($val)
	{
		 $this->_status =  $val;
	}


/**
*   @desc   SELECT METHOD / LOAD
*/

	function select($id=null)
	{
		 $WHERE = 'WHERE 1=1';
		 if(!empty($id)) {
			 $WHERE .= ' AND cms_id = '.$id;
		 }
		 $sql =  "SELECT * FROM cms $WHERE";
		 $row =  $this->_obj->select($sql); 

		 $this->_cms_id = $row[0]['cms_id'];
		 $this->_type = $row[0]['type'];
		 $this->_text = $row[0]['text'];
		 $this->_added_at = $row[0]['added_at'];
		 $this->_update_at = $row[0]['update_at'];
		 $this->_status = $row[0]['status'];
		 return $row;
	}


/**
*   @desc   DELETE
*/

	function delete($id)
	{
		 $sql = "DELETE FROM cms WHERE cms_id = $id";
		 $this->_obj->sql_query($sql);
	}


/**
*   @desc   INSERT
*/

	function insert()
	{
		 $this->cms_id = ""; // clear key for autoincrement

		 $sql = "INSERT INTO cms ( type,text,added_at,update_at,status ) VALUES ( '".$this->_type."','".$this->_text."','".$this->_added_at."','".$this->_update_at."','".$this->_status."' )";
		 $result = $this->_obj->insert($sql);
		 return $result;
	}


/**
*   @desc   UPDATE
*/

	function update($id)
	{

		 $sql = " UPDATE cms SET  type = '".$this->_type."' , text = '".$this->_text."' , added_at = '".$this->_added_at."' , update_at = '".$this->_update_at."' , status = '".$this->_status."'  WHERE cms_id = $id ";
		 $this->_obj->sql_query($sql);

	}


}
