<?php
/**
*
* -------------------------------------------------------
* CLASSNAME:    user_info
* DATE:         30.10.2015
* CLASS FILE:   /var/www/html/flexpay/library/class/class-making/generated_classes/user_info.class.php
* TABLE:        user_info
* DB:           flexpay
* -------------------------------------------------------
* AUTHOR:
* Mindinventory (MI)
* -------------------------------------------------------
*
*/

class user_info
{


/**
*   @desc Variable Declaration with default value
*/

	protected $user_info_id;   // KEY ATTR. WITH AUTOINCREMENT

	protected $_user_info_id;  
	protected $_user_id;  
	protected $_info_type;  
	protected $_session_id;  
	protected $_device_id;  
	protected $_device_model;  
	protected $_system_name;  
	protected $_system_version;  
	protected $_app_version;  
	protected $_country_code;  
	protected $_date_time;  
	protected $_latitude;  
	protected $_longnitude;  
	protected $_status;  



/**
*   @desc   CONSTRUCTOR METHOD
*/

	function __construct()
	{
		global $obj;
		$this->_obj = $obj;

		$this->_user_info_id = null; 
		$this->_user_id = null; 
		$this->_info_type = null; 
		$this->_session_id = null; 
		$this->_device_id = null; 
		$this->_device_model = null; 
		$this->_system_name = null; 
		$this->_system_version = null; 
		$this->_app_version = null; 
		$this->_country_code = null; 
		$this->_date_time = null; 
		$this->_latitude = null; 
		$this->_longnitude = null; 
		$this->_status = null; 
	}

/**
*   @desc   DECONSTRUCTOR METHOD
*/

	function __destruct()
	{
		unset($this->_obj);
	}



/**
*   @desc   GETTER METHODS
*/


	public function getuser_info_id()
	{
		return $this->_user_info_id;
	}

	public function getuser_id()
	{
		return $this->_user_id;
	}

	public function getinfo_type()
	{
		return $this->_info_type;
	}

	public function getsession_id()
	{
		return $this->_session_id;
	}

	public function getdevice_id()
	{
		return $this->_device_id;
	}

	public function getdevice_model()
	{
		return $this->_device_model;
	}

	public function getsystem_name()
	{
		return $this->_system_name;
	}

	public function getsystem_version()
	{
		return $this->_system_version;
	}

	public function getapp_version()
	{
		return $this->_app_version;
	}

	public function getcountry_code()
	{
		return $this->_country_code;
	}

	public function getdate_time()
	{
		return $this->_date_time;
	}

	public function getlatitude()
	{
		return $this->_latitude;
	}

	public function getlongnitude()
	{
		return $this->_longnitude;
	}

	public function getstatus()
	{
		return $this->_status;
	}


/**
*   @desc   SETTER METHODS
*/


	public function setuser_info_id($val)
	{
		 $this->_user_info_id =  $val;
	}

	public function setuser_id($val)
	{
		 $this->_user_id =  $val;
	}

	public function setinfo_type($val)
	{
		 $this->_info_type =  $val;
	}

	public function setsession_id($val)
	{
		 $this->_session_id =  $val;
	}

	public function setdevice_id($val)
	{
		 $this->_device_id =  $val;
	}

	public function setdevice_model($val)
	{
		 $this->_device_model =  $val;
	}

	public function setsystem_name($val)
	{
		 $this->_system_name =  $val;
	}

	public function setsystem_version($val)
	{
		 $this->_system_version =  $val;
	}

	public function setapp_version($val)
	{
		 $this->_app_version =  $val;
	}

	public function setcountry_code($val)
	{
		 $this->_country_code =  $val;
	}

	public function setdate_time($val)
	{
		 $this->_date_time =  $val;
	}

	public function setlatitude($val)
	{
		 $this->_latitude =  $val;
	}

	public function setlongnitude($val)
	{
		 $this->_longnitude =  $val;
	}

	public function setstatus($val)
	{
		 $this->_status =  $val;
	}


/**
*   @desc   SELECT METHOD / LOAD
*/

	function select($id=null)
	{
		 $WHERE = 'WHERE 1=1';
		 if(!empty($id)) {
			 $WHERE .= ' AND user_info_id = '.$id;
		 }
		 $sql =  "SELECT * FROM user_info $WHERE";
		 $row =  $this->_obj->select($sql); 

		 $this->_user_info_id = $row[0]['user_info_id'];
		 $this->_user_id = $row[0]['user_id'];
		 $this->_info_type = $row[0]['info_type'];
		 $this->_session_id = $row[0]['session_id'];
		 $this->_device_id = $row[0]['device_id'];
		 $this->_device_model = $row[0]['device_model'];
		 $this->_system_name = $row[0]['system_name'];
		 $this->_system_version = $row[0]['system_version'];
		 $this->_app_version = $row[0]['app_version'];
		 $this->_country_code = $row[0]['country_code'];
		 $this->_date_time = $row[0]['date_time'];
		 $this->_latitude = $row[0]['latitude'];
		 $this->_longnitude = $row[0]['longnitude'];
		 $this->_status = $row[0]['status'];
		 return $row;
	}


/**
*   @desc   DELETE
*/

	function delete($id)
	{
		 $sql = "DELETE FROM user_info WHERE user_info_id = $id";
		 $this->_obj->sql_query($sql);
	}


/**
*   @desc   INSERT
*/

	function insert()
	{
		 $this->user_info_id = ""; // clear key for autoincrement

		 $sql = "INSERT INTO user_info ( user_id,info_type,session_id,device_id,device_model,system_name,system_version,app_version,country_code,date_time,latitude,longnitude,status ) VALUES ( '".$this->_user_id."','".$this->_info_type."','".$this->_session_id."','".$this->_device_id."','".$this->_device_model."','".$this->_system_name."','".$this->_system_version."','".$this->_app_version."','".$this->_country_code."','".$this->_date_time."','".$this->_latitude."','".$this->_longnitude."','".$this->_status."' )";
		 $result = $this->_obj->insert($sql);
		 return $result;
	}


/**
*   @desc   UPDATE
*/

	function update($id)
	{

		 $sql = " UPDATE user_info SET  user_id = '".$this->_user_id."' , info_type = '".$this->_info_type."' , session_id = '".$this->_session_id."' , device_id = '".$this->_device_id."' , device_model = '".$this->_device_model."' , system_name = '".$this->_system_name."' , system_version = '".$this->_system_version."' , app_version = '".$this->_app_version."' , country_code = '".$this->_country_code."' , date_time = '".$this->_date_time."' , latitude = '".$this->_latitude."' , longnitude = '".$this->_longnitude."' , status = '".$this->_status."'  WHERE user_info_id = $id ";
		 $this->_obj->sql_query($sql);

	}


}
