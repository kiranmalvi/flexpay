<?php
/**
*
* -------------------------------------------------------
* CLASSNAME:    user_transaction_rate
* DATE:         30.10.2015
* CLASS FILE:   /var/www/html/flexpay/library/class/class-making/generated_classes/user_transaction_rate.class.php
* TABLE:        user_transaction_rate
* DB:           flexpay
* -------------------------------------------------------
* AUTHOR:
* Mindinventory (MI)
* -------------------------------------------------------
*
*/

class user_transaction_rate
{


/**
*   @desc Variable Declaration with default value
*/

	protected $user_transaction_rate_id;   // KEY ATTR. WITH AUTOINCREMENT

	protected $_user_transaction_rate_id;  
	protected $_user_id;  
	protected $_debit_rate;  
	protected $_credit_rate;  



/**
*   @desc   CONSTRUCTOR METHOD
*/

	function __construct()
	{
		global $obj;
		$this->_obj = $obj;

		$this->_user_transaction_rate_id = null; 
		$this->_user_id = null; 
		$this->_debit_rate = null; 
		$this->_credit_rate = null; 
	}

/**
*   @desc   DECONSTRUCTOR METHOD
*/

	function __destruct()
	{
		unset($this->_obj);
	}



/**
*   @desc   GETTER METHODS
*/


	public function getuser_transaction_rate_id()
	{
		return $this->_user_transaction_rate_id;
	}

	public function getuser_id()
	{
		return $this->_user_id;
	}

	public function getdebit_rate()
	{
		return $this->_debit_rate;
	}

	public function getcredit_rate()
	{
		return $this->_credit_rate;
	}


/**
*   @desc   SETTER METHODS
*/


	public function setuser_transaction_rate_id($val)
	{
		 $this->_user_transaction_rate_id =  $val;
	}

	public function setuser_id($val)
	{
		 $this->_user_id =  $val;
	}

	public function setdebit_rate($val)
	{
		 $this->_debit_rate =  $val;
	}

	public function setcredit_rate($val)
	{
		 $this->_credit_rate =  $val;
	}


/**
*   @desc   SELECT METHOD / LOAD
*/

	function select($id=null)
	{
		 $WHERE = 'WHERE 1=1';
		 if(!empty($id)) {
			 $WHERE .= ' AND user_transaction_rate_id = '.$id;
		 }
		 $sql =  "SELECT * FROM user_transaction_rate $WHERE";
		 $row =  $this->_obj->select($sql); 

		 $this->_user_transaction_rate_id = $row[0]['user_transaction_rate_id'];
		 $this->_user_id = $row[0]['user_id'];
		 $this->_debit_rate = $row[0]['debit_rate'];
		 $this->_credit_rate = $row[0]['credit_rate'];
		 return $row;
	}


/**
*   @desc   DELETE
*/

	function delete($id)
	{
		 $sql = "DELETE FROM user_transaction_rate WHERE user_transaction_rate_id = $id";
		 $this->_obj->sql_query($sql);
	}


/**
*   @desc   INSERT
*/

	function insert()
	{
		 $this->user_transaction_rate_id = ""; // clear key for autoincrement

		 $sql = "INSERT INTO user_transaction_rate ( user_id,debit_rate,credit_rate ) VALUES ( '".$this->_user_id."','".$this->_debit_rate."','".$this->_credit_rate."' )";
		 $result = $this->_obj->insert($sql);
		 return $result;
	}


/**
*   @desc   UPDATE
*/

	function update($id)
	{

		 $sql = " UPDATE user_transaction_rate SET  user_id = '".$this->_user_id."' , debit_rate = '".$this->_debit_rate."' , credit_rate = '".$this->_credit_rate."'  WHERE user_transaction_rate_id = $id ";
		 $this->_obj->sql_query($sql);

	}


}
