<?php

/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 12/10/15
 * Time: 12:49 PM
 */
class api_message
{
    public function general_data($error_number, $error_type)
    {
        global $msg;
        $this->set_status($error_type);

        $message = array(
            1 => "Type does not match.",
            2 => "FAQ found successfully.",
            3 => "About us found successfully.",
            4 => "Privacy Policy found successfully.",
            5 => "Terms and Condition found successfully.",
        );

        $msg = $message[$error_number];
    }

    private function set_status($error_type)
    {
        global $status;
        $status = 1;
        if ($error_type == 0) {
            $status = 0;
        }
    }
}