<?php

/**
 *
 * -------------------------------------------------------
 * CLASSNAME:    paypal_transaction_map
 * DATE:         17.09.2016
 * CLASS FILE:   /var/www/html/flexpay/library/class/class-making/generated_classes/paypal_transaction_map.class.php
 * TABLE:        paypal_transaction_map
 * DB:           mind_flexpay
 * -------------------------------------------------------
 * AUTHOR:
 * Mindinventory (MI)
 * -------------------------------------------------------
 *
 */
class paypal_transaction_map
{


	/**
	 * @desc Variable Declaration with default value
	 */

	protected $paypal_transaction_map_id;   // KEY ATTR. WITH AUTOINCREMENT

	protected $_paypal_transaction_map_id;
	protected $_transaction_id;
	protected $_paypal_id;
	protected $_currency_code;
	protected $_processable;
	protected $_environment;
	protected $_paypal_sdk_version;
	protected $_platform;
	protected $_product_name;
	protected $_create_time;
	protected $_intent;
	protected $_state;
	protected $_response_type;
	protected $_status;
	protected $_added_at;
	protected $_updated_at;


	/**
	 * @desc   CONSTRUCTOR METHOD
	 */

	function __construct()
	{
		global $obj;
		$this->_obj = $obj;

		$this->_paypal_transaction_map_id = null;
		$this->_transaction_id = null;
		$this->_paypal_id = null;
		$this->_currency_code = null;
		$this->_processable = null;
		$this->_environment = null;
		$this->_paypal_sdk_version = null;
		$this->_platform = null;
		$this->_product_name = null;
		$this->_create_time = null;
		$this->_intent = null;
		$this->_state = null;
		$this->_response_type = null;
		$this->_status = null;
		$this->_added_at = null;
		$this->_updated_at = null;
	}

	/**
	 * @desc   DECONSTRUCTOR METHOD
	 */

	function __destruct()
	{
		unset($this->_obj);
	}


	/**
	 * @desc   GETTER METHODS
	 */


	public function getpaypal_transaction_map_id()
	{
		return $this->_paypal_transaction_map_id;
	}

	/**
	 * @desc   SETTER METHODS
	 */


	public function setpaypal_transaction_map_id($val)
	{
		$this->_paypal_transaction_map_id = $val;
	}

	public function gettransaction_id()
	{
		return $this->_transaction_id;
	}

	public function settransaction_id($val)
	{
		$this->_transaction_id = $val;
	}

	public function getpaypal_id()
	{
		return $this->_paypal_id;
	}

	public function setpaypal_id($val)
	{
		$this->_paypal_id = $val;
	}

	public function getcurrency_code()
	{
		return $this->_currency_code;
	}

	public function setcurrency_code($val)
	{
		$this->_currency_code = $val;
	}

	public function getprocessable()
	{
		return $this->_processable;
	}

	public function setprocessable($val)
	{
		$this->_processable = $val;
	}

	public function getenvironment()
	{
		return $this->_environment;
	}

	public function setenvironment($val)
	{
		$this->_environment = $val;
	}

	public function getpaypal_sdk_version()
	{
		return $this->_paypal_sdk_version;
	}

	public function setpaypal_sdk_version($val)
	{
		$this->_paypal_sdk_version = $val;
	}

	public function getplatform()
	{
		return $this->_platform;
	}

	public function setplatform($val)
	{
		$this->_platform = $val;
	}

	public function getproduct_name()
	{
		return $this->_product_name;
	}

	public function setproduct_name($val)
	{
		$this->_product_name = $val;
	}

	public function getcreate_time()
	{
		return $this->_create_time;
	}

	public function setcreate_time($val)
	{
		$this->_create_time = $val;
	}

	public function getintent()
	{
		return $this->_intent;
	}

	public function setintent($val)
	{
		$this->_intent = $val;
	}

	public function getstate()
	{
		return $this->_state;
	}

	public function setstate($val)
	{
		$this->_state = $val;
	}

	public function getresponse_type()
	{
		return $this->_response_type;
	}

	public function setresponse_type($val)
	{
		$this->_response_type = $val;
	}

	public function getstatus()
	{
		return $this->_status;
	}

	public function setstatus($val)
	{
		$this->_status = $val;
	}

	public function getadded_at()
	{
		return $this->_added_at;
	}

	public function setadded_at($val)
	{
		$this->_added_at = $val;
	}

	public function getupdated_at()
	{
		return $this->_updated_at;
	}

	public function setupdated_at($val)
	{
		$this->_updated_at = $val;
	}


	/**
	 * @desc   SELECT METHOD / LOAD
	 */

	function select($id = null)
	{
		$WHERE = 'WHERE 1=1';
		if (!empty($id)) {
			$WHERE .= ' AND paypal_transaction_map_id = ' . $id;
		}
		$sql = "SELECT * FROM paypal_transaction_map $WHERE";
		$row = $this->_obj->select($sql);

		$this->_paypal_transaction_map_id = $row[0]['paypal_transaction_map_id'];
		$this->_transaction_id = $row[0]['transaction_id'];
		$this->_paypal_id = $row[0]['paypal_id'];
		$this->_currency_code = $row[0]['currency_code'];
		$this->_processable = $row[0]['processable'];
		$this->_environment = $row[0]['environment'];
		$this->_paypal_sdk_version = $row[0]['paypal_sdk_version'];
		$this->_platform = $row[0]['platform'];
		$this->_product_name = $row[0]['product_name'];
		$this->_create_time = $row[0]['create_time'];
		$this->_intent = $row[0]['intent'];
		$this->_state = $row[0]['state'];
		$this->_response_type = $row[0]['response_type'];
		$this->_status = $row[0]['status'];
		$this->_added_at = $row[0]['added_at'];
		$this->_updated_at = $row[0]['updated_at'];
		return $row;
	}


	/**
	 * @desc   DELETE
	 */

	function delete($id)
	{
		$sql = "DELETE FROM paypal_transaction_map WHERE paypal_transaction_map_id = $id";
		$this->_obj->sql_query($sql);
	}


	/**
	 * @desc   INSERT
	 */

	function insert()
	{
		$this->paypal_transaction_map_id = ""; // clear key for autoincrement

		$sql = "INSERT INTO paypal_transaction_map ( transaction_id,paypal_id,currency_code,processable,environment,paypal_sdk_version,platform,product_name,create_time,intent,state,response_type,status,added_at,updated_at ) VALUES ( '" . $this->_transaction_id . "','" . $this->_paypal_id . "','" . $this->_currency_code . "','" . $this->_processable . "','" . $this->_environment . "','" . $this->_paypal_sdk_version . "','" . $this->_platform . "','" . $this->_product_name . "','" . $this->_create_time . "','" . $this->_intent . "','" . $this->_state . "','" . $this->_response_type . "','" . $this->_status . "','" . $this->_added_at . "','" . $this->_updated_at . "' )";
		$result = $this->_obj->insert($sql);
		return $result;
	}


	/**
	 * @desc   UPDATE
	 */

	function update($id)
	{

		$sql = " UPDATE paypal_transaction_map SET  transaction_id = '" . $this->_transaction_id . "' , paypal_id = '" . $this->_paypal_id . "' , currency_code = '" . $this->_currency_code . "' , processable = '" . $this->_processable . "' , environment = '" . $this->_environment . "' , paypal_sdk_version = '" . $this->_paypal_sdk_version . "' , platform = '" . $this->_platform . "' , product_name = '" . $this->_product_name . "' , create_time = '" . $this->_create_time . "' , intent = '" . $this->_intent . "' , state = '" . $this->_state . "' , response_type = '" . $this->_response_type . "' , status = '" . $this->_status . "' , added_at = '" . $this->_added_at . "' , updated_at = '" . $this->_updated_at . "'  WHERE paypal_transaction_map_id = $id ";
		$this->_obj->sql_query($sql);

	}


	/**
	 * @desc   CHECK PAYPAL PAYMENT ID EXISTS IN INTERNAL DATABASE
	 */

	function check_paypal_payment_id($id)
	{
		$sql = "SELECT COUNT(*) as TOT, transaction_id FROM paypal_transaction_map WHERE paypal_id = '{$id}' ";
//		echo $sql;exit;
		$result = $this->_obj->select($sql);
//pr($result);exit;
		if ($result[0]['TOT'] > 0) {
			return 1;
		} else {
			return 0;
		}
	}


	/**
	 * @desc   GET PAYPAL ACCESS-TOKEN BEARER
	 */

	function getPaypalToken()
	{
		$ch = curl_init();
		$clientId = "AWP8HlkXQrHb5LfILlqM_n-a9EAIAv153lj3Ta_fiaKnFxPMYqr_yv2zI-m7MFOoJj5Hh9AtPx6gkj_7";
		$secret = "EOqfGtBJprn9Cbk9QtBT-gz0CuhE6fZ1sNEfY2VfLkzaEVsqQ2anh7ZU7aoCllg9reyB2IssUDYugwTk";
		$accessToken = "";

		curl_setopt($ch, CURLOPT_URL, "https://api.sandbox.paypal.com/v1/oauth2/token");
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_USERPWD, $clientId . ":" . $secret);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=client_credentials");

		$result = curl_exec($ch);

		if (!empty($result)) {
			$json = json_decode($result);
			$accessToken = $json->access_token;
		}
		curl_close($ch);

		return $accessToken;
	}


	/**
	 * @desc   GET PAYPAL PAYMENT DETAIL
	 */

	function getPaypalPaymentDetail($accessToken, $paypal_id)
	{
		$ch = curl_init();
		$return = array();

		$url = "https://api.sandbox.paypal.com/v1/payments/payment/" . $paypal_id;
		$header = array(
			'Content-type:application/json',
			'Authorization: Bearer ' . $accessToken
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POST, false);
		curl_setopt($ch, CURLOPT_HTTPGET, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec($ch);
		curl_close($ch);

		if (!empty($result)) {
			$json = json_decode($result);
			$return['state'] = $json->state;
			$return['id'] = $json->transactions[0]->related_resources[0]->sale->id;
		}

		return $return;
	}


	/**
	 * @desc   GET PAYPAL TRANSACTION DETAIL
	 */

	function getPaypalTransactionDetail($accessToken, $id)
	{
		$ch = curl_init();
		$return = array();

		$postdata = array(
			'USER' => 'jaydeep.583_api1.gmail.com',
			'PWD' => 'W4TSBQM9RGL3EHZS',
			'SIGNATURE' => 'AFcWxV21C7fd0v3bYYYRCpSSRl31A3SDSwYNEXm4zhAZYMOOiBktcKWP',
			'METHOD' => 'GetTransactionDetails',
			'VERSION' => '123',
			'TransactionID' => $id
		);

		$postdata = http_build_query($postdata);

		$url = "https://api-3t.sandbox.paypal.com/nvp";

		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt($curl, CURLOPT_SSLVERSION, 6);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array(
			'Authorization: Bearer ' . $accessToken,
			'Accept: application/json',
			'Content-Type: application/json'
		));
		curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);
		$result1 = curl_exec($curl);
		curl_close($ch);

		$result = explode("&", $result1);

		$finalResult = array();
		foreach ($result as $k) {
			$temp = array();
			$temp = explode("=", $k);
			$finalResult[$temp[0]] = urldecode($temp[1]);
		}
//pr($finalResult);
		if (!empty($finalResult)) {
			$return['amount'] = $finalResult['AMT'];
			$return['fee'] = $finalResult['FEEAMT'];
			$return['finalAmount'] = $return['amount'];
			$return['chargeRate'] = 0.00;
			if (!empty($return['fee']) && !empty($return['amount']) && $return['amount'] > 0 && $return['fee'] > 0) {
				$return['chargeRate'] = (($return['fee'] * 100) / $return['amount']);
				$return['finalAmount'] = $return['amount'] - $return['fee'];
			}
		}
//		pr($return);exit;
		return $return;
	}


}
