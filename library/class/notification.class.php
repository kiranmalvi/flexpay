<?php
/**
*
* -------------------------------------------------------
* CLASSNAME:    notification
* DATE:         31.10.2015
* CLASS FILE:   /var/www/html/flexpay/library/class/class-making/generated_classes/notification.class.php
* TABLE:        notification
* DB:           flexpay
* -------------------------------------------------------
* AUTHOR:
* Mindinventory (MI)
* -------------------------------------------------------
*
*/

class notification
{


/**
*   @desc Variable Declaration with default value
*/

	protected $notification_id;   // KEY ATTR. WITH AUTOINCREMENT

	protected $_notification_id;  
	protected $_user_id;  
	protected $_transaction_id;  
	protected $_eRead;  
	protected $_added_at;  
	protected $_status;  



/**
*   @desc   CONSTRUCTOR METHOD
*/

	function __construct()
	{
		global $obj;
		$this->_obj = $obj;

		$this->_notification_id = null; 
		$this->_user_id = null; 
		$this->_transaction_id = null; 
		$this->_eRead = null; 
		$this->_added_at = null; 
		$this->_status = null; 
	}

/**
*   @desc   DECONSTRUCTOR METHOD
*/

	function __destruct()
	{
		unset($this->_obj);
	}



/**
*   @desc   GETTER METHODS
*/


	public function getnotification_id()
	{
		return $this->_notification_id;
	}

/**
*   @desc   SETTER METHODS
*/


	public function setnotification_id($val)
	{
		 $this->_notification_id =  $val;
	}

	public function getuser_id()
	{
		return $this->_user_id;
	}

	public function setuser_id($val)
	{
		 $this->_user_id =  $val;
	}

	public function gettransaction_id()
	{
		return $this->_transaction_id;
	}

	public function settransaction_id($val)
	{
		 $this->_transaction_id =  $val;
	}

	public function geteRead()
	{
		return $this->_eRead;
	}

	public function seteRead($val)
	{
		 $this->_eRead =  $val;
	}

	public function getadded_at()
	{
		return $this->_added_at;
	}

	public function setadded_at($val)
	{
		 $this->_added_at =  $val;
	}

	public function getstatus()
	{
		return $this->_status;
	}

	public function setstatus($val)
	{
		 $this->_status =  $val;
	}


/**
*   @desc   SELECT METHOD / LOAD
*/

	function select($id=null)
	{
		 $WHERE = 'WHERE 1=1';
		 if(!empty($id)) {
			 $WHERE .= ' AND notification_id = '.$id;
		 }
		 $sql =  "SELECT * FROM notification $WHERE  AND status!='2'";
		 $row =  $this->_obj->select($sql); 

		 $this->_notification_id = $row[0]['notification_id'];
		 $this->_user_id = $row[0]['user_id'];
		 $this->_transaction_id = $row[0]['transaction_id'];
		 $this->_eRead = $row[0]['eRead'];
		 $this->_added_at = $row[0]['added_at'];
		 $this->_status = $row[0]['status'];
		 return $row;
	}


/**
*   @desc   DELETE
*/

	function delete($id)
	{
		 $sql = "DELETE FROM notification WHERE notification_id = $id";
		 $this->_obj->sql_query($sql);
	}


/**
*   @desc   INSERT
*/

	function insert()
	{
		 $this->notification_id = ""; // clear key for autoincrement

		 $sql = "INSERT INTO notification ( user_id,transaction_id,eRead,added_at,status ) VALUES ( '".$this->_user_id."','".$this->_transaction_id."','".$this->_eRead."','".$this->_added_at."','".$this->_status."' )";
		 $result = $this->_obj->insert($sql);
		 return $result;
	}


/**
*   @desc   UPDATE
*/


	function update($id)
	{

		 $sql = " UPDATE notification SET  user_id = '".$this->_user_id."' , transaction_id = '".$this->_transaction_id."' , eRead = '".$this->_eRead."' , added_at = '".$this->_added_at."' , status = '".$this->_status."'  WHERE notification_id = $id ";
		 $this->_obj->sql_query($sql);

	}
    function read_notification($id)
    {

        $sql = " UPDATE notification SET  eRead = '1'  WHERE notification_id = $id ";
       $result=  $this->_obj->sql_query($sql);
        if($result >0)
          return  true;
        else
          return  false;

    }
	function notificationdetails($userid = null)
	{
		$temp_array=array();
		$sql = "select n.notification_id,n.transaction_id,n.eRead,n.added_at,t.transaction_id,t.to_user_id,t.from_user_id,t.type,t.comment,t.amount,t.refunded,t.refunded_transaction_id,t.via_user_id, (select name from user where id = t.via_user_id) as via_user_name from notification AS n LEFT JOIN transaction AS t ON n.transaction_id=t.transaction_id where n.user_id IN ($userid) AND n.status='1'";
		$row =  $this->_obj->select($sql);
		foreach($row AS $details)
		{
			if($details['type'] == "request" && $details['from_user_id'] == $userid && $details['via_user_id'] != 0) {
				if(($details['type'] == "debit" && $details['via_user_id'] == 0) || ($details['type'] != "debit")) {
					$usql	=	$this->_obj->sql_query("UPDATE notification SET eRead = '1' WHERE notification_id = '{$details['notification_id']}' ");
				}
			} else {
				if(($details['type'] == "debit" && $details['via_user_id'] == 0) || ($details['type'] != "debit")) {
					$usql	=	$this->_obj->sql_query("UPDATE notification SET eRead = '1' WHERE notification_id = '{$details['notification_id']}' ");
				}
				$user_name_id = ($details['from_user_id'] == $userid) ? $details['to_user_id'] : $details['from_user_id'];
				$details['type'] = ($details['from_user_id'] == $userid && $details['type'] == 'debit') ? 'credit' : $details['type'];
				$user_query = $this->_obj->select("SELECT name,email FROM `user` WHERE id = '{$user_name_id}'");
				$details['name'] = $user_query[0]['name'];
				$details['email'] = $user_query[0]['email'];
				array_push($temp_array, $details);
			}
		}
		return $temp_array;
	}

	function notificationdetailsOffsetLimit($userid = null, $timestamp = 0)
	{
		global $offset, $limit;
		$temp_array=array();
		$sql = "select n.notification_id,n.transaction_id,n.eRead,n.added_at,t.transaction_id,t.to_user_id,t.from_user_id,t.type,t.comment,t.amount,t.refunded,t.refunded_transaction_id,t.via_user_id,t.updated_at,t.transaction_type, (select name from user where id = t.via_user_id) as via_user_name from notification AS n LEFT JOIN transaction AS t ON n.transaction_id=t.transaction_id where n.user_id IN ($userid) AND n.status='1' AND t.updated_at > '{$timestamp}' ORDER BY t.updated_at DESC LIMIT {$offset}, {$limit}";
		$row =  $this->_obj->select($sql);
		foreach($row AS $details)
		{
			if($details['type'] == "request" && $details['from_user_id'] == $userid && $details['via_user_id'] != 0) {
				if(($details['type'] == "debit" && $details['via_user_id'] == 0) || ($details['type'] != "debit")) {
					$usql	=	$this->_obj->sql_query("UPDATE notification SET eRead = '1' WHERE notification_id = '{$details['notification_id']}' ");
				}
			} else {
				if(($details['type'] == "debit" && $details['via_user_id'] == 0) || ($details['type'] != "debit")) {
					$usql	=	$this->_obj->sql_query("UPDATE notification SET eRead = '1' WHERE notification_id = '{$details['notification_id']}' ");
				}
				$user_name_id = ($details['from_user_id'] == $userid) ? $details['to_user_id'] : $details['from_user_id'];
				$details['type'] = ($details['from_user_id'] == $userid && $details['type'] == 'debit') ? 'credit' : $details['type'];
				$user_query = $this->_obj->select("SELECT name,email FROM `user` WHERE id = '{$user_name_id}'");
				$details['name'] = $user_query[0]['name'];
				$details['email'] = $user_query[0]['email'];
				array_push($temp_array, $details);
			}
		}
		return $temp_array;
	}

	function get_notification($notificationid, $userid)
    {
        $temp_array=array();
		$sql = "select n.notification_id,n.transaction_id,n.eRead,n.added_at,t.transaction_id,t.to_user_id,t.from_user_id,t.type,t.comment,t.amount,t.refunded,t.refunded_transaction_id,t.via_user_id,t.transaction_type, (select name from user where id = t.via_user_id) as via_user_name from notification AS n LEFT JOIN transaction AS t ON n.transaction_id=t.transaction_id where n.notification_id IN ($notificationid) AND n.status='1' ";
        $row =  $this->_obj->select($sql);

        foreach($row AS $details)
        {
			$user_name_id = ($details['from_user_id'] == $userid) ? $details['to_user_id'] : $details['from_user_id'];
			$details['type'] = ($details['from_user_id'] == $userid && $details['type'] == 'debit') ? 'credit' : $details['type'];
			$user_query = $this->_obj->select("SELECT name,email FROM `user` WHERE id = '{$user_name_id}'");
			$details['name'] = $user_query[0]['name'];
			$details['email'] = $user_query[0]['email'];
			array_push($temp_array, $details);
        }
		//pr($temp_array);exit;
        return $temp_array;
    }

	function unread_notifications($id)
	{
		$sql = "SELECT COUNT(*) AS unread_count FROM notification as n LEFT JOIN transaction as t ON n.transaction_id = t.transaction_id WHERE n.user_id = '{$id}' AND n.eRead = '0' AND n.status = '1' AND ((t.type = 'debit' AND t.via_user_id = 0) OR t.type != 'debit')";
		$row = $this->_obj->select($sql);
		return $row;
	}

	function unread_notifications_V3($user_id,$id)
	{
		$sql = "SELECT COUNT(*) AS unread_count FROM notification as n LEFT JOIN transaction as t ON n.transaction_id = t.transaction_id WHERE n.user_id = '{$user_id}' AND n.eRead = '0' AND n.status = '1' AND t.via_user_id = '{$id}' AND t.type = 'debit'";
		$row = $this->_obj->select($sql);
		return $row;
	}

	function get_notificationid($transcationid,$receiverid)
	{
		$sql	=	"SELECT notification_id FROM notification WHERE user_id = '{$receiverid}' AND transaction_id = '{$transcationid}' AND status = '1'";
		$result	=	$this->_obj->select($sql);
		return	$result[0]['notification_id'];
	}


	function notificationdetailsSubUser($userid, $id)
	{
		$temp_array=array();
		$sql = "select n.notification_id,n.transaction_id,n.eRead,n.added_at,t.transaction_id,t.to_user_id,t.from_user_id,t.type,t.comment,t.amount,t.refunded,t.refunded_transaction_id,t.via_user_id, (select name from user where id = t.via_user_id) as via_user_name from notification AS n LEFT JOIN transaction AS t ON n.transaction_id=t.transaction_id where n.user_id IN ($userid) AND n.status='1' and t.via_user_id = '{$id}' and type != 'request'";
		$row =  $this->_obj->select($sql);
//		echo $sql;exit;
		foreach($row AS $details)
		{
			if($details['type'] == "debit" && $details['via_user_id'] == $id) {
				$usql	=	$this->_obj->sql_query("UPDATE notification SET eRead = '1' WHERE notification_id = '{$details['notification_id']}' ");
			}

			$user_name_id = ($details['from_user_id'] == $userid) ? $details['to_user_id'] : $details['from_user_id'];
			$parent_name_id = ($details['from_user_id'] != $userid) ? $details['to_user_id'] : $details['from_user_id'];
			$details['type'] = ($details['from_user_id'] == $userid && $details['type'] == 'debit') ? 'credit' : $details['type'];
			$user_query = $this->_obj->select("SELECT name,email FROM `user` WHERE id = '{$user_name_id}'");
			$user_query1 = $this->_obj->select("SELECT name,email FROM `user` WHERE id = '{$parent_name_id}'");
			$details['name'] = $user_query[0]['name'];
			$details['email'] = $user_query[0]['email'];
			$details['parent_user_name'] = $user_query1[0]['name'];
			$details['parent_user_email'] = $user_query1[0]['email'];
			array_push($temp_array, $details);
		}
		return $temp_array;
	}


	function notificationSubUserOffsetLimit($userid, $id, $timestamp = 0)
	{
		global $offset, $limit;
		$temp_array=array();
		$sql = "select n.notification_id,n.transaction_id,n.eRead,n.added_at,t.transaction_id,t.to_user_id,t.from_user_id,t.type,t.comment,t.amount,t.refunded,t.refunded_transaction_id,t.via_user_id,t.updated_at, (select name from user where id = t.via_user_id) as via_user_name from notification AS n LEFT JOIN transaction AS t ON n.transaction_id=t.transaction_id where n.user_id IN ($userid) AND n.status='1' AND t.via_user_id = '{$id}' AND type != 'request' AND t.updated_at > '{$timestamp}' ORDER BY t.updated_at DESC LIMIT {$offset}, {$limit}";
		$row =  $this->_obj->select($sql);
//		echo $sql;exit;
		foreach($row AS $details)
		{
			if($details['type'] == "debit" && $details['via_user_id'] == $id) {
				$usql	=	$this->_obj->sql_query("UPDATE notification SET eRead = '1' WHERE notification_id = '{$details['notification_id']}' ");
			}

			$user_name_id = ($details['from_user_id'] == $userid) ? $details['to_user_id'] : $details['from_user_id'];
			$parent_name_id = ($details['from_user_id'] != $userid) ? $details['to_user_id'] : $details['from_user_id'];
			$details['type'] = ($details['from_user_id'] == $userid && $details['type'] == 'debit') ? 'credit' : $details['type'];
			$user_query = $this->_obj->select("SELECT name,email FROM `user` WHERE id = '{$user_name_id}'");
			$user_query1 = $this->_obj->select("SELECT name,email FROM `user` WHERE id = '{$parent_name_id}'");
			$details['name'] = $user_query[0]['name'];
			$details['email'] = $user_query[0]['email'];
			$details['parent_user_name'] = $user_query1[0]['name'];
			$details['parent_user_email'] = $user_query1[0]['email'];
			array_push($temp_array, $details);
		}
		return $temp_array;
	}

	function get_notificationSubUser($notificationid, $userid, $id)
	{
		$temp_array=array();
		$sql = "select n.notification_id,n.transaction_id,n.eRead,n.added_at,t.transaction_id,t.to_user_id,t.from_user_id,t.type,t.comment,t.amount,t.refunded,t.refunded_transaction_id,t.via_user_id, (select name from user where id = t.via_user_id) as via_user_name from notification AS n LEFT JOIN transaction AS t ON n.transaction_id=t.transaction_id where n.notification_id IN ($notificationid) AND n.status='1' and t.via_user_id = '{$id}' ";
		$row =  $this->_obj->select($sql);

		foreach($row AS $details)
		{
			$user_name_id = ($details['from_user_id'] == $userid) ? $details['to_user_id'] : $details['from_user_id'];
			$parent_name_id = ($details['from_user_id'] != $userid) ? $details['to_user_id'] : $details['from_user_id'];
			$details['type'] = ($details['from_user_id'] == $userid && $details['type'] == 'debit') ? 'credit' : $details['type'];
			$user_query = $this->_obj->select("SELECT name,email FROM `user` WHERE id = '{$user_name_id}'");
			$user_query1 = $this->_obj->select("SELECT name,email FROM `user` WHERE id = '{$parent_name_id}'");
			$details['name'] = $user_query[0]['name'];
			$details['email'] = $user_query[0]['email'];
			$details['parent_user_name'] = $user_query1[0]['name'];
			$details['parent_user_email'] = $user_query1[0]['email'];
			array_push($temp_array, $details);
		}
		//pr($temp_array);exit;
		return $temp_array;
	}

}
