<?php

/**
 *
 * -------------------------------------------------------
 * CLASSNAME:    promotions
 * DATE:         09.08.2016
 * CLASS FILE:   /var/www/html/flexpay/library/class/class-making/generated_classes/promotions.class.php
 * TABLE:        promotions
 * DB:           mind_flexpay
 * -------------------------------------------------------
 * AUTHOR:
 * Mindinventory (MI)
 * -------------------------------------------------------
 *
 */
class promotions
{


	/**
	 * @desc Variable Declaration with default value
	 */

	protected $promotion_id;   // KEY ATTR. WITH AUTOINCREMENT

	protected $_promotion_id;
	protected $_user_id;
	protected $_company_name;
	protected $_description;
	protected $_address;
	protected $_startdate;
	protected $_enddate;
	protected $_discount;
	protected $_transaction_limit;
	protected $_contact;
	protected $_cashback;
	protected $_status;
	protected $_added_at;
	protected $_updated_at;


	/**
	 * @desc   CONSTRUCTOR METHOD
	 */

	function __construct()
	{
		global $obj;
		$this->_obj = $obj;

		$this->_promotion_id = null;
		$this->_user_id = null;
		$this->_company_name = null;
		$this->_description = null;
		$this->_address = null;
		$this->_startdate = null;
		$this->_enddate = null;
		$this->_discount = null;
		$this->_transaction_limit = null;
		$this->_contact = null;
		$this->_cashback = null;
		$this->_status = null;
		$this->_added_at = null;
		$this->_updated_at = null;
	}

	/**
	 * @desc   DECONSTRUCTOR METHOD
	 */

	function __destruct()
	{
		unset($this->_obj);
	}


	/**
	 * @desc   GETTER METHODS
	 */


	public function getpromotion_id()
	{
		return $this->_promotion_id;
	}


	/**
	 * @desc   SETTER METHODS
	 */

	public function setpromotion_id($val)
	{
		$this->_promotion_id = $val;
	}


	public function getuser_id()
	{
		return $this->_user_id;
	}


	public function setuser_id($val)
	{
		$this->_user_id = $val;
	}


	public function getcompany_name()
	{
		return $this->_company_name;
	}


	public function setcompany_name($val)
	{
		$this->_company_name = $val;
	}


	public function getdescription()
	{
		return $this->_description;
	}


	public function setdescription($val)
	{
		$this->_description = $val;
	}


	public function getstartdate()
	{
		return $this->_startdate;
	}


	public function setstartdate($val)
	{
		$this->_startdate = $val;
	}


	public function getenddate()
	{
		return $this->_enddate;
	}


	public function setenddate($val)
	{
		$this->_enddate = $val;
	}


	public function getdiscount()
	{
		return $this->_discount;
	}


	public function setdiscount($val)
	{
		$this->_discount = $val;
	}


	public function gettransaction_limit()
	{
		return $this->_transaction_limit;
	}


	public function settransaction_limit($val)
	{
		$this->_transaction_limit = $val;
	}


	public function getcontact()
	{
		return $this->_contact;
	}


	public function setcontact($val)
	{
		$this->_contact = $val;
	}


	public function getcashback()
	{
		return $this->_cashback;
	}


	public function setcashback($val)
	{
		$this->_cashback = $val;
	}


	public function getstatus()
	{
		return $this->_status;
	}


	public function setstatus($val)
	{
		$this->_status = $val;
	}


	public function getadded_at()
	{
		return $this->_added_at;
	}


	public function setadded_at($val)
	{
		$this->_added_at = $val;
	}


	public function getupdated_at()
	{
		return $this->_updated_at;
	}


	public function setupdated_at($val)
	{
		$this->_updated_at = $val;
	}

	public function getaddress()
	{
		return $this->_address;
	}

	public function setaddress($address)
	{
		$this->_address = $address;
	}


	/**
	 * @desc   SELECT METHOD / LOAD
	 */

	function select($id = null)
	{
		$WHERE = 'WHERE 1=1';
		if (!empty($id)) {
			$WHERE .= ' AND promotion_id = ' . $id;
		}
		$sql = "SELECT * FROM promotions $WHERE";
		$row = $this->_obj->select($sql);

		$this->_promotion_id = $row[0]['promotion_id'];
		$this->_user_id = $row[0]['user_id'];
		$this->_company_name = $row[0]['company_name'];
		$this->_description = $row[0]['description'];
		$this->_startdate = $row[0]['startdate'];
		$this->_enddate = $row[0]['enddate'];
		$this->_discount = $row[0]['discount'];
		$this->_transaction_limit = $row[0]['transaction_limit'];
		$this->_contact = $row[0]['contact'];
		$this->_cashback = $row[0]['cashback'];
		$this->_status = $row[0]['status'];
		$this->_added_at = $row[0]['added_at'];
		$this->_updated_at = $row[0]['updated_at'];
		return $row;
	}


	/**
	 * @desc   DELETE
	 */

	function delete($id)
	{
		$sql = "DELETE FROM promotions WHERE promotion_id = $id";
		$this->_obj->sql_query($sql);
	}


	/**
	 * @desc   INSERT
	 */

	function insert()
	{
		$this->promotion_id = ""; // clear key for autoincrement

		$sql = "INSERT INTO promotions ( user_id,company_name,description,address,startdate,enddate,discount,transaction_limit,contact,cashback,status,added_at,updated_at ) VALUES ( '" . $this->_user_id . "','" . $this->_company_name . "','" . $this->_description . "','" . $this->_address . "','" . $this->_startdate . "','" . $this->_enddate . "','" . $this->_discount . "','" . $this->_transaction_limit . "','" . $this->_contact . "','" . $this->_cashback . "','" . $this->_status . "','" . $this->_added_at . "','" . $this->_updated_at . "' )";
		$result = $this->_obj->insert($sql);
		return $result;
	}


	/**
	 * @desc   UPDATE
	 */

	function update($id)
	{

		$sql = " UPDATE promotions SET  user_id = '" . $this->_user_id . "' , company_name = '" . $this->_company_name . "' , description = '" . $this->_description . "' , address = '" . $this->_address . "' , startdate = '" . $this->_startdate . "' , enddate = '" . $this->_enddate . "' , discount = '" . $this->_discount . "' , transaction_limit = '" . $this->_transaction_limit . "' , contact = '" . $this->_contact . "' , cashback = '" . $this->_cashback . "' , status = '" . $this->_status . "' , added_at = '" . $this->_added_at . "' , updated_at = '" . $this->_updated_at . "'  WHERE promotion_id = $id ";
		$result = $this->_obj->sql_query($sql);
		return $result;
	}


	/**
	 * @desc   List user promotions
	 */

	function userPromotions($user_id)
	{
		$sql = "SELECT p.* FROM promotions AS p LEFT JOIN user AS u ON p.user_id = u.id WHERE p.user_id = '{$user_id}' AND p.status != '2' AND u.status = '1' ORDER BY p.enddate DESC";
//		echo $sql;exit;
		$result = $this->_obj->select($sql);
		return $result;
	}


	/**
	 * @desc   List promotions API
	 */

	function promotionList($timestamp = 0, $search = '')
	{
		global $offset, $limit;

		$search_sql = "";
		if($search != ''){
			$search_sql = " AND (p.company_name LIKE '%{$search}%' OR p.description LIKE '%{$search}%') ";
		}

		$sql = "SELECT u.email, p.* FROM promotions AS p LEFT JOIN user AS u ON p.user_id = u.id WHERE p.status = '1' AND u.status = '1' AND p.updated_at > '{$timestamp}' $search_sql ORDER BY p.enddate DESC LIMIT {$offset}, {$limit}";
//		echo $sql;exit;
		$result = $this->_obj->select($sql);
		return $result;
	}


	/**
	 * @desc   Running promotions between users date
	 */

	function runningPromotions($userid, $amount)
	{
		// Use later as per user timezone
		/*$userTimezoneSql	=	"SELECT timezone_id, timezone_str, timezone_time FROM user_timezone WHERE user_id = '{$userid}'";
		$timezoneData	=	$this->_obj->select($userTimezoneSql);

		$usersTimezone = $timezoneData[0]['timezone_str'];*/
		$usersTimezone = "America/Grand_Turk"; // by default clients timezone
		$datetime = new DateTime( 'now', new DateTimeZone($usersTimezone) );
		$date	=	$datetime->format('Y-m-d');

		$sql = "SELECT p.promotion_id, p.discount, p.transaction_limit, p.cashback FROM promotions AS p
WHERE p.status = '1' AND p.transaction_limit <= '{$amount}' AND (p.startdate <= '{$date}' AND p.enddate >= '{$date}') AND p.user_id = '{$userid}' ORDER BY p.discount DESC LIMIT 0,1";
//		echo $sql;exit;
		$result = $this->_obj->select($sql);
		return $result;
	}


	/**
	 * @desc   get admin rewards detail
	 */

	function adminRewards($userid)
	{
		$result = array();
		$result['current_spend'] = 0;
		$result['reward'] = 0;
		$result['reward_limit'] = 0;

		//get admin reward details
		$reward_sql = "SELECT vValue, vDefValue FROM setting WHERE vName = 'ADMIN_REWARD_LIMIT' AND vDesc = 'ADMIN_REWARD_LIMIT'";
//		echo $sql;exit;
		$reward_result = $this->_obj->select($reward_sql);
		$result['reward']	=	$reward_result[0]['vDefValue'];
		$result['reward_limit']	=	$reward_result[0]['vValue'];

		//get last cash back information
		$last_cashback = "SELECT transaction_id FROM transaction WHERE to_user_id = 0 AND from_user_id = '{$userid}' AND type = 'debit' AND transaction_status = 'confirm' AND transaction_type = '3' AND promotion_id = 0 AND status = '1' AND added_at > '2016-09-07 00:00:00' ORDER BY transaction_id DESC LIMIT 0,1";
		$last_cashback_result = $this->_obj->select($last_cashback);

		if(count($last_cashback_result) > 0){
			if($last_cashback_result[0]['transaction_id'] > 0) {

				//get current total spend of user after last cash back
				$spend_limit_sql	=	"SELECT SUM(amount) as current_spend FROM transaction WHERE transaction_id > '{$last_cashback_result[0]['transaction_id']}' AND to_user_id = '{$userid}' AND type = 'debit' AND transaction_status = 'confirm' AND status = '1' AND from_user_id != 0 AND transaction_type = '1' AND promotion_id = 0";
				$spend_amount	=	$this->_obj->select($spend_limit_sql);

				if($spend_amount[0]['current_spend'] >= $reward_result[0]['vValue']) {// check user able for reward
					$result['reward']	=	$reward_result[0]['vDefValue'];
					$result['reward_limit']	=	$reward_result[0]['vValue'];
					$result['current_spend']	=	$spend_amount[0]['current_spend'];
				}
			}
		} else {
			//get current total spend of user after last cash back
			$spend_limit_sql = "SELECT SUM(amount) as current_spend FROM transaction WHERE to_user_id = '{$userid}' AND type = 'debit' AND transaction_status = 'confirm' AND status = '1' AND from_user_id != 0 AND transaction_type = '1' AND promotion_id = 0 AND added_at > '2016-09-07 00:00:00'";
			$spend_amount	=	$this->_obj->select($spend_limit_sql);

			if($spend_amount[0]['current_spend'] >= $reward_result[0]['vValue']) {// check user able for reward
				$result['reward']	=	($reward_result[0]['vDefValue'] !== 0) ? $reward_result[0]['vDefValue'] : 0;
				$result['reward_limit']	=	$reward_result[0]['vValue'];
				$result['current_spend']	=	$spend_amount[0]['current_spend'];
			}
		}

		return $result;
	}


	function updatePromotions($id)
	{
		$sql    = " UPDATE promotions SET  user_id = '" . $this->_user_id . "' , company_name = '" . $this->_company_name . "' , description = '" . $this->_description . "' , address = '" . $this->_address . "' , startdate = '" . $this->_startdate . "' , enddate = '" . $this->_enddate . "' , discount = '" . $this->_discount . "' , transaction_limit = '" . $this->_transaction_limit . "' , contact = '" . $this->_contact . "' , cashback = '" . $this->_cashback . "' , updated_at = '" . $this->_updated_at . "'  WHERE promotion_id = '{$id}' ";
		$result = $this->_obj->sql_query($sql);

		return $result;
	}

}
