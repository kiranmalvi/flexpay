<?php
/**
*
* -------------------------------------------------------
* CLASSNAME:    token_log
* DATE:         20.08.2016
* CLASS FILE:   /var/www/html/flexpay/library/class/class-making/generated_classes/token_log.class.php
* TABLE:        token_log
* DB:           mind_flexpay
* -------------------------------------------------------
* AUTHOR:
* Mindinventory (MI)
* -------------------------------------------------------
*
*/

class token_log
{


/**
*   @desc Variable Declaration with default value
*/

	protected $token_id;   // KEY ATTR. WITH AUTOINCREMENT

	protected $_token_id;  
	protected $_user_id;
	protected $_token;  
	protected $_status;  
	protected $_added_at;  
	protected $_updated_at;  



/**
*   @desc   CONSTRUCTOR METHOD
*/

	function __construct()
	{
		global $obj;
		$this->_obj = $obj;

		$this->_token_id = null; 
		$this->_user_id = null;
		$this->_token = null; 
		$this->_status = null; 
		$this->_added_at = null; 
		$this->_updated_at = null; 
	}

/**
*   @desc   DECONSTRUCTOR METHOD
*/

	function __destruct()
	{
		unset($this->_obj);
	}



/**
*   @desc   GETTER METHODS
*/


	public function gettoken_id()
	{
		return $this->_token_id;
	}

	/**
	 * @desc   SETTER METHODS
	 */


	public function settoken_id($val)
	{
		$this->_token_id = $val;
	}

	public function getuser_id()
	{
		return $this->_user_id;
	}

	public function setuser_id($val)
	{
		$this->_user_id = $val;
	}

	public function gettoken()
	{
		return $this->_token;
	}

	public function settoken($val)
	{
		$this->_token = $val;
	}

	public function getstatus()
	{
		return $this->_status;
	}

	public function setstatus($val)
	{
		$this->_status = $val;
	}

	public function getadded_at()
	{
		return $this->_added_at;
	}

	public function setadded_at($val)
	{
		$this->_added_at = $val;
	}

	public function getupdated_at()
	{
		return $this->_updated_at;
	}

	public function setupdated_at($val)
	{
		 $this->_updated_at =  $val;
	}


/**
*   @desc   SELECT METHOD / LOAD
*/

	function select($id=null)
	{
		 $WHERE = 'WHERE 1=1';
		 if(!empty($id)) {
			 $WHERE .= ' AND token_id = '.$id;
		 }
		 $sql =  "SELECT * FROM token_log $WHERE";
		 $row =  $this->_obj->select($sql); 

		 $this->_token_id = $row[0]['token_id'];
		 $this->_user_id = $row[0]['user_id'];
		 $this->_token = $row[0]['token'];
		 $this->_status = $row[0]['status'];
		 $this->_added_at = $row[0]['added_at'];
		 $this->_updated_at = $row[0]['updated_at'];
		 return $row;
	}


/**
*   @desc   DELETE
*/

	function delete($id)
	{
		 $sql = "DELETE FROM token_log WHERE token_id = $id";
		 $this->_obj->sql_query($sql);
	}


/**
*   @desc   INSERT
*/

	function insert()
	{
		 $this->token_id = ""; // clear key for autoincrement

		$sql = "INSERT INTO token_log ( user_id,token,status,added_at,updated_at ) VALUES ( '" . $this->_user_id . "','" . $this->_token . "','" . $this->_status . "','" . $this->_added_at . "','" . $this->_updated_at . "' )";
		 $result = $this->_obj->insert($sql);
		 return $result;
	}


/**
*   @desc   UPDATE
*/

	function update($id)
	{
		 $sql = " UPDATE token_log SET  user_id = '".$this->_user_id."' , token = '".$this->_token."' , status = '".$this->_status."' , added_at = '".$this->_added_at."' , updated_at = '".$this->_updated_at."'  WHERE token_id = $id ";
		 return	$this->_obj->sql_query($sql);
	}

	function token_log_apicall($user_id, $clientAuthToken)
	{
		$gmt_datetime = gmdate('Y-m-d H:i:s');

		$sql =  "SELECT count(*) as tot, token_id FROM token_log WHERE user_id = '".$user_id."' AND token = '".$clientAuthToken."' AND status = '1'";
		$row =  $this->_obj->select($sql);

		if($row[0]['tot'] > 0 && ($row[0]['token_id'] != "" || $row[0]['token_id'] != 0)){
			$token_id = $row[0]['token_id'];
			$update_sql = " UPDATE token_log SET updated_at = '".$gmt_datetime."' WHERE token_id = '".$token_id."' ";
			$this->_obj->sql_query($update_sql);
		} else {
			$insert_sql = "insert into token_log (user_id, token, status, added_at, updated_at) values ('" . $user_id . "', '" . $clientAuthToken . "', '1', '" . $gmt_datetime . "', '" . $gmt_datetime . "')";
			$this->_obj->insert($insert_sql);
		}
		return;
	}

	function getUpdateTokenLog($user_id, $clientAuthToken)
	{
		$sql =  "SELECT updated_at, token_id FROM token_log WHERE user_id = '".$user_id."' AND token = '".$clientAuthToken."' AND status = '1'";
		$row =  $this->_obj->select($sql);
		return $row;
	}

	function UpdateTokenLog($token_id)
	{
		$gmt_datetime = gmdate('Y-m-d H:i:s');
		$sql = "UPDATE token_log SET updated_at = '".$gmt_datetime."', status = '0' WHERE token_id = '".$token_id."' ";
		return $this->_obj->sql_query($sql);

	}


}
