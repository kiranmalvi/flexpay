<?php

/**
 * Created by PhpStorm.
 * User: Mayur
 * Date: 28/6/14
 * Time: 10:46 AM
 */
class API
{

    private static $xml = null;
    private static $encoding = 'UTF-8';

    /**
     * @param $data
     * @return mixed
     */
    function cleandata($data)
    {
        if ($_REQUEST['show'] != 'errors') {
            $i = 0;
            foreach ($data as $k => $v) {
                if (is_array($v)) {
                    array_map(__METHOD__, $v);
                } else {
//                    if (is_null($v)) {
//                        $data[$i][$k] = "";
//                    }
                }
                $i++;
            }
        }
        return $data;
    }

    /**
     * @param $data
     * @param $key
     * @param $sort
     * @return mixed
     */
    function sortarray($data, $key, $sort)
    {
        $count = count($data);
        for ($j = 0; $j < $count; $j++) {
            for ($i = 0; $i < $count; $i++) {
                if ($sort == 'ASC') {
                    $condition = ($data[$j][$key] < $data[$i][$key]);
                } else {
                    $condition = ($data[$j][$key] > $data[$i][$key]);
                }
                if ($condition) {
                    $temp = $data[$i][$key];
                    $data[$i][$key] = $data[$j][$key];
                    $data[$j][$key] = $temp;
                }
            }
        }
        return $data;
    }

    function sortByKey($data, $key, $sort)
    {
        $count = count($data);
        for ($j = 0; $j < $count; $j++) {
            for ($i = 0; $i < $count; $i++) {
                if ($sort == 'ASC') {
                    if (is_string($key)) {
                        $condition = (strtolower($data[$j][$key]) < strtolower($data[$i][$key]));
                    } else {
                        $condition = ($data[$j][$key] < $data[$i][$key]);
                    }
                } else {
                    if (is_string($key)) {
                        $condition = (strtolower($data[$j][$key]) > strtolower($data[$i][$key]));
                    } else {
                        $condition = ($data[$j][$key] > $data[$i][$key]);
                    }
                }
                if ($condition) {
                    $temp = $data[$i];
                    $data[$i] = $data[$j];
                    $data[$j] = $temp;
                }
            }
        }
        return $data;
    }

    /**
     * @param $response
     */
    function sendXML($response)
    {
        ob_get_clean();
        $xmldata = self::createXML('response', $response);
        $xml = $xmldata->saveXML();
        header('Content-Type: application/xml; charset=utf-8');
        header("Cache-Control: no-cache, must-revalidate");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
        header('Content-length: ' . strlen($xml));
        echo $xml;
        exit;
    }

    /**
     * @param $node_name
     * @param array $arr
     * @return null
     */
    public static function &createXML($node_name, $arr = array())
    {
        $xml = self::getXMLRoot();
        $xml->appendChild(self::convert($node_name, $arr));

        self::$xml = null;    // clear the xml node in the class for 2nd time use.
        return $xml;
    }

    /**
     * @return null
     */
    private static function getXMLRoot()
    {
        if (empty(self::$xml)) {
            self::init();
        }
        return self::$xml;
    }

    /**
     * @param string $version
     * @param string $encoding
     * @param bool $format_output
     */
    public static function init($version = '1.0', $encoding = 'UTF-8', $format_output = true)
    {
        self::$xml = new DomDocument($version, $encoding);
        self::$xml->formatOutput = $format_output;
        self::$encoding = $encoding;
    }

    /**
     * @param $node_name
     * @param array $arr
     * @return mixed
     * @throws Exception
     */
    private static function &convert($node_name, $arr = array())
    {

        $xml = self::getXMLRoot();
        $node = $xml->createElement($node_name);

        if (is_array($arr)) {
            if (isset($arr['@attributes'])) {
                foreach ($arr['@attributes'] as $key => $value) {
                    if (!self::isValidTagName($key)) {
                        throw new Exception('[Array2XML] Illegal character in attribute name. attribute: ' . $key . ' in node: ' . $node_name);
                    }
                    $node->setAttribute($key, self::bool2str($value));
                }
                unset($arr['@attributes']);
            }

            if (isset($arr['@value'])) {
                $node->appendChild($xml->createTextNode(self::bool2str($arr['@value'])));
                unset($arr['@value']);
                return $node;
            } else if (isset($arr['@cdata'])) {
                $node->appendChild($xml->createCDATASection(self::bool2str($arr['@cdata'])));
                unset($arr['@cdata']);
                return $node;
            }
        }

        if (is_array($arr)) {
            foreach ($arr as $key => $value) {
                if (!self::isValidTagName($key)) {
                    throw new Exception('[Array2XML] Illegal character in tag name. tag: ' . $key . ' in node: ' . $node_name);
                }
                if (is_array($value) && is_numeric(key($value))) {
                    foreach ($value as $k => $v) {
                        $node->appendChild(self::convert($key, $v));
                    }
                } else {
                    $node->appendChild(self::convert($key, $value));
                }
                unset($arr[$key]);
            }
        }

        if (!is_array($arr)) {
            $node->appendChild($xml->createTextNode(self::bool2str($arr)));
        }

        return $node;
    }

    /**
     * @param $tag
     * @return bool
     */
    private static function isValidTagName($tag)
    {
        $pattern = '/^[a-z_]+[a-z0-9\:\-\.\_]*[^:]*$/i';
        return preg_match($pattern, $tag, $matches) && $matches[0] == $tag;
    }

    /**
     * @param $v
     * @return string
     */
    private static function bool2str($v)
    {
        $v = $v === true ? 'true' : $v;
        $v = $v === false ? 'false' : $v;
        return $v;
    }

    /**
     * @param $response
     */
    function sendJSON($response)
    {
        ob_get_clean();
        $final_response['response'] = $response;
        $json = utf8_encode(json_encode($final_response));
        header("Cache-Control: no-cache, must-revalidate");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
        header('Content-type: application/json; charset=UTF-8');
        header('Content-length: ' . strlen($json));
        echo $json;
        exit;
    }

    function buildResponse($data, $msg, $status, $extras = "")
    {
        foreach ($extras as $k => $v) {
            $response[$k] = $v;
        }
        $response['data'] = $data;
        #pr($extras); exit;
        /*if ($extras != "" OR isset($extras)) {
            $response['vExtras'] = $extras;
        }*/
        $response['status'] = $status;
        $response['message'] = $msg;

        /**  DO NOT DELETE
         *
         * $response = $apiObj->convertArrayKeys($apiObj->cleanAPI($response, $responseParams));
         * $response = $apiObj->cleanAPI($response);
         * $response = $apiObj->convertArrayKeys($response, $responseParams);
         */

        $response = $this->cleanAPI($response);
        return $response;
    }

    /**
     * @param $obj
     * @param array $allowedParams
     * @return array
     */
    function cleanAPI($obj, $allowedParams = [])
    {
        global $removeParams;
        $arrObj = $obj;
        foreach ($arrObj as $key => $val) {
            $val = (is_array($val) || is_object($val)) ? self::cleanAPI((array)$val, $allowedParams) : trim($val);
            if (is_array($obj)) $obj[$key] = $val;
            if (!is_array($val)) {
                $obj[$key] = stripcslashes($val);
                //unset($obj[$key]);
                if ($val == null || is_null($val) || $val == '') {
                    $obj[$key] = '';
                }
            }
            if (in_array($key, $removeParams) && $key !== 0) {
                unset($obj[$key]);
            }

            if (!empty($allowedParams) && is_array($allowedParams) && count($allowedParams) > 0) {
                if (!in_array($key, $allowedParams) && $key !== 0) {
                    unset($obj[$key]);
                }
            }
        }
        return $obj;
    }

    /**
     * @param $array
     * @return mixed
     */
    function convertArrayKeys($array)
    {
        $retVal = [];
        foreach ($array as $key => $val) $retVal[self::convertKey($key)] = is_array($val) ? self::convertArrayKeys($val) : $val;
        return $retVal;
    }

    /**
     * @param $key
     * @return string
     */
    function convertKey($key)
    {
        preg_match('/[$a-z(_?)]+/', $key, $matches);
        if ($matches[0] != $key) {
            $key = substr($key, strlen($matches[0]));
            $key = strtolower(preg_replace('/(?<!^)([A-Z])/', '_$1', $key));
        }
        return $key;
    }

    function notify($user_id, $message, $type, $amt = '',$obj_id)
    {
        $APPLICATION_ID = "SCehjlxYSeW5nAGQPPme5LFsoPHNPK5Igjti3w7U";
        $REST_API_KEY = "lTBJybxW87heaVKT1MAli0E84f2SrIszKQrA0b7g";



        $url = 'https://api.parse.com/1/push';


        $data = array(
            'where' => array(
                'userID' => $user_id,
            ),
            'data' => array(
                'action' => 'com.mi.flexpay',
                'alert' => $message,
                'sound' => 'push.caf',
                'badge' => 'Increment',
                'type' => $type,
                'user_id' => $user_id,
                'amount' => $amt,
                'notification_id' => $obj_id
            )
        );


        $_data = json_encode($data);
        $headers = array(
            'X-Parse-Application-Id: ' . $APPLICATION_ID,
            'X-Parse-REST-API-Key: ' . $REST_API_KEY,
            'Content-Type: application/json',
            'Content-Length: ' . strlen($_data),
        );

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $_data);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $status = json_decode(curl_exec($curl), 1);
        if (array_key_exists('error', $status)) {
            $data = json_encode($status);
            $response['msg'] = $data;
            $response['status'] = '0';
            $final_response['response'] = $response;
            global $allowedTags;
            $final_response = $this->cleanAPI($final_response, $allowedTags);
            header('Content-type: application/json; charset=UTF-8');
            echo utf8_encode(json_encode($final_response));
            ob_flush();
            exit;
        }
    }

	function notifyOneSignal($user_id, $message, $type, $amt = '',$obj_id)
	{
		global $obj;
		##
		$APPLICATION_ID = "db758820-b771-44f7-ba05-bcca8c4ec672";
		$REST_API_KEY = "MDcxM2ZhODUtZTk5OC00OTc2LTk2MjUtZjM0M2Y4NGMwYjE2";

		## Get User Device Token
		$sql = "SELECT device_id,player_id FROM user_login_devices WHERE user_id = '{$user_id}'";
		$deviceTokens	=	$obj->select($sql);

		$playerIds = array();
		foreach ($deviceTokens as $deviceToken) {
			if (!empty($deviceToken['player_id'])) {
				$playerIds[] = $deviceToken['player_id'];
			}
		}

		if (!empty($playerIds)) {
			## Set Content/ Message
			$content = array("en" => $message);

			## Set Data
			$data = array(
				'amount' => $amt,	// amount
				'type' => $type,	// type
				'user_id' => $user_id,	// User_id
				'notification_id' => $obj_id	// notification Id
			);

			## Set Fields
			$fields = array(
				'app_id' => $APPLICATION_ID,
				'include_player_ids' => $playerIds,
				'ios_sound' => 'push.caf',
				'android_sound' => 'push.caf',
				'data' => $data,
				'contents' => $content,
				'ios_badgeType' => 'Increase',
				'ios_badgeCount' => 1
			);

			$fields = json_encode($fields);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Basic ' . $REST_API_KEY));
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_HEADER, FALSE);
			curl_setopt($ch, CURLOPT_POST, TRUE);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

			$response = curl_exec($ch);
			curl_close($ch);
//			pr($response);exit;
			return $response;
		} else {
			return true;
		}
	}

    function sendSms($to, $from, $text)
    {
        require_once('plivo.php');
        $auth_id = "MAOGYYZJU2OGVLMWE3M2";
        $auth_token = "MmU1MDA2ZDBmYjllMjk0MjVlOWI5MTQxNWI5OGRh";
        $p = new RestAPI($auth_id, $auth_token);

        $params = array(
            'src' => $from,
            'dst' => $to,
            'text' => $text,
            'type' => 'sms',
        );
        $response = $p->send_message($params);
        #echo "Gateway Response: <pre>"; print_r($response);exit;

        if ($response['response']['error'] != "") {
            $response_error = $response['response']['error'];
        }
        if ($response['response']['message']) {
            $response_msg_succ = $response['response']['message'];
        }

        if (array_shift(array_values($response)) == "202") {
            return $response_msg_succ;
        } else {
            return false;
        }
    }

}
