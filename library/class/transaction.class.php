<?php
/**
*
* -------------------------------------------------------
* CLASSNAME:    transaction
* DATE:         28.10.2015
* CLASS FILE:   /var/www/html/flexpay/library/class/class-making/generated_classes/transaction.class.php
* TABLE:        transaction
* DB:           flexpay
* -------------------------------------------------------
* AUTHOR:
* Mindinventory (MI)
* -------------------------------------------------------
*
*/

class transaction
{


/**
*   @desc Variable Declaration with default value
*/

	protected $transaction_id;   // KEY ATTR. WITH AUTOINCREMENT

	protected $_transaction_id;  
	protected $_transaction_unique_id;  
	protected $_refunded_transaction_id;
	protected $_from_user_id;
	protected $_to_user_id;  
	protected $_via_user_id;
	protected $_amount;
	protected $_type;  
	protected $_comment;  
	protected $_transaction_status;  
	protected $_refunded;
	protected $_added_at;
	protected $_updated_at;  
	protected $_status;  
	protected $_transaction_type;
	protected $_promotion_id;



/**
*   @desc   CONSTRUCTOR METHOD
*/

	function __construct()
	{
		global $obj;
		$this->_obj = $obj;

		$this->_transaction_id = null; 
		$this->_transaction_unique_id = null; 
		$this->_refunded_transaction_id = null;
		$this->_from_user_id = null;
		$this->_to_user_id = null; 
		$this->_via_user_id = null;
		$this->_amount = null;
		$this->_type = null; 
		$this->_comment = null; 
		$this->_transaction_status = null; 
		$this->_refunded = null;
		$this->_added_at = null;
		$this->_updated_at = null; 
		$this->_status = null; 
		$this->_transaction_type = null;
		$this->_promotion_id = null;
	}

/**
*   @desc   DECONSTRUCTOR METHOD
*/

	function __destruct()
	{
		unset($this->_obj);
	}



/**
*   @desc   GETTER METHODS
*/


	public function gettransaction_id()
	{
		return $this->_transaction_id;
	}

	/**
	 * @desc   SETTER METHODS
	 */


	public function settransaction_id($val)
	{
		$this->_transaction_id = $val;
	}

	public function gettransaction_unique_id()
	{
		return $this->_transaction_unique_id;
	}

	public function settransaction_unique_id($val)
	{
		$this->_transaction_unique_id = $val;
	}

	public function getrefunded_transaction_id()
	{
		return $this->_refunded_transaction_id;
	}

	public function setrefunded_transaction_id($val)
	{
		$this->_refunded_transaction_id = $val;
	}

	public function getfrom_user_id()
	{
		return $this->_from_user_id;
	}

	public function setfrom_user_id($val)
	{
		$this->_from_user_id = $val;
	}

	public function getto_user_id()
	{
		return $this->_to_user_id;
	}

	public function setto_user_id($val)
	{
		$this->_to_user_id = $val;
	}

	public function getvia_user_id()
	{
		return $this->_via_user_id;
	}

	public function setvia_user_id($val)
	{
		$this->_via_user_id = $val;
	}

	public function getamount()
	{
		return $this->_amount;
	}

	public function setamount($val)
	{
		$this->_amount = $val;
	}

	public function gettype()
	{
		return $this->_type;
	}

	public function settype($val)
	{
		$this->_type = $val;
	}

	public function getcomment()
	{
		return $this->_comment;
	}

	public function setcomment($val)
	{
		$this->_comment = $val;
	}

	public function gettransaction_status()
	{
		return $this->_transaction_status;
	}

	public function settransaction_status($val)
	{
		$this->_transaction_status = $val;
	}

	public function getrefunded()
	{
		return $this->_refunded;
	}

	public function setrefunded($val)
	{
		$this->_refunded = $val;
	}

	public function getadded_at()
	{
		return $this->_added_at;
	}

	public function setadded_at($val)
	{
		$this->_added_at = $val;
	}

	public function getupdated_at()
	{
		return $this->_updated_at;
	}

	public function setupdated_at($val)
	{
		$this->_updated_at = $val;
	}

	public function getstatus()
	{
		return $this->_status;
	}

	public function setstatus($val)
	{
		 $this->_status =  $val;
	}

	public function gettransaction_type()
	{
		return $this->_transaction_type;
	}

	public function settransaction_type($val)
	{
		 $this->_transaction_type =  $val;
	}

	public function getpromotion_id()
	{
		return $this->_promotion_id;
	}

	public function setpromotion_id($val)
	{
		 $this->_promotion_id =  $val;
	}


/**
*   @desc   SELECT METHOD / LOAD
*/

	function select($id=null)
	{
		 $WHERE = 'WHERE 1=1';
		 if(!empty($id)) {
			 $WHERE .= ' AND transaction_id = '.$id;
		 }
		 $sql =  "SELECT * FROM transaction $WHERE";
		 $row =  $this->_obj->select($sql); 

		 $this->_transaction_id = $row[0]['transaction_id'];
		 $this->_transaction_unique_id = $row[0]['transaction_unique_id'];
		 $this->_refunded_transaction_id = $row[0]['refunded_transaction_id'];
		 $this->_from_user_id = $row[0]['from_user_id'];
		 $this->_to_user_id = $row[0]['to_user_id'];
		 $this->_via_user_id = $row[0]['via_user_id'];
		 $this->_amount = $row[0]['amount'];
		 $this->_type = $row[0]['type'];
		 $this->_comment = $row[0]['comment'];
		 $this->_transaction_status = $row[0]['transaction_status'];
		 $this->_refunded = $row[0]['refunded'];
		 $this->_added_at = $row[0]['added_at'];
		 $this->_updated_at = $row[0]['updated_at'];
		 $this->_status = $row[0]['status'];
		 $this->_transaction_type = $row[0]['transaction_type'];
		 $this->_promotion_id = $row[0]['promotion_id'];
		 return $row;
	}

	function check_transaction($id)
	{
		$sql = "SELECT COUNT(transaction_id) as tot FROM transaction WHERE transaction_id = $id";
		$row = $this->_obj->select($sql);
		return ($row[0]['tot'] == 1) ? true : false;
	}


/**
*   @desc   DELETE
*/

	function delete($id)
	{
		 $sql = "DELETE FROM transaction WHERE transaction_id = $id";
		 $this->_obj->sql_query($sql);
	}


/**
*   @desc   INSERT
*/

	function insert()
	{
		 $this->transaction_id = ""; // clear key for autoincrement

		 $sql = "INSERT INTO transaction ( transaction_unique_id,refunded_transaction_id,from_user_id,to_user_id,via_user_id,amount,type,comment,transaction_status,refunded,added_at,updated_at,status,transaction_type,promotion_id ) VALUES ( '".$this->_transaction_unique_id."','".$this->_refunded_transaction_id."','".$this->_from_user_id."','".$this->_to_user_id."','".$this->_via_user_id."','".$this->_amount."','".$this->_type."','".$this->_comment."','".$this->_transaction_status."','".$this->_refunded."','".$this->_added_at."','".$this->_updated_at."','".$this->_status."','".$this->_transaction_type."','".$this->_promotion_id."' )";
		 $result = $this->_obj->insert($sql);
		 return $result;
	}


/**
*   @desc   UPDATE
*/

	function update($id)
	{

		$sql = " UPDATE transaction SET  transaction_unique_id = '" . $this->_transaction_unique_id . "' , refunded_transaction_id = '" . $this->_refunded_transaction_id . "' , from_user_id = '" . $this->_from_user_id . "' , to_user_id = '" . $this->_to_user_id . "' , via_user_id = '" . $this->_via_user_id . "' , amount = '" . $this->_amount . "' , type = '" . $this->_type . "' , comment = '" . $this->_comment . "' , transaction_status = '" . $this->_transaction_status . "' , refunded = '" . $this->_refunded . "' , added_at = '" . $this->_added_at . "' , updated_at = '" . $this->_updated_at . "' , status = '" . $this->_status . "' , transaction_type = '" . $this->_transaction_type . "' , promotion_id = '" . $this->_promotion_id . "'  WHERE transaction_id = $id ";
		$this->_obj->sql_query($sql);

	}


	function update_transaction_status($id, $status)
	{
		$sql = " UPDATE transaction SET  transaction_status = '{$status}' , updated_at = '" . $this->_updated_at . "' WHERE transaction_id = $id ";
		$this->_obj->sql_query($sql);
	}

	function  get_transaction($type = '', $userid = '')
    {
        $sql =  "SELECT * FROM transaction WHERE (type ='$type' AND from_user_id ='$userid' ) AND status='1'order by transaction_unique_id DESC";
        $row=$this->_obj->select($sql);
        return $row;
    }
    function  select_transaction($id =null,$type = '')
    {
		$sql = "select t.transaction_id,u.name,u.email,t.amount,t.to_user_id,t.comment,t.added_at,t.transaction_unique_id,t.transaction_status,t.refunded,t.refunded_transaction_id,trm.credit_rate,trm.debit_rate from transaction  AS t  Left Join  user AS u ON t.from_user_id = u.id Left Join transaction_rate_map AS trm ON trm.transaction_id = t.transaction_id where (t.type='$type' AND t.to_user_id = '$id') AND t.status ='1'";
//		echo $sql;exit;
		$row = $this->_obj->select($sql);
		return $row;
	}

    function  transactionOffsetLimit($id =null, $type = '', $timestamp = 0)
    {
		global $offset, $limit;
		$sql = "select t.transaction_id,u.name,u.email,t.amount,t.to_user_id,t.comment,t.added_at,t.updated_at,t.transaction_unique_id,t.transaction_status,t.refunded,t.refunded_transaction_id,trm.credit_rate,trm.debit_rate,t.transaction_type from transaction  AS t  Left Join  user AS u ON t.from_user_id = u.id Left Join transaction_rate_map AS trm ON trm.transaction_id = t.transaction_id where (t.type='$type' AND t.to_user_id = '$id') AND t.status ='1' AND t.updated_at > '{$timestamp}' ORDER BY t.updated_at DESC LIMIT {$offset}, {$limit}";
//		echo $sql;exit;
		$row = $this->_obj->select($sql);
		return $row;
	}

	function select_transaction_credit($id = null, $type = '')
	{
		$sql = "select t.transaction_id,u.name,u.email,t.amount,t.to_user_id,t.comment,t.added_at,t.transaction_unique_id,t.transaction_status,t.refunded,t.refunded_transaction_id,trm.credit_rate,trm.debit_rate from transaction  AS t  Left Join user AS u ON t.to_user_id = u.id Left Join transaction_rate_map AS trm ON trm.transaction_id = t.transaction_id where (t.type='$type' AND t.from_user_id = '$id') AND t.status ='1'";
//		echo $sql;exit;
        $row=$this->_obj->select($sql);
        return $row;
    }

	function transactionCreditOffsetLimit($id = null, $type = '', $timestamp = 0)
	{
		global $offset, $limit;
		$sql = "select t.transaction_id,u.name,u.email,t.amount,t.to_user_id,t.comment,t.added_at,t.updated_at,t.transaction_unique_id,t.transaction_status,t.refunded,t.refunded_transaction_id,trm.credit_rate,trm.debit_rate,t.transaction_type from transaction AS t Left Join user AS u ON t.to_user_id = u.id Left Join transaction_rate_map AS trm ON trm.transaction_id = t.transaction_id where (t.type='$type' AND t.from_user_id = '$id') AND t.status ='1' AND t.updated_at > '{$timestamp}' ORDER BY t.updated_at DESC LIMIT {$offset}, {$limit}";
//		echo $sql;exit;
        $row=$this->_obj->select($sql);
        return $row;
    }

	function update_transaction_record($id, $type, $status, $gmt)
	{
		$sql = "UPDATE transaction set type = '{$type}', transaction_status = '{$status}', updated_at = '{$gmt}' WHERE transaction_id = '{$id}' ";
		$this->_obj->sql_query($sql);
		return;
	}


	function select_transaction_credit_admin($id = null, $type = '')
	{
		$ssql = "";
		if($type == "debit")
		{
			$lfft_user = "from_user_id";
			$ssql .= " AND (t.to_user_id = '$id')";
		}


		if($type == "credit")
		{
			$lfft_user = "to_user_id";
			$ssql .= " AND (t.from_user_id = '$id')";
		}


		$sql = "select t.transaction_id,u.name,u.email,t.amount,t.to_user_id,t.comment,t.added_at,t.transaction_unique_id,t.transaction_status,trm.credit_rate,trm.debit_rate,t.transaction_type from transaction  AS t
Left Join user AS u ON t.$lfft_user = u.id
Left Join transaction_rate_map AS trm ON trm.transaction_id = t.transaction_id where t.status ='1' $ssql ORDER BY t.added_at DESC";
		#echo $sql;#exit;
		$row=$this->_obj->select($sql);
		#pr($row);#exit;
		return $row;
	}


	function select_all_debit_credit_transaction_for_admin($type = '')
	{

		$ssql = "";
		if($type == "debit")
		{
			$ssql .= " group by t.from_user_id ";
		}


		if($type == "credit")
		{
			$ssql .= " group by t.to_user_id ";
		}

		$sql = "select t.transaction_id,u.name,u.email,t.amount,t.from_user_id,t.to_user_id,t.comment,t.added_at,t.transaction_unique_id,t.transaction_status,trm.credit_rate,trm.debit_rate,sum(t.amount) as total_debits from transaction  AS t
Left Join user AS u ON t.to_user_id = u.id
Left Join transaction_rate_map AS trm ON trm.transaction_id = t.transaction_id
where t.status ='1' $ssql ORDER BY t.added_at DESC ";
		#echo $sql;exit;
		$row=$this->_obj->select($sql);
		return $row;
	}



	function select_debit_transaction_by_day($id = null, $type = '',$by='')
	{

		$ssql	=	"";
		$GROUP_BY	=	"";

		if($by == "day")
		{
			$GROUP_BY .= " GROUP BY DATE_FORMAT(t.added_at,'%Y-%m-%d')";
		}


		if($by == "week")
		{
			$ssql .= " ,DATE_ADD(t.added_at, INTERVAL(1-DAYOFWEEK(t.added_at)) DAY) as start_date,
			DATE_ADD(t.added_at, INTERVAL(7-DAYOFWEEK(t.added_at)) DAY) as end_date ";

			$GROUP_BY	.=	" GROUP BY WEEK(t.added_at)";

		}

		if($by == "month")
		{
			$ssql .= " ,DATE_ADD(t.added_at, INTERVAL(1-DAYOFMONTH(t.added_at)) DAY) as start_date,
			DATE_ADD(t.added_at, INTERVAL(30-DAYOFMONTH(t.added_at)) DAY) as end_date,MONTHNAME(t.added_at) as month_name ";

			$GROUP_BY	.=	" GROUP BY MONTH(t.added_at)";

		}


		if($by == "year")
		{
			$ssql .= " ,YEAR(t.added_at) as month_name ";

			$GROUP_BY	.=	" GROUP BY YEAR(t.added_at)";

		}

		$ssql2 = "";
		if($type == "debit")
		{
			$left_join = "to_user_id";
			$ssql2 .= " AND (t.from_user_id = '$id')";
		}


		if($type == "credit")
		{
			$left_join = "from_user_id";
			$ssql2 .= " AND (t.to_user_id = '$id')";
		}


		$sql = "select t.transaction_id,u.name,u.email,t.amount,t.to_user_id,t.comment,t.added_at,t.transaction_unique_id,t.transaction_status,trm.credit_rate,trm.debit_rate,sum(t.amount) as total_amt
$ssql
from transaction  AS t
Left Join user AS u ON t.$left_join = u.id
Left Join transaction_rate_map AS trm ON trm.transaction_id = t.transaction_id
where t.status ='1' $ssql2  $GROUP_BY ORDER BY t.added_at DESC";

		$row=$this->_obj->select($sql);
		#echo $sql;
		#pr($row);#exit;

		return $row;
	}

	function updateIsRefunded($status, $id, $gmt){
		$sql	=	"UPDATE transaction SET refunded = '{$status}', updated_at = '{$gmt}' WHERE transaction_id = '{$id}'";
		$upd	=	$this->_obj->sql_query($sql);
		return $upd;
	}

	function select_transaction_credit_members_user($id = null, $type = '')
	{
		$ssql = "";
		if($type == "debit")
		{
			$lfft_user =  "to_user_id";
			$ssql .= " AND (t.from_user_id = '$id')";
		}


		if($type == "credit")
		{
			$lfft_user =  "from_user_id";
			$ssql .= " AND (t.to_user_id = '$id')";
		}


		$sql = "select t.transaction_id,u.name,u.email,t.amount,t.to_user_id,t.comment,t.added_at,t.transaction_unique_id,t.transaction_status,trm.credit_rate,trm.debit_rate from transaction  AS t
Left Join user AS u ON t.$lfft_user = u.id
Left Join transaction_rate_map AS trm ON trm.transaction_id = t.transaction_id where t.status ='1' $ssql ORDER BY t.added_at DESC";
		#echo $sql;#exit;
		$row=$this->_obj->select($sql);
		#pr($row);#exit;
		return $row;
	}

	function receivedMoneySubUser($user_id, $id)
	{
		$sql = "select t.transaction_id,u.name,u.email,t.amount,t.to_user_id,t.comment,t.added_at,t.transaction_unique_id,t.transaction_status,t.refunded,t.refunded_transaction_id,trm.credit_rate,trm.debit_rate,t.via_user_id from transaction  AS t  Left Join user AS u ON t.to_user_id = u.id Left Join transaction_rate_map AS trm ON trm.transaction_id = t.transaction_id where (t.type='debit' AND t.from_user_id = '{$user_id}') AND t.status ='1' AND t.via_user_id = '{$id}'";
//		echo $sql;exit;
		$row=$this->_obj->select($sql);
		return $row;
	}

	function receivedMoneySubUserOL($user_id, $id, $timestamp = 0)
	{
		global $offset, $limit;
		$sql = "select t.transaction_id,u.name,u.email,t.amount,t.to_user_id,t.comment,t.added_at,t.updated_at,t.transaction_unique_id,t.transaction_status,t.refunded,t.refunded_transaction_id,trm.credit_rate,trm.debit_rate,t.via_user_id from transaction  AS t  Left Join user AS u ON t.to_user_id = u.id Left Join transaction_rate_map AS trm ON trm.transaction_id = t.transaction_id where (t.type='debit' AND t.from_user_id = '{$user_id}') AND t.status ='1' AND t.via_user_id = '{$id}' AND t.updated_at > '{$timestamp}' ORDER BY t.updated_at DESC LIMIT {$offset}, {$limit}";
//		echo $sql;exit;
		$row=$this->_obj->select($sql);
		return $row;
	}

	function checkUniqueCode($code)
	{

		$sql = "SELECT COUNT(*) AS TOT FROM transaction WHERE transaction_unique_id = '{$code}'";
		$result = $this->_obj->select($sql);

		if ($result[0]['TOT'] > 0) {
			return 1;
		} else {
			return 0;
		}

	}
}
