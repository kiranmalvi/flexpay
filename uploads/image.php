<?php
include_once("../miAutoload.php");
include_once("../library/class/Image.php");

$routeName = [
    'QR_code' => '/QR_code/{image}',
    'product' => '/product/{image}',
    'vendor' => '/vendor/{image}',
    'segment' => '/segment/{image}',
    'default' => '/{image}'
];

$router = new League\Route\RouteCollection;

$router->get($routeName['QR_code'], 'Image::QR_code');
$router->get($routeName['product'], 'Image::product');
$router->get($routeName['vendor'], 'Image::vendor');
$router->get($routeName['segment'], 'Image::segment');
$router->get($routeName['default'], 'Image::defaultImg');

$request = \Symfony\Component\HttpFoundation\Request::createFromGlobals();

try {
    $router->getDispatcher()->dispatch($request->getMethod(), $request->getPathInfo())->send();
} catch (\League\Route\Http\Exception $e) {
    header('location: https://placeholdit.imgix.net/~text?txtsize=33&txt=350%C3%97150&w=350&h=150');
}

