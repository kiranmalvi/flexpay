<?php
include_once('miAutoload.php');

@$user_id = base64_decode($_REQUEST['id']);

$user = new user();
$user_data = $user->select($user_id);

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>
    <!-- Bootstrap -->
    <link href="css/bootstrap_vnew.min.css" rel="stylesheet">
    <link href="css/style_vnew.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
<section class="haderflexpay">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <a class=""><img src="img/logo.png" alt=""/></a>
        	</div>
    	</div>
	</div>
</section>

<?php
if ($user_data[0]['status'] == '4') {
//                $user->setstatus('1');
	$user->update_status($user_id);
?>
<section class="wright-iconbg">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<a class="logoimage"><img src="img/green-icon.png" alt=""/></a>
				<h1 class="wright-icon">Account Verification Completed Successfully.</h1>
			</div>
		</div>
	</div>
</section>
<?php
} else { ?>
<section class="wright-iconbg">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<a class="logoimage"><img src="img/green-icon.png" alt=""/></a>
				<h1 class="wright-icon">Invalid verification link.</h1>
			</div>
		</div>
	</div>
</section>
<?php } ?>


<section class="wright-image">
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-center">
            	<h3 class="security">Security</h3>
                <div class="factor-text">2-factor authentication, 256-bit end-to-end encryption, SSL
                	certificates and many such security measures are at work to
                    keep your data and hard earned money safe.</div>
                    <a href="#" class="readmore">Read more</a>
        	</div>
            <div class="col-md-6 text-center">
            	<a><img src="img/handset.png" alt="" class="handset"/></a>
            </div>
    	</div>
	</div>
</section>

<section class="downloadbg">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
            	<h3 class="downloadbg-text">Download Flex pay Now</h3>
                    <a href="#" class="get-app"><img src="img/apple.png">Get iOS APP</a>
        	</div>
    	</div>
	</div>
</section>

<section class="footerbg">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
            	<h3 class="downloadbg-text">Download Flex pay Now</h3>
                <div class="menu"><a href="#">Partners</a>   <a href="#">Team</a>   <a href="#">Careers</a>   <a href="#">Fees</a>
                   <a href="#">Blog</a>   <a href="#">FAQ</a>   <a href="#">User T&C</a>   <a href="#">Merchant T&C</a>   
                   <a href="#">Flex pay</a>    <a href="#">BusinessContact</a>   <a href="#">Security</a></div>
                   <div class="social-icon">
                   		<a href="#" class="social-icon"><img src="img/face.png"></a>
                        <a href="#" class="social-icon"><img src="img/twitter.png"></a>
                        <a href="#" class="social-icon"><img src="img/google.png"></a>
                   </div>
                   <div class="reserved">© 2009-2014. All rights reserved</div>

                   
        	</div>
    	</div>
	</div>
</section>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="js/bootstrap_vnew.min.js"></script>
  </body>
</html>
