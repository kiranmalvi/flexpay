<?php

$account_setting = new account_setting();

$gmt=$generalfuncobj->gm_date();
//echo '<pre>'; print_R($_REQUEST);exit;
$MODE = $_REQUEST['mode'];
$RETURN_LINK = 'as-accountlist';
$iId=$_REQUEST['account_id'];
//pr($_REQUEST);exit;
/* Add Mode */
if ($MODE == 'add') {
//    pr($_REQUEST);exit;
//    ECHO "NFJGHF";

    $account_setting->setupdated_at($gmt);
    $account_setting->setaccount_number($_REQUEST['account_number']);
    $account_setting->setaddress_of_beneficiary($_REQUEST['address_of_beneficiary']);
    $account_setting->setbank_address($_REQUEST['bank_address']);
    $account_setting->setisfc_code($_REQUEST['isfc_code']);
    $account_setting->setmobile_number($_REQUEST['mobile_number']);
    $account_setting->setname_of_beneficiary($_REQUEST['name_of_beneficiary']);
    $account_setting->setbank_name($_REQUEST['bank_name']);
    $account_setting->setstatus($_REQUEST['eStatus']);
    $account_setting->setadded_at($gmt);

    $account_setting->insert();
    $generalfuncobj->func_set_temp_sess_msg("Account setting details added successfully", "success");

    header("Location:index.php?file=" . $RETURN_LINK);

    exit;
}

/* Update Mode */
if ($MODE == 'edit') {

    $account_setting->setupdated_at($gmt);
    $account_setting->setaccount_number($_REQUEST['account_number']);
    $account_setting->setaddress_of_beneficiary($_REQUEST['address_of_beneficiary']);
    $account_setting->setbank_address($_REQUEST['bank_address']);
    $account_setting->setisfc_code($_REQUEST['isfc_code']);
    $account_setting->setmobile_number($_REQUEST['mobile_number']);
    $account_setting->setname_of_beneficiary($_REQUEST['name_of_beneficiary']);
    $account_setting->setbank_name($_REQUEST['bank_name']);
    $account_setting->setstatus($_REQUEST['eStatus']);
//    $account_setting->setadded_at($gmt);

    $account_setting->update($iId);
    $generalfuncobj->func_set_temp_sess_msg("Account setting updated successfully", "success");

    header("Location:index.php?file=" . $RETURN_LINK);

    exit;
}

/* Delete Mode */
if ($MODE == 'delete') {
    if ($_REQUEST['delete_type'] == 'single_delete') {
        $ID = $_REQUEST['iId'];
        $exe_del = "Update account_setting set status='2' where account_setting_id in ($ID)";
        $obj->sql_query($exe_del);

        $generalfuncobj->func_set_temp_sess_msg("Account setting details deleted successfully", "danger");
        header("Location:index.php?file=" . $RETURN_LINK);
        exit;
    } elseif ($_REQUEST['delete_type'] == 'multi_delete') {
        foreach ($_REQUEST['delete'] as $ID) {
            $exe_del = "Update account_setting set status='2' where account_setting_id in ($ID)";
            $obj->sql_query($exe_del);
        }

        $generalfuncobj->func_set_temp_sess_msg("Account setting details deleted successfully", "danger");
        header("Location:index.php?file=" . $RETURN_LINK);
        exit;
    }
}


if ($MODE == 'active' || $MODE == 'inactive') {


    $status = ($MODE == "active") ? '1' : '0';

    foreach ($_REQUEST['delete'] as $ID) {
        $exe_del = "Update account_setting set status='$status' where account_setting_id in ($ID)";
        $obj->sql_query($exe_del);

    }
    if ($MODE == 'active') {
        $generalfuncobj->func_set_temp_sess_msg("Account setting details activated successfully", "success");
    } else {
        $generalfuncobj->func_set_temp_sess_msg("Account setting details inactivated successfully", "danger");
    }
    header("Location:index.php?file=" . $RETURN_LINK);
    exit;

}
if ($MODE == "export") {
    ob_get_clean();
    $content = '';

    $filename = 'account_setting_excel.csv';
    $res_db = $_SESSION[$MODE];
    $heading = array('account_setting_email', 'account_setting_user_name', 'account_setting_type', 'dt_added', 'status');

    for ($i = 0; $i < count($heading); $i++) {
        $content .= $heading[$i] . ",";


    }

    $id = implode(',', $_REQUEST['delete']);
    $exe_del = "SELECT account_setting_email,account_setting_user_name,account_setting_type,dt_added,status FROM  account_setting WHERE account_setting_id in ($id) AND status!='2' ORDER by account_setting_id DESC";


    $data = $obj->sql_query($exe_del);


    $content .= "\n";

    for ($j = 0; $j < count($data); $j++) {
        if ($data[$j]['status'] == '1') {
            $data[$j]['status'] = 'Active';
        } elseif ($data[$j]['status'] == '0') {
            $data[$j]['status'] = 'Inactive';
        }
        $data[$j]['dt_added'] = $generalfuncobj->change_account_setting_date_format($data[$j]['dt_added']);
        foreach ($data[$j] AS $key => $list) {


            $content .= "\"" . $data[$j][$key] . "\",";

        }
        $content .= "\n";
    }
//pr($content);exit;
    ob_get_clean();


    header('Content-type: application/ms-csv');
    header('Content-Disposition: attachment; filename=' . $filename);
    echo $content;
    exit;
}



