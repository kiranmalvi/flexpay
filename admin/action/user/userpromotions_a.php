<?php
//pr($_REQUEST);exit;
$promotions = new promotions();
$iId = $_REQUEST['iId'];
$api = new API();
$gmt=$generalfuncobj->gm_date();

$MODE = $_REQUEST['mode'];
$RETURN_LINK = 'us-userpromotionlist';
//echo '<pre>'; print_r($_REQUEST);exit;

if($MODE == 'add')
{
	$promotions = new promotions();
	$promotions->setuser_id($_REQUEST['user_id']);
	$promotions->setcompany_name(mysqli_real_escape_string($obj->CONN,$_REQUEST['company_name']));
	$promotions->setdescription(mysqli_real_escape_string($obj->CONN, $_REQUEST['description']));
	$promotions->setaddress(mysqli_real_escape_string($obj->CONN,$_REQUEST['address']));
	$promotions->setstartdate(date('Y-m-d',strtotime($_REQUEST['startdate'])));
	$promotions->setenddate(date('Y-m-d',strtotime($_REQUEST['enddate'])));
	$promotions->setdiscount($_REQUEST['discount']);
	$promotions->settransaction_limit($_REQUEST['transaction_limit']);
	$promotions->setcontact($_REQUEST['contact']);
	$promotions->setcashback($_REQUEST['cashback']);
	$promotions->setstatus('1');
	$promotions->setadded_at($gmt);
	$promotions->setupdated_at($gmt);
	$promotions->insert();
	$generalfuncobj->func_set_temp_sess_msg("Promotion added successfully.", "success");
	header("Location:index.php?file=us-userpromotionlist&iId=".$_REQUEST['user_id']);
	exit;

}

if($MODE == 'edit')
{
	//pr($_REQUEST);exit;
	$promotions->setuser_id($_REQUEST['user_id']);
	$promotions->setcompany_name($_REQUEST['company_name']);
	$promotions->setdescription(mysqli_real_escape_string($obj->CONN, $_REQUEST['description']));
	$promotions->setaddress(mysqli_real_escape_string($obj->CONN, $_REQUEST['address']));
	$promotions->setstartdate(date('Y-m-d',strtotime($_REQUEST['startdate'])));
	$promotions->setenddate(date('Y-m-d',strtotime($_REQUEST['enddate'])));
	$promotions->setdiscount($_REQUEST['discount']);
	$promotions->settransaction_limit($_REQUEST['transaction_limit']);
	$promotions->setcontact($_REQUEST['contact']);
	$promotions->setcashback($_REQUEST['cashback']);
	$promotions->setupdated_at($gmt);
	$promotions->updatePromotions($_REQUEST['promotion_id']);
	$generalfuncobj->func_set_temp_sess_msg("Promotion updated successfully.", "success");
	header("Location:index.php?file=us-userpromotionlist&iId=".$_REQUEST['user_id']);
	exit;

}

if ($MODE == 'active' || $MODE == 'inactive') {

    if($MODE == "active")
    {
        $status = '1';
    }elseif($MODE == 'inactive'){
        $status = '0';
    }

    foreach ($_REQUEST['delete'] as $ID) {
        $exe_del = "Update promotions set status='$status', updated_at = '$gmt' where promotion_id in ($ID)";
        $obj->sql_query($exe_del);
    }

    if ($MODE == 'active') {
        $generalfuncobj->func_set_temp_sess_msg("Promotion activated successfully", "success");
    } elseif($MODE == 'inactive') {
        $generalfuncobj->func_set_temp_sess_msg("Promotion inactivated successfully", "success");
    }
    header("Location:index.php?file=" . $RETURN_LINK."&iId=".$_REQUEST['iId']);
    exit;

}

if ($MODE == 'delete') {
    if ($_REQUEST['delete_type'] == 'single_delete') {
        $ID = $_REQUEST['iId'];
	    $exe_del = "Update promotions set status='2', updated_at = '$gmt'  where promotion_id in ($ID)";
        $obj->sql_query($exe_del);

		$generalfuncobj->func_set_temp_sess_msg("Promotion deleted successfully", "danger");
        header("Location:index.php?file=" . $RETURN_LINK."&iId=".$_REQUEST['uId']);
        exit;
    } elseif ($_REQUEST['delete_type'] == 'multi_delete') {
        foreach ($_REQUEST['delete'] as $ID) {
	        $exe_del = "Update promotions set status='2', updated_at = '$gmt'  where promotion_id in ($ID)";
            $obj->sql_query($exe_del);
        }

		$generalfuncobj->func_set_temp_sess_msg("Promotions deleted successfully", "danger");
	    header("Location:index.php?file=" . $RETURN_LINK . "&iId=" . $_REQUEST['iId']);
        exit;
    }
}