<?php
/**
 * Created by PhpStorm.
 * User: mind
 * Date: 19/08/16
 * Time: 11:42 PM
 */

$uId = (isset($_REQUEST['uId'])) ? $_REQUEST['uId'] : '';
$iId = (isset($_REQUEST['iId'])) ? $_REQUEST['iId'] : '';
$mode = ($iId != '') ? 'edit' : 'add';
$read='';
if ($iId != '') {


	$promotions = new promotions();

	$db_res = $promotions->select($iId);

}
$MODE_TYPE = ucfirst($mode);
$MODULE = 'User Promotion';
$PRE_LINK = 'index.php?file=us-userpromotionlist';
$ACT_LINK = 'index.php?file=us-userpromotions_a';



?>

<!-- Start: Topbar -->
<header id="topbar" class="affix">
	<div class="topbar-left">
		<ol class="breadcrumb">
			<li class="crumb-active">
				<a href="index.php">Dashboard</a>
			</li>

			<li class="crumb-active crumb-link">
				<a href="<?php echo $PRE_LINK.'&iId='.$uId; ?>"><?php echo $MODULE;?></a>
			</li>
			<li class="crumb-trail"><?php echo $MODE_TYPE . ' ' . $MODULE; ?> Detail</li>
		</ol>
	</div>
</header>
<!-- End: Topbar -->

<section id="content" class="table-layout">

	<!-- begin: .tray-center -->
	<div class="tray tray-center pv40 ph30 va-t posr">
		<div class="center-block">

			<!-- begin: .admin-form -->
			<div class="admin-form">

				<div id="p1" class="panel heading-border panel-system">

					<div class="panel-body bg-light">
						<form method="post" id="admin_form" class="form-horizontal" role="form"
							  action="<?php echo $ACT_LINK; ?>" enctype="multipart/form-data">

							<div class="section-divider mb40" id="spy1">
								<span class="panel-primary"><?php echo $MODE_TYPE . ' ' . $MODULE; ?> Detail</span>
							</div>

							<input type="hidden" name="mode" value="<?php echo $mode; ?>">
							<input type="hidden" name="user_id" id="user_id"
								   value="<?php echo $uId; ?>">
							<input type="hidden" name="promotion_id" id="promotion_id"
								   value="<?php echo @$db_res[0]['promotion_id']; ?>">
							<input type="hidden" name="added_at" id="added_at"
								   value="<?php echo @$db_res[0]['added_at']; ?>">

							<!-- .section-divider -->

							<div class="form-group">
								<label class="col-md-2 control-label"><span class="red"> *</span> Company Name:</label>

								<div class="col-md-8">
									<input type="text" name="company_name" id="company_name" class="form-control"
										   value="<?php echo @$db_res[0]['company_name']; ?>" placeholder="Company Name" <?php echo $read?>>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label"><span class="red"> *</span> Description:</label>
								<div class="col-md-8">
                                            <textarea class="form-control" placeholder="Description"
													  id="description" name="description"
													  rows="3" <?php echo $read ?>><?php echo @$db_res[0]['description']; ?></textarea>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label"><span class="red"> *</span> Address:</label>
								<div class="col-md-8">
									<!--<textarea class="form-control" placeholder="Address"
													  id="address" name="address"
													  rows="3" <?php /*echo $read*/ ?>><?php /*echo @$db_res[0]['address']; */ ?></textarea>-->
									<input type="text" name="address" id="address" class="form-control"
									       value="<?php echo @$db_res[0]['address']; ?>"
									       placeholder="Address" <?php echo $read ?>>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label"><span class="red"> *</span> Start Date:</label>

								<div class="col-md-8">
									<input type="text" data-date-format="dd M yyyy" data-date-viewmode="years" class="form-control date-picker" name="startdate" id="startdate" placeholder="Start Date" value="<?php echo @($db_res[0]['startdate'] != '')?date('m/d/Y',strtotime($db_res[0]['startdate'])):'';?>" <?php echo $read?>>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label"><span class="red"> *</span> End Date:</label>

								<div class="col-md-8">
									<input type="text" data-date-format="dd M yyyy" data-date-viewmode="years" class="form-control date-picker" name="enddate" id="enddate" placeholder="End Date" value="<?php echo @($db_res[0]['enddate'] != '')?date('m/d/Y',strtotime($db_res[0]['enddate'])):'';?>" <?php echo $read?>>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label"><span class="red"> *</span> Discount (%):</label>

								<div class="col-md-8">
									<input type="number" name="discount" id="discount" class="form-control"
										   value="<?php echo @$db_res[0]['discount']; ?>" placeholder="Discount" <?php echo $read?>>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label"><span class="red"> *</span> Minimum Transaction Limit:</label>

								<div class="col-md-8">
									<input type="text" name="transaction_limit" id="transaction_limit" class="form-control"
										   value="<?php echo @$db_res[0]['transaction_limit']; ?>" placeholder="Transaction Limit" <?php echo $read?>>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label"><span class="red"> *</span> Contact:</label>

								<div class="col-md-8">
									<input type="text" name="contact" id="contact" class="form-control"
										   value="<?php echo @$db_res[0]['contact']; ?>" placeholder="Contact" <?php echo $read?>>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label"><span class="red"> *</span> Maximum Cashback:</label>

								<div class="col-md-8">
									<input type="text" name="cashback" id="cashback" class="form-control"
										   value="<?php echo @$db_res[0]['cashback']; ?>" placeholder="Cashback" <?php echo $read?>>
								</div>
							</div>

							<!--<div class="form-group">
								<label class="col-md-2 control-label"><span class="red"> *</span> Status:</label>

								<div class="col-md-8">
									<select class="form-control" name="eStatus" id="eStatus" <?php /*echo $style; */?>>
										<option selected disabled>- Select Status -</option>
										<option
											value="1" <?php /*echo(@$db_res[0]['status'] == '1' ? 'selected' : ''); */?>>
											Active
										</option>
										<option
											value="0" <?php /*echo(@$db_res[0]['status'] == '0' ? 'selected' : ''); */?>>
											Inactive
										</option>
									</select>
								</div>
							</div>-->


							<div class="form-group">
								<div class="col-lg-offset-2 col-lg-10">
									<button id="addbutton" type="submit"
											class="btn  btn-primary animsition-link"><?php echo ($iId != '') ? 'Save' : 'Add' ?></button>
									<a href="<?php echo $PRE_LINK.'&iId='.$uId; ?>" class="btn btn-dark animsition-link">Back</a>
								</div>
							</div>
							<!-- Basic Inputs -->


						</form>
					</div>
				</div>
			</div>
			<!-- end: .admin-form -->
		</div>
	</div>
</section>
<script src="<?php echo $assets_url ?>plugins/ckeditor-ckfinder-integration/ckeditor/ckeditor.js"></script>
<script src="<?php echo $assets_url ?>js/moment.min.js"></script>
<!--<script src="<?php /*echo $assets_url */?>plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>-->

<?php include_once($admin_path . 'js_form.php'); ?>


<script src="<?php echo $assets_url ?>js/jquery.validate.min.js"></script>

<script>
	var FormAdminValidator = function () {
		// function to initiate Validation Sample 1
		var temp = 0;
		var runValidator1 = function () {
			var form1 = $('#admin_form');

			$('#admin_form').validate({

				errorElement: "span", // contain the error msg in a span tag
				errorClass: 'help-block',
				errorPlacement: function (error, element) { // render error placement for each input type
					//console.log((element[0].type));
					if (element.context.tagName == 'textarea') {
						error.insertAfter($(element).next());
					} else {
						error.insertAfter(element);
					}
					// for other inputs, just perform default behavior
				},
				ignore: "",
				rules: {
					company_name: {
						required: true
					},
					description: {
						/*required: function () {
							CKEDITOR.instances.description.updateElement();
						 }*/
						required: true
					},
					address: {
						required: true
					},
					startdate: {
						required: true
					},
					enddate: {
						required: true
					},
					discount: {
						required: true,
						number:true,
						min:0,
						max:100
					},
					transaction_limit: {
						required: true
					},
					contact: {
						required: true
					},
					cashback: {
						required: true
					}

				},
				messages: {
					company_name: "Please enter company name",

					description: {
						required:"Please enter description"
					},
					address: "Please enter address",
					startdate: "Please select start date",
					enddate: "Please select end date",
					discount:{
						required: "Please enter discount rate",
						number: "Please enter only number"
					},
					transaction_limit: "Please enter transaction limit",
					contact: "Please enter contact number",
					cashback: "Please enter cashback"
				},
				invalidHandler: function (event, validator) { //display error alert on form submit
					//successHandler1.hide();
					//errorHandler1.show();
				},
				highlight: function (element) {
					$(element).closest('.help-block').removeClass('valid');
					// display OK icon
					$(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
					// add the Bootstrap error class to the control group
				},
				unhighlight: function (element) { // revert the change done by hightlight
					$(element).closest('.form-group').removeClass('has-error');
					// set error class to the control group
				},
				success: function (label, element) {
					label.addClass('help-block valid');
					// mark the current input as valid and display OK icon
					$(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
				},
				submitHandler: function (frmadd) {
					successHandler1.show();
					errorHandler1.hide();
				}

			});

		};

		return {
			//main function to initiate template pages
			init: function () {
				runValidator1();
			}
		};

		//$('#frmadd').submit();
	}();

	//$('#frmadd').submit();
	$(document).ready(function () {
		FormAdminValidator.init();

		$('#addbutton').click(function () {
			$('#eStatus').prop("disabled", false);
			$('#admin_form').submit();

		});

		$('#enddate').on('change', function () {
			console.log(($("#enddate").val().length));
			if ($("#enddate").val().length > 0) {
				$(this).parents('div.form-group').removeClass('has-error');
				$('#enddate').parent().find('span').remove();
				//$('.help-block').remove();
				$(this).parents('div.form-group').addClass('has-success');
			}
		});

		/*CKEDITOR.instances.description.on('change', function() {
		 if (CKEDITOR.instances.description.getData().length > 0) {
		 $(this).parents('div.form-group').removeClass('has-error');
		 $('#description').parent().find('span').remove();
		 $(this).parents('div.form-group').addClass('has-success');
		 }
		 });*/

		var today = new Date();
		var date_from = <?=($mode == 'Edit') ? 'new Date("'.date('m/d/y',strtotime($db_res[0]['startdate'])).'");' : 'new Date();'; ?>
		var d = (date_from != '' )?date_from:today;
		$('#startdate').datepicker('destroy');
		$('#startdate').datepicker({
			onSelect: function(date) {
				var new_date = new Date(date);
				var minDate =  new Date(moment(new_date).add('days', 1).format('YYYY-MM-DD'));
				$('#enddate').datepicker('option',{minDate:minDate});
				if (date.length > 0) {
					$(this).parents('div.form-group').removeClass('has-error');
					$('#startdate').parent().find('span').remove();
					$(this).parents('div.form-group').addClass('has-success');
				}
			},
			changeMonth: true,
			changeYear: true,
			minDate : d,
			dateFormat: 'mm/dd/yy'
		});
		var date_to =
		<?=($mode == 'edit') ? 'new Date("'.date('m/d/y',strtotime($db_res[0]['startdate'])).'");' : 'new Date();'; ?>
		var t = (date_to != '') ? new Date(moment(date_to).add('days', 1)) : today;
		$('#enddate').datepicker('destroy');
		$('#enddate').datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: 'mm/dd/yy',
			minDate: t
		});

	});

	$(document).on('keypress', '#transaction_limit', function (event) {
		return (((event.which > 47) && (event.which < 58)) || (event.which == 13) || (event.which == 08));
	});

	$(document).on('keypress', '#contact', function (event) {
		return (((event.which > 47) && (event.which < 58)) || (event.which == 13) || (event.which == 08));
	});

	$(document).on('keypress', '#cashback', function (event) {
		return (((event.which > 47) && (event.which < 58)) || (event.which == 13) || (event.which == 08));
	});

	/*$('#discount').keyup(function(e) {
		var v = parseFloat($('#discount').val());
		var val = '';
		if(v > 100) {
			var a = v.toString().split('');
			a = a.slice(0, 3);
			if(a.join('') > 100) {
				a = v.toString().split('');
				a = a.slice(0, 2);
			}
			$('#discount').val(a.join(''));
		}
	})*/

</script>