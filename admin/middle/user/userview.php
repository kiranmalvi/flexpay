<?php
/**
 * Created by PhpStorm.
 * User: mind
 * Date: 2/10/15
 * Time: 6:20 PM
 */
error_reporting(0);
$iId = $_REQUEST['iId'];

$user = new user();

$res_db = $user->select($iId);

$wallet = new wallet();
$walletlist = $wallet->userwallet($iId);
$transaction = new transaction();
$user_transaction_rate = new user_transaction_rate();
#echo $iId;
$user_credit_debit_rate = $user_transaction_rate->user_transaction_data($iId);
#pr($user_credit_debit_rate);


$ADD_LINK = 'index.php?file=us-user_a';
$viewk = 'index.php?file=us-userview&iId=' . $iId;
$PRE_LINK = 'index.php?file=us-userlist';
$NEXT_LINK = 'index.php?file=us-userpromotionlist';
?>
<header id="topbar" class="affix">
	<div class="topbar-left">
		<ol class="breadcrumb">
			<li class="crumb-link crumb-active ">
				<a href="index.php">Dashboard</a>
			</li>
			<li class="crumb-active crumb-link">
				<a href="<?php echo $PRE_LINK; ?>"><?php echo "User"; ?> </a>
			</li>

			<li class="crumb-trail"><?php echo "User"; ?> Details</li>
		</ol>
	</div>
</header>
<script type="text/html" src="<?php echo $admin_url . 'assets/js/moment-range.js'; ?>"></script>
<section id="content">
	<div class="row">
		<div class="col-md-8 pv10 ph20 bg-light dark br-b br-grey posr">

			<div class="table-layout">
				<div class="va-t m30">

					<h2 class=""> <?php echo $res_db[0]['name']; ?>

					</h2>
					<p class="fs15 mb20"><?php echo $res_db[0]['email']; ?></p>
					<p class="fs15 mb20"><?php echo $res_db[0]['mobile']; ?></p>
					<a href="<?php echo $NEXT_LINK; ?>&iId=<?php echo $iId; ?>" class="btn btn-sm btn-system" style="margin-top: 5px;">Promotion List </a>
				</div>
			</div>

			<!-- <div class="p40 bg-background bg-topbar bg-psuedo-tp"> -->


		</div>


		<div class="col-md-4">
			<h4 class="page-header mtn br-light text-muted hidden">User Info</h4>

			<div class="panel">
				<div class="panel-heading">
					<span class="panel-icon"><i class="fa fa-star"></i></span>
					<span class="panel-title"> User Details</span>
				</div>
				<div class="panel-body pn">

					<table class="table mbn tc-icon-1 tc-med-2 tc-bold-last">
						<tbody>

						<tr>
							<td><span class="fa fa-minus-circle text-success"></span></td>
							<td>Status</td>
							<td>
								<?php
								if ($res_db[0]['status'] == '1') {
									$style = "text-success";

									$text = "Active";
								} elseif ($res_db[0]['status'] == '0') {
									$style = "text-warning";
									$text = "Inactive";
								} elseif ($res_db[0]['status'] == '3') {
									$style = "text-danger";
									$text = "Block";
								}
								?>
								<span class="<?php echo $style ?>"><?php echo $text ?></span>
							</td>
						</tr>


						<tr>

							<td><span class="glyphicons glyphicons-thumbs_up text-primary"></span></td>
							<td>Wallet</td>
							<td><?php if ($walletlist[0]['amount'] == '') {
									$amount = '0.0';
								} else {
									$amount = round($walletlist[0]['amount'], 2);
								}

								echo $amount; ?></td>
						</tr>
						<?php
						if ($res_db[0]['status'] == '1') {
							?>
							<tr>

								<td><a class=" animsition-link" data-style="expand-left"
								       data-toggle="modal" href="#addwallet"><span
											class="fa fa-plus-circle text-success"></span></a>

								</td>
								<td><a class=" animsition-link" data-style="expand-left"
								       data-toggle="modal" href="#addwallet">Add To Wallet</a></td>
								<td></td>
							</tr>

							<tr>
								<td><a class=" animsition-link" data-style="expand-left"
								       data-toggle="modal" href="#removewallet"><span
											class="fa fa-minus-circle text-danger"></span></a>

								</td>
								<td><a class=" animsition-link" data-style="expand-left"
								       data-toggle="modal" href="#removewallet">Deduct From Wallet</a></td>
								<td></td>
							</tr>
							<?php
						}
						?>
						<tr>
							<td><a class=" animsition-link" data-style="expand-left"
							       data-toggle="modal" href="#debitrate"><span class="fa fa-credit-card"></span></a>
							</td>
							<td><a class=" animsition-link" data-style="expand-left"
							       data-toggle="modal" href="#debitrate" onclick="showUpValue(<?php echo $user_credit_debit_rate[0]['debit_rate']; ?>, '#debitrate_val');">Change Debit Rate</a></td>
							<td><?php echo $user_credit_debit_rate[0]['debit_rate']; ?></td>
						</tr>

						<tr>
							<td><a class=" animsition-link" data-style="expand-left"
							       data-toggle="modal" href="#creditrate"><span class="fa fa-credit-card"></span></a>

							</td>
							<td><a class=" animsition-link" data-style="expand-left"
							       data-toggle="modal" href="#creditrate" onclick="showUpValue(<?php echo $user_credit_debit_rate[0]['credit_rate']; ?>, '#creditrate_val');">Change Credit Rate</a></td>
							<td><?php echo $user_credit_debit_rate[0]['credit_rate']; ?></td>
						</tr>

						<tr>
							<td><a class=" animsition-link" data-style="expand-left"
							       data-toggle="modal" href="#transferLimit"><span class="fa fa-money"></span></a>

							</td>
							<td><a class=" animsition-link" data-style="expand-left"
							       data-toggle="modal" href="#transferLimit" onclick="showUpValue(<?php echo $res_db[0]['transfer_limit']; ?>, '#transferLimit_val');">Change Daily Spending Limit</a></td>
							<td><?php echo $res_db[0]['transfer_limit']; ?></td>
						</tr>


						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<div class="row">


		<div class="tab-block psor">

			<ul class="nav nav-tabs">
				<li class="active">
					<a href="#credit" data-toggle="tab">Debit</a>
				</li>
				<!--                                <li>-->
				<!--                                    <a href="-->
				<?php //echo $ADD_LINK;?><!--" data-toggle="tab">View Order</a>-->
				<!--                                </li>-->
				<li>
					<a href="#debit" data-toggle="tab">Credit</a>
				</li>


			</ul>

			<div class="tab-content" style="height: 725px;">
				<div id="credit" class="tab-pane active p15">
					<div class="media pb10">
						<table class="table table-striped table-condensed" id="datatable4">
							<thead>
							<tr>

								<th>Transaction Id</th>
								<th>To User</th>
								<th>Amount</th>
								<th>Rate</th>
								<th>Transaction Status</th>
								<th>Created date</th>


							</tr>
							</thead>
							<tbody>

							<?php
							$creditlist = $transaction->select_transaction_credit_admin($iId, 'debit');
							#pr($creditlist);

							if (count($creditlist) > 0) {
								foreach ($creditlist as $creditdata) {

									?>
									<tr>


										<td><?php echo $creditdata['transaction_unique_id'] ?></td>
										<td>
											<?php
											if ($creditdata['to_user_id'] == "0") {
												echo $ADMIN_NAME;
											} else {
												echo $creditdata['name'];
											}


											?>
										</td>

										<td><?php echo round($creditdata['amount'], 2) ?></td>
										<td><?php echo ( $creditdata['debit_rate'] != null || $creditdata['debit_rate'] != '' ) ? $creditdata['debit_rate'] : '0.00' ?></td>
										<?php
										if ($creditdata['transaction_status'] == 'pending') {
											$style = "text-warning";
										} elseif ($creditdata['transaction_status'] == 'confirm') {
											$style = "text-success";

										} elseif ($creditdata['transaction_status'] == 'cancel') {
											$style = "text-danger";
										}
										?>
										<td class="<?php echo $style ?>"><?php echo ucfirst($creditdata['transaction_status']) ?></td>
										<!--<td><?php /*echo $generalfuncobj->full_date_formate($creditdata['added_at']); */?></td>-->
										<td><?php echo str_replace(" ", "T", $creditdata['added_at'])."Z"; ?></td>

									</tr>
									<?php
								}
							} else {
								echo $generalfuncobj->no_record_found(7);
							}
							?>


							</tbody>
						</table>
					</div>
				</div>
				<div id="debit" class="tab-pane acitve p15">
					<div class="media pb10">
						<table class="table table-striped table-condensed" id="datatable5">
							<thead>
							<tr>

								<th>Transaction Id</th>
								<th>From User</th>
								<th>Amount</th>
								<th>Rate</th>
								<th>Transaction Status</th>
								<th>Created date</th>


							</tr>
							</thead>
							<tbody>

							<?php
							$creditlist = $transaction->select_transaction_credit_admin($iId, 'credit');
							#pr($creditlist);

							if (count($creditlist) > 0) {
								foreach ($creditlist as $creditdata) {
									?>

									<tr>


										<td><?php echo $creditdata['transaction_unique_id'] ?></td>
										<td>
											<?php
											if ($creditdata['to_user_id'] == "0") {
												if ($creditdata['transaction_type'] == '4') {
													echo 'PayPal';
												} else {
													echo $ADMIN_NAME;
												}
											} else {
												echo $creditdata['name'];
											}


											?>
										</td>


										<td><?php echo $creditdata['amount'] ?></td>
										<td><?php echo ( $creditdata['credit_rate'] != null || $creditdata['debit_rate'] != '' ) ? $creditdata['credit_rate'] : '0.00'; ?></td>
										<?php
										if ($creditdata['transaction_status'] == 'pending') {
											$style = "text-warning";
										} elseif ($creditdata['transaction_status'] == 'confirm') {
											$style = "text-success";

										} elseif ($creditdata['transaction_status'] == 'cancel') {
											$style = "text-danger";
										}
										?>
										<td class="<?php echo $style ?>"><?php echo ucfirst($creditdata['transaction_status']); ?></td>
										<!--<td class="moment-time" data-time="<?php /*echo date('Y-m-d H:i:s', strtotime($creditdata['added_at'])); */?>"></td>-->
										<td><?php echo str_replace(" ", "T", $creditdata['added_at'])."Z"; ?></td>

									</tr>
									<?php


								}
							} else {
								echo $generalfuncobj->no_record_found(7);
							}
							?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

	</div>


</section>


<div id="addwallet" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false"
     style="display: none;">
	<form method="post" id="user_wallet" class="form-horizontal" role="form"
	      action="<?php echo $ADD_LINK; ?>" enctype="multipart/form-data" name="user_wallet">
		<div class="modal-header">
			<div class="panel-title">
				Add To Wallet
			</div>
		</div>

		<div class="modal-body">
			<input type="hidden" name="wallet_id" id="wallet_id" class="form-control"
			       value="<?PHP echo $walletlist[0]['wallet_id'] ?>" placeholder="Email Address">
			<input type="hidden" name="user_id" id="user_id" class="form-control"
			       value="<?PHP echo $walletlist[0]['user_id'] ?>" placeholder="">
			<input type="hidden" name="mode" id="mode" class="form-control"
			       value="addamount" placeholder="">
			<div class="form-group">
				<div class="col-md-12">
					<label class="col-md-4 control-label"><span class="red"> *</span> Amount:</label>
					<div class="col-md-7">
						<input type="text" name="amount" id="amount" class="form-control"
						       value="" placeholder="Enter amount">
					</div>

					<label class="col-md-4 text-right"> Enter Comment:</label>
					<div class="col-md-7">
						<textarea class="form-group" id="comment" name="comment" placeholder="Enter Comment" rows="5"
						          cols="32" style="margin-top: 10px;margin-left: 1px;"></textarea>
					</div>
				</div>
			</div>


		</div>
		<div class="modal-footer">
			<!--        <button type="submit" data-dismiss="modal" class="btn btn-success" id="amountadd">Save</button>-->
			<input type="submit"
			       data-dismiss="" class="btn btn-success" id="amountadd" value="Save">
			<a href="javascript:void(0);" data-dismiss="modal" class="btn btn-default">Close</a>

		</div>
	</form>
</div>

<div id="removewallet" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false"
     style="display: none;">
	<form method="post" id="deduct_from_wallet" class="form-horizontal" role="form"
	      action="<?php echo $ADD_LINK; ?>" enctype="multipart/form-data" name="deduct_from_wallet">
		<div class="modal-header">
			<div class="panel-title">
				Deduct From Wallet
			</div>
		</div>

		<div class="modal-body">


			<input type="hidden" name="wallet_id" id="wallet_id" class="form-control"
			       value="<?PHP echo $walletlist[0]['wallet_id'] ?>" placeholder="Email Address">
			<input type="hidden" name="user_id" id="user_id" class="form-control"
			       value="<?PHP echo $walletlist[0]['user_id'] ?>" placeholder="">
			<input type="hidden" name="mode" id="mode" class="form-control"
			       value="minusamount" placeholder="">
			<div class="form-group">
				<div class="col-md-12">
					<label class="col-md-4 control-label"><span class="red"> *</span> Amount:</label>

					<div class="col-md-7">
						<input type="text" name="amount_reduce" id="amount_reduce" class="form-control"
						       value="" placeholder="Enter amount">

					</div>
					<label class="col-md-4 text-right"> Enter Comment:</label>
					<div class="col-md-7">
						<textarea class="form-group" id="comment_deduct" name="comment_deduct"
						          placeholder="Enter Comment" rows="5" cols="32"
						          style="margin-top: 10px;margin-left: 1px;"></textarea>
					</div>
				</div>
			</div>


		</div>
		<div class="modal-footer">
			<!--        <button type="submit" data-dismiss="modal" class="btn btn-success" id="amountadd">Save</button>-->
			<input type="submit"
			       data-dismiss="" class="btn btn-success" id="amountminus" value="Save">
			<a href="javascript:void(0);" data-dismiss="modal" class="btn btn-default">Close</a>

		</div>
	</form>
</div>

<div id="debitrate" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false"
     style="display: none;">
	<form method="post" id="debitrate_form" class="form-horizontal" role="form"
	      action="<?php echo $ADD_LINK; ?>" enctype="multipart/form-data" name="debitrate_form">
		<div class="modal-header">
			<div class="panel-title">
				Change Debit Rate
			</div>
		</div>

		<div class="modal-body">


			<input type="hidden" name="user_transaction_rate_id" id="user_transaction_rate_id" class="form-control"
			       value="<?PHP echo $user_credit_debit_rate[0]['user_transaction_rate_id'] ?>" placeholder="">
			<input type="hidden" name="user_id" id="user_id" class="form-control"
			       value="<?PHP echo $user_credit_debit_rate[0]['user_id'] ?>" placeholder="">
			<input type="hidden" name="mode" id="mode" class="form-control"
			       value="change_debit_rate" placeholder="">
			<div class="form-group">
				<div class="col-md-12">
					<label class="col-md-3 control-label"><span class="red"> *</span> Rate:</label>

					<div class="col-md-8">
						<input type="text" name="debitrate_val" id="debitrate_val" class="form-control"
						       value="<?php echo $user_credit_debit_rate[0]['debit_rate']; ?>"
						       placeholder="Enter debit rate">

					</div>
				</div>
			</div>


		</div>
		<div class="modal-footer">
			<!--        <button type="submit" data-dismiss="modal" class="btn btn-success" id="amountadd">Save</button>-->
			<input type="submit"
			       data-dismiss="" class="btn btn-success" id="debitrate_btn" value="Save">
			<a href="javascript:void(0);" data-dismiss="modal" class="btn btn-default">Close</a>

		</div>
	</form>
</div>

<div id="creditrate" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false"
     style="display: none;">
	<form method="post" id="creditrate_form" class="form-horizontal" role="form"
	      action="<?php echo $ADD_LINK; ?>" enctype="multipart/form-data" name="creditrate_form">
		<div class="modal-header">
			<div class="panel-title">
				Change Credit Rate
			</div>
		</div>

		<div class="modal-body">


			<input type="hidden" name="user_transaction_rate_id" id="user_transaction_rate_id" class="form-control"
			       value="<?PHP echo $user_credit_debit_rate[0]['user_transaction_rate_id'] ?>"
			       placeholder="Email Address">
			<input type="hidden" name="user_id" id="user_id" class="form-control"
			       value="<?PHP echo $user_credit_debit_rate[0]['user_id'] ?>" placeholder="">
			<input type="hidden" name="mode" id="mode" class="form-control"
			       value="change_credit_rate" placeholder="">
			<div class="form-group">
				<div class="col-md-12">
					<label class="col-md-3 control-label"><span class="red"> *</span> Rate:</label>

					<div class="col-md-8">
						<input type="text" name="creditrate_val" id="creditrate_val" class="form-control"
						       value="<?php echo $user_credit_debit_rate[0]['credit_rate']; ?>"
						       placeholder="Enter credit rate">

					</div>
				</div>
			</div>


		</div>
		<div class="modal-footer">
			<!--        <button type="submit" data-dismiss="modal" class="btn btn-success" id="amountadd">Save</button>-->
			<input type="submit"
			       data-dismiss="" class="btn btn-success" id="creditrate_btn" value="Save">
			<a href="javascript:void(0);" data-dismiss="modal" class="btn btn-default">Close</a>

		</div>
	</form>
</div>

<div id="transferLimit" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false"
     style="display: none;">
	<form method="post" id="transferLimit_form" class="form-horizontal" role="form"
	      action="<?php echo $ADD_LINK; ?>" enctype="multipart/form-data" name="transferLimit_form">
		<div class="modal-header">
			<div class="panel-title">
				Change Daily Spending Limit
			</div>
		</div>

		<div class="modal-body">


			<input type="hidden" name="user_id" id="user_id" class="form-control"
			       value="<?PHP echo $res_db[0]['id'] ?>" placeholder="">
			<input type="hidden" name="mode" id="mode" class="form-control"
			       value="change_transfer_limit" placeholder="">
			<div class="form-group">
				<div class="col-md-12">
					<label class="col-md-3 control-label"><span class="red"> *</span> Amount:</label>

					<div class="col-md-8">
						<input type="text" name="transferLimit_val" id="transferLimit_val" class="form-control"
						       value="<?php echo $res_db[0]['transfer_limit']; ?>" placeholder="Enter limit amount">

					</div>
				</div>
			</div>


		</div>
		<div class="modal-footer">
			<input type="submit"
			       data-dismiss="" class="btn btn-success" id="transferLimit_btn" value="Save">
			<a href="javascript:void(0);" data-dismiss="modal" class="btn btn-default">Close</a>
		</div>
	</form>
</div>
<?php include_once($admin_path . 'js_table.php'); ?>
<?php /*include_once($admin_path . 'js_form.php'); */?>
<?php include_once($admin_path . 'modalpopup.php'); ?>
<script src="<?php echo $assets_url ?>js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo $admin_url . 'assets/js/moment-with-locales.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo $admin_url . 'assets/js/moment-timezone.min.js'; ?>"></script>

<script>
	var FormValidator = function () {
		// function to initiate Validation Sample 1
		var temp = 0;
		var runValidator1 = function () {
			var form1 = $('#user_wallet');

			$('#user_wallet').validate({

				errorElement: "span", // contain the error msg in a span tag
				errorClass: 'help-block',
				errorPlacement: function (error, element) { // render error placement for each input type
					error.insertAfter(element);
					// for other inputs, just perform default behavior
				},
				ignore: "",
				rules: {
					amount: {
						required: true,
						number: true,
						max: 5000,
						min: 0
					}

				},
				messages: {
					amount: {
						required: "Please enter amount",
						number: "Please enter only number",
						max: "Please enter a amount less than or equal to 5000"
					}
				},
				invalidHandler: function (event, validator) { //display error alert on form submit
					//successHandler1.hide();
					//errorHandler1.show();
				},
				highlight: function (element) {
					$(element).closest('.help-block').removeClass('valid');
					// display OK icon
					$(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
					// add the Bootstrap error class to the control group
				},
				unhighlight: function (element) { // revert the change done by hightlight
					$(element).closest('.form-group').removeClass('has-error');
					// set error class to the control group
				},
				success: function (label, element) {
					label.addClass('help-block valid');
					// mark the current input as valid and display OK icon
					$(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
				},
				submitHandler: function (frmadd) {
					successHandler1.show();
					errorHandler1.hide();
				}

			});

		};

		return {
			//main function to initiate template pages
			init: function () {
				runValidator1();
			}
		};

		//$('#frmadd').submit();
	}();

	//$('#frmadd').submit();
	$(document).ready(function () {
		FormValidator.init();

		$('#amountadd').click(function () {

			$('#user_wallet').submit();

		});
	});

</script>

<script>
	var FormdeductValidator = function () {
		// function to initiate Validation Sample 1
		var temp = 0;
		var runValidator1 = function () {
			var form1 = $('#deduct_from_wallet');

			$('#deduct_from_wallet').validate({

				errorElement: "span", // contain the error msg in a span tag
				errorClass: 'help-block',
				errorPlacement: function (error, element) { // render error placement for each input type
					error.insertAfter(element);
					// for other inputs, just perform default behavior
				},
				ignore: "",
				rules: {
					amount_reduce: {
						required: true,
						number: true,
						min: 0,
						max: <?php echo $amount;?>
					}

				},
				messages: {
					amount_reduce: {
						required: "Please enter amount",
						number: "Please enter only number",
						max: "You can not deduct amount more than $<?php echo $amount;?>"
					}
				},
				invalidHandler: function (event, validator) { //display error alert on form submit
					//successHandler1.hide();
					//errorHandler1.show();
				},
				highlight: function (element) {
					$(element).closest('.help-block').removeClass('valid');
					// display OK icon
					$(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
					// add the Bootstrap error class to the control group
				},
				unhighlight: function (element) { // revert the change done by hightlight
					$(element).closest('.form-group').removeClass('has-error');
					// set error class to the control group
				},
				success: function (label, element) {
					label.addClass('help-block valid');
					// mark the current input as valid and display OK icon
					$(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
				},
				submitHandler: function (frmadd) {
					successHandler1.show();
					errorHandler1.hide();
				}

			});

		};

		return {
			//main function to initiate template pages
			init: function () {
				runValidator1();
			}
		};

		//$('#frmadd').submit();
	}();

	//$('#frmadd').submit();
	$(document).ready(function () {
		FormdeductValidator.init();

		$('#amountminus').click(function () {

			$('#deduct_from_wallet').submit();

		});
	});

</script>

<script>
	var FormDebitValidator = function () {
		// function to initiate Validation Sample 1
		var temp = 0;
		var runValidator1 = function () {
			var form1 = $('#debitrate_form');

			$('#debitrate_form').validate({

				errorElement: "span", // contain the error msg in a span tag
				errorClass: 'help-block',
				errorPlacement: function (error, element) { // render error placement for each input type
					error.insertAfter(element);
					// for other inputs, just perform default behavior
				},
				ignore: "",
				rules: {
					debitrate_val: {
						required: true,
						number: true,
						min: 0,
						max: 100
					}


				},
				messages: {
					debitrate_val: {
						required: "Please enter rate",
						number: "Please enter only number"
					}
				},
				invalidHandler: function (event, validator) { //display error alert on form submit
					//successHandler1.hide();
					//errorHandler1.show();
				},
				highlight: function (element) {
					$(element).closest('.help-block').removeClass('valid');
					// display OK icon
					$(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
					// add the Bootstrap error class to the control group
				},
				unhighlight: function (element) { // revert the change done by hightlight
					$(element).closest('.form-group').removeClass('has-error');
					// set error class to the control group
				},
				success: function (label, element) {
					label.addClass('help-block valid');
					// mark the current input as valid and display OK icon
					$(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
				},
				submitHandler: function (frmadd) {
					successHandler1.show();
					errorHandler1.hide();
				}

			});

		};

		return {
			//main function to initiate template pages
			init: function () {
				runValidator1();
			}
		};

		//$('#frmadd').submit();
	}();

	//$('#frmadd').submit();
	$(document).ready(function () {
		FormDebitValidator.init();

		$('#debitrate_btn').click(function () {

			$('#debitrate_form').submit();

		});
	});

</script>

<script>
	var FormCreditValidator = function () {
		// function to initiate Validation Sample 1
		var temp = 0;
		var runValidator1 = function () {
			var form1 = $('#creditrate_form');

			$('#creditrate_form').validate({

				errorElement: "span", // contain the error msg in a span tag
				errorClass: 'help-block',
				errorPlacement: function (error, element) { // render error placement for each input type
					error.insertAfter(element);
					// for other inputs, just perform default behavior
				},
				ignore: "",
				rules: {
					creditrate_val: {
						required: true,
						number: true,
						min: 0,
						max: 100
					}


				},
				messages: {
					creditrate_val: {
						required: "Please enter rate",
						number: "Please enter only number"
					}
				},
				invalidHandler: function (event, validator) { //display error alert on form submit
					//successHandler1.hide();
					//errorHandler1.show();
				},
				highlight: function (element) {
					$(element).closest('.help-block').removeClass('valid');
					// display OK icon
					$(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
					// add the Bootstrap error class to the control group
				},
				unhighlight: function (element) { // revert the change done by hightlight
					$(element).closest('.form-group').removeClass('has-error');
					// set error class to the control group
				},
				success: function (label, element) {
					label.addClass('help-block valid');
					// mark the current input as valid and display OK icon
					$(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
				},
				submitHandler: function (frmadd) {
					successHandler1.show();
					errorHandler1.hide();
				}

			});

		};

		return {
			//main function to initiate template pages
			init: function () {
				runValidator1();
			}
		};

		//$('#frmadd').submit();
	}();

	//$('#frmadd').submit();
	$(document).ready(function () {
		FormCreditValidator.init();

		$('#creditrate_btn').click(function () {

			$('#creditrate_form').submit();

		});
	});

</script>

<script>
	var FormTransferLimitValidator = function () {
		// function to initiate Validation Sample 1
		var temp = 0;
		var runValidator1 = function () {
			var form1 = $('#transferLimit_form');

			$('#transferLimit_form').validate({

				errorElement: "span", // contain the error msg in a span tag
				errorClass: 'help-block',
				errorPlacement: function (error, element) { // render error placement for each input type
					error.insertAfter(element);
					// for other inputs, just perform default behavior
				},
				ignore: "",
				rules: {
					transferLimit_val: {
						required: true,
						number: true,
						min: 0
					}


				},
				messages: {
					transferLimit_val: {
						required: "Please enter amount",
						number: "Please enter only number",
						min: "Please enter a valid amount"
					}
				},
				invalidHandler: function (event, validator) { //display error alert on form submit
					//successHandler1.hide();
					//errorHandler1.show();
				},
				highlight: function (element) {
					$(element).closest('.help-block').removeClass('valid');
					// display OK icon
					$(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
					// add the Bootstrap error class to the control group
				},
				unhighlight: function (element) { // revert the change done by hightlight
					$(element).closest('.form-group').removeClass('has-error');
					// set error class to the control group
				},
				success: function (label, element) {
					label.addClass('help-block valid');
					// mark the current input as valid and display OK icon
					$(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
				},
				submitHandler: function (frmadd) {
					successHandler1.show();
					errorHandler1.hide();
				}

			});

		};

		return {
			//main function to initiate template pages
			init: function () {
				runValidator1();
			}
		};

		//$('#frmadd').submit();
	}();

	//$('#frmadd').submit();
	$(document).ready(function () {
		FormTransferLimitValidator.init();
		$('#transferLimit_btn').click(function () {
			$('#transferLimit_form').submit();
		});

		$('.moment-time').each(function () {
			var formattedTime = moment($(this).data('time').replace(" ", "T") + "Z").format('D MMMM YYYY, LT');
			$(this).html(formattedTime);
		});
	});

</script>
<script>
	function showUpValue(data,id){
		$(id).val(data);
	}
</script>