<?php
error_reporting(0);
$iId = $_REQUEST['iId'];
$promotions = new promotions();

$res_db = $promotions->userPromotions($iId);
//pr($res_db);
$MODULE = 'User Promotions';
$ADD_LINK = 'index.php?file=us-userpromotionsadd';
$ACT_LINK = 'index.php?file=us-userpromotions_a';
$PRE_LINK = 'index.php?file=us-userview&iId=' . $iId;

?>
<!-- Start: Extra CSS -->
<style>
	.body-scroll-box{
		overflow-y: scroll;
		height: 103px;
	}

	.body-scroll-box p{
		word-break: break-all;
	}
</style>
<!-- End: Extra CSS -->

<!-- Start: Topbar -->
<header id="topbar" class="affix">
    <div class="topbar-left">
        <ol class="breadcrumb">
            <li class="crumb-link crumb-active ">
                <a href="index.php">Dashboard</a>
            </li>
			<li class="crumb-active crumb-link">
				<a href="<?php echo $PRE_LINK; ?>"><?php echo "User"; ?> </a>
			</li>
            <li class="crumb-trail"><?php echo $MODULE; ?></li>
        </ol>
    </div>
</header>
<!-- End: Topbar -->

<!-- Start: Extra CSS -->
<style>
	.table-scroll{
		width: 100%;
		overflow: auto;
	}
</style>
<!-- Stop: Extra CSS -->

<section id="content" class="tray tray-center pv40 ph30 va-t posr">
    <div class="row">
        <div class="col-md-12">
            <div id="msg" class="notification" data-note-stack="stack_bar_top"
                 data-note-style="<?php echo $_SESSION[$session_prefix . 'admin_var_msg_class']; ?>"></div>
            <div class="panel panel-visible <?php echo $PANEL_HEADER; ?>">

                <div class="panel-heading">
                    <div class="panel-title hidden-xs">
						<span class="pull-left">
                            <a href="<?php echo $ADD_LINK; ?>&uId=<?php echo $iId; ?>" class="btn btn-sm btn-primary btn-dark animsition-link"
							   data-style="expand-left">Add Promotion</a>
                            <a href="#" class="btn btn-sm btn-danger" data-style="expand-left"
							   onclick="return changeStatusEvent('delete','admin_list');">Delete</a>
                        </span>
						<span class="pull-left" style="margin-left:5px">
						   <a href="#" class="btn btn-sm btn-system btn-block" style="margin-top: 5px;"
							  onclick="return changeStatusEvent('active','admin_list');">Active </a>
						</span>

						<span class="pull-left" style="margin-top: 5px; margin-left: 5px;">
							<a href="#" class="btn btn-sm btn-warning btn-block"
							  onclick="return changeStatusEvent('inactive','admin_list');">Inactive </a>
						</span>
                    </div>
                </div>

                <div class="panel-body pn">
                    <form name="admin_list" id="admin_list" method="post" action="<?php echo $ACT_LINK; ?>">
                        <input type="hidden" name="mode" id="mode" value="">
                        <input type="hidden" name="iId" id="iId" value="<?php echo $iId;?>">
                        <input type="hidden" name="delete_type" value="multi_delete">
                        <div class="table-scroll">
							<table class="table table-striped table-bordered table-hover" id="datatable3" cellspacing="0" width="100%">
								<thead>
								<tr class="<?php echo $TBL_HEADER; ?>">
									<th width="10%">
										<center><input type="checkbox" onclick="check_all();"></center>
									</th>
									<th width="10%">
										<center>Action</center>
									</th>
									<th width="10%">Name</th>
									<th width="10%">Discount</th>
									<th width="10%">Spend</th>
									<th width="10%">Contact</th>
									<th width="10%">Cash Back</th>
									<th width="10%">Start Date</th>
									<th width="10%">End Date</th>
									<th width="10%">
										<center>Status</center>
									</th>
								</tr>
								</thead>
								<tbody>
								<?php
								if (count($res_db) > 0) {
									foreach ($res_db as $userpromotionlist) {
										if($userpromotionlist['promotion_id'] != 0) {
											?>
											<tr>
												<td align="center">
													<input type="checkbox" name="delete[]" id='iId' value="<?php echo $userpromotionlist['promotion_id']; ?>">
												</td>
												<td align="center">
													<a href="<?php echo $ADD_LINK; ?>&mode=update&iId=<?php echo $userpromotionlist['promotion_id'].'&uId='.$iId; ?>" class="btn btn-xs btn-success">Edit</a>
													<a href="#" onclick="changeDeleteEvent('<?php echo $ACT_LINK; ?>&mode=delete&iId=<?php echo $userpromotionlist['promotion_id'].'&uId='.$iId; ?>&delete_type=single_delete');" class="btn btn-xs btn-danger">Delete</a>
												</td>
												<td><?php echo $userpromotionlist['company_name']; ?></td>
												<td><?php echo $userpromotionlist['discount']; ?></td>
												<td><?php echo $userpromotionlist['transaction_limit']; ?></td>
												<td><?php echo $userpromotionlist['contact']; ?></td>
												<td><?php echo $userpromotionlist['cashback']; ?></td>
												<td><?php echo $userpromotionlist['startdate']; ?></td>
												<td><?php echo $userpromotionlist['enddate']; ?></td>

												<?php if ($userpromotionlist['status'] == '1') { ?>
													<td align="center"><span class="tm-tag tm-tag-system">Active</span>
													</td>
												<?php } elseif ($userpromotionlist['status'] == '0') { ?>
													<td align="center"><span class="tm-tag tm-tag-warning">Inactive</span>
													</td>
												<?php } elseif ($userpromotionlist['status'] == '2') { ?>
													<td align="center"><span class="tm-tag tm-tag-danger">Deleted</span>
													</td>
												<?php }?>
											</tr>

											<?php
										}
									}
								} else {

									echo $generalfuncobj->no_record_found(10);
								} ?>
								</tbody>
							</table>
						</div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Start: Content -->
<div id="modalpopup" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false"
	 style="display: none;">
<!-- End: Content -->

<?php include_once($admin_path . 'js_table.php'); ?>
<?php include_once($admin_path . 'modalpopup.php'); ?>
