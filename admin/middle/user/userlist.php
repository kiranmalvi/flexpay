<?php
error_reporting(0);
$user = new user();

$res_db = $user->selectMain_User();

//pr($res_db);
$MODULE = 'User ';
$ADD_LINK = 'index.php?file=us-useradd';
$ACT_LINK = 'index.php?file=us-user_a';
$PRE_LINK ='index.php?file=us-userview';
$orderlistexcel = 'export';


$wallet = new wallet();

function fullescape($in)
{
	$out = '';
	for ($i=0;$i<strlen($in);$i++)
	{
		$hex = dechex(ord($in[$i]));
		if ($hex=='')
			$out = $out.urlencode($in[$i]);
		else
			$out = $out .'%'.((strlen($hex)==1) ? ('0'.strtoupper($hex)):(strtoupper($hex)));
	}
	$out = str_replace('+','%20',$out);
	$out = str_replace('_','%5F',$out);
	$out = str_replace('.','%2E',$out);
	$out = str_replace('-','%2D',$out);
	return $out;
}

?>
<!-- Start: Topbar -->
<style>
	.body-scroll-box{
		overflow-y: scroll;
		height: 103px;
	}

	.body-scroll-box p{
		word-break: break-all;
	}
</style>
<!-- Start: Topbar -->
<header id="topbar" class="affix">
    <div class="topbar-left">
        <ol class="breadcrumb">
            <li class="crumb-link crumb-active ">
                <a href="index.php">Dashboard</a>
            </li>

            <li class="crumb-trail"><?php echo $MODULE; ?></li>
        </ol>
    </div>
</header>
<!-- End: Topbar -->
<style>
	.table-scroll{
		width: 100%;
		overflow: auto;
	}
</style>
<script>
	function add_note(id, note){
		$('#user_id').val(id);
		$('#addNote_val').val(decodeURIComponent(note));
	}

	function view_note(note){
		$('#view_note').html((note) ? decodeURIComponent(note) : "No notes added for this User." );
	}
</script>
<section id="content" class="tray tray-center pv40 ph30 va-t posr">
    <div class="row">
        <div class="col-md-12">
            <div id="msg" class="notification" data-note-stack="stack_bar_top"
                 data-note-style="<?php echo $_SESSION[$session_prefix . 'admin_var_msg_class']; ?>"></div>
            <div class="panel panel-visible <?php echo $PANEL_HEADER; ?>">

                <div class="panel-heading">
                    <div class="panel-title hidden-xs">
						<span class="pull-left">
							<a href="#" class="btn btn-sm btn-danger" data-style="expand-left"
							   onclick="return changeStatusEvent('block','admin_list');">Block</a>
						</span>

						<span class="pull-left" style="margin-left:5px">
							<a href="#" class="btn btn-sm btn-dark" data-style="expand-left"
							   onclick="return changeStatusEvent('un-block','admin_list');">Un-Block</a>
						</span>

						<span class="pull-left" style="margin-left:5px">
						   <a href="#" class="btn btn-sm btn-system btn-block" style="margin-top: 5px;"
							  onclick="return changeStatusEvent('active','admin_list');">Active </a>
						</span>

						<span class="pull-left" style="margin-top: 5px; margin-left: 5px;">
							<a href="#" class="btn btn-sm btn-warning btn-block"
							  onclick="return changeStatusEvent('inactive','admin_list');">Inactive </a>
						</span>
                    </div>
                </div>

                <div class="panel-body pn">
                    <form name="admin_list" id="admin_list" method="post" action="<?php echo $ACT_LINK; ?>">
                        <input type="hidden" name="mode" id="mode" value="">
                        <input type="hidden" name="delete_type" value="multi_delete">
                        <div class="table-scroll">
							<table class="table table-striped table-bordered table-hover" id="datatable3" cellspacing="0" width="100%">
								<thead>
								<tr class="<?php echo $TBL_HEADER; ?>">
									<th width="4%"><input type="checkbox" onclick="check_all();"></th>
									<th width="5%">
										<center>Action</center>
									</th>
									<th width="5%">Name</th>
									<th width="5%">Email</th>
									<th width="8%">Mobile</th>
									<th width="8%">Total Amount</th>
									<th width="15%">Company Name</th>
									<th width="10%">Security Pin</th>
									<th width="15%">Verification Code</th>
									<th width="15%">
										<center>Register DateTime</center>
									</th>
									<th width="10%">
										<center>Status</center>
									</th>
								</tr>
								</thead>
								<tbody>
								<?php
								if (count($res_db) > 0) {
									foreach ($res_db as $userlist) {
										if($userlist['id'] != 0) {

											$walletlist = $wallet->userwallet($userlist['id']);

											?>
											<tr>
												<td align="center">

													<input type="checkbox" name="delete[]" id='iId'
														   value="<?php echo $userlist['id']; ?>">

												</td>
												<td align="center">
													<?php
													if($userlist['status'] != "4") {
														?>
														<a href="<?php echo $PRE_LINK; ?>&iId=<?php echo $userlist['id']; ?>"
														   class="btn btn-xs btn-alert animsition-link">View</a>
														<?php
													}
													else{
														?>
														<a href="#"
														   onclick="changeDeleteEvent('<?php echo $ACT_LINK; ?>&mode=delete&iId=<?php echo $userlist['id']; ?>&delete_type=single_delete');"
														   class="btn btn-xs btn-danger">Delete</a>
														<?php
													}
													?>
													<a class="btn btn-xs btn-success animsition-link" data-style="expand-left"
													   data-toggle="modal" href="#addNote" onclick="add_note(<?php echo $userlist['id'];?>, '<?php echo fullescape(trim($userlist['notes']));?>');">Add Note</a>
													<a class="btn btn-xs btn-info animsition-link" data-style="expand-left"
													   data-toggle="modal" href="#viewNote" onclick="view_note('<?php echo fullescape(trim($userlist['notes']));?>');">View Note</a>
												</td>
												<td><?php echo $userlist['name']; ?></td>
												<td><?php echo $userlist['email']; ?></td>
												<td><?php echo $userlist['mobile']; ?></td>
												<td><?php if($walletlist[0]['amount'] == '' || $walletlist[0]['amount'] < 0) {
														$amount = '0.0';
													} else {
														$amount = round($walletlist[0]['amount'], 2);
													}

													echo $amount; ?></td>
												<td>
													<?php
													if($userlist['company_name'] != "")
													{
														echo $userlist['company_name'];
													}
													else{
														echo '---';
													}
													?>
												</td>
												<td>
													<?php
													if($userlist['security_pin'] != "")
													{
														echo $userlist['security_pin'];
													}
													else{
														echo '---';
													}
													?>
												</td>
												<td>
													<?php
													if($userlist['verification_code'] != "")
													{
														echo $userlist['verification_code'];
													}
													else{
														echo '---';
													}
													?>
												</td>


												<td align="center"><?php echo ($userlist['added_at'] != "" && $userlist['added_at'] != "0000-00-00 00:00:00") ? $userlist['added_at'] : '-'; ?></td>

												<?php if ($userlist['status'] == '1') { ?>
													<td align="center"><span class="tm-tag tm-tag-system">Active</span></td>
												<?php } elseif ($userlist['status'] == '0') { ?>
													<td align="center"><span class="tm-tag tm-tag-warning">Inactive</span>
													</td>
												<?php } elseif ($userlist['status'] == '2') { ?>
													<td align="center"><span class="tm-tag tm-tag-danger">Deleted</span>
													</td>
												<?php } elseif ($userlist['status'] == '3') { ?>
													<td align="center"><span class="tm-tag tm-tag-danger">Block</span></td>
												<?php } elseif ($userlist['status'] == '4') { ?>
													<td align="center"><span class="tm-tag tm-tag-danger">Un-Verified</span>
													</td>
												<?php } ?>

											</tr>

											<?php
										}
									}
								} else {

									echo $generalfuncobj->no_record_found(5);
								} ?>
								</tbody>
							</table>
						</div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Start: Content -->
<div id="modalpopup" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false"
	 style="display: none;">
	<!-- End: Content -->

<!-- Add Note popup - Start: Content -->
<div id="addNote" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" style="display: none;">
	<form method="post" id="addNote_form" class="form-horizontal" role="form" action="<?php echo $ACT_LINK; ?>" enctype="multipart/form-data" name="addNote_form">
		<div class="modal-header">
			<div class="panel-title">
				Add Note For User
			</div>
		</div>
		<div class="modal-body">
			<input type="hidden" name="user_id" id="user_id" class="form-control"
				   value="" placeholder=""/>
			<input type="hidden" name="mode" id="mode" class="form-control"
				   value="add_note" placeholder=""/>
			<div class="form-group">
				<div class="col-md-12">
					<label class="col-md-3 control-label">Note :</label>
					<div class="col-md-8">
						<textarea name="addNote_val" id="addNote_val" class="form-control" placeholder="Enter note for user"></textarea>
					</div>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<input type="submit" data-dismiss="" class="btn btn-success" id="addNote_btn" value="Save"/>
			<button data-dismiss="modal" class="btn btn-default">Close</button>
		</div>
	</form>
</div>
<!-- Add Note popup - End: Content -->

<!-- View Note popup - Start: Content -->
<div id="viewNote" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" style="display: none;">
	<div class="modal-header">
		<div class="panel-title">
			User Note
		</div>
	</div>
	<div class="modal-body body-scroll-box">
		<div class="form-group">
			<div class="col-md-12">
				<p id="view_note"></p>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button data-dismiss="modal" class="btn btn-default">Close</button>
	</div>
</div>
<!-- View Note popup - End: Content -->
    <?php include_once($admin_path . 'js_table.php'); ?>
    <?php include_once($admin_path . 'modalpopup.php'); ?>
