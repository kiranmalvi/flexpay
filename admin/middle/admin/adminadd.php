<?php
/**
 * Created by PhpStorm.
 * User: mind
 * Date: 9/4/15
 * Time: 6:26 PM
 */
error_reporting(0);
$iId = (isset($_REQUEST['iId'])) ? $_REQUEST['iId'] : '';
$mode = ($iId != '') ? 'edit' : 'add';
$read='';
if ($iId != '') {


    $admin = new admin();

    $db_res = $admin->select($iId);
    $read ="readonly";
}
//pr($db_res);exit;

//$style='';
//if($_SESSION[$Project_Name]['ADMIN']['id'] == $adinlist['admin_id']) {
//    $style="readonly";
//                                }
$MODE_TYPE = ucfirst($mode);
$MODULE = 'Admin';
$ADD_LINK = 'index.php?file=a-adminlist';
$ACT_LINK = 'index.php?file=a-admin_a';
$PRE_LINK = 'index.php?file=a-adminlist';

?>

<!-- Start: Topbar -->
<header id="topbar" class="affix">
    <div class="topbar-left">
        <ol class="breadcrumb">
            <li class="crumb-active">
                <a href="index.php">Dashboard</a>
            </li>

            <li class="crumb-active crumb-link">
                <a href="<?php echo $PRE_LINK; ?>"><?php echo $MODULE; ?> </a>
            </li>
            <li class="crumb-trail"><?php echo $MODE_TYPE . ' ' . $MODULE; ?> Detail</li>
        </ol>
    </div>
</header>
<!-- End: Topbar -->

<section id="content" class="table-layout">

    <!-- begin: .tray-center -->
    <div class="tray tray-center pv40 ph30 va-t posr">
        <div class="center-block">

            <!-- begin: .admin-form -->
            <div class="admin-form">

                <div id="p1" class="panel heading-border panel-system">

                    <div class="panel-body bg-light">
                        <form method="post" id="admin_form" class="form-horizontal" role="form"
                              action="<?php echo $ACT_LINK; ?>" enctype="multipart/form-data">

                            <div class="section-divider mb40" id="spy1">
                                <span class="panel-primary"><?php echo $MODE_TYPE . ' ' . $MODULE; ?> Detail</span>
                            </div>

                            <input type="hidden" name="mode" value="<?php echo $mode; ?>">
                            <input type="hidden" name="admin_id" id="admin_id"
                                   value="<?php echo $db_res[0]['id']; ?>">

                            <input type="hidden" name="password" id="password"
                                   value="<?php echo $db_res[0]['password']; ?>">
                            <!-- .section-divider -->

                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> Email:</label>

                                <div class="col-md-8">
                                    <input type="email" name="admin_email" id="admin_email" class="form-control"
                                           value="<?php echo $db_res[0]['email']; ?>" placeholder="Email Address" <?php echo $read?>>
                                </div>
                            </div>

							<div class="form-group">
								<label class="col-md-2 control-label"><span class="red"> *</span> User Name:</label>

								<div class="col-md-8">
									<input type="text" name="admin_user_name" id="admin_user_name" class="form-control"
										   value="<?php echo $db_res[0]['name']; ?>" placeholder="User Name">

								</div>
							</div>

							<?php if ($_SESSION[$Project_Name]['ADMIN']['id'] !== $db_res[0]['admin_id'] && $mode == 'add') { ?>
                                <div class="form-group">
                                    <label class="col-md-2 control-label"><span class="red"> *</span> Password:</label>
                                    <div class="col-md-8">
                                        <input type="password" name="admin_password" id="admin_password" class="form-control" value="" placeholder="Password"/>
                                    </div>
                                </div>
                            <?php } ?>
<!--                            <div class="form-group">-->
<!--                                <label class="col-md-2 control-label"><span class="red"> *</span> Admin Type-->
<!--                                    :</label>-->
<!---->
<!--                                <div class="col-md-8">-->
<!--                                    <select class="form-control" name="admin_type" id="admin_type">-->
<!--                                        <option selected disabled>- Select Admin Type -</option>-->
<!--                                        <option-->
<!--                                            value="super_admin" --><?php //echo($db_res[0]['admin_type'] == 'super_admin' ? 'selected' : ''); ?><!-->
<!--                                            Super admin-->
<!--                                        </option>-->
<!---->
<!--                                    </select>-->
<!---->
<!--                                </div>-->
<!--                            </div>-->

                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> Status:</label>

                                <div class="col-md-8">
                                    <select class="form-control" name="eStatus" id="eStatus" <?php echo $style; ?>>
                                        <option selected disabled>- Select Status -</option>
                                        <option
                                            value="1" <?php echo($db_res[0]['status'] == '1' ? 'selected' : ''); ?>>
                                            Active
                                        </option>
                                        <option
                                            value="0" <?php echo($db_res[0]['status'] == '0' ? 'selected' : ''); ?>>
                                            Inactive
                                        </option>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button id="addbutton" type="submit"
                                            class="btn  btn-primary animsition-link"><?php echo ($iId != '') ? 'Save' : 'Add' ?></button>
                                    <a href="<?php echo $ADD_LINK; ?>" class="btn btn-dark animsition-link">Back</a>
                                </div>
                            </div>
                            <!-- Basic Inputs -->


                        </form>
                    </div>
                </div>
            </div>
            <!-- end: .admin-form -->
        </div>
    </div>
</section>

<?php include_once($admin_path . 'js_form.php'); ?>


<script src="<?php echo $assets_url ?>js/jquery.validate.min.js"></script>

<script>
    var FormAdminValidator = function () {
        // function to initiate Validation Sample 1
        var temp = 0;
        var runValidator1 = function () {
            var form1 = $('#admin_form');

            $('#admin_form').validate({

                errorElement: "span", // contain the error msg in a span tag
                errorClass: 'help-block',
                errorPlacement: function (error, element) { // render error placement for each input type
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
                },
                ignore: "",
                rules: {
					<?php if($mode == "add") {?>
                    admin_email: {
                        required: true,
                        email: true,
						remote: {
							url: "<?php echo $ACT_LINK . '&mode=checkEmail';?>",
							type: "post",
							data: {
								email: function () {
									return $("#admin_email").val();
								}
							}
						}
                    },
					admin_password: {
                        required: true,
                        minlength: 6
                    },
					<?php } ?>
                    admin_user_name: {
                        required: true
                    },
                    eStatus: {
                        required: true
                    }
                },
                messages: {
                    admin_email: {
                        required: "Please enter email address",
                        email: "Please enter valid email address",
						remote:"This email is already registered with another admin."
                    },
					<?php if($mode == "add") {?>
					admin_password: {
						required: "Please enter password",
					},
					<?php } ?>

                    admin_user_name: "Please enter user name",

                    eStatus: "Please select status"
                },
                invalidHandler: function (event, validator) { //display error alert on form submit
                    //successHandler1.hide();
                    //errorHandler1.show();
                },
                highlight: function (element) {
                    $(element).closest('.help-block').removeClass('valid');
                    // display OK icon
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                    // add the Bootstrap error class to the control group
                },
                unhighlight: function (element) { // revert the change done by hightlight
                    $(element).closest('.form-group').removeClass('has-error');
                    // set error class to the control group
                },
                success: function (label, element) {
                    label.addClass('help-block valid');
                    // mark the current input as valid and display OK icon
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
                },
                submitHandler: function (frmadd) {
                    successHandler1.show();
                    errorHandler1.hide();
                }

            });

        };

        return {
            //main function to initiate template pages
            init: function () {
                runValidator1();
            }
        };

        //$('#frmadd').submit();
    }();

    //$('#frmadd').submit();
    $(document).ready(function () {
        FormAdminValidator.init();

        $('#addbutton').click(function () {
            $('#eStatus').prop("disabled", false);
            $('#admin_form').submit();

        });
    });

</script>