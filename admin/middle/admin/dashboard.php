<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 5/10/15
 * Time: 3:16 PM
 */

$user = new user();

$get_total_users = $user->get_total_users();
$get_total_amount = $user->total_amount_in_user_wallet();
$total_trasactions = $user->total_trasactions();

$sql    =   "SELECT * FROM setting";
$db =   $obj->select($sql);
#pr($db);

foreach($db as $db_arr)
{
    $db_array[$db_arr['vName']] = $db_arr['vValue'];
}


?>

    <!-- Start: Topbar -->
    <header id="topbar" class="affix">
        <div class="topbar-left">
            <ol class="breadcrumb">
                <li class="crumb-link crumb-active ">
                    <a href="index.php">Dashboard</a>
                </li>
            </ol>
        </div>
    </header>
    <!-- End: Topbar -->

	<!-- Style - Custom-->
	<style>
		.custom-rate{
			width:70px;
		}
	</style>
	<!-- Style - Custom-->

    <section id="content" class="pn">
        <!-- <div class="p40 bg-background bg-topbar bg-psuedo-tp"> -->
		<div id="msg" class="notification" data-note-stack="stack_bar_top"
			 data-note-style="<?php echo $_SESSION[$session_prefix . 'admin_var_msg_class']; ?>"></div>
        <div class="pv30 ph40 bg-light dark br-b br-grey posr" style="padding-top: 45px !important; padding-bottom: 0px !important;">

            <div class="row mb10 col-md-offset-2">
                <a href="javascript:void(0)" title="Totla Users">
                    <div class="col-md-3">
                        <div class="panel bg-alert light of-h mb10">
                            <div class="pn pl20 p5">
                                <div class="icon-bg"> <i class="glyphicons glyphicons-user"></i> </div>
                                <h2 class="mt15 lh15"> <b><?php echo $get_total_users[0]['tot'];?></b> </h2>
                                <h5 class="text-muted" style="color: #FFF;">Users</h5>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="javascript:void(0)" title="Total Amount">
                    <div class="col-md-3">
                        <div class="panel bg-info light of-h mb10">
                            <div class="pn pl20 p5">
                                <div class="icon-bg"> <i class="fa fa-dollar"></i> </div>
                                <h2 class="mt15 lh15"> <b><?php echo round($get_total_amount[0]['total_amt'],2);?></b> </h2>
                                <h5 class="text-muted" style="color: #FFF;">Amount</h5>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="javascript:void(0)" title="Total Transactions">
                    <div class="col-md-3">
                        <div class="panel bg-danger light of-h mb10">
                            <div class="pn pl20 p5">
                                <div class="icon-bg"> <i class="glyphicons glyphicons-iphone_transfer"></i> </div>
                                <h2 class="mt15 lh15"> <b><?php echo $total_trasactions[0]['total_transaction'];?></b> </h2>
                                <h5 class="text-muted" style="color: #FFF;">Transactions</h5>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </section>
    <section id="content" class="pn">
        <div class="pv30 ph40 bg-light dark br-b br-grey posr" style="padding-bottom: 60px !important;">

            <div class="col-md-4">
                <form action="index.php?file=a-admin_a" method="post">
                    Default Credit Rate:
                    <input type="text" class="custom-rate" name="DEFAULT_CREDIT_RATE" id="DEFAULT_CREDIT_RATE" value="<?php echo $db_array['DEFAULT_CREDIT_RATE'];?>">
                    <input type="submit" class="btn-primary animsition-link" name="credit_rate" id="credit_rate" value="SAVE">
                </form>
            </div>
            <div class="col-md-4">
                <form action="index.php?file=a-admin_a" method="post">
                    Default Debit Rate:
                    <input type="text" class="custom-rate" name="DEFAULT_DEBIT_RATE" id="DEFAULT_DEBIT_RATE" value="<?php echo $db_array['DEFAULT_DEBIT_RATE']; ?>">
                    <input type="submit" class="btn-primary animsition-link" name="debit_rate" id="debit_rate" value="SAVE">
                </form>
            </div>
            <div class="col-md-4">
                <form action="index.php?file=a-admin_a" method="post">
                    Default Spending Limit:
                    <input type="text" class="custom-rate" name="DEFAULT_SPEND_LIMIT" id="DEFAULT_SPEND_LIMIT" value="<?php echo $db_array['DEFAULT_SPEND_LIMIT']; ?>">
                    <input type="submit" class="btn-primary animsition-link" name="spend_limit" id="spend_limit" value="SAVE">
                </form>
            </div>
        </div>
		<div class="pv30 ph40 bg-light dark br-b br-grey posr" style="padding-bottom: 60px !important;">
			<div class="col-md-12 col-md-offset-1">
            <form action="index.php?file=a-admin_a" method="post">
				<div class="col-md-5 col-md-offset-1">
					Spend Limit For Admin Reward:
                    <input type="text" class="custom-rate" name="ADMIN_REWARD_LIMIT" id="ADMIN_REWARD_LIMIT" value="<?php echo $db_array['ADMIN_REWARD_LIMIT'];?>">
                </div>
				<div class="col-md-6">
                    Admin Reward Default Value:
                    <input type="text" class="custom-rate" name="vDefValue" id="vDefValue" value="<?php echo $db[3]['vDefValue'];?>">
					<input type="submit" class="btn-primary animsition-link" name="reward_limit" id="reward_limit"
						   value="SAVE">
                </div>
            </form>
            </div>
		</div>

    </section>
<?php include_once($admin_path . 'js_table.php'); ?>
<?php include_once($admin_path . 'modalpopup.php'); ?>