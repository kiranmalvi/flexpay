<?php
/**
 * Created by PhpStorm.
 * User: mind
 * Date: 9/4/15
 * Time: 6:26 PM
 */

//echo $iId;


$cms = new cms();
$iId = $_REQUEST['iId'];

$db_res =$cms->select($iId);
//print_r($db_res);exit;
if($iId == '1')
{
    $MODE_TYPE = 'About Us';
}
else if($iId == '2')
{
    $MODE_TYPE = 'Terms & Conditions';
}
else if($iId == '3')
{
    $MODE_TYPE = 'Privacy Policy';
} else if ($iId == '4') {
    $MODE_TYPE = 'Service Free Agreement';
} else if ($iId == '5') {
    $MODE_TYPE = 'Memorandum Of Understanding';
} else if ($iId == '6') {
    $MODE_TYPE = 'Consumer Terms & Conditions';
}

$MODULE = 'CMS';
$ADD_LINK = 'index.php?file=cms-cmsadd';
$ACT_LINK = 'index.php?file=cms-cms_a';

?>
<!--<link rel="stylesheet" type="text/css" href="--><?php //echo $admin_url ?><!--vendor/editors/summernote/summernote.css">-->
<!--<link rel="stylesheet" type="text/css" href="--><?php //echo $admin_url ?><!--vendor/editors/summernote/summernote-bs3.css">-->


<!-- Start: Topbar -->
<header id="topbar" class="affix">
    <div class="topbar-left">
        <ol class="breadcrumb">
            <li class="crumb-active">
                <a href="index.php">Dashboard</a>
            </li>
            <!---->
            <!--                <li class="crumb-active crumb-link">-->
            <!--                    <a href="--><?php //echo $PRE_LINK; ?><!--">-->
            <?php //echo $MODULE; ?><!--</a>-->
            <!--                </li>-->
            <li class="crumb-trail"><?php echo $MODE_TYPE; ?></li>
        </ol>
    </div>
</header>
<!-- End: Topbar -->

<section id="content" class="table-layout">

    <!-- begin: .tray-center -->
    <div class="tray tray-center pv40 ph30 va-t posr ">
        <div class="center-block">

            <!-- begin: .admin-form -->
            <div class="admin-form">
                <div id="msg" class="notification" data-note-stack="stack_bar_top"
                     data-note-style="<?php echo $_SESSION[$session_prefix . 'admin_var_msg_class']; ?>"></div>

                <div id="p1" class="panel heading-border panel-system">

                    <div class="panel-body bg-light">
                        <form method="post" id="cms_form" class="form-horizontal" role="form"
                              action="<?php echo $ACT_LINK; ?>" enctype="multipart/form-data">

                            <div class="section-divider mb40" id="spy1">
                                <span class="panel-primary"><?php echo $MODE_TYPE; ?> </span>
                            </div>

                            <input type="hidden" name="mode" value="edit">
                            <input type="hidden" name="cms_id" id="cms_id"
                                   value="<?php echo $db_res[0]['cms_id']; ?>">


 <input type="hidden" name="cms_type" id="cms_type"
                                   value="<?php echo $db_res[0]['type']; ?>">



                            <!-- .section-divider -->
                            <div class="form-group">

                                <label class="col-md-1 control-label"><span class="red"> </span>
                                </label>



                                <div class="col-md-10">
                                            <textarea class="ckeditor form-control" placeholder="Cms Description"
                                                      id="text" name="text"
                                                      rows="10"><?php echo $db_res[0]['text']; ?></textarea>
                                </div>

                            </div>

                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button id="addbutton" type="submit"
                                            class="btn  btn-primary"> Save</button>
                                    <a href="<?php echo $ADD_LINK; ?>" class="btn btn-dark animsition-link">Back</a>
                                </div>
                            </div>
                            <!-- Basic Inputs -->


                        </form>
                    </div>
                </div>
            </div>
            <!-- end: .admin-form -->
        </div>
    </div>
</section>


<!-- jQuery -->
<!---->

<script src="<?php echo $assets_url ?>plugins/ckeditor-ckfinder-integration/ckeditor/ckeditor.js"></script>
<script src="<?php echo $assets_url ?>js/jquery.validate.min.js"></script>

<script>
    var FormAdminValidator = function () {
        // function to initiate Validation Sample 1
        var temp = 0;
        var runValidator1 = function () {
            var form1 = $('#cms_form');

            $('#cms_form').validate({

                errorElement: "span", // contain the error msg in a span tag
                errorClass: 'help-block',
                errorPlacement: function (error, element) { // render error placement for each input type
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
                },
                ignore: "",
                rules: {
                    cms_data: {
                        required: true
                    }

                },
                messages: {
                    cms_data: "Please enter CMS name."
                },
                invalidHandler: function (event, validator) { //display error alert on form submit
                    //successHandler1.hide();
                    //errorHandler1.show();
                },
                highlight: function (element) {
                    $(element).closest('.help-block').removeClass('valid');
                    // display OK icon
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                    // add the Bootstrap error class to the control group
                },
                unhighlight: function (element) { // revert the change done by hightlight
                    $(element).closest('.form-group').removeClass('has-error');
                    // set error class to the control group
                },
                success: function (label, element) {
                    label.addClass('help-block valid');
                    // mark the current input as valid and display OK icon
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
                },
                submitHandler: function (frmadd) {
                    successHandler1.show();
                    errorHandler1.hide();
                }

            });

        };

        return {
            //main function to initiate template pages
            init: function () {
                runValidator1();
            }
        };

        //$('#frmadd').submit();
    }();

    //$('#frmadd').submit();
    $(document).ready(function () {
        $(.ckeditor
        form - control
        ).
        ckeditor();
        FormAdminValidator.init();
        $('#addbutton').click(function () {
            $('#cms_form').submit();
        });
    });
</script>
<?php include_once($admin_path . 'js_form.php'); ?>
<?php include_once($admin_path . 'modalpopup.php'); ?>

