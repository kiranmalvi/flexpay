<?php
/**
 * Created by PhpStorm.
 * User: mind
 * Date: 9/4/15
 * Time: 6:26 PM
 */
error_reporting(0);
$iId = (isset($_REQUEST['iId'])) ? $_REQUEST['iId'] : '';
$mode = ($iId != '') ? 'edit' : 'add';

if ($iId != '') {



    $account_setting = new account_setting();


    $db_res = $account_setting->select($iId);
}
#pr($db_res);exit;

//$style='';
//if($_SESSION[$Project_Name]['ADMIN']['id'] == $adinlist['admin_id']) {
//    $style="readonly";
//                                }
$MODE_TYPE = ucfirst($mode);
$MODULE = 'Account Setting';

$ACT_LINK = 'index.php?file=as-accountsetting_a';


$PRE_LINK = 'index.php?file=as-accountlist';

?>

<!-- Start: Topbar -->
<header id="topbar" class="affix">
    <div class="topbar-left">
        <ol class="breadcrumb">
            <li class="crumb-active">
                <a href="index.php">Dashboard</a>
            </li>

            <li class="crumb-active crumb-link">
                <a href="<?php echo $PRE_LINK; ?>"><?php echo $MODULE; ?> </a>
            </li>
            <li class="crumb-trail"><?php echo $MODE_TYPE . ' ' . $MODULE; ?> Detail</li>
        </ol>
    </div>
</header>
<!-- End: Topbar -->

<section id="content" class="table-layout">

    <!-- begin: .tray-center -->
    <div class="tray tray-center pv40 ph30 va-t posr">
        <div class="center-block">

            <!-- begin: .admin-form -->
            <div class="admin-form">

                <div id="p1" class="panel heading-border panel-system">

                    <div class="panel-body bg-light">
                        <form method="post" id="admin_form" class="form-horizontal" role="form"
                              action="<?php echo $ACT_LINK; ?>" enctype="multipart/form-data">

                            <div class="section-divider mb40" id="spy1">
                                <span class="panel-primary"><?php echo $MODE_TYPE . ' ' . $MODULE; ?> Detail</span>
                            </div>

                            <input type="hidden" name="mode" value="<?php echo $mode; ?>">
                            <input type="hidden" name="account_id" id="account_id"
                                   value="<?php echo $db_res[0]['account_setting_id']; ?>">


                            <!-- .section-divider -->

                                <div class="form-group">
                                    <label class="col-md-2 control-label"><span class="red"> *</span> Account Number:</label>

                                    <div class="col-md-8">
                                        <input type="text" name="account_number" id="account_number" class="form-control"
                                               value="<?php echo $db_res[0]['account_number']; ?>" placeholder="Enter account number">
                                    </div>
                                </div>


                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> Bank Name:</label>

                                <div class="col-md-8">
                                    <input type="text" name="bank_name" id="bank_name" class="form-control"
                                           value="<?php echo $db_res[0]['bank_name']; ?>" placeholder="Enter bank name">

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> Isfc Code:</label>

                                <div class="col-md-8">
                                    <input type="text" name="isfc_code" id="isfc_code" class="form-control"
                                           value="<?php echo $db_res[0]['isfc_code']; ?>" placeholder="Enter isfc code">

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> Bank Address:</label>

                                <div class="col-md-8">

                                    <textarea  name="bank_address" id="bank_address" class="form-control" placeholder="Bank address"><?php echo $db_res[0]['bank_address']; ?></textarea>

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> Name Of Beneficiary:</label>

                                <div class="col-md-8">
                                    <input type="text" name="name_of_beneficiary" id="name_of_beneficiary" class="form-control"
                                           value="<?php echo $db_res[0]['name_of_beneficiary']; ?>" placeholder="Enter name of beneficiary ">

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> Address Of Beneficiary:</label>

                                <div class="col-md-8">

                                    <textarea name="address_of_beneficiary" id="address_of_beneficiary" class="form-control" placeholder="Address of beneficiary"><?php echo $db_res[0]['address_of_beneficiary']; ?></textarea>

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> Mobile Number:</label>

                                <div class="col-md-8">
                                    <input type="text" name="mobile_number" id="mobile_number" class="form-control"
                                           value="<?php echo $db_res[0]['mobile_number']; ?>" placeholder="Enter mobile number ">

                                </div>
                            </div>



                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> Status:</label>

                                <div class="col-md-8">
                                    <select class="form-control" name="eStatus" id="eStatus" <?php echo $style; ?>>
                                        <option selected disabled>- Select Status -</option>
                                        <option
                                            value="1" <?php echo($db_res[0]['status'] == '1' ? 'selected' : ''); ?>>
                                            Active
                                        </option>
                                        <option
                                            value="0" <?php echo($db_res[0]['status'] == '0' ? 'selected' : ''); ?>>
                                            Inactive
                                        </option>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button id="addbutton" type="submit"
                                            class="btn  btn-primary animsition-link"><?php echo ($iId != '') ? 'Save' : 'Add' ?></button>
                                    <a href="<?php echo $PRE_LINK; ?>" class="btn btn-dark animsition-link">Back</a>
                                </div>
                            </div>
                            <!-- Basic Inputs -->


                        </form>
                    </div>
                </div>
            </div>
            <!-- end: .admin-form -->
        </div>
    </div>
</section>

<?php include_once($account_setting_path . 'js_form.php'); ?>


<script src="<?php echo $assets_url ?>js/jquery.validate.min.js"></script>

<script>
    var FormAdminValidator = function () {
        // function to initiate Validation Sample 1
        var temp = 0;
        var runValidator1 = function () {
            var form1 = $('#admin_form');

            $('#admin_form').validate({

                errorElement: "span", // contain the error msg in a span tag
                errorClass: 'help-block',
                errorPlacement: function (error, element) { // render error placement for each input type
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
                },
                ignore: "",
                rules: {
                    account_number: {
                        required: true
                    },
                    bank_address: {
                        required: true
                    },
                    name_of_beneficiary: {
                        required: true
                    },
                    bank_name: {
                        required: true
                    },
                    isfc_code:{
                        required:true
                    },
                    mobile_number: {
                        required: true,
                        number :true,
                        maxlength :10
                    },
                    address_of_beneficiary: {
                        required: true
                    },
                    eStatus: {
                        required: true
                    }

                },
                messages: {
                    account_number: {
                        required: "Please enter account number"
                    },
                    mobile_number: {
                        required: "Please enter mobile number",
                        number :"Enter only number",
                        maxlength :"Mobile number should be maximum 10 digits"
                    },

                    isfc_code: "Please enter isfc code",
                    bank_name: "Please enter bank name",
                    bank_address: "Please enter bank address",
                    name_of_beneficiary: "Please enter name of beneficiary",
                    address_of_beneficiary: "Please enter address of beneficiary",

                    eStatus: "Please select status"
                },
                invalidHandler: function (event, validator) { //display error alert on form submit
                    //successHandler1.hide();
                    //errorHandler1.show();
                },
                highlight: function (element) {
                    $(element).closest('.help-block').removeClass('valid');
                    // display OK icon
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                    // add the Bootstrap error class to the control group
                },
                unhighlight: function (element) { // revert the change done by hightlight
                    $(element).closest('.form-group').removeClass('has-error');
                    // set error class to the control group
                },
                success: function (label, element) {
                    label.addClass('help-block valid');
                    // mark the current input as valid and display OK icon
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
                },
                submitHandler: function (frmadd) {
                    successHandler1.show();
                    errorHandler1.hide();
                }

            });

        };

        return {
            //main function to initiate template pages
            init: function () {
                runValidator1();
            }
        };

        //$('#frmadd').submit();
    }();

    //$('#frmadd').submit();
    $(document).ready(function () {
        FormAdminValidator.init();

        $('#addbutton').click(function () {
            $('#eStatus').prop("disabled", false);
            $('#admin_form').submit();

        });
    });

</script>