<?php

$account_setting = new account_setting();

$res_db = $account_setting->select();
//pr($res_db);
$MODULE = 'Account Setting ';
$ADD_LINK = 'index.php?file=as-accountadd';
$ACT_LINK = 'index.php?file=as-accountsetting_a';
$orderlistexcel = 'export';
//pr($_SESSION[$Project_Name]['ADMIN']['id']);exit;
?>
<!-- Start: Topbar -->
<header id="topbar" class="affix">
    <div class="topbar-left">
        <ol class="breadcrumb">
            <li class="crumb-link crumb-active ">
                <a href="index.php">Dashboard</a>
            </li>

            <li class="crumb-trail"><?php echo $MODULE; ?></li>
        </ol>
    </div>
</header>
<!-- End: Topbar -->

<section id="content" class="tray tray-center pv40 ph30 va-t posr">
    <div class="row">
        <div class="col-md-12">
            <div id="msg" class="notification" data-note-stack="stack_bar_top"
                 data-note-style="<?php echo $_SESSION[$session_prefix . 'admin_var_msg_class']; ?>"></div>
            <div class="panel panel-visible <?php echo $PANEL_HEADER; ?>">

                <div class="panel-heading">
                    <div class="panel-title hidden-xs">
                        <span class="pull-left">
                            <a href="<?php echo $ADD_LINK; ?>" class="btn btn-sm btn-primary btn-dark animsition-link"
                               data-style="expand-left">Add account setting</a>
                            <a href="#" class="btn btn-sm btn-danger" data-style="expand-left"
                               onclick="return changeStatusEvent('delete','admin_list');">Delete</a>
                        </span>
                      <span class="pull-left" style="margin-left:5px">
                           <a href="#" class="btn btn-sm btn-system btn-block" style="margin-top: 5px;"
                              onclick="return changeStatusEvent('active','admin_list');">Active </a>

                        </span>

                            <span class="pull-left" style="margin-top: 5px; margin-left: 5px;">
                           <a href="#" class="btn btn-sm btn-warning btn-block"
                              onclick="return changeStatusEvent('inactive','admin_list');">Inactive </a>

                        </span>

                    </div>

                </div>

                <div class="panel-body pn">
                    <form name="admin_list" id="admin_list" method="post" action="<?php echo $ACT_LINK; ?>">
                        <input type="hidden" name="mode" id="mode" value="">
                        <input type="hidden" name="delete_type" value="multi_delete">
                        <table class="table table-striped table-bordered table-hover" id="datatable3"
                               cellspacing="0"
                               width="100%">
                            <thead>
                            <tr class="<?php echo $TBL_HEADER; ?>">
                                <th width="4%"><input type="checkbox" onclick="check_all();"></th>
                                <th width="15%">
                                    <center>Action</center>
                                </th>
                                <th >Account Number</th>
                                <th >Bank Name</th>
                                <th >Isfc Code</th>
                                <th >Bank Address</th>
                                <th >Mobile Number</th>

                                <th width="">
                                    <center>Added Date</center>
                                </th>
                                <th width="">
                                    <center>Status</center>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (count($res_db) > 0) {
                                foreach ($res_db as $account) {
                                    ?>
                                    <tr>
                                        <td align="center">

                                                <input type="checkbox" name="delete[]" id='iId'
                                                       value="<?php echo $account['account_setting_id']; ?>">

                                        </td>
                                        <td align="center">
                                            <a href="<?php echo $ADD_LINK; ?>&mode=update&iId=<?php echo $account['account_setting_id']; ?>"
                                               class="btn btn-xs btn-success">Edit</a>

                                                <a href="#"
                                                   onclick="changeDeleteEvent('<?php echo $ACT_LINK; ?>&mode=delete&iId=<?php echo $account['account_setting_id']; ?>&delete_type=single_delete');"
                                                   class="btn btn-xs btn-danger">Delete</a>

                                        </td>
                                        <td><?php echo $account['account_number']; ?></td>
                                        <td><?php echo $account['bank_name']; ?></td>
                                        <td><?php echo $account['isfc_code']; ?></td>
                                        <td><?php echo $account['bank_address']; ?></td>
                                        <td><?php echo $account['mobile_number']; ?></td>


                                        <td align="center"><?php echo $account['updated_at']; ?></td>

                                        <?php if ($account['status'] == '1') { ?>
                                            <td align="center"><span class="tm-tag tm-tag-system">Active</span></td>
                                        <?php } else { ?>
                                            <td align="center"><span class="tm-tag tm-tag-warning">Inactive</span></td>
                                        <?php } ?>

                                    </tr>
                                <?php }
                            } else {

                                echo $generalfuncobj->no_record_found(9);
                            } ?>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Start: Content -->
<div id="modalpopup" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false"
     style="display: none;">
    <?php include_once($admin_path . 'js_table.php'); ?>
    <?php include_once($admin_path . 'modalpopup.php'); ?>
