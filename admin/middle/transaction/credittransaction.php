<?php

$user = new user();
$transaction = new transaction();

$res_db = $user->select();
$debit_transactionlist = $transaction->select_all_debit_credit_transaction_for_admin('credit');
#pr($debit_transactionlist);exit;
$MODULE = 'User Wise Credit Transactions ';
$ADD_LINK = 'index.php?file=us-useradd';
$ACT_LINK = 'index.php?file=us-user_a';
$PRE_LINK ='index.php?file=t-credittransactiondetail';
$orderlistexcel = 'export';
?>
<!-- Start: Topbar -->
<header id="topbar" class="affix">
    <div class="topbar-left">
        <ol class="breadcrumb">
            <li class="crumb-link crumb-active ">
                <a href="index.php">Dashboard</a>
            </li>

            <li class="crumb-trail"><?php echo $MODULE; ?></li>
        </ol>
    </div>
</header>
<!-- End: Topbar -->

<section id="content" class="tray tray-center pv40 ph30 va-t posr">
    <div class="row">
        <div class="col-md-12">
            <div id="msg" class="notification" data-note-stack="stack_bar_top"
                 data-note-style="<?php echo $_SESSION[$session_prefix . 'admin_var_msg_class']; ?>"></div>
            <div class="panel panel-visible <?php echo $PANEL_HEADER; ?>">

                <div class="panel-heading">
                    <div class="panel-title hidden-xs">
                    </div>

                </div>

                <div class="panel-body pn">
                    <form name="admin_list" id="admin_list" method="post" action="<?php echo $ACT_LINK; ?>">
                        <input type="hidden" name="mode" id="mode" value="">
                        <input type="hidden" name="delete_type" value="multi_delete">
                        <table class="table table-striped table-bordered table-hover" id="datatable3"
                               cellspacing="0"
                               width="100%">
                            <thead>
                            <tr class="<?php echo $TBL_HEADER; ?>">
                                <th width="4%"><input type="checkbox" onclick="check_all();"></th>
                                <th width="10%">
                                    <center>Action</center>
                                </th>
                                <th>Name</th>
                                <th width="30%">Email</th>
                                <th width="30%">Total Credit Transactions</th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (count($debit_transactionlist) > 0) {
                                foreach ($debit_transactionlist as $userlist) {
                                    if ($userlist['to_user_id'] != 0) {
                                        ?>
                                        <tr>
                                            <td align="center">

                                                <input type="checkbox" name="delete[]" id='iId'
                                                       value="<?php echo $userlist['id']; ?>">

                                            </td>
                                            <td align="center">
                                                <a href="<?php echo $PRE_LINK; ?>&iId=<?php echo $userlist['to_user_id']; ?>"
                                                   class="btn btn-xs btn-alert animsition-link">View</a>

                                            </td>
                                            <td>
                                                <?php
                                                if ($userlist['to_user_id'] == "0") {
                                                    echo $ADMIN_NAME;
                                                } else {
                                                    echo $userlist['name'];
                                                }


                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                if ($userlist['to_user_id'] == "0") {
                                                    echo $ADMIN_EMAIL;
                                                } else {
                                                    echo $userlist['name'];
                                                }


                                                ?>

                                            </td>
                                            <td><?php echo round($userlist['total_debits'], 2); ?></td>
                                        </tr>
                                    <?php }
                                }

                            } else {

                                echo $generalfuncobj->no_record_found(5);
                            } ?>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Start: Content -->
<div id="modalpopup" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false"
     style="display: none;">
    <?php include_once($admin_path . 'js_table.php'); ?>
    <?php include_once($admin_path . 'modalpopup.php'); ?>
