<?php
/**
 * Created by PhpStorm.
 * User: mind
 * Date: 2/10/15
 * Time: 6:20 PM
 */

$iId = $_REQUEST['iId'];

$user = new user();

$res_db = $user->select($iId);

$wallet = new wallet();
$walletlist = $wallet->userwallet($iId);
$transaction = new transaction();

$debit_transactionlist = $transaction->select_debit_transaction_by_day($iId, 'debit','day');
#pr($debit_transactionlist);
$credit_transactionlist = $transaction->select_transaction_credit_admin($iId, 'debit');

$ADD_LINK = 'index.php?file=us-user_a';
$view = 'index.php?file=us-userview&iId='. $res_db[0]['id'];
$PRE_LINK = 'index.php?file=t-credittransaction';

?>
<header id="topbar" class="affix">
    <div class="topbar-left">
        <ol class="breadcrumb">
            <li class="crumb-link crumb-active ">
                <a href="index.php">Dashboard</a>
            </li>
            <li class="crumb-active crumb-link">
                <a href="<?php echo $PRE_LINK; ?>"><?php echo "User wise Credit Transactions"; ?> </a>
            </li>

            <li class="crumb-trail"><?php echo "User Credit Transaction"; ?> Details</li>
        </ol>
    </div>
</header>
<section id="content">
<div class="row">
<div class="col-md-8 pv10 ph20 bg-light dark br-b br-grey posr">

    <div class="table-layout">
        <div class="va-t m30">

            <h2 class=""> <?php echo $res_db[0]['name'];?>

            </h2>
            <p class="fs15 mb20"><?php echo $res_db[0]['email'];?></p>
            <p class="fs15 mb20"><?php echo $res_db[0]['mobile'];?></p>
        </div>
    </div>

    <!-- <div class="p40 bg-background bg-topbar bg-psuedo-tp"> -->








</div>



<div class="col-md-4">
    <h4 class="page-header mtn br-light text-muted hidden">User Info</h4>

    <div class="panel">
        <div class="panel-heading">
            <span class="panel-icon"><i class="fa fa-star"></i></span>
            <span class="panel-title"> User Details</span>
        </div>
        <div class="panel-body pn">

            <table class="table mbn tc-icon-1 tc-med-2 tc-bold-last">
                <tbody>

                <tr>
                    <td><span class="fa fa-plus-circle text-success"></span></td>
                    <td>Status</td>
                    <td>
                        <?php
                        $style = "";
                        if($res_db[0]['status'] == '1' )
                        {
                            $style="text-success";

                            $text ="Active";
                        }elseif($res_db[0]['status'] == '0')
                        {
                            $style="text-warning";
                            $text ="Inactive";
                        }elseif($res_db[0]['status'] == '3')
                        {
                            $style="text-danger";
                            $text ="Block";
                        }
                        ?>
                        <span class="<?php echo $style?>"><?php echo $text?></span>
                    </td>
                </tr>


                <tr>

                    <td><span class="glyphicons glyphicons-thumbs_up text-primary"></span></td>
                    <td>Wallet</td>
                    <td><?php if($walletlist[0]['amount']=='')
                        {
                            $amount ='0.0';
                        }else{
                            $amount =$walletlist[0]['amount'];
                        }

                        echo $amount ;?></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>

<div class="row">




        <div class="tab-block psor">

            <ul class="nav nav-tabs">
                <li  class="active">
                    <a href="#day" data-toggle="tab">Day</a>
                </li>
                <!--                                <li>-->
                <!--                                    <a href="--><?php //echo $ADD_LINK;?><!--" data-toggle="tab">View Order</a>-->
                <!--                                </li>-->
                <li>
                    <a href="#week" data-toggle="tab">Weekly</a>
                </li>
                <li>
                    <a href="#month" data-toggle="tab">Monthly</a>
                </li>
                <li>
                    <a href="#year" data-toggle="tab">Yearly</a>
                </li>


            </ul>

            <div class="tab-content" style="height: 725px;">
                <div id="day" class="tab-pane active p15">
                    <div class="media pb10">
                        <table class="table table-striped table-condensed">
                            <thead>
                            <tr>

                                <th>Transaction Id</th>
                                <th>From User</th>
                                <th >Total Amount</th>
                                <th >Transaction Status</th>
                                <th >Created Date</th>


                            </tr>
                            </thead>
                            <tbody>

                            <?php
                            $debit_transactionlist = $transaction->select_debit_transaction_by_day($iId, 'credit','day');

                            #pr($creditlist);

                        if(count($debit_transactionlist)>0)
                        {
                            foreach ($debit_transactionlist as $creditdata) {

                                ?>
                                    <tr>


                                            <td><?php echo $creditdata['transaction_unique_id'] ?></td>
                                            <td><?php
                                                if($creditdata['to_user_id'] == "0")
                                                {
                                                    echo $ADMIN_NAME ;
                                                }
                                                else{
                                                    echo $creditdata['name'] ;
                                                }


                                                ?></td>

                                            <td><?php echo round($creditdata['amount'],2) ?></td>
                                        <?php
                                        if($creditdata['transaction_status'] == 'pending' )
                                        {
                                            $style="text-warning";
                                        }elseif($creditdata['transaction_status'] == 'confirm' )
                                        {
                                            $style="text-success";

                                        }elseif($creditdata['transaction_status'] == 'cancel' )
                                        {
                                            $style="text-danger";
                                        }
                                        ?>
                                            <td class="<?php echo $style?>"><?php echo ucfirst($creditdata['transaction_status']) ?></td>
                                        <td><?php echo $generalfuncobj->full_date_formate($creditdata['added_at']);  ?></td>

                                    </tr>
                                <?php
                                        }
                                    }else{
                                        echo $generalfuncobj->no_record_found(7);
                                    }
                                    ?>



                                    </tbody>
                                </table>
                    </div>
                </div>
                <div id="week" class="tab-pane acitve p15">
                    <div class="media pb10">
                        <table class="table table-striped table-condensed">
                            <thead>
                            <tr>

                                <th>Transaction Id</th>
                                <th>From User</th>
                                <th >Amount</th>
                                <th >Transaction Status</th>
                                <th >Start Date</th>
                                <th >End Date</th>


                            </tr>
                            </thead>
                            <tbody>

                            <?php
                            $creditlist =$transaction->select_debit_transaction_by_day($iId, 'credit','week');
                            #                        pr($creditlist);

                            if(count($creditlist)>0)
                            {
                                foreach ($creditlist as $creditdata) {
                                    ?>

                                    <tr>


                                        <td><?php echo $creditdata['transaction_unique_id'] ?></td>
                                        <td><?php
                                            if($creditdata['to_user_id'] == "0")
                                            {
                                                echo $ADMIN_NAME ;
                                            }
                                            else{
                                                echo $creditdata['name'] ;
                                            }


                                            ?></td>


                                        <td><?php echo round($creditdata['amount'],2) ?></td>
                                        <?php
                                        if($creditdata['transaction_status'] == 'pending' )
                                        {
                                            $style="text-warning";
                                        }elseif($creditdata['transaction_status'] == 'confirm' )
                                        {
                                            $style="text-success";

                                        }elseif($creditdata['transaction_status'] == 'cancel' )
                                        {
                                            $style="text-danger";
                                        }
                                        ?>
                                        <td class="<?php echo $style?>"><?php echo ucfirst($creditdata['transaction_status']); ?></td>
                                        <td><?php echo $generalfuncobj->full_date_formate($creditdata['start_date']);  ?></td>
                                        <td><?php echo $generalfuncobj->full_date_formate($creditdata['end_date']);  ?></td>

                                    </tr>
                                <?php


                                }
                            }else{
                                echo $generalfuncobj->no_record_found(7);
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div id="month" class="tab-pane acitve p15">
                    <div class="media pb10">
                        <table class="table table-striped table-condensed">
                            <thead>
                            <tr>

                                <th>Transaction Id</th>
                                <th>From User</th>
                                <th >Amount</th>
                                <th >Transaction Status</th>
                                <th >Month</th>


                            </tr>
                            </thead>
                            <tbody>

                            <?php
                            $creditlist =$transaction->select_debit_transaction_by_day($iId, 'credit','month');
                            #                        pr($creditlist);

                            if(count($creditlist)>0)
                            {
                                foreach ($creditlist as $creditdata) {
                                    ?>

                                    <tr>


                                        <td><?php echo $creditdata['transaction_unique_id'] ?></td>
                                        <td><?php
                                            if($creditdata['to_user_id'] == "0")
                                            {
                                                echo $ADMIN_NAME ;
                                            }
                                            else{
                                                echo $creditdata['name'] ;
                                            }


                                            ?></td>


                                        <td><?php echo round($creditdata['amount'],2) ?></td>
                                        <?php
                                        if($creditdata['transaction_status'] == 'pending' )
                                        {
                                            $style="text-warning";
                                        }elseif($creditdata['transaction_status'] == 'confirm' )
                                        {
                                            $style="text-success";

                                        }elseif($creditdata['transaction_status'] == 'cancel' )
                                        {
                                            $style="text-danger";
                                        }
                                        ?>
                                        <td class="<?php echo $style?>"><?php echo ucfirst($creditdata['transaction_status']); ?></td>
                                        <td><?php echo ($creditdata['month_name']);  ?></td>

                                    </tr>
                                    <?php


                                }
                            }else{
                                echo $generalfuncobj->no_record_found(7);
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div id="year" class="tab-pane acitve p15">
                    <div class="media pb10">
                        <table class="table table-striped table-condensed">
                            <thead>
                            <tr>

                                <th>Transaction Id</th>
                                <th>From User</th>
                                <th >Amount</th>
                                <th >Transaction Status</th>
                                <th >Year</th>


                            </tr>
                            </thead>
                            <tbody>

                            <?php
                            $creditlist =$transaction->select_debit_transaction_by_day($iId, 'credit','year');
                            #                        pr($creditlist);

                            if(count($creditlist)>0)
                            {
                                foreach ($creditlist as $creditdata) {
                                    ?>

                                    <tr>


                                        <td><?php echo $creditdata['transaction_unique_id'] ?></td>
                                        <td><?php
                                            if($creditdata['to_user_id'] == "0")
                                            {
                                                echo $ADMIN_NAME ;
                                            }
                                            else{
                                                echo $creditdata['name'] ;
                                            }


                                            ?></td>


                                        <td><?php echo round($creditdata['amount'],2) ?></td>
                                        <?php
                                        if($creditdata['transaction_status'] == 'pending' )
                                        {
                                            $style="text-warning";
                                        }elseif($creditdata['transaction_status'] == 'confirm' )
                                        {
                                            $style="text-success";

                                        }elseif($creditdata['transaction_status'] == 'cancel' )
                                        {
                                            $style="text-danger";
                                        }
                                        ?>
                                        <td class="<?php echo $style?>"><?php echo ucfirst($creditdata['transaction_status']); ?></td>
                                        <td><?php echo ($creditdata['month_name']);  ?></td>

                                    </tr>
                                    <?php


                                }
                            }else{
                                echo $generalfuncobj->no_record_found(7);
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
       </div>

</div>




</section>
<?php include_once($admin_path . 'js_form.php'); ?>
<?php include_once($admin_path . 'modalpopup.php'); ?>

<script src="<?php echo $assets_url ?>js/jquery.validate.min.js"></script>




<script>
    var FormAdminValidator = function () {
        // function to initiate Validation Sample 1
        var temp = 0;
        var runValidator1 = function () {
            var form1 = $('#user_wallet');

            $('#user_wallet').validate({

                errorElement: "span", // contain the error msg in a span tag
                errorClass: 'help-block',
                errorPlacement: function (error, element) { // render error placement for each input type
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
                },
                ignore: "",
                rules: {
                    amount: {
                        required: true,
                        number: true
                    }


                },
                messages: {
                    amount: {
                        required: "Please enter amount",
                        number: "Please enter only number"
                    }
                },
                invalidHandler: function (event, validator) { //display error alert on form submit
                    //successHandler1.hide();
                    //errorHandler1.show();
                },
                highlight: function (element) {
                    $(element).closest('.help-block').removeClass('valid');
                    // display OK icon
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                    // add the Bootstrap error class to the control group
                },
                unhighlight: function (element) { // revert the change done by hightlight
                    $(element).closest('.form-group').removeClass('has-error');
                    // set error class to the control group
                },
                success: function (label, element) {
                    label.addClass('help-block valid');
                    // mark the current input as valid and display OK icon
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
                },
                submitHandler: function (frmadd) {
                    successHandler1.show();
                    errorHandler1.hide();
                }

            });

        };

        return {
            //main function to initiate template pages
            init: function () {
                runValidator1();
            }
        };

        //$('#frmadd').submit();
    }();

    //$('#frmadd').submit();
    $(document).ready(function () {
        FormAdminValidator.init();

        $('#amountadd').click(function () {

            $('#user_wallet').submit();

        });
    });

</script>