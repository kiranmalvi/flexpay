<?php
/**
 * Created by PhpStorm.
 * User: mind
 * Date: 9/4/15
 * Time: 3:14 PM
 */

require_once('include.php');

if (empty($_SESSION[$Project_Name])) {
    header('location:login.php');
    exit;
}

include("header.php");

@$file = $_REQUEST['file'];

@$var = explode("-", $_GET['file']);
@$prefix = $var[0];
@$script = $var[1];

if ($prefix == "") {
    $prefix = "a";
}
if ($script == "") {
    $script = "dashboard";
}
switch ($prefix) {

    case "as" :
        $module = "accountsetting";
        break;

    case "cms" :
        $module = "cms";
        break;


    case "us" :
        $module = "user";
        break;


    case "t" :
        $module = "transaction";
        break;

    default :
        $module = "admin";
        break;
}
// Add File Path
$include_script = "middle/" . $module . "/" . $script . ".php";
#echo $include_script;exit;
//Action File Path middle/admin/adminlist.php
$include_script_a = "action/" . $module . "/" . $script . ".php";//action/admin/adminlist.php
?>
    <body class="admin-elements-page" data-spy="scroll" data-target="#nav-spy" data-offset="300">
    <!--<body class="admin-elements-page" data-spy="scroll" data-target="#nav-spy" data-offset="300" id="main_body">-->
    <!-- Start: Main -->
    <!-- Start: Main -->
    <div id="main" class="">
        <!--	<div id="main" class="animsition-overlay bg-init">-->
        <?php
        include("top.php");
        include("menu.php");
        ?>
        <section id="content_wrapper">
            <?php
            if (file_exists($include_script)) {
                include_once($include_script);
            } else {
                include_once($include_script_a);
            }
            ?>
        </section>
    </div>
    </body>
<?php
include("footer.php");