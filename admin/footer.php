</html>
<script>
    $(document).ready(function () {
        $('.animsition-overlay').animsition({
            inClass: 'overlay-slide-in-top',
            outClass: 'overlay-slide-out-top',
            inDuration: 500,
            outDuration: 500,
            linkElement: '.animsition-link',
            overlay: true,
            overlayClass: 'animsition-overlay-slide',
            overlayParentElement: 'body'
        })
            .one('animsition.inStart', function () {
                $(this).removeClass('bg-init');
                $(this)
                    .find('.item')
                    .append('<h2 class="target">Callback: Start</h2>');
                console.log('event -> inStart');
            })
            .one('animsition.inEnd', function () {
                $('.target', this).html('Callback: End');
                console.log('event -> inEnd');
            });

//        $('#main_body').slimScroll({
//            height: '100%'
//        });
    });
</script>
</html>