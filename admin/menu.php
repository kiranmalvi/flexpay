<!-- Start: Sidebar -->
<aside id="sidebar_left" class="ano nano-primary has-scrollbar sidebar-default affix">
    <div class="nano-content">
        <?php
        $iId = @$_REQUEST['iId'];
       # echo $script;
        ?>

        <!-- sidebar menu -->
        <ul class="nav sidebar-menu">

            <li <?php echo (in_array($script, array('dashboard'))) ? 'class="active"' : ''; ?>>
                <a href="index.php" class="animsition-link">
                    <span class="glyphicons glyphicons-home"></span>
                    <span class="sidebar-title">Dashboard</span>
                </a>
            </li>

            <li <?php echo (in_array($script, array('adminlist', 'adminadd'))) ? 'class="active"' : ''; ?>>
                <a href="index.php?file=a-adminlist" class="animsition-link">
                    <span class="glyphicons glyphicons-eye_open"></span>
                    <span class="sidebar-title">Admin</span>
                </a>
            </li>



            <li <?php echo (in_array($script, array('accountlist', 'accountadd'))) ? 'class="active"' : ''; ?>>
                <a href="index.php?file=as-accountlist" class="animsition-link">
                    <span class="glyphicons glyphicons-settings"></span>
                    <span class="sidebar-title">Account setting</span>
                </a>
            </li>
            <li class="<?= ($module == "cms") ? 'active' : ''; ?>">
                <a href="#" class="<?= ($module == "cms") ? 'accordion-toggle menu-open' : 'accordion-toggle'; ?>">
                    <span class="glyphicons glyphicons-fire"></span>
                    <span class="sidebar-title">CMS</span>

                    <span class="caret"></span>
                </a>
                <ul class="nav sub-nav">

                    <li <?php echo (in_array($iId, array('1'))) ? 'class="active"' : '';  ?>>
                        <a href="index.php?file=cms-cmsadd&iId=1" class="animsition-link">
                            <span class="glyphicons glyphicons-resize_full"></span>About Us
                        </a>
                    </li>
                    <li <?php echo (in_array($iId, array('2'))) ? 'class="active"' : '';  ?>>
                        <a href="index.php?file=cms-cmsadd&iId=2" class="animsition-link">
                            <span class="glyphicons glyphicons-resize_full"></span>Terms and Conditions
                        </a>
                    </li>
                    <!--<li <?php /*echo (in_array($iId, array('3'))) ? 'class="active"' : '';  */?>>
                        <a href="index.php?file=cms-cmsadd&iId=3" class="animsition-link">
                            <span class="glyphicons glyphicons-resize_full"></span>Privacy Policy
                        </a>
                    </li>-->

                </ul>
            </li>
			<li <?php echo (in_array($script, array('userlist', 'useradd', 'userview', 'userpromotionlist', 'userpromotionsadd'))) ? 'class="active"' : ''; ?>>
                <a href="index.php?file=us-userlist" class="animsition-link">
                    <span class="glyphicons glyphicons-user"></span>
                    <span class="sidebar-title">User</span>
                </a>
            </li>
            <li class="<?= ($module == "transaction") ? 'active' : ''; ?>">
                <a href="#" class="<?= ($module == "transaction") ? 'accordion-toggle menu-open' : 'accordion-toggle'; ?>">
                    <span class="glyphicons glyphicons-iphone_transfer"></span>
                    <span class="sidebar-title">Transaction</span>

                    <span class="caret"></span>
                </a>
                <ul class="nav sub-nav">

                    <li <?php echo (in_array($script, array('debittransaction','debittransactiondetail'))) ? 'class="active"' : '';  ?>>
                        <a href="index.php?file=t-debittransaction" class="animsition-link">
                            <span class="glyphicons glyphicons-credit_card"></span> Debit Transactions
                        </a>
                    </li>
                    <li <?php echo (in_array($script, array('credittransaction','credittransactiondetail'))) ? 'class="active"' : '';  ?>>
                        <a href="index.php?file=t-credittransaction" class="animsition-link">
                            <span class="glyphicons glyphicons-credit_card"></span> Credit Transactions
                        </a>
                    </li>

                </ul>
            </li>

        </ul>
        <!--        <div class="sidebar-toggle-mini">-->
        <!--            <a href="#">-->
        <!--                <span class="fa fa-sign-out"></span>-->
        <!--            </a>-->
        <!--        </div>-->
    </div>
</aside>
<!-- End: Sidebar -->