<?php
/**
 * Created by PhpStorm.
 * User: mind
 * Date: 14/4/15
 * Time: 6:40 PM
 */
error_reporting(0);

?>

<script>
    var Stacks = {
        stack_bar_top: {
            "dir1": "down",
            "dir2": "right",
            "push": "top",
            "spacing1": 0,
            "spacing2": 0
        }
    }
    <?php if(isset($_SESSION[$session_prefix.'admin_var_msg'])){ ?>
    // PNotify Plugin Event Init
    $(document).ready(function () {
        $('.notification').trigger('click');
    });
    $('.notification').click(function () {
        var noteStyle = $(this).data('note-style');
        var noteShadow = $(this).data('note-shadow');
        var noteOpacity = $(this).data('note-opacity');
        var noteStack = $(this).data('note-stack');
        var width = "290px";

        var noteStack = noteStack ? noteStack : "stack_top_right";
        var noteOpacity = noteOpacity ? noteOpacity : "1";

        function findWidth() {
            if (noteStack == "stack_bar_top") {
                return "100%";
            }
            if (noteStack == "stack_bar_bottom") {
                return "70%";
            } else {
                return "290px";
            }
        }

        new PNotify({
            title: '<?php  echo $_SESSION[$session_prefix.'admin_var_msg']; ?>',
            /*text: '<?php echo $_SESSION[$session_prefix.'admin_var_msg']; ?>',*/
            shadow: noteShadow,
            opacity: noteOpacity,
            addclass: noteStack,
            type: noteStyle,
            stack: Stacks[noteStack],
            width: findWidth(),
            delay: 1400
        });


    });

    <? unset($_SESSION[$session_prefix.'admin_var_msg']);
    } ?>

    function check_all() {
        $('input[name="delete[]"]').each(function () {
            this.checked = this.checked ? false : true;
        });
    }

    function getCheckCount() {
        var x = 0;
        for (i = 0; i < document.admin_list.elements.length; i++) {
            if (document.admin_list.elements[i].id == 'iId' && document.admin_list.elements[i].checked == true) {
                x++;
            }
        }
        return x;
    }

    function changeStatusEvent(param_status, frm) {
        var y = 0;
        var ans;
        y = getCheckCount();
        //alert(y);
        if (y > 0) {
            var_html = '<div class="modal-body"><p>Are you sure you want to ' + param_status + ' record(s)? </p></div><div class="modal-footer"><button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button><button type="button" data-dismiss="modal" class="btn btn-primary notification" data-note-stack="stack_bar_top" data-note-style="<?php echo $_SESSION[$session_prefix.'admin_msg_class'] ?>"  onclick="setmodeandsubmit(\'' + param_status + '\',\'' + frm + '\');" >Yes - Continue Task</button></div>';

            $("#modalpopup").html(var_html);
            $('#modalpopup').modal();
        }
        else {
            var_html = '<div class="modal-body"><p>Please select atleast one record to ' + param_status + '.</p></div><div class="modal-footer"><button type="button" data-dismiss="modal" class="btn btn-primary">Ok</button></div>';

            $("#modalpopup").html(var_html);
            $('#modalpopup').modal();
        }
    }

    function setmodeandsubmit(param_status, frm) {
        $('form[name="' + frm + '"]').find('#mode').val(param_status);
        $('#' + frm + '').submit();
    }

    function changeDeleteEvent(link) {
        var_html = '<div class="modal-body"><p>Are you sure you want to delete record? </p></div><div class="modal-footer"><button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button><button type="button" data-dismiss="modal" class="btn btn-primary notification" data-note-stack="stack_bar_top" data-note-style="<?php echo $_SESSION[$session_prefix.'admin_msg_class'] ?>"  onclick="location.href=\'' + link + '\'" >Yes - Continue Task</button></div>';

        $("#modalpopup").html(var_html);
        $('#modalpopup').modal();
    }
</script>

<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script src="<?php echo $admin_url ?>assets/plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
<script src="<?php echo $admin_url ?>assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
<script src="<?php echo $admin_url ?>assets/js/ui-modals.js"></script>